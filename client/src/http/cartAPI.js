import { authHost } from './index'

export const getAllCartItems = async () => {
  const { data } = await authHost.get('api/cart')
  return data
}

export const getCartItem = async (id) => {
  const { data } = await authHost.get('api/cart/' + id)
  return data
}

export const createCartItem = async (productId, quantity) => {
  const { data } = await authHost.post('api/cart', {
    productId,
    quantity
  })
  return data
}

export const updateCartItem = async (id, quantity) => {
  const { data } = await authHost.put('api/cart/' + id, {
    quantity
  })
  return data
}

export const deleteCartItem = async (id) => {
  const { data } = await authHost.delete('api/cart/' + id)
  return data
}

export const deleteAllCartItems = async () => {
  const { data } = await authHost.delete('api/cart')
  return data
}
