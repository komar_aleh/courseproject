import { host, authHost } from './index'

export const registration = async (data) => {
  const { data: res } = await host.post('api/user/register', data)
  return res
}

export const registrationAdmin = async (data) => {
  const { data: res } = await authHost.post('api/user/reg-admin', data)
  return res
}

export const login = async (data) => {
  const res = await host.post('api/user/login', data)
  return res
}

export const confirmation = async (userId, token) => {
  const { data } = await host.get(
    `api/user/confirm-email?userId=${userId}&token=${token}`
  )
  return data
}

export const forgotPassword = async (data) => {
  const res = await host.post('api/user/forgot-password', data)
  return res
}

export const resetPassword = async (
  userId,
  password,
  confirmPassword,
  token
) => {
  const { data } = await host.post('api/user/reset-password', {
    userId,
    password,
    confirmPassword,
    token
  })
  return data
}

export const changePassword = async (
  oldPassword,
  newPassword,
  confirmNewPassword
) => {
  const { data } = await authHost.post('api/user/change-password', {
    oldPassword,
    newPassword,
    confirmNewPassword
  })
  return data
}

export const activateAccount = async (data) => {
  const { data: res } = await host.post('api/user/activate', data)
  return res
}

export const getUser = async (id) => {
  const { data } = await authHost.get('api/user/' + id)
  return data
}

export const updateUser = async (id, firstName, lastName) => {
  const { data } = await authHost.put('api/user/' + id, { firstName, lastName })
  return data
}

export const deleteUser = async (id) => {
  const { data } = await authHost.delete('api/user/' + id)
  return data
}

export const getAllUsers = async (
  userName,
  orderBy,
  role,
  pageNumber,
  pageSize
) => {
  const { data } = await authHost.get(
    `api/user?UserName=${userName}&OrderBy=${orderBy}&Role=${role}&PageNumber=${pageNumber}&PageSize=${pageSize}`
  )
  return data
}
