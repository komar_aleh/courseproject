import { host, authHost } from './index'

export const getAllProperties = async () => {
  const { data } = await host.get('api/AllProperties')
  return data
}

export const getAllItems = async (property) => {
  const { data } = await host.get(`api/${property}/getall`)
  return data
}

export const getAll = async (
  property,
  value,
  orderBy,
  pageNumber,
  pageSize
) => {
  const { data } = await authHost.get(
    `api/${property.toString()}?Value=${value}&OrderBy=${orderBy}&PageNumber=${pageNumber}&PageSize=${pageSize}`
  )
  return data
}

export const getItem = async (property, id) => {
  const { data } = await authHost.get(`api/${property}/` + id)
  return data
}

export const deleteItem = async (property, id) => {
  const { data } = await authHost.delete(`api/${property}/` + id)
  return data
}

export const createItem = async (property, value) => {
  const { data } = await authHost.post(`api/${property}`, { value })
  return data
}

export const updateItem = async (property, id, value) => {
  const { data } = await authHost.put(`api/${property}/` + id, { value })
  return data
}
