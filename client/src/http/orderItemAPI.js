import { host, authHost } from './index'
export const getAllOrderItems = async (orderBy, pageNumber, pageSize) => {
  const { data } = await authHost.get(
    `api/orderItem?OrderBy=${orderBy}&PageNumber=${pageNumber}&PageSize=${pageSize}`
  )
  return data
}

export const getOrderItem = async (id) => {
  const { data } = await authHost.get('api/orderItem/' + id)
  return data
}

export const createOrderItem = async (orderId, productId, qunatity) => {
  const { data } = await authHost.post('api/orderItem', {
    orderId,
    productId,
    qunatity
  })
  return data
}

export const updateOrderItem = async (id, productId, qunatity) => {
  const { data } = await authHost.put('api/orderItem/' + id, {
    productId,
    qunatity
  })
  return data
}

export const deleteOrderItem = async (id) => {
  const { data } = await authHost.delete('api/orderItem/' + id)
  return data
}
