import { authHost } from './index'

export const getAllStatuses = async () => {
  const { data } = await authHost.get(`api/status`)
  return data
}
