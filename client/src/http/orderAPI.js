import { authHost } from './index'

export const getAllOrders = async (
  orderBy,
  statusId,
  userId,
  pageNumber,
  pageSize
) => {
  const { data } = await authHost.get(
    `api/order?OrderBy=${orderBy}&StatusId=${statusId}&UserId=${userId}&PageNumber=${pageNumber}&PageSize=${pageSize}`
  )
  return data
}

export const getOrder = async (id) => {
  const { data } = await authHost.get('api/order/' + id)
  return data
}

export const createOrder = async (
  shippingFirstName,
  shippingLastName,
  shippingZip,
  shippingCity,
  shippingAddressLine,
  shippingPhoneNumber
) => {
  const { data } = await authHost.post(`api/order`, {
    shippingFirstName,
    shippingLastName,
    shippingZip,
    shippingCity,
    shippingAddressLine,
    shippingPhoneNumber
  })
  return data
}

export const updateOrder = async (id, statusId) => {
  const { data } = await authHost.put('api/order/' + id, {
    statusId
  })
  return data
}

export const deleteOrder = async (id) => {
  const { data } = await authHost.delete('api/order/' + id)
  return data
}
