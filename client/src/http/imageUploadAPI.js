import { authHost } from './index'

export const uploadImage = async (file) => {
  const formData = new FormData()
  formData.append('file', file)
  const { data } = await authHost.post('api/imageUpload', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
  return data
}
