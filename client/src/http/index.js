import { HOST } from '../config'
import axios from 'axios'
import Cookies from 'js-cookie'

const host = axios.create({
  baseURL: `${HOST}`
})

const authHost = axios.create({
  baseURL: `${HOST}`
})

const authInterceptor = (config) => {
  const authToken = Cookies.get('auth-token')
  config.headers.authorization = `Bearer ${authToken}`
  return config
}

authHost.interceptors.request.use(authInterceptor)

export { host, authHost }
