import * as React from 'react'
import Cookies from 'js-cookie'
import { AuthContext } from '../../contexts/AuthContext'
import { getAllCartItems } from '../../http/cartAPI'

const AuthProvider = (props) => {
  const [isLoaded, setIsLoaded] = React.useState(false)
  const [authed, setAuthed] = React.useState(false)
  const [token, setTokenData] = React.useState(null)
  const [isAdmin, setAdminData] = React.useState(false)
  const [userId, setUserIdData] = React.useState(null)
  const [userName, setUserNameData] = React.useState(null)
  const [cartItems, setCartItemsData] = React.useState(0)

  const setToken = React.useCallback((tokenData) => {
    setTokenData(tokenData)
    if (tokenData) {
      Cookies.set('auth-token', tokenData)
    } else {
      Cookies.remove('auth-token')
    }
  }, [])

  const setUserId = React.useCallback((userIdData) => {
    setUserIdData(userIdData)
    if (userIdData) {
      Cookies.set('userId', userIdData)
    } else {
      Cookies.remove('userId')
    }
  }, [])

  const setAdmin = React.useCallback((adminData) => {
    setAdminData(adminData)
    if (adminData) {
      Cookies.set('isAdmin', adminData)
    } else {
      Cookies.remove('isAdmin')
    }
  }, [])

  const setUserName = React.useCallback((userNameData) => {
    setUserNameData(userNameData)
    if (userNameData) {
      Cookies.set('userName', userNameData)
    } else {
      Cookies.remove('userName')
    }
  }, [])

  const setCartItems = React.useCallback((cartItemsdData) => {
    setCartItemsData(cartItemsdData)
    if (cartItemsdData) {
      Cookies.set('carts', cartItemsdData)
    } else {
      Cookies.remove('carts')
    }
  }, [])

  const logOut = React.useCallback(() => {
    setToken(null)
    setUserId(null)
    setAdmin(false)
    setUserName(null)
    setAuthed(false)
    setCartItems(0)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [setToken])

  const loadData = React.useCallback(async () => {
    try {
      const tokenData = Cookies.get('auth-token')
      setTokenData(tokenData)
      const adminData = Cookies.get('isAdmin')
      setAdminData(adminData)
      const userIdData = Cookies.get('userId')
      setUserIdData(userIdData)
      const userNameData = Cookies.get('userName')
      setUserNameData(userNameData)

      if (tokenData) {
        const cartsData = await getAllCartItems()
        setCartItemsData(cartsData.data.length)
        setAuthed(true)
      } else {
        setAuthed(false)
      }
    } catch {
      setToken(null)
      setAuthed(false)
    } finally {
      setIsLoaded(true)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [setToken])

  React.useEffect(() => {
    loadData()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [token, cartItems])

  const contextValue = React.useMemo(
    () => ({
      isLoaded,
      authed,
      userId,
      token,
      isAdmin,
      userName,
      cartItems,
      setUserId,
      setToken,
      setAdmin,
      setAuthed,
      setUserName,
      setCartItems,
      logOut
    }),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [
      isLoaded,
      authed,
      userId,
      token,
      isAdmin,
      userName,
      cartItems,
      setToken,
      setCartItems,
      setAuthed,
      setCartItems,
      logOut
    ]
  )

  return (
    <AuthContext.Provider value={contextValue}>
      {props.children}
    </AuthContext.Provider>
  )
}

export default AuthProvider
