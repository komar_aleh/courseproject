import * as React from 'react'

export const AuthContext = React.createContext({
  isLoaded: false,
  authed: false,
  userId: null,
  userName: null,
  token: null,
  isAdmin: false,
  cartItems: 0,
  setAuthed: () => {},
  setUserId: () => {},
  setUserName: () => {},
  setToken: () => {},
  setAdmin: () => {},
  setCartItems: () => {},
  logOut: () => {}
})
