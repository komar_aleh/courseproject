import * as React from 'react'
import { useSearchParams } from 'react-router-dom'
import { confirmation } from '../../../http/userAPI'
import { Box, Button, Container, CssBaseline } from '@mui/material'
import { styled } from '@mui/material/styles'
import { Link, useNavigate } from 'react-router-dom'
import ErrorSuccess from '../../../components/ErrorSucces'

const ElLink = styled(Link)`
  align-items: center;
  text-decoration: none;
  padding: 0;
  height: 100%;
  cursor: pointer;

  &.active {
    color: #ff0000;
    text-decoration: none;
  }

  &:hover {
    color: #556cd6;
    text-decoration: none;
  }
`

const Confirm = () => {
  const navigate = useNavigate()
  const [openError, setOpenError] = React.useState(false)
  const [openSuccess, setOpenSuccess] = React.useState(false)
  const [messageError, setMessageError] = React.useState('')
  const [messageSuccess, setMessageSuccess] = React.useState('')
  const [queryParams] = useSearchParams()
  const uid = queryParams.get('userId')
  const token = queryParams.get('token')

  const result = async () => {
    try {
      const result = await confirmation(uid, token)
      setOpenSuccess(true)
      setMessageSuccess(result.message)
    } catch (error) {
      const erdm = error.response.data.message
        ? error.response.data.message
        : ''
      const erde = error.response.data.errors ? error.response.data.errors : ''
      const em = error.message ? error.message : ''
      setMessageError(erdm, erde, em)
      setOpenError(true)
    }
  }
  React.useEffect(() => {
    result()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [token])

  return (
    <Container component="main" maxWidth="sm">
      <CssBaseline />
      <ErrorSuccess
        openE={openError}
        messageE={messageError}
        closeE={() => {
          setOpenError(false)
        }}
        openS={openSuccess}
        messageS={messageSuccess}
        closeS={() => {
          setOpenSuccess(false)
          navigate('/')
        }}
      />
      <Box
        sx={{
          marginTop: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center'
        }}
      >
        <ElLink sx={{ fontSize: '1.5rem' }} to="/">
          <Button variant="contained">Вернуться на главную!</Button>
        </ElLink>
      </Box>
    </Container>
  )
}

export default Confirm
