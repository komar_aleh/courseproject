import * as React from 'react'
import {
  Avatar,
  Box,
  Button,
  Container,
  CssBaseline,
  TextField,
  Typography
} from '@mui/material'
import LockOutlinedIcon from '@mui/icons-material/LockOutlined'
import { forgotPassword } from '../../../http/userAPI'
import { useNavigate } from 'react-router-dom'
import { useForm, Controller } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import validationSchema from './validation'
import ErrorSuccess from '../../../components/ErrorSucces'

const ForgotPage = () => {
  const navigate = useNavigate()
  const [openError, setOpenError] = React.useState(false)
  const [openSuccess, setOpenSuccess] = React.useState(false)
  const [messageError, setMessageError] = React.useState('')
  const [messageSuccess, setMessageSuccess] = React.useState('')

  React.useEffect(() => {
    document.title = 'Забыли пароль? Восстановим!'
  }, [])

  const {
    control,
    handleSubmit,
    formState: { errors, isValid },
    reset
  } = useForm({
    resolver: yupResolver(validationSchema),
    mode: 'onChange'
  })

  const onSubmit = async (data) => {
    try {
      const { data: result } = await forgotPassword(data)
      setOpenSuccess(true)
      setMessageSuccess(result.message)
    } catch (error) {
      const erdm = error.response.data.message
        ? error.response.data.message
        : ''
      const erde = error.response.data.errors ? error.response.data.errors : ''
      const em = error.message ? error.message : ''
      setMessageError(erdm, erde, em)
      setOpenError(true)
    }
    reset({
      email: ''
    })
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <ErrorSuccess
        openE={openError}
        messageE={messageError}
        closeE={() => {
          setOpenError(false)
        }}
        openS={openSuccess}
        messageS={messageSuccess}
        closeS={() => {
          setOpenSuccess(false)
          navigate('/')
        }}
      />
      <Box
        sx={{
          marginTop: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center'
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: 'primary.main' }}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Выслать ссылку для изменения пароля
        </Typography>
        <Box
          component="form"
          onSubmit={handleSubmit(onSubmit)}
          noValidate
          sx={{ mt: 1 }}
        >
          <Controller
            name="email"
            control={control}
            defaultValue=""
            render={({ field }) => (
              <TextField
                {...field}
                error={Boolean(errors.email?.message)}
                fullWidth={true}
                type="text"
                label="Email адрес"
                variant="outlined"
                margin="normal"
                id="email"
                helperText={errors.email?.message}
              />
            )}
          />
          <Button
            type="submit"
            fullWidth
            disabled={!isValid}
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Сбросить пароль
          </Button>
          <Typography component="h5" variant="body2">
            Сейчас вам будет выслана ссылка для сброса пароля на электронную
            почту, указанную выше.
          </Typography>
        </Box>
      </Box>
    </Container>
  )
}

export default ForgotPage
