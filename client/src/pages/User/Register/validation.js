import * as yup from 'yup'

const schema = yup.object().shape({
  firstName: yup
    .string()
    .required('Поле не может быть пустым')
    .min(2, 'Минимум 2 символа')
    .max(20, 'Максимум 20 символов'),
  lastName: yup
    .string()
    .required('Поле не может быть пустым')
    .min(2, 'Минимум 2 символа')
    .max(30, 'Максимум 30 символов'),
  userName: yup
    .string()
    .required('Поле не может быть пустым')
    .min(2, 'Минимум 2 символа')
    .max(20, 'Максимум 20 символов'),
  email: yup
    .string()
    .required('Поле не может быть пустым')
    .email('Некорректный email'),
  password: yup
    .string()
    .required('Пароль не может быть пустым')
    .min(6, 'Пароль должен быть длинною не менее 6 символов')
    .max(100, 'Пароль должен быть длинною не более 100 символов'),
  confirmPassword: yup
    .string()
    .oneOf([yup.ref('password'), null], 'Пароли не совпадают')
})

export default schema
