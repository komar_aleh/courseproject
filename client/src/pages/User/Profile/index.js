import * as React from 'react'
import {
  Box,
  Button,
  Container,
  FormControl,
  FormHelperText,
  Grid,
  MenuItem,
  Modal,
  Pagination,
  Paper,
  Select,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  TextField,
  Typography
} from '@mui/material'
import useAuth from '../../../hooks/useAuth'
import OrderList from '../../../components/Orders/OrderList'
import { getUser, updateUser, changePassword } from '../../../http/userAPI'
import { getAllOrders } from '../../../http/orderAPI'
import Spinner from '../../../components/Spinner'
import ErrorSuccess from '../../../components/ErrorSucces'

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 300,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 5,
  p: 4
}

const Profile = () => {
  const auth = useAuth()
  const [data, setData] = React.useState([])
  const [appState, setAppState] = React.useState({
    loading: false
  })
  const [firstName, setFirstName] = React.useState('')
  const [lastName, setLastName] = React.useState('')
  const [userName, setUserName] = React.useState('')
  const [email, setEmail] = React.useState('')
  const [oldPassword, setOldPassword] = React.useState('')
  const [newPassword, setNewPassword] = React.useState('')
  const [confirmNewPassword, setConfirmNewPassword] = React.useState('')

  const handleChangeFirstName = (e) => {
    setFirstName(e.target.value)
  }

  const handleChangeLastName = (e) => {
    setLastName(e.target.value)
  }

  const handleChangeOldPassword = (e) => {
    setOldPassword(e.target.value)
  }

  const handleChangeNewPassword = (e) => {
    setNewPassword(e.target.value)
  }

  const handleChangeConfirmNewPassword = (e) => {
    setConfirmNewPassword(e.target.value)
  }

  const [openError, setOpenError] = React.useState(false)
  const [openSuccess, setOpenSuccess] = React.useState(false)
  const [messageError, setMessageError] = React.useState('')
  const [messageSuccess, setMessageSuccess] = React.useState('')

  const [open, setOpen] = React.useState(false)
  const handleClose = () => setOpen(false)
  const handleOpen = () => setOpen(true)

  const [openChange, setOpenChange] = React.useState(false)
  const handleCloseChange = () => setOpenChange(false)
  const handleOpenChange = () => setOpenChange(true)

  const getProfile = async () => {
    try {
      setAppState({ loading: true })
      const { data } = await getUser(auth.userId)
      setFirstName(data.firstName)
      setLastName(data.lastName)
      setUserName(data.userName)
      setEmail(data.email)
      setAppState({ loading: false })
    } catch (error) {
      const erdm = error.response.data.message
        ? error.response.data.message
        : ''
      const erde = error.response.data.errors ? error.response.data.errors : ''
      const em = error.message ? error.message : ''
      setMessageError(erdm, erde, em)
      setOpenError(true)
    }
  }

  const [filter, setFilter] = React.useState({
    orderBy: 'OrderDate desc',
    searchName: '',
    statusId: 0,
    pageNumber: 1,
    pageSize: 10
  })

  const [totalPages, setTotalPages] = React.useState(0)
  const [recordsFiltered, setRecordsFiltered] = React.useState(0)
  const [totalRecords, setTotalRecords] = React.useState(0)

  const [initialFilter, setInitialFilter] = React.useState([])

  const handleChangeFilter = (prop) => (value) => {
    setFilter({ ...filter, [prop]: value })
  }
  const setParameters = (prop) => (event) => {
    setFilter({ ...filter, [prop]: event.target.value })
  }

  const getMyOrders = async () => {
    try {
      setAppState({ loading: true })
      const response = await getAllOrders(
        filter.orderBy,
        filter.statusId,
        auth.userId,
        filter.pageNumber,
        filter.pageSize
      )
      setData(response.data)
      setTotalPages(response.totalPages)
      setTotalRecords(response.totalRecords)
      setRecordsFiltered(response.recordsFiltered)
      setAppState({ loading: false })
    } catch (error) {
      const erdm = error.response.data.message
        ? error.response.data.message
        : ''
      const erde = error.response.data.errors ? error.response.data.errors : ''
      const em = error.message ? error.message : ''
      setMessageError(erdm, erde, em)
      setOpenError(true)
    }
  }

  const handleCancel = async () => {
    getMyOrders()
  }

  const onSubmitChangeProfile = async (e) => {
    e.preventDefault()
    try {
      const response = await updateUser(auth.userId, firstName, lastName)
      setOpenSuccess(true)
      setMessageSuccess(response.message)
    } catch (error) {
      getProfile()
      const erdm = error.response.data.message
        ? error.response.data.message
        : ''
      const erde = error.response.data.errors ? error.response.data.errors : ''
      const em = error.message ? error.message : ''
      setMessageError(erdm, erde, em)
      setOpenError(true)
    }
    setOpen(false)
  }

  const onSubmitChangePassword = async (e) => {
    e.preventDefault()

    try {
      const response = await changePassword(
        oldPassword,
        newPassword,
        confirmNewPassword
      )
      setOpenSuccess(true)
      setMessageSuccess(response.message)
    } catch (error) {
      const erdm = error.response.data.message
        ? error.response.data.message
        : ''
      const erde = error.response.data.errors ? error.response.data.errors : ''
      const em = error.message ? error.message : ''
      setMessageError(erdm, erde, em)
      setOpenError(true)
    }
    setOpenChange(false)
  }

  React.useEffect(() => {
    document.title = 'Профиль'
    setInitialFilter(filter)
    getProfile()
    getMyOrders()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter, initialFilter])

  return appState.loading ? (
    <Spinner />
  ) : (
    <Container
      component="main"
      maxWidth="lg"
      sx={{ p: 2, margin: 'auto', flexGrow: 1, minWidth: 400 }}
    >
      <ErrorSuccess
        openE={openError}
        messageE={messageError}
        closeE={() => {
          setOpenError(false)
        }}
        openS={openSuccess}
        messageS={messageSuccess}
        closeS={() => {
          setOpenSuccess(false)
        }}
      />
      <Paper variant="outlined" sx={{ p: 2, margin: '5px 0 5px' }}>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            marginBottom: 2
          }}
        >
          <Typography variant="h5" component="div" gutterBottom>
            Здравствуйте, <b>{userName}</b>. Данные вашего профиля:
          </Typography>
        </Box>
        <Modal
          open={open}
          onClose={handleClose}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style}>
            <Box
              component="form"
              onSubmit={onSubmitChangeProfile}
              noValidate
              sx={{ mt: 1 }}
            >
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <TextField
                    autoComplete="given-name"
                    name="firstName"
                    required
                    defaultValue={firstName}
                    fullWidth
                    id="firstName"
                    label="Имя"
                    variant="standard"
                    onChange={handleChangeFirstName}
                    autoFocus
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    defaultValue={lastName}
                    id="lastName"
                    label="Фамилия"
                    name="lastName"
                    variant="standard"
                    autoComplete="family-name"
                    onChange={handleChangeLastName}
                  />
                </Grid>
              </Grid>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
              >
                Изменить
              </Button>
            </Box>
          </Box>
        </Modal>
        <Modal
          open={openChange}
          onClose={handleCloseChange}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style}>
            <Box
              component="form"
              onSubmit={onSubmitChangePassword}
              noValidate
              sx={{ mt: 1 }}
            >
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <TextField
                    autoComplete="oldPassword"
                    name="oldPassword"
                    required
                    fullWidth
                    type="password"
                    id="oldPassword"
                    label="Старый пароль"
                    variant="standard"
                    onChange={handleChangeOldPassword}
                    autoFocus
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    type="password"
                    id="newPassword"
                    label="Новый пароль"
                    name="newPassword"
                    variant="standard"
                    autoComplete="newPassword"
                    onChange={handleChangeNewPassword}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    type="password"
                    id="confirmNewPassword"
                    label="Подтвреждение пароля"
                    name="confirmNewPassword"
                    variant="standard"
                    autoComplete="confirmNewPassword"
                    onChange={handleChangeConfirmNewPassword}
                  />
                </Grid>
              </Grid>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
              >
                Изменить
              </Button>
            </Box>
          </Box>
        </Modal>
        <Grid container spacing={2}>
          <Grid item xs={8}>
            <TableContainer sx={{ minWidth: 400 }}>
              <Table>
                <TableBody>
                  <TableRow>
                    <TableCell
                      align="center"
                      style={{ width: '50%' }}
                      sx={{ fontSize: '1rem', border: '1px solid #ddd' }}
                    >
                      Имя
                    </TableCell>
                    <TableCell
                      align="center"
                      style={{ width: '50%' }}
                      sx={{ fontSize: '1rem', border: '1px solid #ddd' }}
                    >
                      {firstName}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell
                      align="center"
                      style={{ width: '50%' }}
                      sx={{ fontSize: '1rem', border: '1px solid #ddd' }}
                    >
                      Фамилия
                    </TableCell>
                    <TableCell
                      align="center"
                      style={{ width: '50%' }}
                      sx={{ fontSize: '1rem', border: '1px solid #ddd' }}
                    >
                      {lastName}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell
                      align="center"
                      style={{ width: '50%' }}
                      sx={{ fontSize: '1rem', border: '1px solid #ddd' }}
                    >
                      Никнейм
                    </TableCell>
                    <TableCell
                      align="center"
                      style={{ width: '50%' }}
                      sx={{ fontSize: '1rem', border: '1px solid #ddd' }}
                    >
                      {userName}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell
                      align="center"
                      style={{ width: '50%' }}
                      sx={{ fontSize: '1rem', border: '1px solid #ddd' }}
                    >
                      Email
                    </TableCell>
                    <TableCell
                      align="center"
                      style={{ width: '50%' }}
                      sx={{ fontSize: '1rem', border: '1px solid #ddd' }}
                    >
                      {email}
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
          </Grid>
          <Grid item xs={4}>
            <Paper
              elevation={0}
              sx={{
                height: '100%',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <Stack>
                <Button
                  sx={{ ml: 'auto', mr: 'auto' }}
                  variant="text"
                  onClick={handleOpen}
                >
                  Изменить профиль
                </Button>
                <Button
                  sx={{ ml: 'auto', mr: 'auto' }}
                  variant="text"
                  onClick={handleOpenChange}
                >
                  Изменить пароль
                </Button>
              </Stack>
            </Paper>
          </Grid>
        </Grid>
      </Paper>
      {!auth.isAdmin && (
        <Paper
          variant="outlined"
          sx={{ p: 2, margin: '5px 0 5px', flexGrow: 1 }}
        >
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              marginBottom: 2
            }}
          >
            <Typography variant="h5" component="div" gutterBottom>
              История ваших заказов
            </Typography>
            {data.length > 0 ? (
              <Grid container spacing={1}>
                <Grid item xs={12}>
                  <Paper variant="outlined" sx={{ p: 2, margin: '5px 0 5px' }}>
                    <Grid
                      container
                      spacing={2}
                      alignItems="center"
                      justifyContent="center"
                    >
                      <Grid item xs={5}>
                        <FormControl sx={{ ml: 'auto', mr: 'auto' }} fullWidth>
                          <FormHelperText>Сортировка:</FormHelperText>
                          <Select
                            value={filter.orderBy}
                            onChange={setParameters('orderBy')}
                          >
                            <MenuItem value={'OrderDate desc'}>
                              Сначала новые
                            </MenuItem>
                            <MenuItem value={'OrderDate'}>
                              Сначала старые
                            </MenuItem>
                          </Select>
                        </FormControl>
                      </Grid>
                      <Grid item xs={4}>
                        <FormControl sx={{ ml: 'auto', mr: 'auto' }} fullWidth>
                          <FormHelperText>Показывать:</FormHelperText>
                          <Select
                            value={filter.pageSize}
                            onChange={setParameters('pageSize')}
                          >
                            <MenuItem value={1}>1 на странице</MenuItem>
                            <MenuItem value={2}>2 на странице</MenuItem>
                            <MenuItem value={3}>3 на странице</MenuItem>
                            <MenuItem value={5}>5 на странице</MenuItem>
                            <MenuItem value={10}>10 на странице</MenuItem>
                          </Select>
                        </FormControl>
                      </Grid>
                      <Grid item xs={3}>
                        <Typography sx={{ ml: 3 }}>
                          Выбрано {recordsFiltered} из {totalRecords}
                        </Typography>
                      </Grid>
                    </Grid>
                  </Paper>
                  <Paper variant="outlined" sx={{ p: 2, margin: '5px 0 5px' }}>
                    <OrderList
                      isLoading={appState.loading}
                      items={data}
                      onCancel={handleCancel}
                    />
                  </Paper>
                  <Paper
                    elevation={0}
                    sx={{
                      display: 'flex',
                      p: 2,
                      margin: '5px 0 5px'
                    }}
                  >
                    <Paper
                      elevation={0}
                      sx={{
                        display: 'flex',
                        p: 2,
                        marginTop: '5px',
                        marginBottom: '5px',
                        marginLeft: 'auto',
                        marginRight: 'auto'
                      }}
                    >
                      <Pagination
                        count={totalPages}
                        color="primary"
                        boundaryCount={2}
                        showFirstButton
                        showLastButton
                        sx={{ ml: 'auto', mr: 'auto', mb: 2 }}
                        size="large"
                        page={filter.pageNumber}
                        onChange={(_, value) =>
                          handleChangeFilter('pageNumber')(value)
                        }
                      />
                    </Paper>
                  </Paper>
                </Grid>
              </Grid>
            ) : (
              <Typography
                gutterBottom
                variant="h3"
                component="div"
                sx={{ display: 'flex', p: 2, margin: '20px' }}
              >
                У вас не было заказов.
              </Typography>
            )}
          </Box>
        </Paper>
      )}
    </Container>
  )
}

export default Profile
