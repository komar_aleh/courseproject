import * as React from 'react'
import jwt_decode from 'jwt-decode'
import { Link } from 'react-router-dom'
import { useNavigate } from 'react-router-dom'
import {
  Alert,
  Avatar,
  Box,
  Button,
  Collapse,
  Container,
  CssBaseline,
  Grid,
  IconButton,
  TextField,
  Typography
} from '@mui/material'
import CloseIcon from '@mui/icons-material/Close'
import LockOutlinedIcon from '@mui/icons-material/LockOutlined'
import useAuth from '../../../hooks/useAuth'
import { login } from '../../../http/userAPI'
import { getAllCartItems } from './../../../http/cartAPI'
import { useForm, Controller } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import validationSchema from './validation'

const Login = () => {
  const auth = useAuth()
  const navigate = useNavigate()

  const [openError, setOpenError] = React.useState(false)
  const [messageError, setMessageError] = React.useState('')

  React.useEffect(() => {
    document.title = 'Войти'
  }, [])

  const {
    control,
    handleSubmit,
    formState: { errors, isValid },
    reset
  } = useForm({
    resolver: yupResolver(validationSchema),
    mode: 'onChange'
  })

  const onSubmit = async (data) => {
    try {
      const { data: result } = await login(data)
      if (result.data) {
        auth.setAuthed(true)
      }

      const tokenData = jwt_decode(result.data)
      auth.setToken(result.data)
      auth.setUserName(tokenData.UserName)
      auth.setUserId(tokenData.UserId)
      if (tokenData.Role === 'Admin') {
        auth.setAdmin(true)
      }
      const response = await getAllCartItems()
      auth.setCartItems(response.data.length)
      //navigate(-1)
      navigate('/')
    } catch (error) {
      setOpenError(true)
      setMessageError('Ошибка ввода данных')
    }
    reset({
      userName: '',
      password: ''
    })
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <Collapse in={openError}>
        <Alert
          variant="filled"
          severity="error"
          action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              onClick={() => {
                setOpenError(false)
              }}
            >
              <CloseIcon fontSize="inherit" />
            </IconButton>
          }
          sx={{ mb: 2 }}
        >
          {messageError}
        </Alert>
      </Collapse>
      <Box
        sx={{
          marginTop: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center'
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: 'primary.main' }}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Вход
        </Typography>
        <Box component="form" onSubmit={handleSubmit(onSubmit)} sx={{ mt: 1 }}>
          <Controller
            name="userName"
            control={control}
            defaultValue=""
            render={({ field }) => (
              <TextField
                {...field}
                error={Boolean(errors.userName?.message)}
                fullWidth={true}
                type="text"
                label="Имя пользователя (никнейм)"
                variant="outlined"
                margin="normal"
                id="userName"
                helperText={errors.userName?.message}
              />
            )}
          />
          <Controller
            name="password"
            control={control}
            defaultValue=""
            render={({ field }) => (
              <TextField
                {...field}
                error={Boolean(errors.password?.message)}
                type="password"
                fullWidth={true}
                label="Пароль"
                variant="outlined"
                helperText={errors.password?.message}
              />
            )}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            disabled={!isValid}
            sx={{ mt: 3, mb: 2 }}
          >
            Войти
          </Button>
          <Grid container>
            <Grid item xs>
              <Link to="/forgot">Забыли пароль?</Link>
            </Grid>
            <Grid item>
              <Link to="/register">Нет аккаунта? Зарегистрировать</Link>
            </Grid>
          </Grid>
          <Grid container>
            <Grid item xs>
              <Link to="/activate">Активировать аккаунт</Link>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </Container>
  )
}

export default Login
