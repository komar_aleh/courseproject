import * as yup from 'yup'

const schema = yup.object().shape({
  userName: yup
    .string()
    .required('Поле не может быть пустым')
    .min(2, 'Минимум 2 символа')
    .max(20, 'Максимум 20 символов'),
  password: yup
    .string()
    .required('Пароль не может быть пустым')
    .min(6, 'Пароль должен быть длинною не менее 6 символов')
    .max(100, 'Пароль должен быть длинною не более 100 символов')
})

export default schema
