import * as React from 'react'
import { useSearchParams } from 'react-router-dom'
import {
  Avatar,
  Box,
  Button,
  Container,
  CssBaseline,
  Grid,
  TextField,
  Typography
} from '@mui/material'
import LockOutlinedIcon from '@mui/icons-material/LockOutlined'
import { resetPassword } from '../../../http/userAPI'
import { useNavigate } from 'react-router-dom'
import ErrorSuccess from '../../../components/ErrorSucces'

const ResetPassword = () => {
  const navigate = useNavigate()
  const [openError, setOpenError] = React.useState(false)
  const [openSuccess, setOpenSuccess] = React.useState(false)
  const [messageError, setMessageError] = React.useState('')
  const [messageSuccess, setMessageSuccess] = React.useState('')

  React.useEffect(() => {
    document.title = 'Сброс пароля'
  }, [])

  const [values, setValues] = React.useState({
    password: '',
    confirmPassword: '',
    showPassword: false
  })

  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value })
  }

  const [queryParams] = useSearchParams()
  const uid = queryParams.get('userId')
  const token = queryParams.get('token')

  const onSubmit = async (e) => {
    e.preventDefault()
    try {
      const result = resetPassword(
        uid,
        values.password,
        values.confirmPassword,
        token
      )
      setOpenSuccess(true)
      setMessageSuccess(result.message)
    } catch (error) {
      const erdm = error.response.data.message
        ? error.response.data.message
        : ''
      const erde = error.response.data.errors ? error.response.data.errors : ''
      const em = error.message ? error.message : ''
      setMessageError(erdm, erde, em)
      setOpenError(true)
    }
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <ErrorSuccess
        openE={openError}
        messageE={messageError}
        closeE={() => {
          setOpenError(false)
        }}
        openS={openSuccess}
        messageS={messageSuccess}
        closeS={() => {
          setOpenSuccess(false)
          navigate('/')
        }}
      />
      <Box
        sx={{
          marginTop: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center'
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: 'primary.main' }}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Сброс пароля
        </Typography>
        <Box component="form" noValidate onSubmit={onSubmit} sx={{ mt: 3 }}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                name="password"
                label="Пароль"
                type={'password'}
                id="password"
                variant="outlined"
                onChange={handleChange('password')}
                autoComplete="new-password"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                name="confirmPassword"
                label="Подтверждение пароля"
                type={values.showPassword ? 'text' : 'password'}
                id="confirmPassword"
                variant="outlined"
                onChange={handleChange('confirmPassword')}
                autoComplete="new-confirmPassword"
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Изменить пароль
          </Button>
        </Box>
      </Box>
    </Container>
  )
}

export default ResetPassword
