import * as yup from 'yup'

const schema = yup.object().shape({
  password: yup
    .string()
    .required('Пароль не может быть пустым')
    .min(6, 'Пароль должен быть длинною не менее 6 символов')
    .max(100, 'Пароль должен быть длинною не более 100 символов'),
  confirmPassword: yup
    .string()
    .oneOf([yup.ref('password'), null], 'Пароли не совпадают')
})

export default schema
