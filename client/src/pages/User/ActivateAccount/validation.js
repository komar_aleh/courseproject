import * as yup from 'yup'

const schema = yup.object().shape({
  email: yup
    .string()
    .required('Email не может быть пустым')
    .email('Некорректный email')
})

export default schema
