import * as React from 'react'
import {
  Alert,
  Box,
  Button,
  Collapse,
  Container,
  CssBaseline,
  Grid,
  IconButton,
  Stack,
  Step,
  StepLabel,
  Stepper,
  TextField,
  Typography
} from '@mui/material'
import { styled } from '@mui/material/styles'
import { Link } from 'react-router-dom'
import CloseIcon from '@mui/icons-material/Close'
import { createOrder } from './../../http/orderAPI'
import useAuth from '../../hooks/useAuth'
import { useForm, Controller } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import validationSchema from './validation'

const ElLink = styled(Link)`
  align-items: center;
  text-decoration: none;
  padding: 0;
  height: 100%;
  cursor: pointer;

  &.active {
    color: #ff0000;
    text-decoration: none;
  }

  &:hover {
    color: #556cd6;
    text-decoration: none;
  }
`

const steps = ['Информация о доставке', 'Подтверждение заказа']

const Checkout = () => {
  const [activeStep, setActiveStep] = React.useState(0)
  const [openError, setOpenError] = React.useState(false)
  const [messageError, setMessageError] = React.useState('')
  const [errs, setErrs] = React.useState([])
  const [orderNumber, setOrderNumber] = React.useState(0)
  const auth = useAuth()

  const handleNext = () => {
    if (activeStep === steps.length - 1) {
      try {
        orderCreate()
      } catch (error) {
        setActiveStep((prevActiveStep) => prevActiveStep - 1)
      }
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1)
  }

  const orderCreate = async () => {
    try {
      const result = await createOrder(
        shippingFirstName,
        shippingLastName,
        shippingZip,
        shippingCity,
        shippingAddressLine,
        shippingPhoneNumber
      )
      setOrderNumber(result.data.id)
      auth.setCartItems(0)
    } catch (error) {
      setOpenError(true)
      setMessageError(error.response.data.message)
      if (error.response.data.errors) {
        setErrs(error.response.data.errors)
      }
    }
  }

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1)
  }
  const [shippingFirstName, setShippingFirstName] = React.useState('')
  const [shippingLastName, setShippingLastName] = React.useState('')
  const [shippingZip, setShippingZip] = React.useState('')
  const [shippingCity, setShippingCity] = React.useState('')
  const [shippingAddressLine, setShippingAddressLine] = React.useState('')
  const [shippingPhoneNumber, setShippingPhoneNumber] = React.useState('')

  const onSubmit = (data) => {
    setShippingFirstName(data.shippingFirstName)
    setShippingLastName(data.shippingLastName)
    setShippingZip(data.shippingZip)
    setShippingCity(data.shippingCity)
    setShippingAddressLine(data.shippingAddressLine)
    setShippingPhoneNumber(data.shippingPhoneNumber)
    setActiveStep((prevActiveStep) => prevActiveStep + 1)
  }

  const {
    control,
    handleSubmit,
    formState: { errors, isValid }
  } = useForm({
    resolver: yupResolver(validationSchema),
    mode: 'onChange'
  })

  return (
    <Container
      component="main"
      maxWidth="lg"
      sx={{ p: 2, margin: 'auto', flexGrow: 1, minWidth: 400 }}
    >
      <Collapse in={openError}>
        <Alert
          variant="filled"
          severity="error"
          action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              onClick={() => {
                setOpenError(false)
              }}
            >
              <CloseIcon fontSize="inherit" />
            </IconButton>
          }
          sx={{ mb: 2 }}
        >
          {messageError}
          {errs}
        </Alert>
      </Collapse>
      <Box sx={{ width: '100%' }}>
        <Stepper activeStep={activeStep}>
          {steps.map((label) => {
            const stepProps = {}
            const labelProps = {}
            return (
              <Step key={label} {...stepProps}>
                <StepLabel {...labelProps}>{label}</StepLabel>
              </Step>
            )
          })}
        </Stepper>
        {activeStep === 0 && (
          <>
            <Container component="main" maxWidth="xs">
              <CssBaseline />
              <Box
                component="form"
                sx={{ mt: 1 }}
                onSubmit={handleSubmit(onSubmit)}
              >
                <Grid container spacing={1}>
                  <Grid item xs={12} sm={6}>
                    <Controller
                      name="shippingFirstName"
                      control={control}
                      defaultValue=""
                      render={({ field }) => (
                        <TextField
                          {...field}
                          error={Boolean(errors.shippingFirstName?.message)}
                          fullWidth={true}
                          type="text"
                          label="Имя получателя"
                          variant="outlined"
                          margin="normal"
                          id="shippingFirstName"
                          helperText={errors.shippingFirstName?.message}
                        />
                      )}
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Controller
                      name="shippingLastName"
                      control={control}
                      defaultValue=""
                      render={({ field }) => (
                        <TextField
                          {...field}
                          error={Boolean(errors.shippingLastName?.message)}
                          fullWidth={true}
                          type="text"
                          label="Фамилия получателя"
                          variant="outlined"
                          margin="normal"
                          id="shippingLastName"
                          helperText={errors.shippingLastName?.message}
                        />
                      )}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Controller
                      name="shippingZip"
                      control={control}
                      defaultValue=""
                      render={({ field }) => (
                        <TextField
                          {...field}
                          error={Boolean(errors.shippingZip?.message)}
                          fullWidth={true}
                          type="text"
                          label="Индекс получателя"
                          variant="outlined"
                          margin="normal"
                          id="shippingZip"
                          helperText={errors.shippingZip?.message}
                        />
                      )}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Controller
                      name="shippingCity"
                      control={control}
                      defaultValue=""
                      render={({ field }) => (
                        <TextField
                          {...field}
                          error={Boolean(errors.shippingCity?.message)}
                          fullWidth={true}
                          type="text"
                          label="Город получателя"
                          variant="outlined"
                          margin="normal"
                          id="shippingCity"
                          helperText={errors.shippingCity?.message}
                        />
                      )}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Controller
                      name="shippingAddressLine"
                      control={control}
                      defaultValue=""
                      render={({ field }) => (
                        <TextField
                          {...field}
                          error={Boolean(errors.shippingAddressLine?.message)}
                          fullWidth={true}
                          type="text"
                          label="Адрес"
                          variant="outlined"
                          margin="normal"
                          id="shippingAddressLine"
                          helperText={errors.shippingAddressLine?.message}
                        />
                      )}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Controller
                      name="shippingPhoneNumber"
                      control={control}
                      defaultValue=""
                      render={({ field }) => (
                        <TextField
                          {...field}
                          error={Boolean(errors.shippingPhoneNumber?.message)}
                          fullWidth={true}
                          type="text"
                          label="Телефон получателя (мобильный)"
                          variant="outlined"
                          margin="normal"
                          id="shippingPhoneNumber"
                          helperText={errors.shippingPhoneNumber?.message}
                        />
                      )}
                    />
                  </Grid>
                </Grid>
                <Stack direction="row">
                  <Button
                    type="submit"
                    variant="contained"
                    sx={{ ml: 'auto' }}
                    disabled={!isValid}
                  >
                    Далее
                  </Button>
                </Stack>
              </Box>
            </Container>
          </>
        )}
        {activeStep === 1 && (
          <>
            <Container component="main" maxWidth="md">
              <Typography
                sx={{ mt: 2, mb: 1 }}
                variant="h4"
                gutterBottom
                component="div"
              >
                Проверьте правильность введенных данных. И нажмите кнопку
                ЗАКАЗАТЬ для потдтверждения заказа.
              </Typography>
              <Grid container>
                <Grid item xs={6}>
                  <Typography sx={{ mt: 2, mb: 1 }}>Имя получателя:</Typography>
                </Grid>
                <Grid item xs={6}>
                  <Typography sx={{ mt: 2, mb: 1, fontSize: '1.5rem' }}>
                    {shippingFirstName}
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <Typography sx={{ mt: 2, mb: 1 }}>
                    Фамилия получателя:
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <Typography sx={{ mt: 2, mb: 1, fontSize: '1.5rem' }}>
                    {shippingLastName}
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <Typography sx={{ mt: 2, mb: 1 }}>
                    Индекс получателя:
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <Typography sx={{ mt: 2, mb: 1, fontSize: '1.5rem' }}>
                    {shippingZip}
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <Typography sx={{ mt: 2, mb: 1 }}>
                    Город получателя:
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <Typography sx={{ mt: 2, mb: 1, fontSize: '1.5rem' }}>
                    {shippingCity}
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <Typography sx={{ mt: 2, mb: 1 }}>
                    Адрес получателя:
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <Typography sx={{ mt: 2, mb: 1, fontSize: '1.5rem' }}>
                    {shippingAddressLine}
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <Typography sx={{ mt: 2, mb: 1 }}>
                    Телефон получателя:
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <Typography sx={{ mt: 2, mb: 1, fontSize: '1.5rem' }}>
                    {shippingPhoneNumber}
                  </Typography>
                </Grid>
              </Grid>
              <Box sx={{ display: 'flex', flexDirection: 'row', pt: 2 }}>
                <Button
                  color="inherit"
                  variant="contained"
                  onClick={handleBack}
                >
                  Назад
                </Button>
                <Box sx={{ flex: '1 1 auto' }} />
                <Button variant="contained" onClick={handleNext}>
                  Заказать
                </Button>
              </Box>
            </Container>
          </>
        )}
        {activeStep === steps.length && (
          <>
            <Typography sx={{ mt: 2, mb: 1, fontSize: '2rem' }}>
              Ваш заказ оформлен! Номер заказа: {orderNumber}
            </Typography>
            <Box sx={{ display: 'flex', flexDirection: 'row', pt: 2 }}>
              {/* <Box sx={{ flex: '1 1 auto' }} /> */}
              <ElLink sx={{ fontSize: '1.5rem' }} to="/">
                <Button variant="contained">Вернуться на главную!</Button>
              </ElLink>
              <ElLink sx={{ fontSize: '1.5rem', ml: 'auto' }} to="/profile">
                <Button variant="contained">Посмотреть историю заказов</Button>
              </ElLink>
            </Box>
          </>
        )}
      </Box>
    </Container>
  )
}

export default Checkout
