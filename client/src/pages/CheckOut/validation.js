import * as yup from 'yup'

const schema = yup.object().shape({
  shippingFirstName: yup.string().required('Поле не может быть пустым'),
  shippingLastName: yup.string().required('Поле не может быть пустым'),
  shippingZip: yup
    .string()
    .required('Поле не может быть пустым')
    .matches(/[0-9]{6}/, 'Индекс имеет неверный формат'),
  shippingCity: yup.string().required('Поле не может быть пустым'),
  shippingAddressLine: yup.string().required('Поле не может быть пустым'),
  shippingPhoneNumber: yup
    .string()
    .matches(
      /^(\+375(25|29|33|44)[0-9]{7})|(\+375(\s+)?(25|29|33|44)(\s+)?[0-9]{7})|(\+375-?(25|29|33|44)-?[0-9]{7})$/,
      'Телефон должен иметь формат: +375xxxxxxxxx / +375 xx xxxxxxx / +375-xx-xxxxxxx. Доступные для ввода коды оператора: 25/29/33/44'
    )
})

export default schema
