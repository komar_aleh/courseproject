import './404.scss'

const NotFound = () => {
  return (
    <>
      <div className="numberError">
        <h1 className="error" data-text="404">
          404
        </h1>
      </div>
      <h2 className="notFound">Страница не найдена</h2>
    </>
  )
}

export default NotFound
