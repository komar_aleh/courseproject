import React from 'react'
import { Link } from 'react-router-dom'
import { Button, Container, Stack, Typography } from '@mui/material'
import CartList from '../../components/Cart/CartList'
import { getAllCartItems, deleteAllCartItems } from './../../http/cartAPI'
import Spinner from './../../components/Spinner/index'
import useAuth from '../../hooks/useAuth'
import { styled } from '@mui/material/styles'

const ElLink = styled(Link)`
  color: #000000;
  align-items: center;
  text-decoration: none;
  padding: 0;
  height: 100%;
  cursor: pointer;

  &.active {
    color: #ff0000;
    text-decoration: none;
  }

  &:hover {
    color: #556cd6;
    text-decoration: none;
  }
  }
`

const Cart = () => {
  const [data, setData] = React.useState([])
  const [appState, setAppState] = React.useState({
    loading: false
  })
  const auth = useAuth()

  const getCarts = async () => {
    setAppState({ loading: true })
    const response = await getAllCartItems()
    setData(response.data)
    setAppState({ loading: false })
  }
  React.useEffect(() => {
    document.title = 'Корзина'
    getCarts()

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const handleDelete = async () => {
    try {
      getCarts()
    } catch (error) {
      console.log(error)
    }
  }

  const handleDeleteAll = () => {
    deleteAllCartItems()
    auth.setCartItems(0)
    getCarts()
  }

  return appState.loading ? (
    <Spinner />
  ) : (
    <Container
      component="main"
      maxWidth="lg"
      sx={{ p: 2, margin: 'auto', flexGrow: 1, minWidth: 400 }}
    >
      {data.length > 0 ? (
        <>
          <CartList
            isLoading={appState.loading}
            items={data}
            onDelete={handleDelete}
          />
          <Stack direction="row" spacing={2} sx={{ mt: 2 }}>
            <Button
              variant="contained"
              onClick={handleDeleteAll}
              sx={{ mr: 'auto' }}
            >
              Очистить корзину
            </Button>
            <ElLink to={'/checkout'}>
              <Button variant="contained">Перейти к формлению заказа</Button>
            </ElLink>
          </Stack>
        </>
      ) : (
        <Typography
          gutterBottom
          variant="h3"
          component="div"
          sx={{ display: 'flex', p: 2, margin: '20px' }}
        >
          Нет товаров в корзине. Добавьте новый.
        </Typography>
      )}
    </Container>
  )
}

export default Cart
