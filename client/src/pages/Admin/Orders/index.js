import * as React from 'react'
import {
  Box,
  Button,
  Container,
  FormControl,
  FormHelperText,
  Grid,
  MenuItem,
  Pagination,
  Paper,
  Select,
  Typography
} from '@mui/material'
import { getAllOrders } from '../../../http/orderAPI'
import Spinner from '../../../components/Spinner'
import ErrorSuccess from '../../../components/ErrorSucces'
import OrderListAdmin from '../../../components/Admin/Order/OrderListAdmin'

const Orders = () => {
  const [data, setData] = React.useState([])
  const [appState, setAppState] = React.useState({
    loading: false
  })

  const [openError, setOpenError] = React.useState(false)
  const [openSuccess, setOpenSuccess] = React.useState(false)
  const [messageError, setMessageError] = React.useState('')

  const [filter, setFilter] = React.useState({
    orderBy: 'OrderDate desc',
    searchName: '',
    statusId: 0,
    pageNumber: 1,
    pageSize: 10
  })

  const [totalPages, setTotalPages] = React.useState(0)
  const [recordsFiltered, setRecordsFiltered] = React.useState(0)
  const [totalRecords, setTotalRecords] = React.useState(0)

  const [initialFilter, setInitialFilter] = React.useState([])

  const handleChangeFilter = (prop) => (value) => {
    setFilter({ ...filter, [prop]: value })
  }
  const setParameters = (prop) => (event) => {
    setFilter({ ...filter, [prop]: event.target.value })
  }

  const getOrders = async () => {
    try {
      setAppState({ loading: true })
      const response = await getAllOrders(
        filter.orderBy,
        filter.statusId,
        '',
        filter.pageNumber,
        filter.pageSize
      )
      setData(response.data)
      setTotalPages(response.totalPages)
      setTotalRecords(response.totalRecords)
      setRecordsFiltered(response.recordsFiltered)
      setAppState({ loading: false })
    } catch (error) {
      const erdm = error.response.data.message
        ? error.response.data.message
        : ''
      const erde = error.response.data.errors ? error.response.data.errors : ''
      const em = error.message ? error.message : ''
      setMessageError(erdm, erde, em)
      setOpenError(true)
    }
  }

  const handleChangeStatus = async () => {
    try {
      getOrders()
    } catch (error) {
      console.log(error)
    }
  }

  const handleResetFilter = () => {
    setFilter(initialFilter)
  }

  const handleDelete = async () => {
    try {
      getOrders()
    } catch (error) {
      console.log(error)
    }
  }

  React.useEffect(() => {
    document.title = 'Администратор: заказы'
    setInitialFilter(filter)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [initialFilter])

  React.useEffect(() => {
    getOrders()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter])

  return appState.loading ? (
    <Spinner />
  ) : (
    <Container
      component="main"
      maxWidth="lg"
      sx={{ p: 2, margin: 'auto', flexGrow: 1, minWidth: 400 }}
    >
      <ErrorSuccess
        openE={openError}
        messageE={messageError}
        closeE={() => {
          setOpenError(false)
        }}
        openS={openSuccess}
        messageS={messageError}
        closeS={() => {
          setOpenSuccess(false)
        }}
      />
      <Paper variant="outlined" sx={{ p: 2, margin: '5px 0 5px', flexGrow: 1 }}>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            marginBottom: 2
          }}
        >
          <Typography variant="h3" component="div" gutterBottom>
            Заказы
          </Typography>
          {data.length > 0 ? (
            <Grid container spacing={1}>
              <Grid item xs={12}>
                <Paper variant="outlined" sx={{ p: 2, margin: '5px 0 5px' }}>
                  <Grid
                    container
                    spacing={2}
                    alignItems="center"
                    justifyContent="center"
                  >
                    <Grid item xs={4}>
                      <FormControl sx={{ ml: 'auto', mr: 'auto' }} fullWidth>
                        <FormHelperText>Сортировка:</FormHelperText>
                        <Select
                          value={filter.orderBy}
                          onChange={setParameters('orderBy')}
                        >
                          <MenuItem value={'OrderDate desc'}>
                            Сначала новые
                          </MenuItem>
                          <MenuItem value={'OrderDate'}>
                            Сначала старые
                          </MenuItem>
                        </Select>
                      </FormControl>
                    </Grid>
                    <Grid item xs={3}>
                      <FormControl sx={{ ml: 'auto', mr: 'auto' }} fullWidth>
                        <FormHelperText>Показывать:</FormHelperText>
                        <Select
                          value={filter.pageSize}
                          onChange={setParameters('pageSize')}
                        >
                          <MenuItem value={5}>5 на странице</MenuItem>
                          <MenuItem value={10}>10 на странице</MenuItem>
                          <MenuItem value={15}>15 на странице</MenuItem>
                          <MenuItem value={20}>20 на странице</MenuItem>
                          <MenuItem value={25}>25 на странице</MenuItem>
                        </Select>
                      </FormControl>
                    </Grid>
                    <Grid item xs={3}>
                      <Typography sx={{ ml: 3 }}>
                        Выбрано {recordsFiltered} из {totalRecords}
                      </Typography>
                    </Grid>
                    <Grid item xs={2}>
                      <Button
                        variant="contained"
                        sx={{ mb: 2, mt: 2 }}
                        fullWidth
                        onClick={handleResetFilter}
                      >
                        Сбросить фильтр
                      </Button>
                    </Grid>
                  </Grid>
                </Paper>
                <Paper variant="outlined" sx={{ p: 2, margin: '5px 0 5px' }}>
                  <OrderListAdmin
                    isLoading={appState.loading}
                    items={data}
                    onChangeStatus={handleChangeStatus}
                    onDelete={handleDelete}
                  />
                </Paper>
                <Paper
                  elevation={0}
                  sx={{
                    display: 'flex',
                    p: 2,
                    margin: '5px 0 5px'
                  }}
                >
                  <Paper
                    elevation={0}
                    sx={{
                      display: 'flex',
                      p: 2,
                      marginTop: '5px',
                      marginBottom: '5px',
                      marginLeft: 'auto',
                      marginRight: 'auto'
                    }}
                  >
                    <Pagination
                      count={totalPages}
                      color="primary"
                      boundaryCount={2}
                      showFirstButton
                      showLastButton
                      sx={{ ml: 'auto', mr: 'auto', mb: 2 }}
                      size="large"
                      page={filter.pageNumber}
                      onChange={(_, value) =>
                        handleChangeFilter('pageNumber')(value)
                      }
                    />
                  </Paper>
                </Paper>
              </Grid>
            </Grid>
          ) : (
            <Typography
              gutterBottom
              variant="h3"
              component="div"
              sx={{ display: 'flex', p: 2, margin: '20px' }}
            >
              Заказов нет.
            </Typography>
          )}
        </Box>
      </Paper>
    </Container>
  )
}

export default Orders
