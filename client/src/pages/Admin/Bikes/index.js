import * as React from 'react'
import { Link } from 'react-router-dom'
import {
  Box,
  Button,
  Checkbox,
  Container,
  Divider,
  FormControl,
  FormHelperText,
  Grid,
  IconButton,
  InputBase,
  MenuItem,
  Pagination,
  Paper,
  Select,
  Stack,
  Typography
} from '@mui/material'
import { styled, alpha } from '@mui/material/styles'
import { Icon } from '@iconify/react'
import SearchIcon from '@mui/icons-material/Search'
import ListLoading from '../../../components/ListLoading'
import { getAllBycicles } from '../../../http/productAPI'
import BicycleListAdmin from '../../../components/Admin/Bicycle/BicycleListAdmin'

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  '&:hover': {
    backgroundColor: alpha(theme.palette.common.white, 0.25)
  },
  marginLeft: 0,
  width: '100%'
}))

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center'
}))

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%'
  }
}))

const DashboardBikes = () => {
  const [filter, setFilter] = React.useState({
    minPrice: '',
    maxPrice: '',
    minCommonDate: '',
    maxCommonDate: '',
    minAbsorberStroke: '',
    maxAbsorberStroke: '',
    minSpeedNumber: '',
    maxSpeedNumber: '',
    minFrontStar: '',
    maxFrontStar: '',
    minRearStar: '',
    maxRearStar: '',
    minWeight: '',
    maxWeight: '',
    minWheelDiameter: '',
    maxWheelDiameter: '',
    absorberStroke: '',
    cassette: '',
    chain: '',
    classOfBike: '',
    commonDate: '',
    forkMaterial: '',
    forkModel: '',
    forkType: '',
    frameColor: '',
    frameMaterial: '',
    frameSize: '',
    frontBrakeModel: '',
    frontBrakeType: '',
    frontStar: '',
    frontSwitch: '',
    gripsModel: '',
    handlebarModel: '',
    handlebarType: '',
    manufacturer: '',
    pedalsMaterial: '',
    pedalsModel: '',
    pedalsType: '',
    rearBrakeModel: '',
    rearBrakeType: '',
    rearStar: '',
    rearSwitch: '',
    rimMaterial: '',
    saddleModel: '',
    saddleType: '',
    shifterModel: '',
    shifterType: '',
    speedNumber: '',
    systemModel: '',
    tiresModel: '',
    wheelDiameter: '',
    hardTail: '',
    female: '',
    kidTeen: '',
    doubleRim: '',
    tiresTubeless: '',
    forkLock: '',
    orderBy: 'Manufacturer',
    orderByThen: 'Name',
    searchName: '',
    isAvailable: false,
    pageNumber: 1,
    pageSize: 25
  })
  const [initialFilter, setInitialFilter] = React.useState([])

  React.useEffect(() => {
    document.title = 'Администратор: каталог велоспипедов'
    setInitialFilter(filter)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [initialFilter])

  const handleChangeFilter = (prop) => (value) => {
    setFilter({ ...filter, [prop]: value })
  }
  const setParameters = (prop) => (event) => {
    setFilter({ ...filter, [prop]: event.target.value })
  }

  const handleChangeChecked = (prop) => (event) => {
    setFilter({ ...filter, [prop]: event.target.checked })
  }

  const [appState, setAppState] = React.useState({
    loading: false,
    items: null
  })

  const handleResetFilter = () => {
    setFilter(initialFilter)
    setSearchValue('')
  }

  const [searchValue, setSearchValue] = React.useState('')

  const handleSearchValue = (event) => {
    setSearchValue(event.target.value)
  }
  const handleSearch = () => {
    handleChangeFilter('searchName')(searchValue)
  }

  const clearSearchValue = () => {
    handleChangeFilter('searchName')('')
    setSearchValue('')
  }

  const LoadingList = ListLoading(BicycleListAdmin)

  const handleDelete = async () => {
    try {
      getBikes()
    } catch (error) {
      console.log(error)
    }
  }

  const [totalPages, setTotalPages] = React.useState(0)
  const [recordsFiltered, setRecordsFiltered] = React.useState(0)
  const [totalRecords, setTotalRecords] = React.useState(0)

  const getBikes = async () => {
    setAppState({ loading: true })
    await getAllBycicles(
      filter.minPrice,
      filter.maxPrice,
      filter.minCommonDate,
      filter.maxCommonDate,
      filter.minAbsorberStroke,
      filter.maxAbsorberStroke,
      filter.minSpeedNumber,
      filter.maxSpeedNumber,
      filter.minFrontStar,
      filter.maxFrontStar,
      filter.minRearStar,
      filter.maxRearStar,
      filter.minWeight,
      filter.maxWeight,
      filter.minWheelDiameter,
      filter.maxWheelDiameter,
      filter.cassette,
      filter.chain,
      filter.classOfBike,
      filter.forkMaterial,
      filter.forkModel,
      filter.forkType,
      filter.frameColor,
      filter.frameMaterial,
      filter.frameSize,
      filter.frontBrakeModel,
      filter.frontBrakeType,
      filter.frontSwitch,
      filter.gripsModel,
      filter.handlebarModel,
      filter.handlebarType,
      filter.manufacturer,
      filter.pedalsMaterial,
      filter.pedalsModel,
      filter.pedalsType,
      filter.rearBrakeModel,
      filter.rearBrakeType,
      filter.rearSwitch,
      filter.rimMaterial,
      filter.saddleModel,
      filter.saddleType,
      filter.shifterModel,
      filter.shifterType,
      filter.systemModel,
      filter.tiresModel,
      filter.hardTail,
      filter.female,
      filter.kidTeen,
      filter.doubleRim,
      filter.tiresTubeless,
      filter.forkLock,
      filter.orderBy,
      filter.orderByThen,
      filter.searchName,
      filter.isAvailable,
      filter.pageNumber,
      filter.pageSize
    ).then((response) => {
      setTotalPages(response.totalPages)
      setTotalRecords(response.totalRecords)
      setRecordsFiltered(response.recordsFiltered)
      setAppState({ loading: false, items: response.data })
    })
  }

  React.useEffect(() => {
    try {
      getBikes()
    } catch (error) {
      console.log(error)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter])

  return (
    <Container
      component="main"
      maxWidth="lg"
      sx={{ p: 2, margin: 'auto', flexGrow: 1, minWidth: 400 }}
    >
      <Paper variant="outlined" sx={{ p: 2, margin: '5px 0 5px', flexGrow: 1 }}>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            marginBottom: 2
          }}
        >
          <Typography variant="h3" component="div" gutterBottom>
            Каталог велосипедов
          </Typography>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <Paper
                variant="outlined"
                sx={{ p: 2, margin: '5px 0 5px' }}
                position="static"
              >
                <Search>
                  <Stack direction="row" alignItems="center" spacing={2}>
                    <SearchIconWrapper>
                      <SearchIcon color="primary" size="large" />
                    </SearchIconWrapper>
                    <StyledInputBase
                      value={searchValue}
                      onChange={handleSearchValue}
                      fullWidth
                      placeholder="Поиск…"
                      inputProps={{ 'aria-label': 'search' }}
                    />
                    {searchValue && (
                      <IconButton onClick={clearSearchValue}>
                        <Icon icon="akar-icons:cross" />
                      </IconButton>
                    )}
                    <Button
                      variant="contained"
                      endIcon={<SearchIcon color="white" size="large" />}
                      onClick={handleSearch}
                    >
                      Найти
                    </Button>
                    <Divider orientation="vertical" flexItem />
                    <Link to="/dashboard/bikes/create">
                      <Button
                        variant="contained"
                        endIcon={<Icon icon="gridicons:create" color="white" />}
                      >
                        Новый
                      </Button>
                    </Link>
                  </Stack>
                </Search>
              </Paper>
              <Paper variant="outlined" sx={{ p: 2, margin: '5px 0 5px' }}>
                <Grid
                  container
                  spacing={2}
                  alignItems="center"
                  justifyContent="center"
                >
                  <Grid item xs={4}>
                    <FormControl sx={{ ml: 'auto', mr: 'auto' }} fullWidth>
                      <FormHelperText>Сортировка:</FormHelperText>
                      <Select
                        value={filter.orderBy}
                        onChange={setParameters('orderBy')}
                      >
                        <MenuItem value={'Manufacturer'}>
                          Название (по возрастанию)
                        </MenuItem>
                        <MenuItem value={'Manufacturer desc'}>
                          Название (по убыванию)
                        </MenuItem>
                        <MenuItem value={'Price'}>
                          Цена (по возрастанию)
                        </MenuItem>
                        <MenuItem value={'Price desc'}>
                          Цена (по убыванию)
                        </MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={2}>
                    <FormControl sx={{ ml: 'auto', mr: 'auto' }}>
                      <FormHelperText>Показывать:</FormHelperText>
                      <Select
                        value={filter.pageSize}
                        onChange={setParameters('pageSize')}
                      >
                        <MenuItem value={25}>25 на странице</MenuItem>
                        <MenuItem value={50}>50 на странице</MenuItem>
                        <MenuItem value={75}>75 на странице</MenuItem>
                        <MenuItem value={100}>100 на странице</MenuItem>
                        <MenuItem value={recordsFiltered}>Все</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={2}>
                    Только доступные
                    <Checkbox
                      checked={filter.isAvailable}
                      onChange={handleChangeChecked('isAvailable')}
                      inputProps={{ 'aria-label': 'controlled' }}
                    />
                  </Grid>
                  <Grid item xs={2}>
                    <Typography>
                      Выбрано {recordsFiltered} из {totalRecords}
                    </Typography>
                  </Grid>
                  <Grid item xs={2}>
                    <Button
                      variant="contained"
                      sx={{ mb: 2, mt: 2 }}
                      fullWidth
                      onClick={handleResetFilter}
                    >
                      Сбросить фильтр
                    </Button>
                  </Grid>
                </Grid>
              </Paper>
              <LoadingList
                isLoading={appState.loading}
                items={appState.items}
                onDelete={handleDelete}
              />
              <Paper
                elevation={0}
                sx={{
                  display: 'flex',
                  p: 2,
                  margin: '5px 0 5px'
                }}
              >
                <Paper
                  elevation={0}
                  sx={{
                    display: 'flex',
                    p: 2,
                    marginTop: '5px',
                    marginBottom: '5px',
                    marginLeft: 'auto',
                    marginRight: 'auto'
                  }}
                >
                  <Pagination
                    count={totalPages}
                    color="primary"
                    boundaryCount={2}
                    showFirstButton
                    showLastButton
                    sx={{ ml: 'auto', mr: 'auto', mb: 2 }}
                    size="large"
                    page={filter.pageNumber}
                    onChange={(_, value) =>
                      handleChangeFilter('pageNumber')(value)
                    }
                  />
                </Paper>
              </Paper>
            </Grid>
          </Grid>
        </Box>
      </Paper>
    </Container>
  )
}

export default DashboardBikes
