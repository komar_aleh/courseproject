import * as React from 'react'
import { IMG_MD } from '../../../../config'
import {
  Button,
  Checkbox,
  Container,
  MenuItem,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField
} from '@mui/material'

import { createBycicle } from '../../../../http/productAPI'
import { getAllProperties } from '../../../../http/propertyAPI'
import { styled } from '@mui/material/styles'
import Spinner from '../../../../components/Spinner'
import ImageUpload from '../../../../components/ImageUpload'
import ErrorSuccess from '../../../../components/ErrorSucces'

const Img = styled('img')({
  margin: 'auto',
  display: 'block',
  maxWidth: '100%',
  maxHeight: '100%'
})

const ItemEdit = styled(TextField)(({ theme }) => ({
  ...theme.typography.body1,
  padding: theme.spacing(0),
  textAlign: 'center',
  fontSize: '1.3rem',
  color: theme.palette.text.primary,
  paddingLeft: theme.spacing(4)
}))

const BikeCreate = () => {
  const [absorberStroke, setAbsorberStroke] = React.useState(null)
  const [cassette, setCassette] = React.useState(null)
  const [chain, setChain] = React.useState(null)
  const [classOfBike, setClassOfBike] = React.useState(null)
  const [commonDate, setCommonDate] = React.useState(null)
  const [forkMaterial, setForkMaterial] = React.useState(null)
  const [forkModel, setForkModel] = React.useState(null)
  const [forkType, setForkType] = React.useState(null)
  const [frameColor, setFrameColor] = React.useState(null)
  const [frameMaterial, setFrameMaterial] = React.useState(null)
  const [frameSize, setFrameSize] = React.useState(null)
  const [frontBrakeModel, setFrontBrakeModel] = React.useState(null)
  const [frontBrakeType, setFrontBrakeType] = React.useState(null)
  const [frontStar, setFrontStar] = React.useState(null)
  const [frontSwitch, setFrontSwitch] = React.useState(null)
  const [gripsModel, setGripsModel] = React.useState(null)
  const [handlebarModel, setHandlebarModel] = React.useState(null)
  const [handlebarType, setHandlebarType] = React.useState(null)
  const [manufacturer, setManufacturer] = React.useState(null)
  const [pedalsMaterial, setPedalsMaterial] = React.useState(null)
  const [pedalsModel, setPedalsModel] = React.useState(null)
  const [pedalsType, setPedalsType] = React.useState(null)
  const [rearBrakeModel, setRearBrakeModel] = React.useState(null)
  const [rearBrakeType, setRearBrakeType] = React.useState(null)
  const [rearStar, setRearStar] = React.useState(null)
  const [rearSwitch, setRearSwitch] = React.useState(null)
  const [rimMaterial, setRimMaterial] = React.useState(null)
  const [saddleModel, setSaddleModel] = React.useState(null)
  const [saddleType, setSaddleType] = React.useState(null)
  const [shifterModel, setShifterModel] = React.useState(null)
  const [shifterType, setShifterType] = React.useState(null)
  const [speedNumber, setSpeedNumber] = React.useState(null)
  const [systemModel, setSystemModel] = React.useState(null)
  const [tiresModel, setTiresModel] = React.useState(null)
  const [wheelDiameter, setWheelDiameter] = React.useState(null)
  const [hardTail, setHardTail] = React.useState(false)
  const [female, setFemale] = React.useState(false)
  const [kidTeen, setKidTeen] = React.useState(false)
  const [doubleRim, setDoubleRim] = React.useState(false)
  const [tiresTubeless, setTiresTubeless] = React.useState(false)
  const [forkLock, setForkLock] = React.useState(false)
  const [imgName, setImgName] = React.useState('')
  const [name, setName] = React.useState('')
  const [price, setPrice] = React.useState(0)
  const [weight, setWeight] = React.useState(0)
  const [quantity, setQuantity] = React.useState(0)
  const [isAvailable, setIsAvailable] = React.useState(false)
  const [isLoading, setIsLoading] = React.useState(false)

  const handleChangeName = (event) => {
    setName(event.target.value)
  }
  const handleChangePrice = (event) => {
    setPrice(event.target.value)
  }
  const handleChangeQuantity = (event) => {
    setQuantity(event.target.value)
  }
  const handleChangeWeight = (event) => {
    setWeight(event.target.value)
  }
  const handleChangeIsAvailable = (event) => {
    setIsAvailable(event.target.checked)
  }
  const handleChangeManufacturer = (event) => {
    setManufacturer(event.target.value)
  }
  const handleChangeClassOfBike = (event) => {
    setClassOfBike(event.target.value)
  }
  const handleChangeFrameMaterial = (event) => {
    setFrameMaterial(event.target.value)
  }
  const handleChangeFrameSize = (event) => {
    setFrameSize(event.target.value)
  }
  const handleChangeFrameColor = (event) => {
    setFrameColor(event.target.value)
  }
  const handleChangeAbsorberStroke = (event) => {
    setAbsorberStroke(event.target.value)
  }
  const handleChangeCassette = (event) => {
    setCassette(event.target.value)
  }
  const handleChangeCommonDate = (event) => {
    setCommonDate(event.target.value)
  }
  const handleChangeChain = (event) => {
    setChain(event.target.value)
  }
  const handleChangeForkMaterial = (event) => {
    setForkMaterial(event.target.value)
  }
  const handleChangeForkModel = (event) => {
    setForkModel(event.target.value)
  }
  const handleChangeForkType = (event) => {
    setForkType(event.target.value)
  }
  const handleChangeFrontBrakeModel = (event) => {
    setFrontBrakeModel(event.target.value)
  }
  const handleChangeFrontBrakeType = (event) => {
    setFrontBrakeType(event.target.value)
  }
  const handleChangeFrontStar = (event) => {
    setFrontStar(event.target.value)
  }
  const handleChangeFrontSwitch = (event) => {
    setFrontSwitch(event.target.value)
  }
  const handleChangeGripsModel = (event) => {
    setGripsModel(event.target.value)
  }
  const handleChangeHandleBarModel = (event) => {
    setHandlebarModel(event.target.value)
  }
  const handleChangeHandleBarType = (event) => {
    setHandlebarType(event.target.value)
  }
  const handleChangePedalsMaterial = (event) => {
    setPedalsMaterial(event.target.value)
  }
  const handleChangePedalsModel = (event) => {
    setPedalsModel(event.target.value)
  }
  const handleChangePedalsType = (event) => {
    setPedalsType(event.target.value)
  }
  const handleChangeRearBrakeModel = (event) => {
    setRearBrakeModel(event.target.value)
  }
  const handleChangeRearBrakeType = (event) => {
    setRearBrakeType(event.target.value)
  }
  const handleChangeRearStar = (event) => {
    setRearStar(event.target.value)
  }
  const handleChangeRearSwitch = (event) => {
    setRearSwitch(event.target.value)
  }
  const handleChangeRimMaterial = (event) => {
    setRimMaterial(event.target.value)
  }
  const handleChangeSaddleModel = (event) => {
    setSaddleModel(event.target.value)
  }
  const handleChangeSaddleType = (event) => {
    setSaddleType(event.target.value)
  }
  const handleChangeShifterModel = (event) => {
    setShifterModel(event.target.value)
  }
  const handleChangeShifterType = (event) => {
    setShifterType(event.target.value)
  }
  const handleChangeSystemModel = (event) => {
    setSystemModel(event.target.value)
  }
  const handleChangeTiresModel = (event) => {
    setTiresModel(event.target.value)
  }
  const handleChangeWheelDiameter = (event) => {
    setWheelDiameter(event.target.value)
  }
  const handleChangeSpeedNumber = (event) => {
    setSpeedNumber(event.target.value)
  }
  const handleChangeFemale = (event) => {
    setFemale(event.target.checked)
  }
  const handleChangeKidTeen = (event) => {
    setKidTeen(event.target.checked)
  }
  const handleChangeHardTail = (event) => {
    setHardTail(event.target.checked)
  }
  const handleChangeDoubleRim = (event) => {
    setDoubleRim(event.target.checked)
  }
  const handleChangeTiresTubeless = (event) => {
    setTiresTubeless(event.target.checked)
  }
  const handleChangeForkLock = (event) => {
    setForkLock(event.target.checked)
  }

  const handleDeleteImg = () => {
    setImgName('')
  }

  const handleSave = async () => {
    try {
      const { message } = await createBycicle(
        name,
        quantity,
        price,
        manufacturer,
        isAvailable,
        classOfBike,
        commonDate,
        imgName,
        weight,
        hardTail,
        female,
        kidTeen,
        doubleRim,
        tiresTubeless,
        forkLock,
        frameMaterial,
        frameSize,
        forkMaterial,
        wheelDiameter,
        frameColor,
        forkModel,
        forkType,
        absorberStroke,
        speedNumber,
        systemModel,
        frontStar,
        rearStar,
        cassette,
        frontSwitch,
        rearSwitch,
        chain,
        shifterModel,
        shifterType,
        frontBrakeModel,
        frontBrakeType,
        rearBrakeModel,
        rearBrakeType,
        rimMaterial,
        tiresModel,
        handlebarModel,
        handlebarType,
        gripsModel,
        saddleType,
        saddleModel,
        pedalsType,
        pedalsModel,
        pedalsMaterial
      )
      setMessageSuccess(message)
      setOpenSuccess(true)
    } catch (error) {
      const erdm = error.response.data.message
        ? error.response.data.message
        : ''
      const erde = error.response.data.errors ? error.response.data.errors : ''
      const em = error.message ? error.message : ''
      setMessageError(erdm, erde, em)
      setOpenError(true)
    }
  }

  const [manufacturers, setManufacturers] = React.useState([])
  const [classOfBikes, setClassOfBikes] = React.useState([])
  const [frameMaterials, setFrameMaterials] = React.useState([])
  const [frameSizes, setFrameSizes] = React.useState([])
  const [frameColors, setFrameColors] = React.useState([])
  const [absorberStrokes, setAbsorberStrokes] = React.useState([''])
  const [cassettes, setCassettes] = React.useState([])
  const [chains, setChains] = React.useState([])
  const [commonDates, setCommonDates] = React.useState([])
  const [forkMaterials, setForkMaterials] = React.useState([])
  const [forkModels, setForkModels] = React.useState([])
  const [forkTypes, setForkTypes] = React.useState([])
  const [frontBrakeModels, setFrontBrakeModels] = React.useState([])
  const [frontBrakeTypes, setFrontBrakeTypes] = React.useState([])
  const [frontStars, setFrontStars] = React.useState([])
  const [frontSwitches, setFrontSwitches] = React.useState([])
  const [gripsModels, setGripsModels] = React.useState([])
  const [handleBarModels, setHandleBarModels] = React.useState([])
  const [handleBarTypes, setHandleBarTypes] = React.useState([])
  const [pedalsMaterials, setPedalsMaterials] = React.useState([])
  const [pedalsModels, setPedalsModels] = React.useState([])
  const [pedalsTypes, setPedalsTypes] = React.useState([])
  const [rearBrakeModels, setRearBrakeModels] = React.useState([])
  const [rearBrakeTypes, setRearBrakeTypes] = React.useState([])
  const [rearStars, setRearStars] = React.useState([])
  const [rearSwitches, setRearSwitches] = React.useState([])
  const [rimMaterials, setRimMaterials] = React.useState([])
  const [saddleModels, setSaddleModels] = React.useState([])
  const [saddleTypes, setSaddleTypes] = React.useState([])
  const [shifterModels, setShifterModels] = React.useState([])
  const [shifterTypes, setShifterTypes] = React.useState([])
  const [speedNumbers, setSpeedNumbers] = React.useState([])
  const [systemModels, setSystemModels] = React.useState([])
  const [tiresModels, setTiresModels] = React.useState([])
  const [wheelDiameters, setWheelDiameters] = React.useState([])

  const getProperties = async () => {
    setIsLoading(true)
    await getAllProperties().then((response) => {
      setManufacturers(response.manufacturers)
      setClassOfBikes(response.classOfBikes)
      setFrameMaterials(response.frameMaterials)
      setFrameSizes(response.frameSizes)
      setFrameColors(response.frameColors)
      setAbsorberStrokes(response.absorberStrokes)
      setCassettes(response.cassettes)
      setChains(response.chains)
      setCommonDates(response.commonDates)
      setForkMaterials(response.forkMaterials)
      setForkModels(response.forkModels)
      setForkTypes(response.forkTypes)
      setFrontBrakeModels(response.frontBrakeModels)
      setFrontBrakeTypes(response.frontBrakeTypes)
      setFrontStars(response.frontStars)
      setFrontSwitches(response.frontSwitches)
      setGripsModels(response.gripsModels)
      setHandleBarModels(response.handleBarModels)
      setHandleBarTypes(response.handleBarTypes)
      setPedalsMaterials(response.pedalsMaterials)
      setPedalsModels(response.pedalsModels)
      setPedalsTypes(response.pedalsTypes)
      setRearBrakeModels(response.rearBrakeModels)
      setRearBrakeTypes(response.rearBrakeTypes)
      setRearStars(response.rearStars)
      setRearSwitches(response.rearSwitches)
      setRimMaterials(response.rimMaterials)
      setSaddleModels(response.saddleModels)
      setSaddleTypes(response.saddleTypes)
      setShifterModels(response.shifterModels)
      setShifterTypes(response.shifterTypes)
      setSpeedNumbers(response.speedNumbers)
      setSystemModels(response.systemModels)
      setTiresModels(response.tiresModels)
      setWheelDiameters(response.wheelDiameters)
      setIsLoading(false)
    })
  }

  const [openError, setOpenError] = React.useState(false)
  const [openSuccess, setOpenSuccess] = React.useState(false)
  const [messageError, setMessageError] = React.useState('')
  const [messageSuccess, setMessageSuccess] = React.useState('')

  React.useEffect(() => {
    try {
      getProperties()
    } catch (error) {
      console.log(error)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [openError])

  if (isLoading) return <Spinner />

  return (
    <Container
      component="main"
      maxWidth="lg"
      sx={{ p: 2, margin: 'auto', flexGrow: 1, minWidth: 400 }}
    >
      <ErrorSuccess
        openE={openError}
        messageE={messageError}
        closeE={() => {
          setOpenError(false)
        }}
        openS={openSuccess}
        messageS={messageSuccess}
        closeS={() => {
          setOpenSuccess(false)
        }}
      />
      <Paper variant="outlined" sx={{ p: 3, margin: '5px 0 5px', flexGrow: 1 }}>
        <TableContainer sx={{ minWidth: 800 }}>
          <Table>
            <TableBody>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', minHeight: 480 }}
                  align="left"
                  style={{ width: '50%' }}
                >
                  {imgName && <Img alt="photo" src={`${IMG_MD}${imgName}`} />}
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem' }}
                  align="center"
                  style={{ width: '50%' }}
                >
                  <ImageUpload onUpload={(e) => setImgName(e)} />
                  <Button
                    variant="outlined"
                    color="primary"
                    sx={{ m: 3 }}
                    onClick={handleDeleteImg}
                  >
                    Удалить изображение
                  </Button>
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </TableContainer>
        <Button
          variant="contained"
          color="primary"
          onClick={handleSave}
          sx={{
            m: 5,
            p: 2,
            fontSize: '1.5rem'
          }}
          //startIcon={<ShoppingCartRoundedIcon />}
        >
          Сохранить
        </Button>
        <TableContainer sx={{ minWidth: 800 }}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Название
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  Значение параметра
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Производитель
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={manufacturer ? manufacturer : ``}
                    select
                    onChange={handleChangeManufacturer}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {manufacturers.map((item, index) => (
                      <MenuItem key={index} value={`${item.id}`}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Название
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <ItemEdit
                    variant="outlined"
                    value={name ? name : ''}
                    fullWidth
                    onChange={handleChangeName}
                  />
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Доступен для продажи
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <Checkbox
                    checked={isAvailable}
                    onChange={handleChangeIsAvailable}
                    inputProps={{ 'aria-label': 'controlled' }}
                  />
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Цена
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <ItemEdit
                    variant="outlined"
                    value={price}
                    fullWidth
                    onChange={handleChangePrice}
                  />
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Количество
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <ItemEdit
                    variant="outlined"
                    value={quantity}
                    fullWidth
                    onChange={handleChangeQuantity}
                  />
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Дата выхода на рынок
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={commonDate ? commonDate : ''}
                    select
                    onChange={handleChangeCommonDate}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {commonDates.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Класс
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={classOfBike ? classOfBike : ''}
                    select
                    onChange={handleChangeClassOfBike}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {classOfBikes.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Материал рамы
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={frameMaterial ? frameMaterial : ''}
                    select
                    onChange={handleChangeFrameMaterial}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {frameMaterials.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Размер рамы
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={frameSize ? frameSize : ''}
                    select
                    onChange={handleChangeFrameSize}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {frameSizes.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Цвет рамы
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={frameColor ? frameColor : ''}
                    select
                    onChange={handleChangeFrameColor}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {frameColors.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Модель вилки
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={forkModel ? forkModel : ''}
                    select
                    onChange={handleChangeForkModel}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {forkModels.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Материал вилки
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={forkMaterial ? forkMaterial : ''}
                    select
                    onChange={handleChangeForkMaterial}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {forkMaterials.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Тип вилки
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={forkType ? forkType : ''}
                    select
                    onChange={handleChangeForkType}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {forkTypes.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Ход амортизатора
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={absorberStroke ? absorberStroke : ''}
                    select
                    onChange={handleChangeAbsorberStroke}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {absorberStrokes.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Количество скоростей
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={speedNumber ? speedNumber : ''}
                    select
                    onChange={handleChangeSpeedNumber}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {speedNumbers.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Система шатунов
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={systemModel ? systemModel : ''}
                    select
                    onChange={handleChangeSystemModel}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {systemModels.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Количество звезд в системе
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={frontStar ? frontStar : ''}
                    select
                    onChange={handleChangeFrontStar}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {frontStars.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Модель кассеты или трещотки
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={cassette ? cassette : ''}
                    select
                    onChange={handleChangeCassette}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {cassettes.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Количество звезд в кассете
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={rearStar ? rearStar : ''}
                    select
                    onChange={handleChangeRearStar}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {rearStars.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Передний переключатель
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={frontSwitch ? frontSwitch : ''}
                    select
                    onChange={handleChangeFrontSwitch}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {frontSwitches.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Задний переключатель
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={rearSwitch ? rearSwitch : ''}
                    select
                    onChange={handleChangeRearSwitch}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {rearSwitches.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Цепь
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={chain ? chain : ''}
                    select
                    onChange={handleChangeChain}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {chains.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Тип манеток
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={shifterType ? shifterType : ''}
                    select
                    onChange={handleChangeShifterType}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {shifterTypes.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Модель манеток
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={shifterModel ? shifterModel : ''}
                    select
                    onChange={handleChangeShifterModel}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {shifterModels.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Тип переднего тормоза
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={frontBrakeType ? frontBrakeType : ''}
                    select
                    onChange={handleChangeFrontBrakeType}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {frontBrakeTypes.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Модель переднего тормоза
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={frontBrakeModel ? frontBrakeModel : ''}
                    select
                    onChange={handleChangeFrontBrakeModel}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {frontBrakeModels.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Тип заднего тормоза
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={rearBrakeType ? rearBrakeType : ''}
                    select
                    onChange={handleChangeRearBrakeType}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {rearBrakeTypes.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Модель заднего тормоза
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={rearBrakeModel ? rearBrakeModel : ''}
                    select
                    onChange={handleChangeRearBrakeModel}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {rearBrakeModels.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Диаметр колес
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={wheelDiameter ? wheelDiameter : ''}
                    select
                    onChange={handleChangeWheelDiameter}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {wheelDiameters.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Материал ободьев
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={rimMaterial ? rimMaterial : ''}
                    select
                    onChange={handleChangeRimMaterial}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {rimMaterials.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Модель покрышек
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={tiresModel ? tiresModel : ''}
                    select
                    onChange={handleChangeTiresModel}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {tiresModels.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Модель руля
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={handlebarModel ? handlebarModel : ''}
                    select
                    onChange={handleChangeHandleBarModel}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {handleBarModels.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Тип руля
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={handlebarType ? handlebarType : ''}
                    select
                    onChange={handleChangeHandleBarType}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {handleBarTypes.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Грипсы
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={gripsModel ? gripsModel : ''}
                    select
                    onChange={handleChangeGripsModel}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {gripsModels.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Модель седла
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={saddleModel ? saddleModel : ''}
                    select
                    onChange={handleChangeSaddleModel}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {saddleModels.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Тип седла
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={saddleType ? saddleType : ''}
                    select
                    onChange={handleChangeSaddleType}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {saddleTypes.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Модель педалей
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={pedalsModel ? pedalsModel : ''}
                    select
                    onChange={handleChangePedalsModel}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {pedalsModels.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Материал педалей
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={pedalsMaterial ? pedalsMaterial : ''}
                    select
                    onChange={handleChangePedalsMaterial}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {pedalsMaterials.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Тип педалей
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <TextField
                    sx={{ mt: 2, paddingLeft: 4 }}
                    value={pedalsType ? pedalsType : ''}
                    select
                    onChange={handleChangePedalsType}
                    fullWidth
                    size="small"
                  >
                    <MenuItem value="">
                      <em>Не указано</em>
                    </MenuItem>
                    {pedalsTypes.map((item, index) => (
                      <MenuItem key={index} value={item.id}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </TextField>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Вес
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <ItemEdit
                    variant="outlined"
                    value={weight}
                    fullWidth
                    onChange={handleChangeWeight}
                  />
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Женский
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <Checkbox
                    checked={female}
                    onChange={handleChangeFemale}
                    inputProps={{ 'aria-label': 'controlled' }}
                  />
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Подростковый
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <Checkbox
                    checked={kidTeen}
                    onChange={handleChangeKidTeen}
                    inputProps={{ 'aria-label': 'controlled' }}
                  />
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Двойные обода
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <Checkbox
                    checked={doubleRim}
                    onChange={handleChangeDoubleRim}
                    inputProps={{ 'aria-label': 'controlled' }}
                  />
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Задний амортизатор
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <Checkbox
                    checked={hardTail}
                    onChange={handleChangeHardTail}
                    inputProps={{ 'aria-label': 'controlled' }}
                  />
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Блокировка вилки
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <Checkbox
                    checked={forkLock}
                    onChange={handleChangeForkLock}
                    inputProps={{ 'aria-label': 'controlled' }}
                  />
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell sx={{ fontSize: '1.3rem' }} align="left">
                  Бескамерные покрышки
                </TableCell>
                <TableCell sx={{ fontSize: '1.3rem' }} align="center">
                  <Checkbox
                    checked={tiresTubeless}
                    onChange={handleChangeTiresTubeless}
                    inputProps={{ 'aria-label': 'controlled' }}
                  />
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
    </Container>
  )
}

export default BikeCreate
