import * as React from 'react'
import {
  Box,
  Container,
  CssBaseline,
  Grid,
  Paper,
  Typography
} from '@mui/material'
import { Link } from 'react-router-dom'
import { styled } from '@mui/material/styles'

const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  display: 'flex',
  padding: theme.spacing(1),
  //textAlign: 'center',z
  minWidth: 200,
  minHeight: 200,
  alignItems: 'center',
  justifyContent: 'center',
  fontSize: '2rem',
  color: theme.palette.text.secondary
}))

const Dashboard = () => {
  React.useEffect(() => {
    document.title = 'Панель администратора'
  })
  return (
    <Container component="main" maxWidth="lg">
      <CssBaseline />

      <Box
        sx={{
          width: '100%',
          mt: 3,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center'
        }}
      >
        <Typography variant="h3" component="div" gutterBottom>
          Панель администратора
        </Typography>
        <Grid
          container
          rowSpacing={3}
          columnSpacing={{ xs: 1, sm: 2, md: 3 }}
          sx={{ mt: 3 }}
        >
          <Grid item xs={6}>
            <Link to="/dashboard/orders">
              <Item elevation={6}>Заказы</Item>
            </Link>
          </Grid>
          <Grid item xs={6}>
            <Link to="/dashboard/bikes">
              <Item elevation={6}>Велосипеды</Item>
            </Link>
          </Grid>
          <Grid item xs={6}>
            <Link to="/dashboard/users">
              <Item elevation={6}>Пользователи</Item>
            </Link>
          </Grid>
          <Grid item xs={6}>
            <Link to="/dashboard/properties">
              <Item elevation={6}>Свойства</Item>
            </Link>
          </Grid>
        </Grid>
      </Box>
    </Container>
  )
}
export default Dashboard
