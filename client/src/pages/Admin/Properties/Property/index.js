import * as React from 'react'
import {
  Autocomplete,
  Box,
  Button,
  Container,
  FormControl,
  FormHelperText,
  Grid,
  MenuItem,
  Pagination,
  Paper,
  Select,
  Stack,
  TextField,
  Typography
} from '@mui/material'
import PropertyList from '../../../../components/Admin/Property/PropertyList'
import { createItem, getAll, getAllItems } from '../../../../http/propertyAPI'
import Spinner from '../../../../components/Spinner'

const Property = (props) => {
  const [data, setData] = React.useState([])
  const [totalPages, setTotalPages] = React.useState(0)
  const [recordsFiltered, setRecordsFiltered] = React.useState(0)
  const [totalRecords, setTotalRecords] = React.useState(0)
  const [filter, setFilter] = React.useState({
    value: '',
    orderBy: 'Value',
    pageNumber: 1,
    pageSize: 10
  })

  const [initialFilter, setInitialFilter] = React.useState([])

  const handleChangeFilter = (prop) => (value) => {
    setFilter({ ...filter, [prop]: value })
  }
  const setParameters = (prop) => (event) => {
    setFilter({ ...filter, [prop]: event.target.value })
  }

  const [values, setValues] = React.useState([])
  const [newValue, setNewValue] = React.useState('')
  const onChange = (e) => {
    setNewValue(e.target.value)
  }

  const [appState, setAppState] = React.useState({
    loading: false
  })

  const getAllProps = async () => {
    setAppState({ loading: true })
    await getAll(
      props.control,
      filter.value,
      filter.orderBy,
      filter.pageNumber,
      filter.pageSize
    ).then((response) => {
      setData(response.data)
      setTotalPages(response.totalPages)
      setTotalRecords(response.totalRecords)
      setRecordsFiltered(response.recordsFiltered)
      setAppState({ loading: false })
    })
  }

  const getProperties = async () => {
    await getAllItems(props.control).then((response) => {
      setValues(response)
    })
  }

  const createNewValue = async () => {
    await createItem(props.control, newValue).then(() => {
      getAllProps()
      setNewValue('')
    })
  }
  React.useEffect(() => {
    document.title = `Администратор: ${props.title}`
    setInitialFilter(filter)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [initialFilter])

  React.useEffect(() => {
    getProperties()
    getAllProps()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter])

  const handleResetFilter = () => {
    setFilter(initialFilter)
  }

  const handleDelete = async () => {
    try {
      getAllProps()
    } catch (error) {
      console.log(error)
    }
  }

  return appState.loading ? (
    <Spinner />
  ) : (
    <Container
      component="main"
      maxWidth="lg"
      sx={{ p: 2, margin: 'auto', flexGrow: 1, minWidth: 400 }}
    >
      <Paper variant="outlined" sx={{ p: 2, margin: '5px 0 5px', flexGrow: 1 }}>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            marginBottom: 2
          }}
        >
          <Typography variant="h3" component="div" gutterBottom>
            {props.title}
          </Typography>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <Paper variant="outlined" sx={{ p: 2, margin: '5px 0 5px' }}>
                <Grid
                  container
                  spacing={2}
                  alignItems="center"
                  justifyContent="center"
                >
                  <Grid item xs={4}>
                    <FormControl sx={{ ml: 'auto', mr: 'auto' }} fullWidth>
                      <FormHelperText>Сортировка:</FormHelperText>
                      <Select
                        value={filter.orderBy}
                        onChange={setParameters('orderBy')}
                      >
                        <MenuItem value={'Value desc'}>
                          Название (по убыванию)
                        </MenuItem>
                        <MenuItem value={'Value'}>
                          Название (по возрастанию)
                        </MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={3}>
                    <FormControl sx={{ ml: 'auto', mr: 'auto' }}>
                      <FormHelperText>Показывать:</FormHelperText>
                      <Select
                        value={filter.pageSize}
                        onChange={setParameters('pageSize')}
                      >
                        <MenuItem value={5}>5 на странице</MenuItem>
                        <MenuItem value={10}>10 на странице</MenuItem>
                        <MenuItem value={20}>20 на странице</MenuItem>
                        <MenuItem value={30}>30 на странице</MenuItem>
                        <MenuItem value={recordsFiltered}>Все</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={3}>
                    <Typography>
                      Выбрано {recordsFiltered} из {totalRecords}
                    </Typography>
                  </Grid>
                  <Grid item xs={2}>
                    <Button
                      variant="contained"
                      sx={{ mb: 2, mt: 2 }}
                      fullWidth
                      onClick={handleResetFilter}
                    >
                      Сбросить фильтр
                    </Button>
                  </Grid>
                </Grid>
              </Paper>
              <Paper
                variant="outlined"
                sx={{ p: 2, margin: '5px 0 5px', flexGrow: 1 }}
              >
                <Autocomplete
                  options={values}
                  size="string"
                  onChange={(_, value) => {
                    handleChangeFilter('value')(value ? value.value : '')
                  }}
                  getOptionLabel={(item) => (item.value ? item.value : '')}
                  isOptionEqualToValue={(option, val) =>
                    option.value === val.value
                  }
                  id="filter"
                  renderInput={(params) => (
                    <TextField {...params} label="Название" />
                  )}
                />
              </Paper>
              <Paper
                variant="outlined"
                sx={{
                  p: 2,
                  margin: '5px 0 5px'
                }}
              >
                <Stack
                  direction="row"
                  spacing={3}
                  sx={{
                    p: 2,
                    margin: '5px 0 5px',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center'
                  }}
                >
                  <Typography variant="h5" component="div">
                    Новое значение:
                  </Typography>
                  <TextField
                    value={newValue}
                    fullWidth
                    onChange={onChange}
                  ></TextField>
                  <Button
                    variant="contained"
                    size="large"
                    color="primary"
                    onClick={createNewValue}
                  >
                    Добавить
                  </Button>
                </Stack>
              </Paper>
              {data.length ? (
                <PropertyList
                  isLoading={appState.loading}
                  items={data}
                  onDelete={handleDelete}
                  control={props.control}
                />
              ) : (
                <Typography
                  gutterBottom
                  variant="h3"
                  component="div"
                  sx={{ display: 'flex', p: 2, margin: '20px' }}
                >
                  Нет производителей. Добавьте новый.
                </Typography>
              )}

              <Paper
                elevation={0}
                sx={{
                  display: 'flex',
                  p: 2,
                  marginTop: '5px',
                  marginBottom: '5px',
                  marginLeft: 'auto',
                  marginRight: 'auto'
                }}
              >
                <Pagination
                  count={totalPages}
                  color="primary"
                  showFirstButton
                  showLastButton
                  sx={{ ml: 'auto', mr: 'auto', mb: 2 }}
                  size="large"
                  page={filter.pageNumber}
                  onChange={(_, value) =>
                    handleChangeFilter('pageNumber')(value)
                  }
                />
              </Paper>
            </Grid>
          </Grid>
        </Box>
      </Paper>
    </Container>
  )
}

export default Property
