import * as React from 'react'
import { Link } from 'react-router-dom'
import { Box, Container, CssBaseline, Grid, Paper } from '@mui/material'
import { styled } from '@mui/material/styles'
import { properties } from './../../../config'

const Element = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(2),
  textAlign: 'center',
  fontSize: '1.5rem',
  color: theme.palette.text.secondary
}))

const Properties = () => {
  return (
    <Container component="main" maxWidth="lg">
      <CssBaseline />
      <Box
        sx={{
          width: '100%',
          mt: 3,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          marginBottom: '80px'
        }}
      >
        <Grid
          container
          spacing={{ xs: 2, md: 3 }}
          columns={{ xs: 4, sm: 8, md: 12 }}
        >
          {properties
            .sort((a, b) => (a.label > b.label ? 1 : -1))
            .map((item, index) => (
              <Grid item xs={2} sm={4} md={4} key={index}>
                <Link to={`/dashboard/properties/${item.controller}`}>
                  <Element>{item.label}</Element>
                </Link>
              </Grid>
            ))}
        </Grid>
      </Box>
    </Container>
  )
}

export default Properties
