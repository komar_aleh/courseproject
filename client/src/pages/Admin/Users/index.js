import * as React from 'react'
import { Link } from 'react-router-dom'
import {
  Box,
  Button,
  Container,
  Divider,
  FormControl,
  FormControlLabel,
  FormHelperText,
  FormLabel,
  Grid,
  IconButton,
  InputBase,
  MenuItem,
  Pagination,
  Paper,
  Radio,
  RadioGroup,
  Select,
  Stack,
  Typography
} from '@mui/material'
import { styled, alpha } from '@mui/material/styles'
import { Icon } from '@iconify/react'
import SearchIcon from '@mui/icons-material/Search'
import { getAllUsers } from '../../../http/userAPI'
import Spinner from '../../../components/Spinner'
import ErrorSuccess from '../../../components/ErrorSucces'
import UserList from '../../../components/Admin/User/UserList'

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  '&:hover': {
    backgroundColor: alpha(theme.palette.common.white, 0.25)
  },
  marginLeft: 0,
  width: '100%'
}))

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center'
}))

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%'
  }
}))

const Users = () => {
  const [data, setData] = React.useState([])
  const [appState, setAppState] = React.useState({
    loading: false
  })

  const [openError, setOpenError] = React.useState(false)
  const [openSuccess, setOpenSuccess] = React.useState(false)
  const [messageError, setMessageError] = React.useState('')

  const [filter, setFilter] = React.useState({
    userName: '',
    orderBy: 'UserName',
    role: '',
    pageNumber: 1,
    pageSize: 10
  })

  const [totalPages, setTotalPages] = React.useState(0)
  const [recordsFiltered, setRecordsFiltered] = React.useState(0)
  const [totalRecords, setTotalRecords] = React.useState(0)

  const [initialFilter, setInitialFilter] = React.useState([])

  const handleChangeFilter = (prop) => (value) => {
    setFilter({ ...filter, [prop]: value })
  }
  const setParameters = (prop) => (event) => {
    setFilter({ ...filter, [prop]: event.target.value })
  }

  const getUsers = async () => {
    try {
      setAppState({ loading: true })
      const response = await getAllUsers(
        filter.userName,
        filter.orderBy,
        filter.role,
        filter.pageNumber,
        filter.pageSize
      )
      setData(response.data)
      setTotalPages(response.totalPages)
      setTotalRecords(response.totalRecords)
      setRecordsFiltered(response.recordsFiltered)
      setAppState({ loading: false })
    } catch (error) {
      const erdm = error.response.data.message
        ? error.response.data.message
        : ''
      const erde = error.response.data.errors ? error.response.data.errors : ''
      const em = error.message ? error.message : ''
      setMessageError(erdm, erde, em)
      setOpenError(true)
    }
  }

  const handleChangeRole = async () => {
    try {
      getUsers()
    } catch (error) {
      console.log(error)
    }
  }

  const handleDelete = async () => {
    try {
      getUsers()
    } catch (error) {
      console.log(error)
    }
  }

  const handleResetFilter = () => {
    setFilter(initialFilter)
    setSearchValue('')
  }

  const [searchValue, setSearchValue] = React.useState('')
  const handleSearchValue = (event) => {
    setSearchValue(event.target.value)
  }
  const handleSearch = () => {
    handleChangeFilter('userName')(searchValue)
  }

  const clearSearchValue = () => {
    setSearchValue('')
    handleChangeFilter('userName')('')
  }

  React.useEffect(() => {
    document.title = 'Администратор: пользователи'
    setInitialFilter(filter)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [initialFilter])

  React.useEffect(() => {
    getUsers()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter])

  return appState.loading ? (
    <Spinner />
  ) : (
    <Container
      component="main"
      maxWidth="lg"
      sx={{ p: 2, margin: 'auto', flexGrow: 1, minWidth: 400 }}
    >
      <ErrorSuccess
        openE={openError}
        messageE={messageError}
        closeE={() => {
          setOpenError(false)
        }}
        openS={openSuccess}
        messageS={messageError}
        closeS={() => {
          setOpenSuccess(false)
        }}
      />
      <Paper variant="outlined" sx={{ p: 2, margin: '5px 0 5px', flexGrow: 1 }}>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            marginBottom: 2
          }}
        >
          <Typography variant="h3" component="div" gutterBottom>
            Пользователи
          </Typography>
          {data.length > 0 ? (
            <Grid container spacing={1}>
              <Grid item xs={12}>
                <Paper
                  variant="outlined"
                  sx={{ p: 2, margin: '5px 0 5px' }}
                  position="static"
                >
                  <Search>
                    <Stack direction="row" alignItems="center" spacing={2}>
                      <SearchIconWrapper>
                        <SearchIcon color="primary" size="large" />
                      </SearchIconWrapper>
                      <StyledInputBase
                        value={searchValue}
                        onChange={handleSearchValue}
                        fullWidth
                        placeholder="Поиск…"
                        inputProps={{ 'aria-label': 'search' }}
                      />
                      {searchValue && (
                        <IconButton onClick={clearSearchValue}>
                          <Icon icon="akar-icons:cross" />
                        </IconButton>
                      )}
                      <Button
                        variant="contained"
                        endIcon={<SearchIcon color="white" size="large" />}
                        onClick={handleSearch}
                      >
                        Найти
                      </Button>
                      <Divider orientation="vertical" flexItem />
                      <Link to="/dashboard/users/createAdmin">
                        <Button
                          variant="contained"
                          endIcon={
                            <Icon icon="gridicons:create" color="white" />
                          }
                        >
                          Добавить администратора
                        </Button>
                      </Link>
                    </Stack>
                  </Search>
                </Paper>
                <Paper variant="outlined" sx={{ p: 2, margin: '5px 0 5px' }}>
                  <Grid
                    container
                    spacing={2}
                    alignItems="center"
                    justifyContent="center"
                  >
                    <Grid item xs={4}>
                      <FormControl sx={{ ml: 'auto', mr: 'auto' }} fullWidth>
                        <FormHelperText>Сортировка:</FormHelperText>
                        <Select
                          value={filter.orderBy}
                          onChange={setParameters('orderBy')}
                        >
                          <MenuItem value={'UserName'}>
                            По имени пользователя (возр.)
                          </MenuItem>
                          <MenuItem value={'UserName desc'}>
                            По имени пользователя (убыв.)
                          </MenuItem>
                          <MenuItem value={'Email'}>По email (возр.)</MenuItem>
                          <MenuItem value={'Email desc'}>
                            По email (убыв.)
                          </MenuItem>
                        </Select>
                      </FormControl>
                    </Grid>
                    <Grid item xs={2}>
                      <FormControl sx={{ ml: 'auto', mr: 'auto' }} fullWidth>
                        <FormHelperText>Показывать:</FormHelperText>
                        <Select
                          value={filter.pageSize}
                          onChange={setParameters('pageSize')}
                        >
                          <MenuItem value={5}>5 на странице</MenuItem>
                          <MenuItem value={10}>10 на странице</MenuItem>
                          <MenuItem value={15}>15 на странице</MenuItem>
                          <MenuItem value={20}>20 на странице</MenuItem>
                          <MenuItem value={25}>25 на странице</MenuItem>
                        </Select>
                      </FormControl>
                    </Grid>
                    <Grid item xs={2}>
                      <FormControl component="fieldset">
                        <FormLabel component="legend">Роль</FormLabel>
                        <RadioGroup
                          row
                          aria-label="gender"
                          name="controlled-radio-buttons-group"
                          value={filter.role}
                          onChange={setParameters('role')}
                        >
                          <FormControlLabel
                            value=""
                            control={<Radio />}
                            label="Все"
                          />
                          <FormControlLabel
                            value="Admin"
                            control={<Radio />}
                            label="Администраторы"
                          />
                        </RadioGroup>
                      </FormControl>
                    </Grid>
                    <Grid item xs={2}>
                      <Typography sx={{ ml: 3 }}>
                        Выбрано {recordsFiltered} из {totalRecords}
                      </Typography>
                    </Grid>
                    <Grid item xs={2}>
                      <Button
                        variant="contained"
                        sx={{ mb: 2, mt: 2 }}
                        fullWidth
                        onClick={handleResetFilter}
                      >
                        Сбросить фильтр
                      </Button>
                    </Grid>
                  </Grid>
                </Paper>
                <Paper variant="outlined" sx={{ p: 2, margin: '5px 0 5px' }}>
                  <UserList
                    isLoading={appState.loading}
                    items={data}
                    onChangeRole={handleChangeRole}
                    onDelete={handleDelete}
                  />
                </Paper>
                <Paper
                  elevation={0}
                  sx={{
                    display: 'flex',
                    p: 2,
                    margin: '5px 0 5px'
                  }}
                >
                  <Paper
                    elevation={0}
                    sx={{
                      display: 'flex',
                      p: 2,
                      marginTop: '5px',
                      marginBottom: '5px',
                      marginLeft: 'auto',
                      marginRight: 'auto'
                    }}
                  >
                    <Pagination
                      count={totalPages}
                      color="primary"
                      boundaryCount={2}
                      showFirstButton
                      showLastButton
                      sx={{ ml: 'auto', mr: 'auto', mb: 2 }}
                      size="large"
                      page={filter.pageNumber}
                      onChange={(_, value) =>
                        handleChangeFilter('pageNumber')(value)
                      }
                    />
                  </Paper>
                </Paper>
              </Grid>
            </Grid>
          ) : (
            <Typography
              gutterBottom
              variant="h3"
              component="div"
              sx={{ display: 'flex', p: 2, margin: '20px' }}
            >
              Пользователей нет.
            </Typography>
          )}
        </Box>
      </Paper>
    </Container>
  )
}

export default Users
