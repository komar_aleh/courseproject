import * as React from 'react'
import {
  Autocomplete,
  Button,
  Collapse,
  Container,
  FormControl,
  FormHelperText,
  Grid,
  IconButton,
  InputBase,
  MenuItem,
  Pagination,
  Paper,
  Select,
  Stack,
  TextField,
  Typography
} from '@mui/material'
import { styled, alpha } from '@mui/material/styles'
import { Icon } from '@iconify/react'
import SearchIcon from '@mui/icons-material/Search'
import BicycleList from '../../components/Bikes/BicycleList'
import ListLoading from '../../components/ListLoading'
import { getAllBycicles } from '../../http/productAPI'
import { getAllProperties } from '../../http/propertyAPI'

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  '&:hover': {
    backgroundColor: alpha(theme.palette.common.white, 0.25)
  },
  marginLeft: 0,
  width: '100%'
}))

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center'
}))

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%'
  }
}))

const ExpandMore = styled((props) => {
  const { expand, ...other } = props
  return <IconButton {...other} />
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  //marginRight: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest
  })
}))

const Home = () => {
  const [filter, setFilter] = React.useState({
    minPrice: '',
    maxPrice: '',
    minCommonDate: '',
    maxCommonDate: '',
    minAbsorberStroke: '',
    maxAbsorberStroke: '',
    minSpeedNumber: '',
    maxSpeedNumber: '',
    minFrontStar: '',
    maxFrontStar: '',
    minRearStar: '',
    maxRearStar: '',
    minWeight: '',
    maxWeight: '',
    minWheelDiameter: '',
    maxWheelDiameter: '',
    absorberStroke: '',
    cassette: '',
    chain: '',
    classOfBike: '',
    commonDate: '',
    forkMaterial: '',
    forkModel: '',
    forkType: '',
    frameColor: '',
    frameMaterial: '',
    frameSize: '',
    frontBrakeModel: '',
    frontBrakeType: '',
    frontStar: '',
    frontSwitch: '',
    gripsModel: '',
    handlebarModel: '',
    handlebarType: '',
    manufacturer: '',
    pedalsMaterial: '',
    pedalsModel: '',
    pedalsType: '',
    rearBrakeModel: '',
    rearBrakeType: '',
    rearStar: '',
    rearSwitch: '',
    rimMaterial: '',
    saddleModel: '',
    saddleType: '',
    shifterModel: '',
    shifterType: '',
    speedNumber: '',
    systemModel: '',
    tiresModel: '',
    wheelDiameter: '',
    hardTail: '',
    female: '',
    kidTeen: '',
    doubleRim: '',
    tiresTubeless: '',
    forkLock: '',
    orderBy: 'Manufacturer',
    orderByThen: 'Name',
    searchName: '',
    isAvailable: true,
    pageNumber: 1,
    pageSize: 10
  })
  const [initialFilter, setInitialFilter] = React.useState([])

  React.useEffect(() => {
    document.title = 'Каталог велосипедов'
    getProperties()
    setInitialFilter(filter)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [initialFilter])

  const handleChangeFilter = (prop) => (value) => {
    setFilter({ ...filter, [prop]: value })
  }
  const setParameters = (prop) => (event) => {
    setFilter({ ...filter, [prop]: event.target.value })
  }

  const [appState, setAppState] = React.useState({
    loading: false,
    items: null
  })

  const handleResetFilter = () => {
    setFilter(initialFilter)
    setSearchValue('')
  }

  const [searchValue, setSearchValue] = React.useState('')
  const handleSearchValue = (event) => {
    setSearchValue(event.target.value)
  }
  const handleSearch = () => {
    handleChangeFilter('searchName')(searchValue)
  }

  const clearSearchValue = () => {
    setSearchValue('')
    handleChangeFilter('searchName')('')
  }

  const [expanded, setExpanded] = React.useState(false)

  const handleExpandClick = () => {
    setExpanded(!expanded)
  }

  const LoadingList = ListLoading(BicycleList)

  const [manufacturers, setManufacturers] = React.useState([])
  const [classOfBikes, setClassOfBikes] = React.useState([])
  const [frameMaterials, setFrameMaterials] = React.useState([])
  const [frameSizes, setFrameSizes] = React.useState([])
  const [frameColors, setFrameColors] = React.useState([])
  const [absorberStrokes, setAbsorberStrokes] = React.useState([''])
  const [cassettes, setCassettes] = React.useState([])
  const [chains, setChains] = React.useState([])
  const [commonDates, setCommonDates] = React.useState([])
  const [forkMaterials, setForkMaterials] = React.useState([])
  const [forkModels, setForkModels] = React.useState([])
  const [forkTypes, setForkTypes] = React.useState([])
  const [frontBrakeModels, setFrontBrakeModels] = React.useState([])
  const [frontBrakeTypes, setFrontBrakeTypes] = React.useState([])
  const [frontStars, setFrontStars] = React.useState([])
  const [frontSwitches, setFrontSwitches] = React.useState([])
  const [gripsModels, setGripsModels] = React.useState([])
  const [handleBarModels, setHandleBarModels] = React.useState([])
  const [handleBarTypes, setHandleBarTypes] = React.useState([])
  const [pedalsMaterials, setPedalsMaterials] = React.useState([])
  const [pedalsModels, setPedalsModels] = React.useState([])
  const [pedalsTypes, setPedalsTypes] = React.useState([])
  const [rearBrakeModels, setRearBrakeModels] = React.useState([])
  const [rearBrakeTypes, setRearBrakeTypes] = React.useState([])
  const [rearStars, setRearStars] = React.useState([])
  const [rearSwitches, setRearSwitches] = React.useState([])
  const [rimMaterials, setRimMaterials] = React.useState([])
  const [saddleModels, setSaddleModels] = React.useState([])
  const [saddleTypes, setSaddleTypes] = React.useState([])
  const [shifterModels, setShifterModels] = React.useState([])
  const [shifterTypes, setShifterTypes] = React.useState([])
  const [speedNumbers, setSpeedNumbers] = React.useState([])
  const [systemModels, setSystemModels] = React.useState([])
  const [tiresModels, setTiresModels] = React.useState([])
  const [wheelDiameters, setWheelDiameters] = React.useState([])

  const [totalPages, setTotalPages] = React.useState(0)
  const [recordsFiltered, setRecordsFiltered] = React.useState(0)
  const [totalRecords, setTotalRecords] = React.useState(0)

  const getBikes = async () => {
    setAppState({ loading: true })
    await getAllBycicles(
      filter.minPrice,
      filter.maxPrice,
      filter.minCommonDate,
      filter.maxCommonDate,
      filter.minAbsorberStroke,
      filter.maxAbsorberStroke,
      filter.minSpeedNumber,
      filter.maxSpeedNumber,
      filter.minFrontStar,
      filter.maxFrontStar,
      filter.minRearStar,
      filter.maxRearStar,
      filter.minWeight,
      filter.maxWeight,
      filter.minWheelDiameter,
      filter.maxWheelDiameter,
      filter.cassette,
      filter.chain,
      filter.classOfBike,
      filter.forkMaterial,
      filter.forkModel,
      filter.forkType,
      filter.frameColor,
      filter.frameMaterial,
      filter.frameSize,
      filter.frontBrakeModel,
      filter.frontBrakeType,
      filter.frontSwitch,
      filter.gripsModel,
      filter.handlebarModel,
      filter.handlebarType,
      filter.manufacturer,
      filter.pedalsMaterial,
      filter.pedalsModel,
      filter.pedalsType,
      filter.rearBrakeModel,
      filter.rearBrakeType,
      filter.rearSwitch,
      filter.rimMaterial,
      filter.saddleModel,
      filter.saddleType,
      filter.shifterModel,
      filter.shifterType,
      filter.systemModel,
      filter.tiresModel,
      filter.hardTail,
      filter.female,
      filter.kidTeen,
      filter.doubleRim,
      filter.tiresTubeless,
      filter.forkLock,
      filter.orderBy,
      filter.orderByThen,
      filter.searchName,
      filter.isAvailable,
      filter.pageNumber,
      filter.pageSize
    ).then((response) => {
      setTotalPages(response.totalPages)
      setTotalRecords(response.totalRecords)
      setRecordsFiltered(response.recordsFiltered)
      setAppState({ loading: false, items: response.data })
    })
  }

  const getProperties = async () => {
    await getAllProperties().then((response) => {
      setManufacturers(response.manufacturers)
      setClassOfBikes(response.classOfBikes)
      setFrameMaterials(response.frameMaterials)
      setFrameSizes(response.frameSizes)
      setFrameColors(response.frameColors)
      setAbsorberStrokes(response.absorberStrokes)
      setCassettes(response.cassettes)
      setChains(response.chains)
      setCommonDates(response.commonDates)
      setForkMaterials(response.forkMaterials)
      setForkModels(response.forkModels)
      setForkTypes(response.forkTypes)
      setFrontBrakeModels(response.frontBrakeModels)
      setFrontBrakeTypes(response.frontBrakeTypes)
      setFrontStars(response.frontStars)
      setFrontSwitches(response.frontSwitches)
      setGripsModels(response.gripsModels)
      setHandleBarModels(response.handleBarModels)
      setHandleBarTypes(response.handleBarTypes)
      setPedalsMaterials(response.pedalsMaterials)
      setPedalsModels(response.pedalsModels)
      setPedalsTypes(response.pedalsTypes)
      setRearBrakeModels(response.rearBrakeModels)
      setRearBrakeTypes(response.rearBrakeTypes)
      setRearStars(response.rearStars)
      setRearSwitches(response.rearSwitches)
      setRimMaterials(response.rimMaterials)
      setSaddleModels(response.saddleModels)
      setSaddleTypes(response.saddleTypes)
      setShifterModels(response.shifterModels)
      setShifterTypes(response.shifterTypes)
      setSpeedNumbers(response.speedNumbers)
      setSystemModels(response.systemModels)
      setTiresModels(response.tiresModels)
      setWheelDiameters(response.wheelDiameters)
    })
  }

  React.useEffect(() => {
    try {
      getBikes()
    } catch (err) {
      console.log(err)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter])
  return (
    <Container
      component="main"
      maxWidth="lg"
      sx={{ p: 2, margin: 'auto', flexGrow: 1, minWidth: 400 }}
    >
      <Grid container spacing={1}>
        <Grid item xs={3}>
          <Paper
            variant="outlined"
            sx={{ p: 2, margin: '5px 0 5px', flexGrow: 1 }}
          >
            <Autocomplete
              options={manufacturers}
              onChange={(_, value) => {
                handleChangeFilter('manufacturer')(value ? value.id : '')
              }}
              getOptionLabel={(manufacturer) =>
                manufacturer.value ? manufacturer.value : ''
              }
              isOptionEqualToValue={(option, val) => option.id === val.id}
              id="filter-manufacturers"
              renderInput={(params) => (
                <TextField {...params} label="Производитель" size="small" />
              )}
            />
            <Typography
              variant="body2"
              component="div"
              align="center"
              sx={{ mt: 1, mb: 1 }}
            >
              Цена от и до
            </Typography>
            <Grid container spacing={0}>
              <Grid item xs={6}>
                <TextField
                  margin="normal"
                  fullWidth
                  type="number"
                  id="filter-minPrice"
                  variant="outlined"
                  name="minPrice"
                  size="small"
                  value={filter.minPrice}
                  onChange={setParameters('minPrice')}
                  sx={{ mt: 0 }}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  margin="normal"
                  fullWidth
                  type="number"
                  id="filter-maxPrice"
                  variant="outlined"
                  name="maxPrice"
                  size="small"
                  value={filter.maxPrice}
                  onChange={setParameters('maxPrice')}
                  sx={{ mt: 0 }}
                />
              </Grid>
            </Grid>
            <Typography
              variant="body2"
              component="div"
              align="center"
              sx={{ mt: 1, mb: 1 }}
            >
              Год выпуска
            </Typography>
            <Grid container spacing={0}>
              <Grid item xs={6}>
                <Autocomplete
                  sx={{ mt: 0 }}
                  options={commonDates}
                  onChange={(_, value) => {
                    handleChangeFilter('minCommonDate')(
                      value ? value.value : ''
                    )
                  }}
                  getOptionLabel={(item) =>
                    item.value ? item.value.toString() : ''
                  }
                  isOptionEqualToValue={(option, val) =>
                    option.value === val.value
                  }
                  id="filter-minCommonDates"
                  renderInput={(params) => (
                    <TextField {...params} label="От" size="small" />
                  )}
                />
              </Grid>
              <Grid item xs={6}>
                <Autocomplete
                  sx={{ mt: 0 }}
                  options={commonDates}
                  onChange={(_, value) => {
                    handleChangeFilter('maxCommonDate')(
                      value ? value.value : ''
                    )
                  }}
                  getOptionLabel={(item) =>
                    item.value ? item.value.toString() : ''
                  }
                  isOptionEqualToValue={(option, val) =>
                    option.value === val.value
                  }
                  id="filter-maxCommonDates"
                  renderInput={(params) => (
                    <TextField {...params} label="До" size="small" />
                  )}
                />
              </Grid>
            </Grid>
            <Autocomplete
              sx={{ mt: 2 }}
              options={classOfBikes}
              onChange={(_, value) => {
                handleChangeFilter('classOfBike')(value ? value.id : '')
              }}
              getOptionLabel={(item) => (item.value ? item.value : '')}
              isOptionEqualToValue={(option, val) => option.id === val.id}
              id="filter-classesOfBikes"
              renderInput={(params) => (
                <TextField {...params} label="Класс" size="small" />
              )}
            />
            <Typography
              variant="body2"
              component="div"
              align="center"
              sx={{ mt: 1, mb: 1 }}
            >
              Вес
            </Typography>
            <Grid container spacing={0}>
              <Grid item xs={6}>
                <TextField
                  margin="normal"
                  fullWidth
                  type="number"
                  id="filter-minWeight"
                  variant="outlined"
                  name="minWeight"
                  size="small"
                  value={filter.minWeight}
                  onChange={setParameters('minWeight')}
                  sx={{ mt: 0 }}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  margin="normal"
                  fullWidth
                  type="number"
                  id="filter-maxWeight"
                  variant="outlined"
                  name="maxWeight"
                  size="small"
                  value={filter.maxWeight}
                  onChange={setParameters('maxWeight')}
                  sx={{ mt: 0 }}
                />
              </Grid>
            </Grid>
            <Typography
              variant="body2"
              component="div"
              align="center"
              sx={{ mt: 1, mb: 1 }}
            >
              Диаметр колес
            </Typography>
            <Grid container spacing={0}>
              <Grid item xs={6}>
                <Autocomplete
                  sx={{ mt: 0 }}
                  options={wheelDiameters}
                  onChange={(_, value) => {
                    handleChangeFilter('minWheelDiameter')(
                      value ? value.value : ''
                    )
                  }}
                  getOptionLabel={(item) =>
                    item.value ? item.value.toString() : ''
                  }
                  isOptionEqualToValue={(option, val) =>
                    option.value === val.value
                  }
                  id="filter-minWheelDiameters"
                  renderInput={(params) => (
                    <TextField {...params} label="От" size="small" />
                  )}
                />
              </Grid>
              <Grid item xs={6}>
                <Autocomplete
                  sx={{ mt: 0 }}
                  options={wheelDiameters}
                  onChange={(_, value) => {
                    handleChangeFilter('maxWheelDiameter')(
                      value ? value.value : ''
                    )
                  }}
                  getOptionLabel={(item) =>
                    item.value ? item.value.toString() : ''
                  }
                  isOptionEqualToValue={(option, val) =>
                    option.value === val.value
                  }
                  id="filter-maxWheelDiameters"
                  renderInput={(params) => (
                    <TextField {...params} label="До" size="small" />
                  )}
                />
              </Grid>
            </Grid>
            <Autocomplete
              sx={{ mt: 2 }}
              options={frontBrakeTypes}
              onChange={(_, value) => {
                handleChangeFilter('frontBrakeType')(value ? value.id : '')
              }}
              getOptionLabel={(item) => (item.value ? item.value : '')}
              isOptionEqualToValue={(option, val) => option.id === val.id}
              id="filter-frontBrakeTypes"
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Передний тормоз (тип)"
                  size="small"
                />
              )}
            />
            <Autocomplete
              sx={{ mt: 2 }}
              options={rearBrakeTypes}
              onChange={(_, value) => {
                handleChangeFilter('rearBrakeType')(value ? value.id : '')
              }}
              getOptionLabel={(item) => (item.value ? item.value : '')}
              isOptionEqualToValue={(option, val) => option.id === val.id}
              id="filter-rearBrakeTypes"
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Задний тормоз (тип)"
                  size="small"
                />
              )}
            />
            <Stack direction="row" spacing={2} sx={{ mt: 1 }}>
              <Typography
                variant="body2"
                component="div"
                align="center"
                sx={{ mt: 1 }}
              >
                {expanded ? 'Меньше фильтров' : 'Больше фильтров'}
              </Typography>
              <ExpandMore
                expand={expanded}
                onClick={handleExpandClick}
                aria-expanded={expanded}
                aria-label="show more"
              >
                <Icon icon="flat-color-icons:expand" />
              </ExpandMore>
            </Stack>
            <Collapse in={expanded} unmountOnExit>
              <TextField
                sx={{ mt: 2 }}
                id="select-female"
                label="Женский"
                value={filter.female}
                select
                onChange={setParameters('female')}
                fullWidth
                size="small"
              >
                <MenuItem value="">
                  <em>Не важно</em>
                </MenuItem>
                <MenuItem value={true}>Да</MenuItem>
                <MenuItem value={false}>Нет</MenuItem>
              </TextField>
              <TextField
                sx={{ mt: 2 }}
                id="select-kidTeen"
                label="Подростковый"
                value={filter.kidTeen}
                select
                onChange={setParameters('kidTeen')}
                fullWidth
                size="small"
              >
                <MenuItem value="">
                  <em>Не важно</em>
                </MenuItem>
                <MenuItem value={true}>Да</MenuItem>
                <MenuItem value={false}>Нет</MenuItem>
              </TextField>
              <TextField
                sx={{ mt: 2 }}
                id="select-hardtail"
                label="Двухподвес"
                select
                value={filter.hardTail}
                onChange={setParameters('hardTail')}
                fullWidth
                size="small"
              >
                <MenuItem value="">
                  <em>Не важно</em>
                </MenuItem>
                <MenuItem value={true}>Да</MenuItem>
                <MenuItem value={false}>Нет</MenuItem>
              </TextField>
              <Autocomplete
                sx={{ mt: 2 }}
                options={frameMaterials}
                onChange={(_, value) => {
                  handleChangeFilter('frameMaterial')(value ? value.id : '')
                }}
                getOptionLabel={(item) => (item.value ? item.value : '')}
                isOptionEqualToValue={(option, val) => option.id === val.id}
                id="filter-frameMaterials"
                renderInput={(params) => (
                  <TextField {...params} label="Материал рамы" size="small" />
                )}
              />
              <Autocomplete
                sx={{ mt: 2 }}
                options={frameSizes}
                onChange={(_, value) => {
                  handleChangeFilter('frameSize')(value ? value.id : '')
                }}
                getOptionLabel={(item) => (item.value ? item.value : '')}
                isOptionEqualToValue={(option, val) => option.id === val.id}
                id="filter-frameSizes"
                renderInput={(params) => (
                  <TextField {...params} label="Размер рамы" size="small" />
                )}
              />
              <Autocomplete
                sx={{ mt: 2 }}
                options={frameColors}
                onChange={(_, value) => {
                  handleChangeFilter('frameColor')(value ? value.id : '')
                }}
                getOptionLabel={(item) => (item.value ? item.value : '')}
                isOptionEqualToValue={(option, val) => option.id === val.id}
                id="filter-frameColors"
                renderInput={(params) => (
                  <TextField {...params} label="Цвет рамы" size="small" />
                )}
              />
              <Autocomplete
                sx={{ mt: 2 }}
                options={forkTypes}
                onChange={(_, value) => {
                  handleChangeFilter('forkType')(value ? value.id : '')
                }}
                getOptionLabel={(item) => (item.value ? item.value : '')}
                isOptionEqualToValue={(option, val) => option.id === val.id}
                id="filter-frameColors"
                renderInput={(params) => (
                  <TextField {...params} label="Тип вилки" size="small" />
                )}
              />
              <Autocomplete
                sx={{ mt: 2 }}
                options={forkMaterials}
                onChange={(_, value) => {
                  handleChangeFilter('forkMaterial')(value ? value.id : '')
                }}
                getOptionLabel={(item) => (item.value ? item.value : '')}
                isOptionEqualToValue={(option, val) => option.id === val.id}
                id="filter-frameColors"
                renderInput={(params) => (
                  <TextField {...params} label="Материал вилки" size="small" />
                )}
              />
              <Autocomplete
                sx={{ mt: 2 }}
                options={forkModels}
                onChange={(_, value) => {
                  handleChangeFilter('forkModel')(value ? value.id : '')
                }}
                getOptionLabel={(item) => (item.value ? item.value : '')}
                isOptionEqualToValue={(option, val) => option.id === val.id}
                id="filter-frameColors"
                renderInput={(params) => (
                  <TextField {...params} label="Модель вилки" size="small" />
                )}
              />
              <Typography
                variant="body2"
                component="div"
                align="center"
                sx={{ mt: 1, mb: 1 }}
              >
                Ход амортизатора
              </Typography>
              <Grid container spacing={0}>
                <Grid item xs={6}>
                  <Autocomplete
                    sx={{ mt: 0 }}
                    options={absorberStrokes}
                    onChange={(_, value) => {
                      handleChangeFilter('minAbsorberStroke')(
                        value ? value.value : ''
                      )
                    }}
                    getOptionLabel={(item) =>
                      item.value ? item.value.toString() : ''
                    }
                    isOptionEqualToValue={(option, val) =>
                      option.value === val.value
                    }
                    id="filter-minAbsorberStrokes"
                    renderInput={(params) => (
                      <TextField {...params} label="От" size="small" />
                    )}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Autocomplete
                    sx={{ mt: 0 }}
                    options={absorberStrokes}
                    onChange={(_, value) => {
                      handleChangeFilter('maxAbsorberStroke')(
                        value ? value.value : ''
                      )
                    }}
                    getOptionLabel={(item) =>
                      item.value ? item.value.toString() : ''
                    }
                    isOptionEqualToValue={(option, val) =>
                      option.value === val.value
                    }
                    id="filter-maxAbsorberStrokes"
                    renderInput={(params) => (
                      <TextField {...params} label="От" size="small" />
                    )}
                  />
                </Grid>
              </Grid>
              <Autocomplete
                sx={{ mt: 2 }}
                options={frontBrakeModels}
                onChange={(_, value) => {
                  handleChangeFilter('frontBrakeModele')(value ? value.id : '')
                }}
                getOptionLabel={(item) => (item.value ? item.value : '')}
                isOptionEqualToValue={(option, val) => option.id === val.id}
                id="filter-frontBrakeTypes"
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Передний тормоз (модель)"
                    size="small"
                  />
                )}
              />
              <Autocomplete
                sx={{ mt: 2 }}
                options={rearBrakeModels}
                onChange={(_, value) => {
                  handleChangeFilter('rearBrakeModele')(value ? value.id : '')
                }}
                getOptionLabel={(item) => (item.value ? item.value : '')}
                isOptionEqualToValue={(option, val) => option.id === val.id}
                id="filter-rearBrakeTypes"
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Задний тормоз (модель)"
                    size="small"
                  />
                )}
              />
              <Typography
                variant="body2"
                component="div"
                align="center"
                sx={{ mt: 1, mb: 1 }}
              >
                Количество передач
              </Typography>
              <Grid container spacing={0}>
                <Grid item xs={6}>
                  <Autocomplete
                    sx={{ mt: 0 }}
                    options={speedNumbers}
                    onChange={(_, value) => {
                      handleChangeFilter('minSpeedNumber')(
                        value ? value.value : ''
                      )
                    }}
                    getOptionLabel={(item) =>
                      item.value ? item.value.toString() : ''
                    }
                    isOptionEqualToValue={(option, val) =>
                      option.value === val.value
                    }
                    id="filter-minSpeedNumbers"
                    renderInput={(params) => (
                      <TextField {...params} label="От" size="small" />
                    )}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Autocomplete
                    sx={{ mt: 0 }}
                    options={speedNumbers}
                    onChange={(_, value) => {
                      handleChangeFilter('maxSpeedNumber')(
                        value ? value.value : ''
                      )
                    }}
                    getOptionLabel={(item) =>
                      item.value ? item.value.toString() : ''
                    }
                    isOptionEqualToValue={(option, val) =>
                      option.value === val.value
                    }
                    id="filter-maxSpeedNumbers"
                    renderInput={(params) => (
                      <TextField {...params} label="До" size="small" />
                    )}
                  />
                </Grid>
              </Grid>
              <Autocomplete
                sx={{ mt: 2 }}
                options={frontStars}
                onChange={(_, value) => {
                  handleChangeFilter('frontStar')(value ? value.id : '')
                }}
                getOptionLabel={(item) => (item.value ? item.value : '')}
                isOptionEqualToValue={(option, val) => option.id === val.id}
                id="filter-frontStars"
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Количество звезд спереди"
                    size="small"
                  />
                )}
              />
              <Autocomplete
                sx={{ mt: 2 }}
                options={rearStars}
                onChange={(_, value) => {
                  handleChangeFilter('rearStar')(value ? value.id : '')
                }}
                getOptionLabel={(item) => (item.value ? item.value : '')}
                isOptionEqualToValue={(option, val) => option.id === val.id}
                id="filter-rearStars"
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Количество звезд сзади"
                    size="small"
                  />
                )}
              />
              <Autocomplete
                sx={{ mt: 2 }}
                options={cassettes}
                onChange={(_, value) => {
                  handleChangeFilter('cassette')(value ? value.id : '')
                }}
                getOptionLabel={(item) => (item.value ? item.value : '')}
                isOptionEqualToValue={(option, val) => option.id === val.id}
                id="filter-cassettes"
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Кассета (трещотка)"
                    size="small"
                  />
                )}
              />
              <Autocomplete
                sx={{ mt: 2 }}
                options={chains}
                onChange={(_, value) => {
                  handleChangeFilter('chain')(value ? value.id : '')
                }}
                getOptionLabel={(item) => (item.value ? item.value : '')}
                isOptionEqualToValue={(option, val) => option.id === val.id}
                id="filter-chains"
                renderInput={(params) => (
                  <TextField {...params} label="Модель цепи" size="small" />
                )}
              />
              <Autocomplete
                sx={{ mt: 2 }}
                options={systemModels}
                onChange={(_, value) => {
                  handleChangeFilter('systemModel')(value ? value.id : '')
                }}
                getOptionLabel={(item) => (item.value ? item.value : '')}
                isOptionEqualToValue={(option, val) => option.id === val.id}
                id="filter-systemModels"
                renderInput={(params) => (
                  <TextField {...params} label="Система шатунов" size="small" />
                )}
              />
              <Autocomplete
                sx={{ mt: 2 }}
                options={shifterModels}
                onChange={(_, value) => {
                  handleChangeFilter('shifterModel')(value ? value.id : '')
                }}
                getOptionLabel={(item) => (item.value ? item.value : '')}
                isOptionEqualToValue={(option, val) => option.id === val.id}
                id="filter-shifterModels"
                renderInput={(params) => (
                  <TextField {...params} label="Манетки" size="small" />
                )}
              />
              <Autocomplete
                sx={{ mt: 2 }}
                options={shifterTypes}
                onChange={(_, value) => {
                  handleChangeFilter('shifterType')(value ? value.id : '')
                }}
                getOptionLabel={(item) => (item.value ? item.value : '')}
                isOptionEqualToValue={(option, val) => option.id === val.id}
                id="filter-shifterTypes"
                renderInput={(params) => (
                  <TextField {...params} label="Тип манеток" size="small" />
                )}
              />
              <Autocomplete
                sx={{ mt: 2 }}
                options={frontSwitches}
                onChange={(_, value) => {
                  handleChangeFilter('frontSwitch')(value ? value.id : '')
                }}
                getOptionLabel={(item) => (item.value ? item.value : '')}
                isOptionEqualToValue={(option, val) => option.id === val.id}
                id="filter-frontSwitches"
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Передний переключатель"
                    size="small"
                  />
                )}
              />
              <Autocomplete
                sx={{ mt: 2 }}
                options={rearSwitches}
                onChange={(_, value) => {
                  handleChangeFilter('rearSwitch')(value ? value.id : '')
                }}
                getOptionLabel={(item) => (item.value ? item.value : '')}
                isOptionEqualToValue={(option, val) => option.id === val.id}
                id="filter-rearSwitches"
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Задний переключатель"
                    size="small"
                  />
                )}
              />
              <TextField
                sx={{ mt: 2 }}
                id="select-doubleRim"
                label="Двойные ободья"
                select
                value={filter.doubleRim}
                onChange={setParameters('doubleRim')}
                fullWidth
                size="small"
              >
                <MenuItem value="">
                  <em>Не важно</em>
                </MenuItem>
                <MenuItem value={true}>Да</MenuItem>
                <MenuItem value={false}>Нет</MenuItem>
              </TextField>
              <Autocomplete
                sx={{ mt: 2 }}
                options={rimMaterials}
                onChange={(_, value) => {
                  handleChangeFilter('rimMaterial')(value ? value.id : '')
                }}
                getOptionLabel={(item) => (item.value ? item.value : '')}
                isOptionEqualToValue={(option, val) => option.id === val.id}
                id="filter-rimMaterials"
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Материал ободьев"
                    size="small"
                  />
                )}
              />
              <Autocomplete
                sx={{ mt: 2 }}
                options={tiresModels}
                onChange={(_, value) => {
                  handleChangeFilter('tiresModel')(value ? value.id : '')
                }}
                getOptionLabel={(item) => (item.value ? item.value : '')}
                isOptionEqualToValue={(option, val) => option.id === val.id}
                id="filter-tiresModels"
                renderInput={(params) => (
                  <TextField {...params} label="Модель покрышек" size="small" />
                )}
              />
              <Autocomplete
                sx={{ mt: 2 }}
                options={handleBarModels}
                onChange={(_, value) => {
                  handleChangeFilter('handleBarModel')(value ? value.id : '')
                }}
                getOptionLabel={(item) => (item.value ? item.value : '')}
                isOptionEqualToValue={(option, val) => option.id === val.id}
                id="filter-handleBarModels"
                renderInput={(params) => (
                  <TextField {...params} label="Модель руля" size="small" />
                )}
              />
              <Autocomplete
                sx={{ mt: 2 }}
                options={handleBarTypes}
                onChange={(_, value) => {
                  handleChangeFilter('handleBarType')(value ? value.id : '')
                }}
                getOptionLabel={(item) => (item.value ? item.value : '')}
                isOptionEqualToValue={(option, val) => option.id === val.id}
                id="filter-handleBarTypes"
                renderInput={(params) => (
                  <TextField {...params} label="Тип руля" size="small" />
                )}
              />
              <Autocomplete
                sx={{ mt: 2 }}
                options={gripsModels}
                onChange={(_, value) => {
                  handleChangeFilter('gripsModel')(value ? value.id : '')
                }}
                getOptionLabel={(item) => (item.value ? item.value : '')}
                isOptionEqualToValue={(option, val) => option.id === val.id}
                id="filter-gripsModels"
                renderInput={(params) => (
                  <TextField {...params} label="Грипсы" size="small" />
                )}
              />
              <Autocomplete
                sx={{ mt: 2 }}
                options={saddleModels}
                onChange={(_, value) => {
                  handleChangeFilter('saddleModel')(value ? value.id : '')
                }}
                getOptionLabel={(item) => (item.value ? item.value : '')}
                isOptionEqualToValue={(option, val) => option.id === val.id}
                id="filter-saddleModels"
                renderInput={(params) => (
                  <TextField {...params} label="Модель седла" size="small" />
                )}
              />
              <Autocomplete
                sx={{ mt: 2 }}
                options={saddleTypes}
                onChange={(_, value) => {
                  handleChangeFilter('saddleType')(value ? value.id : '')
                }}
                getOptionLabel={(item) => (item.value ? item.value : '')}
                isOptionEqualToValue={(option, val) => option.id === val.id}
                id="filter-saddleTypes"
                renderInput={(params) => (
                  <TextField {...params} label="Тип седла" size="small" />
                )}
              />
              <Autocomplete
                sx={{ mt: 2 }}
                options={pedalsModels}
                onChange={(_, value) => {
                  handleChangeFilter('pedalsModel')(value ? value.id : '')
                }}
                getOptionLabel={(item) => (item.value ? item.value : '')}
                isOptionEqualToValue={(option, val) => option.id === val.id}
                id="filter-pedalsModels"
                renderInput={(params) => (
                  <TextField {...params} label="Модель педалей" size="small" />
                )}
              />
              <Autocomplete
                sx={{ mt: 2 }}
                options={pedalsTypes}
                onChange={(_, value) => {
                  handleChangeFilter('pedalsType')(value ? value.id : '')
                }}
                getOptionLabel={(item) => (item.value ? item.value : '')}
                isOptionEqualToValue={(option, val) => option.id === val.id}
                id="filter-pedalsTypes"
                renderInput={(params) => (
                  <TextField {...params} label="Тип педалей" size="small" />
                )}
              />
              <Autocomplete
                sx={{ mt: 2 }}
                options={pedalsMaterials}
                onChange={(_, value) => {
                  handleChangeFilter('pedalsMaterial')(value ? value.id : '')
                }}
                getOptionLabel={(item) => (item.value ? item.value : '')}
                isOptionEqualToValue={(option, val) => option.id === val.id}
                id="filter-pedalsMaterials"
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Материал педалей"
                    size="small"
                  />
                )}
              />
            </Collapse>
            <Button
              variant="contained"
              sx={{ mb: 2, mt: 2 }}
              fullWidth
              onClick={handleResetFilter}
            >
              Сбросить фильтр
            </Button>
          </Paper>
        </Grid>
        <Grid item xs={9}>
          <Paper
            variant="outlined"
            sx={{ p: 2, margin: '5px 0 5px' }}
            position="static"
          >
            <Search>
              <Stack direction="row" alignItems="center" spacing={2}>
                <SearchIconWrapper>
                  <SearchIcon color="primary" size="large" />
                </SearchIconWrapper>
                <StyledInputBase
                  value={searchValue}
                  onChange={handleSearchValue}
                  fullWidth
                  placeholder="Поиск…"
                  inputProps={{ 'aria-label': 'search' }}
                />
                {searchValue && (
                  <IconButton onClick={clearSearchValue}>
                    <Icon icon="akar-icons:cross" />
                  </IconButton>
                )}
                <Button
                  variant="contained"
                  endIcon={<SearchIcon color="white" size="large" />}
                  onClick={handleSearch}
                >
                  Найти
                </Button>
              </Stack>
            </Search>
          </Paper>
          <Paper variant="outlined" sx={{ p: 2, margin: '5px 0 5px' }}>
            <Grid
              container
              spacing={2}
              alignItems="center"
              justifyContent="center"
            >
              <Grid item xs={4}>
                <FormControl sx={{ ml: 'auto', mr: 'auto' }} fullWidth>
                  <FormHelperText>Сортировка:</FormHelperText>
                  <Select
                    value={filter.orderBy}
                    onChange={setParameters('orderBy')}
                  >
                    <MenuItem value={'Manufacturer'}>
                      Название (по возрастанию)
                    </MenuItem>
                    <MenuItem value={'Manufacturer desc'}>
                      Название (по убыванию)
                    </MenuItem>
                    <MenuItem value={'Price'}>Цена (по возрастанию)</MenuItem>
                    <MenuItem value={'Price desc'}>Цена (по убыванию)</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={4}>
                <FormControl sx={{ ml: 'auto', mr: 'auto' }}>
                  <FormHelperText>Показывать:</FormHelperText>
                  <Select
                    value={filter.pageSize}
                    onChange={setParameters('pageSize')}
                  >
                    <MenuItem value={10}>10 на странице</MenuItem>
                    <MenuItem value={20}>20 на странице</MenuItem>
                    <MenuItem value={30}>30 на странице</MenuItem>
                    <MenuItem value={recordsFiltered}>Все</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={4}>
                <Typography>
                  Выбрано {recordsFiltered} из {totalRecords}
                </Typography>
              </Grid>
            </Grid>
          </Paper>
          <LoadingList isLoading={appState.loading} items={appState.items} />
          <Paper
            elevation={0}
            sx={{
              display: 'flex',
              p: 2,
              margin: '5px 0 5px'
            }}
          >
            <Paper
              elevation={0}
              sx={{
                display: 'flex',
                p: 2,
                marginTop: '5px',
                marginBottom: '5px',
                marginLeft: 'auto',
                marginRight: 'auto'
              }}
            >
              <Pagination
                count={totalPages}
                color="primary"
                boundaryCount={2}
                showFirstButton
                showLastButton
                sx={{ ml: 'auto', mr: 'auto', mb: 2 }}
                size="large"
                page={filter.pageNumber}
                onChange={(_, value) => handleChangeFilter('pageNumber')(value)}
              />
            </Paper>
          </Paper>
        </Grid>
      </Grid>
    </Container>
  )
}

export default Home
