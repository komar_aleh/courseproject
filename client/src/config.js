export const HOST = 'http://localhost:27132'
export const IMG_LG =
  'http://localhost:27132/Resources/Images/Products/LargeImages/'
export const IMG_MD =
  'http://localhost:27132/Resources/Images/Products/MediumImages/'
export const IMG_SM =
  'http://localhost:27132/Resources/Images/Products/SmallImages/'

export const properties = [
  { label: 'Класс велосипеда', controller: 'ClassOfBike' },
  { label: 'Дата выхода на рынок', controller: 'CommonDate' },
  { label: 'Производитель', controller: 'Manufacturer' },
  { label: 'Материал рамы', controller: 'FrameMaterial' },
  { label: 'Размер рамы', controller: 'FrameSize' },
  { label: 'Материал вилки', controller: 'ForkMaterial' },
  { label: 'Диаметр колес', controller: 'WheelDiameter' },
  { label: 'Цвет рамы', controller: 'FrameColor' },
  { label: 'Модель вилки', controller: 'ForkModel' },
  { label: 'Ход амортизатора', controller: 'AbsorberStroke' },
  { label: 'Количество скоростей', controller: 'SpeedNumber' },
  { label: 'Система шатунов', controller: 'SystemModel' },
  { label: 'Количество звезд в системе', controller: 'FrontStar' },
  { label: 'Количество звезд в кассете', controller: 'RearStar' },
  { label: 'Кассета', controller: 'Cassette' },
  { label: 'Тип вилки', controller: 'ForkType' },
  { label: 'Передний переключатель', controller: 'FrontSwitch' },
  { label: 'Задний переключатель', controller: 'RearSwitch' },
  { label: 'Цепь', controller: 'Chain' },
  { label: 'Модель манеток', controller: 'ShifterModel' },
  { label: 'Тип манеток', controller: 'ShifterType' },
  { label: 'Модель переднего тормоза', controller: 'FrontBrakeModel' },
  { label: 'Тип переднего тормоза', controller: 'FrontBrakeType' },
  { label: 'Модель заднего тормоза', controller: 'RearBrakeModel' },
  { label: 'Тип заднего тормоза', controller: 'RearBrakeType' },
  { label: 'Материал ободьев', controller: 'RimMaterial' },
  { label: 'Модель покрышек', controller: 'TiresModel' },
  { label: 'Модель руля', controller: 'HandlebarModel' },
  { label: 'Тип руля', controller: 'HandlebarType' },
  { label: 'Грипсы', controller: 'GripsModel' },
  { label: 'Тип седла', controller: 'SaddleType' },
  { label: 'Модель седла', controller: 'SaddleModel' },
  { label: 'Тип педалей', controller: 'PedalsType' },
  { label: 'Модель педалей', controller: 'PedalsModel' },
  { label: 'Материал педалей', controller: 'PedalsMaterial' }
]
