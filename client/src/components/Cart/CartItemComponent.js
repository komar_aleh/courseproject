import * as React from 'react'
import {
  Box,
  Button,
  IconButton,
  Modal,
  TableCell,
  TableRow,
  Typography
} from '@mui/material'
import PropTypes from 'prop-types'
import AddIcon from '@mui/icons-material/Add'
import RemoveIcon from '@mui/icons-material/Remove'
import { updateCartItem, deleteCartItem } from '../../http/cartAPI'
import { Icon } from '@iconify/react'
import useAuth from '../../hooks/useAuth'

const modalStyle = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4
}

const CartItemComponent = React.memo(({ item, index, onDelete, onChange }) => {
  const [itemData, setItemData] = React.useState(item)
  const auth = useAuth()

  const [errorMessage, setErrorMessage] = React.useState('')
  const [open, setOpen] = React.useState(false)
  const handleClose = () => setOpen(false)

  const handleDelete = async () => {
    try {
      await deleteCartItem(itemData.id)
      auth.setCartItems(auth.cartItems - 1)
      if (onDelete) {
        onDelete(itemData.id)
      }
    } catch (error) {
      console.log(error)
    }
  }

  const decreaseQuantity = async () => {
    try {
      const result = await updateCartItem(
        item.id,
        Math.max(itemData.quantity - 1, 1)
      )
      setItemData(result.data)
      if (onChange) {
        onChange(result.data)
      }
    } catch (error) {
      setErrorMessage(error.response.data.message)
      setOpen(true)
    }
  }

  const increaseQuantity = async () => {
    try {
      const result = await updateCartItem(item.id, itemData.quantity + 1)
      setItemData(result.data)
      if (onChange) {
        onChange(result.data)
      }
    } catch (error) {
      setErrorMessage(error.response.data.message)
      setOpen(true)
    }
  }
  return (
    <>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={modalStyle}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Ошибка.
          </Typography>
          <Typography id="modal-modal-description" sx={{ mt: 2 }}>
            {errorMessage}
          </Typography>
        </Box>
      </Modal>
      <TableRow>
        <TableCell align="center" sx={{ fontSize: '1.2rem' }}>
          {`${index + 1}`}
        </TableCell>
        <TableCell align="left" sx={{ fontSize: '1.2rem' }}>
          {itemData.product.name}
        </TableCell>
        <TableCell
          style={{ width: 30 }}
          //sx={{ fontSize: '1.3rem' }}
          align="center"
        >
          <IconButton onClick={handleDelete}>
            <Icon icon="fluent:delete-16-regular" color="#ccc" />
          </IconButton>
        </TableCell>
        <TableCell
          style={{ width: 180 }}
          align="center"
          sx={{ fontSize: '1.2rem' }}
        >
          <Button aria-label="reduce" onClick={decreaseQuantity}>
            <RemoveIcon fontSize="small" />
          </Button>
          {itemData.quantity}
          <Button aria-label="increase" onClick={increaseQuantity}>
            <AddIcon fontSize="small" />
          </Button>
        </TableCell>
        <TableCell
          style={{ width: 100 }}
          align="center"
          sx={{ fontSize: '1.2rem' }}
        >
          {itemData.product.price}
        </TableCell>
        <TableCell
          style={{ width: 100 }}
          align="center"
          sx={{ fontSize: '1.2rem' }}
        >
          {itemData.totalPrice}
        </TableCell>
      </TableRow>
    </>
  )
})

CartItemComponent.propTypes = {
  item: PropTypes.object.isRequired,
  index: PropTypes.number,
  handleDelete: PropTypes.func
}
export default CartItemComponent
