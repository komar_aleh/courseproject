import * as React from 'react'
import PropTypes from 'prop-types'
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow
} from '@mui/material'
import CartItemComponent from './CartItemComponent'

const CartList = ({ items, onDelete }) => {
  const handleDelete = () => {
    onDelete && onDelete()
  }
  const [itemsData, setItemsData] = React.useState(items)

  const handleChange = (cartItem) => {
    const newItemsData = items.map((i) => {
      if (i.id === cartItem.id) {
        i.quantity = cartItem.quantity
      }
      return i
    })
    setItemsData(newItemsData)
  }

  return (
    <TableContainer sx={{ minWidth: 800 }}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell sx={{ fontSize: '1.3rem' }} align="center">
              №
            </TableCell>
            <TableCell sx={{ fontSize: '1.3rem' }} align="left">
              Название
            </TableCell>
            <TableCell style={{ width: 30 }} align="center" />
            <TableCell
              style={{ width: 180 }}
              sx={{ fontSize: '1.3rem' }}
              align="center"
            >
              Количество
            </TableCell>
            <TableCell
              style={{ width: 100 }}
              sx={{ fontSize: '1.3rem' }}
              align="center"
            >
              Цена 1 ед.
            </TableCell>
            <TableCell
              style={{ width: 100 }}
              sx={{ fontSize: '1.3rem' }}
              align="center"
            >
              Стоимость
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {itemsData?.map((item, index) => (
            <CartItemComponent
              key={index}
              item={item}
              index={index}
              onDelete={handleDelete}
              onChange={handleChange}
            />
          ))}
          <TableRow>
            <TableCell align="right" colSpan={4}></TableCell>
            <TableCell
              style={{ width: 160 }}
              align="center"
              sx={{ fontSize: '1.5rem' }}
            >
              Итого
            </TableCell>
            <TableCell
              style={{ width: 160 }}
              align="center"
              sx={{ fontSize: '1.5rem' }}
            >
              {itemsData
                .map(({ product, quantity }) => product.price * quantity)
                .reduce((sum, i) => sum + i, 0)
                .toFixed(2)}
              {` руб.`}
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </TableContainer>
  )
}
CartList.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object)
}

export default CartList
