import React from 'react'
import Spinner from '../Spinner'

const ListLoading = (Component) => {
  return function WihLoadingComponent({ isLoading, ...props }) {
    if (!isLoading) return <Component {...props} />
    return <Spinner />
  }
}
export default ListLoading
