import * as React from 'react'
import { uploadImage } from '../../http/imageUploadAPI'
import { Button, IconButton, Paper, Stack, Typography } from '@mui/material'
import PhotoCamera from '@mui/icons-material/PhotoCamera'
import { styled } from '@mui/material/styles'
import { Icon } from '@iconify/react'

const ImageUpload = ({ onUpload }) => {
  const [selectedFile, setSelectedFile] = React.useState(undefined)

  const handleSubmit = async (event) => {
    event.preventDefault()
    try {
      const result = await uploadImage(selectedFile)

      if (onUpload) {
        onUpload(result.imgName)
      }
    } catch (error) {
      console.log(error)
    }
  }

  const handleFileSelect = (event) => {
    setSelectedFile(event.target.files[0])
  }

  const clearSelectedFile = () => {
    setSelectedFile(undefined)
  }

  const Input = styled('input')({
    display: 'none'
  })

  return (
    <Stack
      direction="row"
      alignItems="center"
      spacing={2}
      sx={{ paddingLeft: 4 }}
    >
      <label htmlFor="contained-button-file">
        <Input
          accept="image/*"
          id="contained-button-file"
          type="file"
          onChange={handleFileSelect}
        />
        <Button
          variant="contained"
          component="span"
          startIcon={<PhotoCamera />}
        >
          Выберите изображение
        </Button>
      </label>
      <Paper variant="outlined" sx={{ p: 2, margin: '5px 0 5px', width: 250 }}>
        <Typography>{selectedFile ? selectedFile.name : null}</Typography>
      </Paper>
      <Button
        className="btn-upload"
        color="primary"
        variant="contained"
        component="span"
        disabled={!selectedFile}
        onClick={handleSubmit}
      >
        Upload
      </Button>
      {selectedFile && (
        <IconButton onClick={clearSelectedFile}>
          <Icon icon="akar-icons:cross" />
        </IconButton>
      )}
    </Stack>
  )
}

export default ImageUpload
