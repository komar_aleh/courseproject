import * as React from 'react'
import './spinner.scss'

const Spinner = () => {
  return (
    <div className="loading-screen">
      <div className="loading">
        <span>Загрузка...</span>
      </div>
    </div>
  )
}
export default Spinner
