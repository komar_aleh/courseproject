import * as React from 'react'
import { Typography } from '@mui/material'
import { BicycleListItem } from './BicycleListItem'
import PropTypes from 'prop-types'

const BicycleList = (props) => {
  const { items } = props
  if (!items || items.length === 0)
    return (
      <Typography
        gutterBottom
        variant="h5"
        component="div"
        sx={{ display: 'flex', p: 2, margin: '20px' }}
      >
        Велосипедов, удовлетворяющим условиям поиска, у нас нет. Попробуйте
        изменить запрос...
      </Typography>
    )
  return (
    <>
      {props.items.map((item, index) => (
        <BicycleListItem item={item} key={item.id} index={index} />
      ))}
    </>
  )
}
BicycleList.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object)
}

export default BicycleList
