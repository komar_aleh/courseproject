import { IMG_SM } from '../../config'
import * as React from 'react'
import {
  Button,
  ButtonBase,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Grid,
  Paper,
  Typography
} from '@mui/material'
import PropTypes from 'prop-types'
import { styled } from '@mui/material/styles'
import ShoppingCartRoundedIcon from '@mui/icons-material/ShoppingCartRounded'
import { Link } from 'react-router-dom'
import useAuth from '../../hooks/useAuth/index'
import { createCartItem } from '../../http/cartAPI'

const ElLink = styled(Link)`
  color: #000000;
  align-items: center;
  text-decoration: none;
  padding: 0;
  height: 100%;
  cursor: pointer;

  &.active {
    color: #ff0000;
    text-decoration: none;
  }

  &:hover {
    color: #556cd6;
    text-decoration: none;
  }
  }
`
const Img = styled('img')({
  margin: 'auto',
  display: 'block',
  maxWidth: '100%',
  maxHeight: '100%'
})

export const BicycleListItem = React.memo(({ item }) => {
  const auth = useAuth()

  const [open, setOpen] = React.useState(false)
  const handleClose = () => {
    setOpen(false)
  }

  const handleAddToCart = (id) => () => {
    createCartItem(id, 1).then(() => {
      setOpen(true)
      auth.setCartItems(auth.cartItems + 1)
    })
  }
  return (
    <Paper variant="outlined" sx={{ p: 3, margin: '5px 0 5px', flexGrow: 1 }}>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          Товар успешно добавлен в корзину.
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Вы можете продолжить покупки или перейти в корзину для оформления
            заказа.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button href="/cart">Перейти в корзину</Button>
          <Button onClick={handleClose} autoFocus>
            Продложить покупки
          </Button>
        </DialogActions>
      </Dialog>
      <Grid container spacing={2}>
        <Grid item>
          <Link to={`/bike/${item.id}`}>
            <ButtonBase sx={{ width: 150, height: 150 }}>
              <Img alt="photo" src={`${IMG_SM}${item.imgName}`} />
            </ButtonBase>
          </Link>
        </Grid>
        <Grid item xs={12} sm container>
          <Grid item xs container direction="column" spacing={2}>
            <Grid item xs>
              <ElLink to={`/bike/${item.id}`}>
                <Typography gutterBottom variant="h5" component="div">
                  {item.name}
                </Typography>
              </ElLink>
              <Typography variant="body1" gutterBottom>
                {item.description}
              </Typography>
            </Grid>
            {auth.authed &&
              (!auth.isAdmin ? (
                <Grid item>
                  <Typography sx={{ cursor: 'pointer' }} variant="body2">
                    <Button
                      variant="outlined"
                      onClick={handleAddToCart(item.id)}
                      startIcon={<ShoppingCartRoundedIcon />}
                    >
                      В корзину!
                    </Button>
                  </Typography>
                </Grid>
              ) : (
                <></>
              ))}
          </Grid>
          <Grid item>
            <Typography variant="h6" component="div" justifyContent="center">
              {item.price} руб
            </Typography>
          </Grid>
        </Grid>
      </Grid>
    </Paper>
  )
})

BicycleListItem.propTypes = {
  item: PropTypes.object.isRequired,
  index: PropTypes.number
}
