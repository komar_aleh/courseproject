import { IMG_MD, IMG_LG } from '../../config'
import {
  Backdrop,
  Box,
  Button,
  ButtonBase,
  Checkbox,
  Container,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Grid,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Typography
} from '@mui/material'
import * as React from 'react'
import { useParams, Link } from 'react-router-dom'
import { getBycicle } from '../../http/productAPI'
import { styled } from '@mui/material/styles'
import ShoppingCartRoundedIcon from '@mui/icons-material/ShoppingCartRounded'
import Spinner from '../Spinner'
import useAuth from '../../hooks/useAuth'
import { createCartItem } from '../../http/cartAPI'

const Img = styled('img')({
  margin: 'auto',
  display: 'block',
  maxWidth: '100%',
  maxHeight: '100%'
})

const MyCheckbox = styled(Checkbox)(() => ({
  padding: 0,
  margin: 0
}))

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  backgroundColor: theme.palette.action.hover
}))

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  height: '100vh',
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 5,
  p: 1
}

const Bike = () => {
  const auth = useAuth()
  const { id } = useParams()
  const [absorberStroke, setAbsorberStroke] = React.useState()
  const [cassette, setCassette] = React.useState()
  const [chain, setChain] = React.useState()
  const [classOfBike, setClassOfBike] = React.useState()
  const [commonDate, setCommonDate] = React.useState(0)
  const [forkMaterial, setForkMaterial] = React.useState()
  const [forkModel, setForkModel] = React.useState()
  const [forkType, setForkType] = React.useState()
  const [frameColor, setFrameColor] = React.useState()
  const [frameMaterial, setFrameMaterial] = React.useState()
  const [frameSize, setFrameSize] = React.useState()
  const [frontBrakeModel, setFrontBrakeModel] = React.useState()
  const [frontBrakeType, setFrontBrakeType] = React.useState()
  const [frontStar, setFrontStar] = React.useState()
  const [frontSwitch, setFrontSwitch] = React.useState()
  const [gripsModel, setGripsModel] = React.useState()
  const [handlebarModel, setHandlebarModel] = React.useState()
  const [handlebarType, setHandlebarType] = React.useState()
  const [manufacturer, setManufacturer] = React.useState()
  const [pedalsMaterial, setPedalsMaterial] = React.useState()
  const [pedalsModel, setPedalsModel] = React.useState()
  const [pedalsType, setPedalsType] = React.useState()
  const [rearBrakeModel, setRearBrakeModel] = React.useState()
  const [rearBrakeType, setRearBrakeType] = React.useState()
  const [rearStar, setRearStar] = React.useState()
  const [rearSwitch, setRearSwitch] = React.useState()
  const [rimMaterial, setRimMaterial] = React.useState()
  const [saddleModel, setSaddleModel] = React.useState()
  const [saddleType, setSaddleType] = React.useState()
  const [shifterModel, setShifterModel] = React.useState()
  const [shifterType, setShifterType] = React.useState()
  const [speedNumber, setSpeedNumber] = React.useState()
  const [systemModel, setSystemModel] = React.useState()
  const [tiresModel, setTiresModel] = React.useState()
  const [wheelDiameter, setWheelDiameter] = React.useState()
  const [hardTail, setHardTail] = React.useState()
  const [female, setFemale] = React.useState()
  const [kidTeen, setKidTeen] = React.useState()
  const [doubleRim, setDoubleRim] = React.useState()
  const [tiresTubeless, setTiresTubeless] = React.useState()
  const [forkLock, setForkLock] = React.useState()
  const [description, setDescription] = React.useState()
  const [imgName, setImgName] = React.useState('')
  const [name, setName] = React.useState('')
  const [price, setPrice] = React.useState()
  const [weight, setWeight] = React.useState()

  const [isLoading, setIsLoading] = React.useState(false)

  const getBikeDetails = async () => {
    setIsLoading(true)
    await getBycicle(id).then((response) => {
      const bikeData = response.data
      bikeData.absorberStroke &&
        setAbsorberStroke(bikeData.absorberStroke.value)
      bikeData.cassette && setCassette(bikeData.cassette.value)
      bikeData.chain && setChain(bikeData.chain.value)
      bikeData.classOfBike && setClassOfBike(bikeData.classOfBike.value)
      bikeData.commonDate && setCommonDate(bikeData.commonDate.value)
      bikeData.forkMaterial && setForkMaterial(bikeData.forkMaterial.value)
      bikeData.forkModel && setForkModel(bikeData.forkModel.value)
      bikeData.forkType && setForkType(bikeData.forkType.value)
      bikeData.frameColor && setFrameColor(bikeData.frameColor.value)
      bikeData.frameMaterial && setFrameMaterial(bikeData.frameMaterial.value)
      bikeData.frameSize && setFrameSize(bikeData.frameSize.value)
      bikeData.frontBrakeModel &&
        setFrontBrakeModel(bikeData.frontBrakeModel.value)
      bikeData.frontBrakeType &&
        setFrontBrakeType(bikeData.frontBrakeType.value)
      bikeData.frontStar && setFrontStar(bikeData.frontStar.value)
      bikeData.frontSwitch && setFrontSwitch(bikeData.frontSwitch.value)
      bikeData.gripsModel && setGripsModel(bikeData.gripsModel.value)
      bikeData.handlebarModel &&
        setHandlebarModel(bikeData.handlebarModel.value)
      bikeData.handlebarType && setHandlebarType(bikeData.handlebarType.value)
      bikeData.manufacturer && setManufacturer(bikeData.manufacturer.value)
      bikeData.pedalsMaterial &&
        setPedalsMaterial(bikeData.pedalsMaterial.value)
      bikeData.pedalsModel && setPedalsModel(bikeData.pedalsModel.value)
      bikeData.pedalsType && setPedalsType(bikeData.pedalsType.value)
      bikeData.rearBrakeModel &&
        setRearBrakeModel(bikeData.rearBrakeModel.value)
      bikeData.rearBrakeType && setRearBrakeType(bikeData.rearBrakeType.value)
      bikeData.rearStar && setRearStar(bikeData.rearStar.value)
      bikeData.rearSwitch && setRearSwitch(bikeData.rearSwitch.value)
      bikeData.rimMaterial && setRimMaterial(bikeData.rimMaterial.value)
      bikeData.saddleModel && setSaddleModel(bikeData.saddleModel.value)
      bikeData.saddleType && setSaddleType(bikeData.saddleType.value)
      bikeData.shifterModel && setShifterModel(bikeData.shifterModel.value)
      bikeData.shifterType && setShifterType(bikeData.shifterType.value)
      bikeData.speedNumber && setSpeedNumber(bikeData.speedNumber.value)
      bikeData.systemModel && setSystemModel(bikeData.systemModel.value)
      bikeData.tiresModel && setTiresModel(bikeData.tiresModel.value)
      bikeData.wheelDiameter && setWheelDiameter(bikeData.wheelDiameter.value)
      setHardTail(bikeData.hardTail)
      setFemale(bikeData.female)
      setKidTeen(bikeData.kidTeen)
      setDoubleRim(bikeData.doubleRim)
      setTiresTubeless(bikeData.tiresTubeless)
      setForkLock(bikeData.forkLock)
      setDescription(bikeData.description)
      setImgName(bikeData.imgName)
      setName(bikeData.name)
      setPrice(bikeData.price)
      setWeight(bikeData.weight)
      setIsLoading(false)
    })
  }

  const [open, setOpen] = React.useState(false)
  const handleClose = () => {
    setOpen(false)
  }

  const [openImg, setOpenImg] = React.useState(false)
  const handleCloseImg = () => {
    setOpenImg(false)
  }
  const handleToggleImg = () => {
    setOpenImg(!open)
  }

  const handleAddToCart = (id) => () => {
    createCartItem(id, 1).then(() => {
      auth.setCartItems(auth.cartItems + 1)
      setOpen(true)
    })
  }

  React.useEffect(() => {
    getBikeDetails()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id])

  if (isLoading) return <Spinner />

  return (
    <Container
      component="main"
      maxWidth="lg"
      sx={{ p: 2, margin: 'auto', flexGrow: 1, minWidth: 400 }}
    >
      <Backdrop
        sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={openImg}
        onClick={handleCloseImg}
      >
        <Box sx={style}>
          <Img
            alt="photo"
            src={`${IMG_LG}${imgName}`}
            onClick={handleToggleImg}
          />
        </Box>
      </Backdrop>
      <Paper variant="outlined" sx={{ p: 3, margin: '5px 0 5px', flexGrow: 1 }}>
        <Grid container spacing={2}>
          <Grid item>
            <ButtonBase>
              {imgName && (
                <Img
                  alt="photo"
                  src={`${IMG_MD}${imgName}`}
                  onClick={handleToggleImg}
                />
              )}
            </ButtonBase>
          </Grid>
          <Grid item xs container wrap="nowrap" direction="column" spacing={2}>
            <Grid item xs>
              <Typography gutterBottom variant="h4" component="div">
                {name}
              </Typography>
              <Typography variant="h6" component="div" gutterBottom>
                {description}
              </Typography>
              <Typography variant="body1" component="div" gutterBottom>
                Производитель: {manufacturer}
              </Typography>
            </Grid>
            <Grid item sx={{ p: 3, margin: '50px' }}>
              <Typography
                variant="h5"
                component="div"
                textAlign="center"
                sx={{ mb: '20px' }}
              >
                Цена: {price} руб
              </Typography>
              <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
              >
                <DialogTitle id="alert-dialog-title">
                  Товар успешно добавлен в корзину.
                </DialogTitle>
                <DialogContent>
                  <DialogContentText id="alert-dialog-description">
                    Вы можете продолжить покупки или перейти в корзину для
                    оформления заказа.
                  </DialogContentText>
                </DialogContent>
                <DialogActions>
                  <Link to="/cart">
                    <Button>Перейти в корзину</Button>
                  </Link>
                  <Button onClick={handleClose} autoFocus>
                    Продложить покупки
                  </Button>
                </DialogActions>
              </Dialog>
              {auth.authed &&
                (!auth.isAdmin ? (
                  <Typography
                    sx={{ cursor: 'pointer' }}
                    variant="body2"
                    textAlign="center"
                  >
                    <Button
                      variant="contained"
                      color="warning"
                      onClick={handleAddToCart(id)}
                      startIcon={<ShoppingCartRoundedIcon />}
                    >
                      В корзину!
                    </Button>
                  </Typography>
                ) : (
                  <></>
                ))}
            </Grid>
          </Grid>
        </Grid>
        <TableContainer sx={{ minWidth: 800 }}>
          <Table>
            <TableBody>
              <StyledTableRow>
                <TableCell
                  align="center"
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  colSpan={2}
                >
                  Описание
                </TableCell>
              </StyledTableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Дата выхода на рынок
                </TableCell>
                <TableCell
                  sx={{
                    fontSize: '1.3rem',
                    border: '1px solid #ddd'
                  }}
                  align="center"
                >
                  {commonDate}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Класс
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {classOfBike}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Женский
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  <MyCheckbox checked={female} disabled />
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Подростковый
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  <MyCheckbox checked={kidTeen} disabled />
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Вес
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {weight}
                </TableCell>
              </TableRow>
              <StyledTableRow>
                <TableCell
                  align="center"
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  colSpan={2}
                >
                  Рама
                </TableCell>
              </StyledTableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Материал рамы
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {frameMaterial}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Размер рамы
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {frameSize}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Цвет рамы
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {frameColor}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Цвет рамы
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {frameColor}
                </TableCell>
              </TableRow>
              <StyledTableRow>
                <TableCell
                  align="center"
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  colSpan={2}
                >
                  Вилка
                </TableCell>
              </StyledTableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Модель вилки
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {forkModel}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Материал вилки
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {forkMaterial}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Тип вилки
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {forkType}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Блокировка вилки
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  <MyCheckbox checked={forkLock} disabled />
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Ход амортизатора
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {absorberStroke}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Задний амортизатор
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  <MyCheckbox checked={hardTail} disabled />
                </TableCell>
              </TableRow>
              <StyledTableRow>
                <TableCell
                  align="center"
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  colSpan={2}
                >
                  Трансмиссия
                </TableCell>
              </StyledTableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Количество скоростей
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {speedNumber}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Система шатунов
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {systemModel}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Количество звезд в системе
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {frontStar}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Модель кассеты или трещотки
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {cassette}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Количество звезд в кассете
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {rearStar}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Передний переключатель
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {frontSwitch}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Задний переключатель
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {rearSwitch}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Цепь
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {chain}
                </TableCell>
              </TableRow>
              <StyledTableRow>
                <TableCell
                  align="center"
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  colSpan={2}
                >
                  Манетки (шифтеры)
                </TableCell>
              </StyledTableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Тип манеток
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {shifterType}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Модель манеток
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {shifterModel}
                </TableCell>
              </TableRow>
              <StyledTableRow>
                <TableCell
                  align="center"
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  colSpan={2}
                >
                  Тормоза
                </TableCell>
              </StyledTableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Тип переднего тормоза
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {frontBrakeType}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Модель переднего тормоза
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {frontBrakeModel}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Тип заднего тормоза
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {rearBrakeType}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Модель заднего тормоза
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {rearBrakeModel}
                </TableCell>
              </TableRow>
              <StyledTableRow>
                <TableCell
                  align="center"
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  colSpan={2}
                >
                  Колеса
                </TableCell>
              </StyledTableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Диаметр колес в дюймах
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {wheelDiameter}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Материал ободьев
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {rimMaterial}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Двойные обода
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  <MyCheckbox checked={doubleRim} disabled />
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Модель покрышек
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {tiresModel}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Бескамерные покрышки
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  <MyCheckbox checked={tiresTubeless} disabled />
                </TableCell>
              </TableRow>
              <StyledTableRow>
                <TableCell
                  align="center"
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  colSpan={2}
                >
                  Руль
                </TableCell>
              </StyledTableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Модель руля
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {handlebarModel}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Тип руля
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {handlebarType}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Грипсы
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {gripsModel}
                </TableCell>
              </TableRow>
              <StyledTableRow>
                <TableCell
                  align="center"
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  colSpan={2}
                >
                  Седло
                </TableCell>
              </StyledTableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Модель седла
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {saddleModel}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Тип седла
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {saddleType}
                </TableCell>
              </TableRow>
              <StyledTableRow>
                <TableCell
                  align="center"
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  colSpan={2}
                >
                  Педали
                </TableCell>
              </StyledTableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Модель педалей
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {pedalsModel}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Материал педалей
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {pedalsMaterial}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  style={{ width: '50%' }}
                  align="left"
                >
                  Тип педалей
                </TableCell>
                <TableCell
                  sx={{ fontSize: '1.3rem', border: '1px solid #ddd' }}
                  align="center"
                >
                  {pedalsType}
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
    </Container>
  )
}

export default Bike
