import * as React from 'react'
import { NavLink, useNavigate } from 'react-router-dom'
import {
  AppBar,
  Badge,
  Box,
  Button,
  Container,
  IconButton,
  Toolbar,
  Tooltip,
  Typography
} from '@mui/material'
import styled from 'styled-components'
import logo from '../../logo.svg'
import { Icon } from '@iconify/react'

import useAuth from '../../hooks/useAuth'

const authLinks = [
  { name: 'Регистрация', path: '/register' },
  { name: 'Войти', path: '/login' }
]

export const Link = styled(NavLink)`
  color: #fff;
  display: flex;
  align-items: center;
  text-decoration: none;
  padding: 0 1rem;
  height: 100%;
  cursor: pointer;

  &.active {
    color: #00008b;
  }

  &:hover {
    color: #00ffff;
  }
`

const NavBar = () => {
  const auth = useAuth()
  const navigate = useNavigate()
  const [values, setValues] = React.useState({
    anchorElNav: null,
    anchorElUser: null
  })

  const handleCloseMenu = (prop) => (event) => {
    setValues({ ...values, [prop]: null })
  }

  const onLogOut = () => {
    auth.logOut()
    navigate('/')
  }

  return (
    <AppBar position="static">
      <Container maxWidth="lg">
        <Toolbar disableGutters>
          <Typography variant="h6" noWrap component="div" sx={{ mr: 2 }}>
            <img src={logo} alt="logo" width="47px" />
          </Typography>
          <Box sx={{ flexGrow: 1, display: 'flex' }}>
            <Button
              color="inherit"
              onClick={handleCloseMenu('anchorElNav')}
              sx={{ my: 2, color: 'white', display: 'block' }}
            >
              <Link
                to="/"
                className={({ isActive }) => (isActive ? 'active' : 'inactive')}
              >
                Главная
              </Link>
            </Button>
          </Box>

          <Box sx={{ flexGrow: 0 }}>
            <Box sx={{ flexGrow: 1, display: 'flex' }}>
              {!auth.authed &&
                authLinks.map((page, index) => (
                  <Button
                    key={index}
                    color="inherit"
                    onClick={handleCloseMenu('anchorElNav')}
                    sx={{ my: 2, color: 'white', display: 'block' }}
                  >
                    <Link
                      key={index}
                      to={page.path}
                      className={({ isActive }) =>
                        isActive ? 'active' : 'inactive'
                      }
                    >
                      {page.name}
                    </Link>
                  </Button>
                ))}
            </Box>
            {auth.authed && (
              <>
                {auth.isAdmin ? (
                  <>
                    <Tooltip title="Администрирование">
                      <IconButton
                        size="large"
                        color="inherit"
                        onClick={() => {
                          navigate('/dashboard')
                        }}
                      >
                        <Icon
                          icon="ic:round-admin-panel-settings"
                          color="white"
                        />
                      </IconButton>
                    </Tooltip>
                  </>
                ) : (
                  <Tooltip title="Корзина">
                    <IconButton
                      size="large"
                      color="inherit"
                      onClick={() => {
                        navigate('/cart')
                      }}
                    >
                      <Badge color="error" badgeContent={auth.cartItems}>
                        <Icon icon="ic:baseline-shopping-cart" color="white" />
                      </Badge>
                    </IconButton>
                  </Tooltip>
                )}
                <Tooltip title="Аккаунт">
                  <IconButton
                    size="large"
                    color="inherit"
                    onClick={() => {
                      navigate('/profile')
                    }}
                  >
                    <Icon icon="bx:bxs-user" color="white" />
                  </IconButton>
                </Tooltip>
                <Tooltip title="Выход">
                  <IconButton size="large" color="inherit" onClick={onLogOut}>
                    <Icon icon="icomoon-free:exit" color="white" />
                  </IconButton>
                </Tooltip>
              </>
            )}
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  )
}
export default NavBar
