import * as React from 'react'
import { Typography } from '@mui/material'
import { PropertyItem } from './PropertyItem'
import PropTypes from 'prop-types'

const PropertyList = (props) => {
  const { items } = props

  const handleDelete = () => {
    props.onDelete && props.onDelete()
  }

  if (!items || items.length === 0)
    return (
      <Typography
        gutterBottom
        variant="h3"
        component="div"
        sx={{ display: 'flex', p: 2, margin: '20px' }}
      >
        Отсутствуют значения. Добавьте новое.
      </Typography>
    )

  return (
    <>
      {props.items.map((item, index) => (
        <PropertyItem
          item={item}
          key={item.id}
          index={index}
          onDelete={handleDelete}
          control={props.control}
        />
      ))}
    </>
  )
}
PropertyList.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object)
}

export default PropertyList
