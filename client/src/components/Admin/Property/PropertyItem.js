import * as React from 'react'
import {
  Alert,
  Collapse,
  Grid,
  IconButton,
  Paper,
  TextField,
  Tooltip,
  Typography
} from '@mui/material'
import CloseIcon from '@mui/icons-material/Close'
import { deleteItem, updateItem } from '../../../http/propertyAPI'
import PropTypes from 'prop-types'
import { styled } from '@mui/material/styles'
import { Icon } from '@iconify/react'

const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body1,
  padding: theme.spacing(2),
  textAlign: 'left',
  fontSize: '1.3rem',
  color: theme.palette.text.primary,
  paddingLeft: theme.spacing(4)
}))

export const PropertyItem = React.memo(({ item, index, onDelete, control }) => {
  const [itemData, setItemData] = React.useState(item)
  const [isEdit, setIsEdit] = React.useState(false)

  const [openError, setOpenError] = React.useState(false)
  const [messageError, setMessageError] = React.useState('')

  const handleDelete = async () => {
    try {
      await deleteItem(control, itemData.id)
      if (onDelete) {
        onDelete(itemData.id)
      }
    } catch (error) {
      setOpenError(true)
      setMessageError(
        error.response.data.message
          ? error.response.data.message
          : error.message
      )
    }
  }

  const handleEdit = async (value) => {
    try {
      setItemData({ ...itemData, value })
    } catch (error) {
      console.log(error)
    }
  }

  const handleSave = async () => {
    try {
      const { data: newItem } = await updateItem(
        control,
        itemData.id,
        itemData.value
      )
      setItemData(newItem)
      setIsEdit(false)
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <>
      <Collapse in={openError}>
        <Alert
          variant="filled"
          severity="error"
          action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              onClick={() => {
                setOpenError(false)
              }}
            >
              <CloseIcon fontSize="inherit" />
            </IconButton>
          }
          sx={{ mb: 2 }}
        >
          {messageError}
        </Alert>
      </Collapse>
      <Item elevation={0} variant="outlined">
        <Grid container spacing={2}>
          <Grid item xs={10}>
            <Grid container spacing={0}>
              <Grid item xs={2}>
                <Typography variant="h4" component="div">
                  {`${index + 1}`}
                </Typography>
              </Grid>
              <Grid item xs={10}>
                {isEdit ? (
                  <TextField
                    autoFocus
                    value={itemData.value}
                    sx={{ '& .MuiTextField-root': { m: 0, p: 0 } }}
                    fullWidth
                    onChange={(e) => handleEdit(e.target.value)}
                  />
                ) : (
                  <Typography variant="h4" component="div">
                    {itemData.value}
                  </Typography>
                )}
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={1}>
            {isEdit ? (
              <Tooltip title="Сохранить">
                <IconButton size="large" onClick={handleSave} color="primary">
                  <Icon
                    icon="ant-design:save-filled"
                    color="blue"
                    width="24"
                    height="24"
                  />
                </IconButton>
              </Tooltip>
            ) : (
              <Tooltip title="Редактировать">
                <IconButton
                  size="large"
                  onClick={(e) => setIsEdit(!isEdit)}
                  color="primary"
                >
                  <Icon
                    icon="ant-design:edit-filled"
                    color="green"
                    width="24"
                    height="24"
                  />
                </IconButton>
              </Tooltip>
            )}
          </Grid>
          <Grid item xs={1}>
            <Tooltip title="Удалить">
              <IconButton size="large" onClick={handleDelete} color="primary">
                <Icon
                  icon="ant-design:delete-filled"
                  color="red"
                  width="24"
                  height="24"
                />
              </IconButton>
            </Tooltip>
          </Grid>
        </Grid>
      </Item>
    </>
  )
})
PropertyItem.propTypes = {
  item: PropTypes.object.isRequired,
  index: PropTypes.number,
  handleDelete: PropTypes.func
}
