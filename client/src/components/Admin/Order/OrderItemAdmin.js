import * as React from 'react'
import {
  Alert,
  Box,
  Button,
  Collapse,
  Grid,
  IconButton,
  Modal,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Tooltip
} from '@mui/material'
import CloseIcon from '@mui/icons-material/Close'
import { updateOrder, deleteOrder } from '../../../http/orderAPI'
import { Icon } from '@iconify/react'

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  minWidth: 500,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 5,
  p: 4,
  m: 2
}

const OrderItemAdmin = React.memo(({ item, onChangeStatus, onDelete }) => {
  const [open, setOpen] = React.useState(false)
  const [isEdit, setIsEdit] = React.useState(false)
  const handleClose = () => setOpen(false)
  const handleOpen = () => setOpen(true)

  const [openError, setOpenError] = React.useState(false)
  const [messageError, setMessageError] = React.useState('')

  const handleCancel = async () => {
    try {
      await updateOrder(item.id, 2)
      if (onChangeStatus) {
        onChangeStatus(item.id)
      }
    } catch (error) {
      console.log(error)
    }
  }

  const handleProcess = async () => {
    try {
      await updateOrder(item.id, 3)
      if (onChangeStatus) {
        onChangeStatus(item.id)
      }
    } catch (error) {
      console.log(error)
    }
  }

  const handleReady = async () => {
    try {
      await updateOrder(item.id, 4)
      if (onChangeStatus) {
        onChangeStatus(item.id)
      }
    } catch (error) {
      console.log(error)
    }
  }

  const handleCompleted = async () => {
    try {
      await updateOrder(item.id, 5)
      if (onChangeStatus) {
        onChangeStatus(item.id)
      }
    } catch (error) {
      console.log(error)
    }
  }

  const handleDelete = async () => {
    try {
      await deleteOrder(item.id)
      if (onDelete) {
        onDelete(item.id)
        console.log('ondelete в items')
      }
    } catch (error) {
      setOpenError(true)
      setMessageError(
        error.response.data.message
          ? error.response.data.message
          : error.message
      )
    }
  }

  return (
    <>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <TableContainer sx={{ p: 1 }}>
            <Table sx={{ boxSizing: 'border-box' }}>
              <TableHead>
                <TableRow>
                  <TableCell colSpan={2} align="center">
                    Информация о заказе №{item.id}
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow>
                  <TableCell
                    style={{ width: '50%' }}
                    sx={{ border: '1px solid #ddd' }}
                    align="center"
                  >
                    Имя получателя
                  </TableCell>
                  <TableCell
                    style={{ width: '50%' }}
                    sx={{ border: '1px solid #ddd' }}
                    align="center"
                  >
                    {item.shippingFirstName}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell
                    style={{ width: '50%' }}
                    sx={{ border: '1px solid #ddd' }}
                    align="center"
                  >
                    Фамилия получателя
                  </TableCell>
                  <TableCell
                    style={{ width: '50%' }}
                    sx={{ border: '1px solid #ddd' }}
                    align="center"
                  >
                    {item.shippingLastName}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell
                    style={{ width: '50%' }}
                    sx={{ border: '1px solid #ddd' }}
                    align="center"
                  >
                    Индекс получателя
                  </TableCell>
                  <TableCell
                    style={{ width: '50%' }}
                    sx={{ border: '1px solid #ddd' }}
                    align="center"
                  >
                    {item.shippingZip}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell
                    style={{ width: '50%' }}
                    sx={{ border: '1px solid #ddd' }}
                    align="center"
                  >
                    Город получателя
                  </TableCell>
                  <TableCell
                    style={{ width: '50%' }}
                    sx={{ border: '1px solid #ddd' }}
                    align="center"
                  >
                    {item.shippingCity}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell
                    style={{ width: '50%' }}
                    sx={{ border: '1px solid #ddd' }}
                    align="center"
                  >
                    Адрес получателя
                  </TableCell>
                  <TableCell
                    style={{ width: '50%' }}
                    sx={{ border: '1px solid #ddd' }}
                    align="center"
                  >
                    {item.shippingAddressLine}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell
                    style={{ width: '50%' }}
                    sx={{ border: '1px solid #ddd' }}
                    align="center"
                  >
                    Телефон получателя
                  </TableCell>
                  <TableCell
                    style={{ width: '50%' }}
                    sx={{ border: '1px solid #ddd' }}
                    align="center"
                  >
                    {item.shippingPhoneNumber}
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
            <Table sx={{ boxSizing: 'border-box' }}>
              <TableHead>
                <TableRow>
                  <TableCell colSpan={3} align="center">
                    Товары в заказе
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow>
                  <TableCell
                    style={{ width: '60%' }}
                    sx={{ border: '1px solid #ddd' }}
                    align="center"
                  >
                    Название товара
                  </TableCell>
                  <TableCell
                    style={{ width: '15%' }}
                    sx={{ border: '1px solid #ddd' }}
                    align="center"
                  >
                    Количество
                  </TableCell>
                  <TableCell
                    style={{ width: '25%' }}
                    sx={{ border: '1px solid #ddd' }}
                    align="center"
                  >
                    Цена 1 ед.
                  </TableCell>
                </TableRow>
                {item.orderItems.map((el, index) => (
                  <TableRow key={index}>
                    <TableCell
                      style={{ width: '60%' }}
                      sx={{ border: '1px solid #ddd' }}
                      align="left"
                    >
                      <Grid container wrap="nowrap">
                        <Grid item xs zeroMinWidth>
                          {el.product.name}
                        </Grid>
                      </Grid>
                    </TableCell>
                    <TableCell
                      style={{ width: '20%' }}
                      sx={{ border: '1px solid #ddd' }}
                      align="center"
                    >
                      {el.quantity}
                    </TableCell>
                    <TableCell
                      style={{ width: '320%' }}
                      sx={{ border: '1px solid #ddd' }}
                      align="center"
                    >
                      {el.product.price} руб
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Box>
      </Modal>

      {openError && (
        <TableRow>
          <TableCell colSpan={6}>
            <Collapse in={openError} fullWidth>
              <Alert
                variant="filled"
                severity="error"
                action={
                  <IconButton
                    aria-label="close"
                    color="inherit"
                    size="small"
                    onClick={() => {
                      setOpenError(false)
                    }}
                  >
                    <CloseIcon fontSize="inherit" />
                  </IconButton>
                }
                sx={{ mb: 2 }}
              >
                {messageError}
              </Alert>
            </Collapse>
          </TableCell>
        </TableRow>
      )}
      <TableRow>
        <TableCell sx={{ fontSize: '1.2rem' }} align="center">
          {item.id}
        </TableCell>
        <TableCell align="center" sx={{ fontSize: '1.2rem' }}>
          {new Date(item.orderDate + 'Z').toLocaleString('ru', {
            year: 'numeric',
            month: 'long',
            day: 'numeric',
            hour: 'numeric',
            minute: '2-digit',
            second: '2-digit'
          })}
        </TableCell>

        {isEdit ? (
          <>
            <TableCell align="center" sx={{ fontSize: '1.2rem' }}>
              <Box
                sx={{
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  ml: 4
                }}
              >
                <Button variant="outlined" fullWidth onClick={handleCancel}>
                  Отменен
                </Button>
                <Button variant="outlined" fullWidth onClick={handleProcess}>
                  В обработке
                </Button>
                <Button variant="outlined" fullWidth onClick={handleReady}>
                  Готов к выдаче
                </Button>
                <Button variant="outlined" fullWidth onClick={handleCompleted}>
                  Завершен
                </Button>
              </Box>
            </TableCell>
            <TableCell
              style={{ width: '5%' }}
              align="center"
              sx={{ fontSize: '1.2rem', p: 1 }}
            >
              <Tooltip title="Отменить изменение статуса">
                <IconButton
                  size="large"
                  onClick={(e) => setIsEdit(!isEdit)}
                  color="primary"
                >
                  <Icon icon="icons8:cancel" color="#ff4f00" />
                </IconButton>
              </Tooltip>
            </TableCell>
          </>
        ) : (
          <>
            <TableCell align="center" sx={{ fontSize: '1.2rem' }}>
              {item.status.value}
              {item.status.id !== 2 && item.status.id !== 5 && (
                <Tooltip title="Редактировать">
                  <IconButton
                    size="large"
                    onClick={(e) => setIsEdit(!isEdit)}
                    color="primary"
                  >
                    <Icon
                      icon="ant-design:edit-filled"
                      color="green"
                      width="24"
                      height="24"
                    />
                  </IconButton>
                </Tooltip>
              )}
            </TableCell>
            <TableCell
              style={{ width: '5%' }}
              align="center"
              sx={{ fontSize: '1.2rem', p: 1 }}
            >
              <Tooltip title="Удалить заказ">
                <IconButton size="large" onClick={handleDelete} color="primary">
                  <Icon
                    icon="ant-design:delete-filled"
                    color="red"
                    width="24"
                    height="24"
                  />
                </IconButton>
              </Tooltip>
            </TableCell>
          </>
        )}

        <TableCell align="center" sx={{ fontSize: '1.2rem' }}>
          {item.totalAmount} руб
        </TableCell>
        <TableCell align="center" sx={{ fontSize: '1.2rem' }}>
          <Button variant="text" onClick={handleOpen}>
            Инфо
          </Button>
        </TableCell>
      </TableRow>
    </>
  )
})

export default OrderItemAdmin
