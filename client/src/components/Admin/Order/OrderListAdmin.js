import * as React from 'react'
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow
} from '@mui/material'
import OrderItemAdmin from './OrderItemAdmin'

const OrderListAdmin = (props) => {
  const handleChangeStatus = () => {
    props.onChangeStatus && props.onChangeStatus()
  }
  const handleDelete = () => {
    props.onDelete && props.onDelete()
  }

  return (
    <TableContainer sx={{ minWidth: 800 }}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell sx={{ fontSize: '1.3rem' }} align="center">
              № заказа
            </TableCell>
            <TableCell sx={{ fontSize: '1.3rem' }} align="center">
              Дата заказа
            </TableCell>

            <TableCell
              colSpan={2}
              sx={{ fontSize: '1.3rem' }}
              align="center"
              style={{ width: '40%' }}
            >
              Статус
            </TableCell>
            <TableCell sx={{ fontSize: '1.3rem' }} align="center">
              Стоимость
            </TableCell>
            <TableCell sx={{ fontSize: '1.3rem' }} align="center"></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.items.map((item, index) => (
            <OrderItemAdmin
              key={index}
              item={item}
              index={index}
              onDelete={handleDelete}
              onChangeStatus={handleChangeStatus}
            />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
}

export default OrderListAdmin
