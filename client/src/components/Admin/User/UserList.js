import * as React from 'react'
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow
} from '@mui/material'
import PropTypes from 'prop-types'
import { UserItem } from './UserItem'

const UserList = (props) => {
  const handleDelete = () => {
    props.onDelete && props.onDelete()
  }

  const handleChangeRole = () => {
    props.onChangeRole && props.onChangeRole()
  }

  return (
    <TableContainer sx={{ minWidth: 800 }}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell
              sx={{ fontSize: '1.3rem' }}
              align="center"
              style={{ width: '10%' }}
            >
              №
            </TableCell>
            <TableCell sx={{ fontSize: '1.3rem' }} align="center">
              Имя
            </TableCell>
            <TableCell sx={{ fontSize: '1.3rem' }} align="center">
              Фамилия
            </TableCell>
            <TableCell sx={{ fontSize: '1.3rem' }} align="center">
              Никнейм
            </TableCell>
            <TableCell sx={{ fontSize: '1.3rem' }} align="center">
              Email
            </TableCell>
            <TableCell sx={{ fontSize: '1.3rem' }} align="center">
              Роль
            </TableCell>
            <TableCell sx={{ fontSize: '1.3rem' }} align="center"></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.items.map((item, index) => (
            <UserItem
              item={item}
              key={index}
              index={index}
              onDelete={handleDelete}
              onChangeRole={handleChangeRole}
            />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
}
UserList.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object)
}

export default UserList
