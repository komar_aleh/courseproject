import * as React from 'react'
import {
  Alert,
  Button,
  Collapse,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  IconButton,
  TableCell,
  TableRow,
  Tooltip
} from '@mui/material'
import CloseIcon from '@mui/icons-material/Close'
import { deleteUser } from '../../../http/userAPI'
import PropTypes from 'prop-types'
import { Icon } from '@iconify/react'
import useAuth from '../../../hooks/useAuth'

export const UserItem = React.memo(({ item, index, onDelete }) => {
  const auth = useAuth()
  const [openError, setOpenError] = React.useState(false)
  const [messageError, setMessageError] = React.useState('')

  const [open, setOpen] = React.useState(false)
  const handleClose = () => {
    setOpen(false)
  }

  const handleDelete = async () => {
    setOpen(false)
    try {
      await deleteUser(item.id)
      if (onDelete) {
        onDelete(item.id)
      }
    } catch (error) {
      setOpenError(true)
      setMessageError(
        error.response.data.message
          ? error.response.data.message
          : error.message
      )
    }
  }

  return (
    <>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          Удаление пользователя.
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Вы действительно собиратесеь удалить пользователя {item.userName}?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleDelete}>Да</Button>
          <Button onClick={handleClose} autoFocus>
            Нет, ошибся...
          </Button>
        </DialogActions>
      </Dialog>
      {openError && (
        <TableRow>
          <TableCell colSpan={6}>
            <Collapse in={openError} fullWidth>
              <Alert
                variant="filled"
                severity="error"
                action={
                  <IconButton
                    aria-label="close"
                    color="inherit"
                    size="small"
                    onClick={() => {
                      setOpenError(false)
                    }}
                  >
                    <CloseIcon fontSize="inherit" />
                  </IconButton>
                }
                sx={{ mb: 2 }}
              >
                {messageError}
              </Alert>
            </Collapse>
          </TableCell>
        </TableRow>
      )}

      <TableRow>
        <TableCell sx={{ fontSize: '1.2rem' }} align="center">
          {`${index + 1}`}
        </TableCell>
        <TableCell align="center" sx={{ fontSize: '1.2rem' }}>
          {item.firstName}
        </TableCell>
        <TableCell align="center" sx={{ fontSize: '1.2rem' }}>
          {item.lastName}
        </TableCell>
        <TableCell align="center" sx={{ fontSize: '1.2rem' }}>
          {item.userName}
        </TableCell>
        <TableCell align="center" sx={{ fontSize: '1.2rem' }}>
          {item.email}
        </TableCell>
        <TableCell align="center" sx={{ fontSize: '1.2rem' }}>
          {item.role}
        </TableCell>
        <TableCell sx={{ fontSize: '1.3rem' }} align="center">
          {auth.userId !== item.id ? (
            <Tooltip title="Удалить">
              <IconButton
                onClick={() => {
                  setOpen(true)
                }}
              >
                <Icon
                  icon="fluent:delete-16-filled"
                  color="red"
                  width="20"
                  height="20"
                />
              </IconButton>
            </Tooltip>
          ) : (
            <></>
          )}
        </TableCell>
      </TableRow>
    </>
  )
})
UserItem.propTypes = {
  item: PropTypes.object.isRequired,
  index: PropTypes.number,
  handleDelete: PropTypes.func
}
