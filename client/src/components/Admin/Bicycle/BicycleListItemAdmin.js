import * as React from 'react'
import {
  Alert,
  Collapse,
  Grid,
  IconButton,
  Paper,
  Tooltip,
  Typography
} from '@mui/material'
import CloseIcon from '@mui/icons-material/Close'
import PropTypes from 'prop-types'
import { styled } from '@mui/material/styles'
import { Link } from 'react-router-dom'
import { Icon } from '@iconify/react'
import { deleteBycicle } from '../../../http/productAPI'

const ElLink = styled(Link)`
  color: #000000;
  align-items: center;
  text-decoration: none;
  padding: 0;
  height: 100%;
  cursor: pointer;

  &.active {
    color: #ff0000;
    text-decoration: none;
  }

  &:hover {
    color: #556cd6;
    text-decoration: none;
  }
  }
`

export const BicycleListItemAdmin = React.memo(({ item, index, onDelete }) => {
  const [openError, setOpenError] = React.useState(false)
  const [messageError, setMessageError] = React.useState('')
  const handleDelete = async () => {
    try {
      await deleteBycicle(item.id)
      if (onDelete) {
        onDelete(item.id)
      }
    } catch (error) {
      setOpenError(true)
      setMessageError(
        error.response.data.message
          ? error.response.data.message
          : error.message
      )
    }
  }
  return (
    <>
      <Collapse in={openError}>
        <Alert
          variant="filled"
          severity="error"
          action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              onClick={() => {
                setOpenError(false)
              }}
            >
              <CloseIcon fontSize="inherit" />
            </IconButton>
          }
          sx={{ mb: 2 }}
        >
          {messageError}
        </Alert>
      </Collapse>
      <Paper variant="outlined" sx={{ p: 3, margin: '5px 0 5px', flexGrow: 1 }}>
        <Grid container direction="row" spacing={2}>
          <Grid item xs={1}>
            <Typography variant="h5" component="div">
              {`${index + 1}`}
            </Typography>
          </Grid>
          <Grid item xs={7}>
            <ElLink to={`/dashboard/bikes/details/${item.id}`}>
              <Typography gutterBottom variant="h5" component="div">
                {item.name}
              </Typography>
            </ElLink>
          </Grid>
          <Grid item xs={2}>
            <Typography variant="h5" component="div" justifyContent="center">
              {item.price} руб
            </Typography>
          </Grid>
          <Grid item xs={1}>
            <Tooltip title="Удалить">
              <IconButton onClick={handleDelete}>
                <Icon
                  icon="fluent:delete-16-filled"
                  color="red"
                  width="20"
                  height="20"
                />
              </IconButton>
            </Tooltip>
          </Grid>
          <Grid item xs={1}>
            <Typography variant="h5" component="div" justifyContent="center">
              {item.quantity} шт.
            </Typography>
          </Grid>
        </Grid>
      </Paper>
    </>
  )
})

BicycleListItemAdmin.propTypes = {
  item: PropTypes.object.isRequired,
  index: PropTypes.number
}
