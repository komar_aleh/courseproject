import * as React from 'react'
import { Typography } from '@mui/material'
import { BicycleListItemAdmin } from './BicycleListItemAdmin'
import PropTypes from 'prop-types'

const BicycleListAdmin = (props) => {
  const { items } = props

  const handleDelete = () => {
    props.onDelete && props.onDelete()
  }

  if (!items || items.length === 0)
    return (
      <Typography
        gutterBottom
        variant="h5"
        component="div"
        sx={{ display: 'flex', p: 2, margin: '20px' }}
      >
        Велосипедов нет. Добавьте новый.
      </Typography>
    )
  return (
    <>
      {props.items.map((item, index) => (
        <BicycleListItemAdmin
          item={item}
          key={item.id}
          index={index}
          onDelete={handleDelete}
        />
      ))}
    </>
  )
}
BicycleListAdmin.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object)
}

export default BicycleListAdmin
