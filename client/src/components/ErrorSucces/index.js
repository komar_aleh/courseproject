import * as React from 'react'
import { Alert, Collapse, IconButton } from '@mui/material'
import CloseIcon from '@mui/icons-material/Close'

const ErrorSuccess = (props) => {
  return (
    <>
      <Collapse in={props.openE}>
        <Alert
          variant="filled"
          severity="error"
          action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              onClick={props.closeE}
            >
              <CloseIcon fontSize="inherit" />
            </IconButton>
          }
          sx={{ mb: 2 }}
        >
          {props.messageE}
        </Alert>
      </Collapse>
      <Collapse in={props.openS}>
        <Alert
          variant="filled"
          severity="success"
          action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              onClick={props.closeS}
            >
              <CloseIcon fontSize="inherit" />
            </IconButton>
          }
          sx={{ mb: 2 }}
        >
          {props.messageS}
        </Alert>
      </Collapse>
    </>
  )
}

export default ErrorSuccess
