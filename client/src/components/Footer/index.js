import * as React from 'react'
import { Typography } from '@mui/material'
import styled from '@emotion/styled'

const FooterContainer = styled.div`
  background-color: #1976d2;
  padding: 15px;
  text-align: center;
  z-index: 1;
`
const Footer = () => {
  return (
    <FooterContainer
      style={{
        color: 'white',
        position: 'fixed',
        bottom: 0,
        textAlign: 'center',
        width: '100%'
      }}
    >
      <Typography variant="title">
        Shop Of Bikes 2021. Все права защищены.
      </Typography>
    </FooterContainer>
  )
}

export default Footer
