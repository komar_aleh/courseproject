import * as React from 'react'
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow
} from '@mui/material'
import OrderItemComponent from './OrderItemComponent'

const OrderList = (props) => {
  const handleCancel = () => {
    props.onCancel && props.onCancel()
  }

  return (
    <TableContainer sx={{ minWidth: 800 }}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell
              //style={{ width: 180 }}
              sx={{ fontSize: '1.3rem' }}
              align="center"
            >
              № заказа
            </TableCell>
            <TableCell sx={{ fontSize: '1.3rem' }} align="center">
              Дата заказа
            </TableCell>

            <TableCell
              //style={{ width: 100 }}
              sx={{ fontSize: '1.3rem' }}
              align="center"
            >
              Статус
            </TableCell>
            <TableCell
              //style={{ width: 100 }}
              sx={{ fontSize: '1.3rem' }}
              align="center"
            >
              Стоимость
            </TableCell>
            <TableCell
              //style={{ width: 100 }}
              sx={{ fontSize: '1.3rem' }}
              align="center"
            ></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.items.map((item, index) => (
            <OrderItemComponent
              key={index}
              item={item}
              index={index}
              onCancel={handleCancel}
            />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
}

export default OrderList
