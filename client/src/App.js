import * as React from 'react'
import Router from './routes/Router'
import AuthProvider from './providers/AuthProvider'

const App = () => {
  return (
    <div>
      <AuthProvider>
        <Router></Router>
      </AuthProvider>
    </div>
  )
}

export default App
