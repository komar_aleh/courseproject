import * as React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { properties } from '../config'
import Activate from '../pages/User/ActivateAccount'
import AdminRoute from './components/AdminRoute'
import Bike from '../components/Bikes/Bike'
import BikeCreate from '../pages/Admin/Bikes/Create/BikeCreate'
import BikeEdit from '../pages/Admin/Bikes/Edit/BikeEdit'
import Cart from '../pages/Cart'
import Checkout from '../pages/CheckOut'
import Confirm from '../pages/User/ConfirmEmail'
import CreateAdmin from '../pages/Admin/Users/CreateAdmin'
import Dashboard from '../pages/Admin'
import DashboardBikes from '../pages/Admin/Bikes'
import Footer from '../components/Footer'
import ForgotPage from '../pages/User/ForgotPassword'
import Home from '../pages/Home'
import ImageUpload from '../components/ImageUpload'
import Login from '../pages/User/Login'
import NavBar from '../components/NavBar'
import NotFound from '../pages/NotFound'
import Orders from '../pages/Admin/Orders'
import Profile from '../pages/User/Profile'
import Properties from '../pages/Admin/Properties'
import Property from '../pages/Admin/Properties/Property'
import Register from '../pages/User/Register'
import ResetPassword from '../pages/User/ResetPassword'
import Spinner from '../components/Spinner'
import useAuth from '../hooks/useAuth'
import UserRoute from './components/UserRoute'
import Users from '../pages/Admin/Users'

const Router = () => {
  const auth = useAuth()
  return auth.isLoaded ? (
    <BrowserRouter>
      <NavBar />
      <Routes>
        <Route path="/" exact element={<Home />} />
        <Route path="/login" element={<Login />} />
        <Route path="/confirm" element={<Confirm />} />
        <Route path="/register" element={<Register />} />
        <Route path="/forgot" element={<ForgotPage />} />
        <Route path="/reset-password" element={<ResetPassword />} />
        <Route path="/activate" element={<Activate />} />
        <Route path="/bike/:id" element={<Bike />} />
        <Route path="/404" element={<NotFound />} />
        <Route path="*" element={<NotFound />} />
        <Route path="/img" element={<ImageUpload />} />
        <Route
          path="/dashboard"
          element={
            <AdminRoute>
              <Dashboard />
            </AdminRoute>
          }
        />

        <Route
          path="/dashboard/bikes"
          element={
            <AdminRoute>
              <DashboardBikes />
            </AdminRoute>
          }
        />

        <Route
          path="/dashboard/bikes/details/:id"
          element={
            <AdminRoute>
              <BikeEdit />
            </AdminRoute>
          }
        />

        <Route
          path="/dashboard/bikes/create"
          element={
            <AdminRoute>
              <BikeCreate />
            </AdminRoute>
          }
        />

        <Route
          path="/dashboard/properties"
          element={
            <AdminRoute>
              <Properties />
            </AdminRoute>
          }
        />
        <Route
          path="/dashboard/orders"
          element={
            <AdminRoute>
              <Orders />
            </AdminRoute>
          }
        />

        <Route
          path="/dashboard/users"
          element={
            <AdminRoute>
              <Users />
            </AdminRoute>
          }
        />

        <Route
          path="/dashboard/users/createAdmin"
          element={
            <AdminRoute>
              <CreateAdmin />
            </AdminRoute>
          }
        />

        {properties.map((item, index) => (
          <Route
            key={index}
            path={`/dashboard/properties/${item.controller}`}
            element={
              <AdminRoute>
                <Property control={item.controller} title={item.label} />
              </AdminRoute>
            }
          />
        ))}

        <Route
          path="/profile"
          element={
            <UserRoute>
              <Profile />
            </UserRoute>
          }
        />

        <Route
          path="/cart"
          element={
            <UserRoute>
              <Cart />
            </UserRoute>
          }
        />
        <Route
          path="/checkout"
          element={
            <UserRoute>
              <Checkout />
            </UserRoute>
          }
        />
      </Routes>
      <Footer />
    </BrowserRouter>
  ) : (
    <Spinner />
  )
}

export default Router
