import * as React from 'react'
import useAuth from '../../../hooks/useAuth'
import { Navigate } from 'react-router-dom'

const UserRoute = ({ children }) => {
  const auth = useAuth()
  return auth.authed ? children : <Navigate replace to="/login" />
}

export default UserRoute
