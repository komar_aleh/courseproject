import * as React from 'react'
import useAuth from '../../../hooks/useAuth'
import { Navigate } from 'react-router-dom'

const AdminRoute = ({ children }) => {
  const auth = useAuth()
  return auth.isAdmin ? children : <Navigate replace to="/login" />
}

export default AdminRoute
