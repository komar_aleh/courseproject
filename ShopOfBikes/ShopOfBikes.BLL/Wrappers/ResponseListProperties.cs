﻿using ShopOfBikes.BLL.DTO.Property;
using System.Collections.Generic;
using System.Linq;

namespace ShopOfBikes.BLL.Wrappers
{
    public class ResponseListProperties
    {
        public List<PropertyIntValueDto> AbsorberStrokes { get; set; }
        public List<PropertyStringValueDto> Cassettes { get; set; }
        public List<PropertyStringValueDto> Chains { get; set; }
        public List<PropertyStringValueDto> ClassOfBikes { get; set; }
        public List<PropertyIntValueDto> CommonDates { get; set; }
        public List<PropertyStringValueDto> ForkMaterials { get; set; }
        public List<PropertyStringValueDto> ForkModels { get; set; }
        public List<PropertyStringValueDto> ForkTypes { get; set; }
        public List<PropertyStringValueDto> FrameColors { get; set; }
        public List<PropertyStringValueDto> FrameMaterials { get; set; }
        public List<PropertyStringValueDto> FrameSizes { get; set; }
        public List<PropertyStringValueDto> FrontBrakeModels { get; set; }
        public List<PropertyStringValueDto> FrontBrakeTypes { get; set; }
        public List<PropertyIntValueDto> FrontStars { get; set; }
        public List<PropertyStringValueDto> FrontSwitches { get; set; }
        public List<PropertyStringValueDto> GripsModels { get; set; }
        public List<PropertyStringValueDto> HandleBarModels { get; set; }
        public List<PropertyStringValueDto> HandleBarTypes { get; set; }
        public List<PropertyStringValueDto> Manufacturers { get; set; }
        public List<PropertyStringValueDto> PedalsMaterials { get; set; }
        public List<PropertyStringValueDto> PedalsModels { get; set; }
        public List<PropertyStringValueDto> PedalsTypes { get; set; }
        public List<PropertyStringValueDto> RearBrakeModels { get; set; }
        public List<PropertyStringValueDto> RearBrakeTypes { get; set; }
        public List<PropertyIntValueDto> RearStars { get; set; }
        public List<PropertyStringValueDto> RearSwitches { get; set; }
        public List<PropertyStringValueDto> RimMaterials { get; set; }
        public List<PropertyStringValueDto> SaddleModels { get; set; }
        public List<PropertyStringValueDto> SaddleTypes { get; set; }
        public List<PropertyStringValueDto> ShifterModels { get; set; }
        public List<PropertyStringValueDto> ShifterTypes { get; set; }
        public List<PropertyIntValueDto> SpeedNumbers { get; set; }
        public List<PropertyStringValueDto> SystemModels { get; set; }
        public List<PropertyStringValueDto> TiresModels { get; set; }
        public List<PropertyFloatValueDto> WheelDiameters { get; set; }
        public decimal MinPrice { get; set; }
        public decimal MaxPrice { get; set; }
        public int MinCommonDate => CommonDates.Min(x => x.Value);
        public int MaxCommonDate => CommonDates.Max(x => x.Value);
        public int MinAbsorberStroke => AbsorberStrokes.Min(x => x.Value);
        public int MaxAbsorberStroke => AbsorberStrokes.Max(x => x.Value);
        public int MinSpeedNumber => SpeedNumbers.Min(x => x.Value);
        public int MaxSpeedNumber => SpeedNumbers.Max(x => x.Value);
        public int MinFrontStar => FrontStars.Min(x => x.Value);
        public int MaxFrontStar => FrontStars.Max(x => x.Value);
        public int MinRearStar => RearStars.Min(x => x.Value);
        public int MaxRearStar => RearStars.Max(x => x.Value);
        public float? MinWeight { get; set; }
        public float? MaxWeight { get; set; }
        public float? MinWheelDiameter => WheelDiameters.Min(x => x.Value);
        public float? MaxWheelDiameter => WheelDiameters.Max(x => x.Value);

        public ResponseListProperties(
            List<PropertyIntValueDto> absorberStrokes,
            List<PropertyStringValueDto> cassettes,
            List<PropertyStringValueDto> chains,
            List<PropertyStringValueDto> classOfBikes,
            List<PropertyIntValueDto> commonDates,
            List<PropertyStringValueDto> forkMaterials,
            List<PropertyStringValueDto> forkModels,
            List<PropertyStringValueDto> forkTypes,
            List<PropertyStringValueDto> frameColors,
            List<PropertyStringValueDto> frameMaterials,
            List<PropertyStringValueDto> frameSizes,
            List<PropertyStringValueDto> frontBrakeModels,
            List<PropertyStringValueDto> frontBrakeTypes,
            List<PropertyIntValueDto> frontStars,
            List<PropertyStringValueDto> frontSwitches,
            List<PropertyStringValueDto> gripsModels,
            List<PropertyStringValueDto> handleBarModels,
            List<PropertyStringValueDto> handleBarTypes,
            List<PropertyStringValueDto> manufacturers,
            List<PropertyStringValueDto> pedalsMaterials,
            List<PropertyStringValueDto> pedalsModels,
            List<PropertyStringValueDto> pedalsTypes,
            List<PropertyStringValueDto> rearBrakeModels,
            List<PropertyStringValueDto> rearBrakeTypes,
            List<PropertyIntValueDto> rearStars,
            List<PropertyStringValueDto> rearSwitches,
            List<PropertyStringValueDto> rimMaterials,
            List<PropertyStringValueDto> saddleModels,
            List<PropertyStringValueDto> saddleTypes,
            List<PropertyStringValueDto> shifterModels,
            List<PropertyStringValueDto> shifterTypes,
            List<PropertyIntValueDto> speedNumbers,
            List<PropertyStringValueDto> systemModels,
            List<PropertyStringValueDto> tiresModels,
            List<PropertyFloatValueDto> wheelDiameters,
            decimal minPrice,
            decimal maxPrice,
            float? minWeight,
            float? maxWeight
        )
        {
            AbsorberStrokes = absorberStrokes;
            Cassettes = cassettes;
            Chains = chains;
            ClassOfBikes = classOfBikes;
            CommonDates = commonDates;
            ForkMaterials = forkMaterials;
            ForkModels = forkModels;
            ForkTypes = forkTypes;
            FrameColors = frameColors;
            FrameMaterials = frameMaterials;
            FrameSizes = frameSizes;
            FrontBrakeModels = frontBrakeModels;
            FrontBrakeTypes = frontBrakeTypes;
            FrontStars = frontStars;
            FrontSwitches = frontSwitches;
            GripsModels = gripsModels;
            HandleBarModels = handleBarModels;
            HandleBarTypes = handleBarTypes;
            Manufacturers = manufacturers;
            PedalsMaterials = pedalsMaterials;
            PedalsModels = pedalsModels;
            PedalsTypes = pedalsTypes;
            RearBrakeModels = rearBrakeModels;
            RearBrakeTypes = rearBrakeTypes;
            RearStars = rearStars;
            RearSwitches = rearSwitches;
            RimMaterials = rimMaterials;
            SaddleModels = saddleModels;
            SaddleTypes = saddleTypes;
            ShifterModels = shifterModels;
            ShifterTypes = shifterTypes;
            SpeedNumbers = speedNumbers;
            SystemModels = systemModels;
            TiresModels = tiresModels;
            WheelDiameters = wheelDiameters;
            MinPrice = minPrice;
            MaxPrice = maxPrice;
            MinWeight = minWeight;
            MaxWeight = maxWeight;
        }
    }
}
