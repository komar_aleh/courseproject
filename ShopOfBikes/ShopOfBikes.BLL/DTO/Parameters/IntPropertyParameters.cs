﻿namespace ShopOfBikes.BLL.DTO.Parameters
{
    public class IntPropertyParameters
    {
        public int? Value { get; set; }
        public string OrderBy { get; set; } = "Value";
    }
}
