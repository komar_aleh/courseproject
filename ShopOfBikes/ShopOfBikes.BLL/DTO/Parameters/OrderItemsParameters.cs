﻿namespace ShopOfBikes.BLL.DTO.Parameters
{
    public class OrderItemsParameters
    {
        public string OrderBy { get; set; } = "Id";
    }
}
