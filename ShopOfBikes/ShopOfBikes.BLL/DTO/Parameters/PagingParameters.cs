﻿namespace ShopOfBikes.BLL.DTO.Parameters
{
    public class PagingParameters
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        private const int maxPageSize = 200;

        public PagingParameters()
        {
            PageNumber = 1;
            PageSize = 10;
        }

        public PagingParameters(int pageNumber, int pageSize)
        {
            PageNumber = pageNumber < 1 ? 1 : pageNumber;
            PageSize = pageSize > maxPageSize ? maxPageSize : pageSize;
        }
    }
}