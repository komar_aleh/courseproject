﻿namespace ShopOfBikes.BLL.DTO.Parameters
{
    public class UsersParameters
    {
        public string UserName { get; set; }
        public string OrderBy { get; set; } = "UserName";
        public string Role { get; set; }
    }
}
