﻿namespace ShopOfBikes.BLL.DTO.Parameters
{
    public class StringPropertyParameters
    {
        public string Value { get; set; }
        public string OrderBy { get; set; } = "Value";
    }
}
