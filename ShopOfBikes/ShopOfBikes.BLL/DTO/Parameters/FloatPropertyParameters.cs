﻿namespace ShopOfBikes.BLL.DTO.Parameters
{
    public class FloatPropertyParameters
    {
        public float? Value { get; set; }
        public string OrderBy { get; set; } = "Value";
    }
}
