﻿namespace ShopOfBikes.BLL.DTO.Parameters
{
    public class OrdersParameters
    {
        public string OrderBy { get; set; } = "OrderDate desc";
        public string UserId { get; set; }
        public int StatusId { get; set; }
    }
}
