﻿namespace ShopOfBikes.BLL.DTO.Parameters
{
    public class ProductsParameters
    {
        public decimal? MinPrice { get; set; }
        public decimal? MaxPrice { get; set; }

        public int? MinCommonDate { get; set; }
        public int? MaxCommonDate { get; set; }

        public int? MinAbsorberStroke { get; set; }
        public int? MaxAbsorberStroke { get; set; }

        public int? MinSpeedNumber { get; set; }
        public int? MaxSpeedNumber { get; set; }

        public int? MinFrontStar { get; set; }
        public int? MaxFrontStar { get; set; }

        public int? MinRearStar { get; set; }
        public int? MaxRearStar { get; set; }

        public float? MinWeight { get; set; }
        public float? MaxWeight { get; set; }

        public float? MinWheelDiameter { get; set; }
        public float? MaxWheelDiameter { get; set; }

        public int? CassetteId { get; set; }
        public int? ChainId { get; set; }
        public int? ClassOfBikeId { get; set; }
        public int? ForkMaterialId { get; set; }
        public int? ForkModelId { get; set; }
        public int? ForkTypeId { get; set; }
        public int? FrameColorId { get; set; }
        public int? FrameMaterialId { get; set; }
        public int? FrameSizeId { get; set; }
        public int? FrontBrakeModelId { get; set; }
        public int? FrontBrakeTypeId { get; set; }
        public int? FrontSwitchId { get; set; }
        public int? GripsModelId { get; set; }
        public int? HandlebarModelId { get; set; }
        public int? HandlebarTypeId { get; set; }
        public int? ManufacturerId { get; set; }
        public int? PedalsMaterialId { get; set; }
        public int? PedalsModelId { get; set; }
        public int? PedalsTypeId { get; set; }
        public int? RearBrakeModelId { get; set; }
        public int? RearBrakeTypeId { get; set; }
        public int? RearSwitchId { get; set; }
        public int? RimMaterialId { get; set; }
        public int? SaddleModelId { get; set; }
        public int? SaddleTypeId { get; set; }
        public int? ShifterModelId { get; set; }
        public int? ShifterTypeId { get; set; }
        public int? SystemModelId { get; set; }
        public int? TiresModelId { get; set; }

        public bool? HardTail { get; set; }
        public bool? Female { get; set; }
        public bool? KidTeen { get; set; }
        public bool? DoubleRim { get; set; }
        public bool? TiresTubeless { get; set; }
        public bool? ForkLock { get; set; }
        public string OrderBy { get; set; } = "Manufacturer";
        public string OrderByThen { get; set; } = "Name";
        public string SearchName { get; set; }
        public bool? IsAvailable { get; set; } = true;
    }
}
