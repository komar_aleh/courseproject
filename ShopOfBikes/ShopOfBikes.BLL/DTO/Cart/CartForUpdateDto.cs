﻿namespace ShopOfBikes.BLL.DTO.Cart
{
    public class CartForUpdateDto
    {
        public uint Quantity { get; set; }
    }
}
