﻿using ShopOfBikes.BLL.DTO.Product;

namespace ShopOfBikes.BLL.DTO.Cart
{
    public class CartDto
    {
        public int Id { get; set; }
        public int ProductId { get; set; }

        public ProductDetailsDto Product { get; set; }

        public uint Quantity { get; set; }

        public decimal TotalPrice { get; set; }
    }
}
