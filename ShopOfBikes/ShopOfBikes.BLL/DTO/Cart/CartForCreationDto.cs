﻿namespace ShopOfBikes.BLL.DTO.Cart
{
    public class CartForCreationDto
    {
        public int ProductId { get; set; }

        public uint Quantity { get; set; }
    }
}
