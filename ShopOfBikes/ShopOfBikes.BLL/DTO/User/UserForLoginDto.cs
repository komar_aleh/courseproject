﻿namespace ShopOfBikes.BLL.DTO.User
{
    public class UserForLoginDto
    {
        public string UserName { get; set; }

        public string Password { get; set; }
    }
}
