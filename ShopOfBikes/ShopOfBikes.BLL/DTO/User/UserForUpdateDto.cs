﻿namespace ShopOfBikes.BLL.DTO.User
{
    public class UserForUpdateDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
