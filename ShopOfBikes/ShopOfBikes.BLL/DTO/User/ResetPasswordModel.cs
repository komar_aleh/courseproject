﻿namespace ShopOfBikes.BLL.DTO.User
{
    public class ResetPasswordModel
    {
        public string UserId { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }

        public string Token { get; set; }
    }
}