﻿namespace ShopOfBikes.BLL.DTO.User
{
    public class SendEmailModel
    {
        public string Email { get; set; }
    }
}
