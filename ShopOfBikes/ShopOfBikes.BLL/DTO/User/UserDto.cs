﻿using ShopOfBikes.BLL.DTO.Order;
using System.Collections.Generic;

namespace ShopOfBikes.BLL.DTO.User
{
    public class UserDto
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string Role { get; set; }

        public List<OrderDto> Orders { get; set; }
    }
}
