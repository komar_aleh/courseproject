﻿using ShopOfBikes.BLL.DTO.OrderItem;
using ShopOfBikes.BLL.DTO.Property;
using System.Collections.Generic;

namespace ShopOfBikes.BLL.DTO.Order
{
    public class OrderDto
    {
        public int Id { get; set; }
        public string OrderDate { get; set; }
        public string UserId { get; set; }
        public PropertyStringValueDto Status { get; set; }
        public string ShippingFirstName { get; set; }
        public string ShippingLastName { get; set; }
        public string ShippingZip { get; set; }
        public string ShippingCity { get; set; }
        public string ShippingAddressLine { get; set; }
        public string ShippingPhoneNumber { get; set; }
        public List<OrderItemDto> OrderItems { get; set; }
        public decimal TotalAmount { get; set; }
    }
}
