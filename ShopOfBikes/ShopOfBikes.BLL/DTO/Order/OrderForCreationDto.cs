﻿using System.ComponentModel;

namespace ShopOfBikes.BLL.DTO.Order
{
    public class OrderForCreationDto
    {
        public string ShippingFirstName { get; set; }
        public string ShippingLastName { get; set; }
        [DefaultValue("230000")]
        public string ShippingZip { get; set; }
        public string ShippingCity { get; set; }
        public string ShippingAddressLine { get; set; }
        [DefaultValue("+375291234567")]
        public string ShippingPhoneNumber { get; set; }
    }
}
