﻿using ShopOfBikes.BLL.DTO.Property;

namespace ShopOfBikes.BLL.DTO.Product
{
    public class ProductDetailsDto
    {
        public int Id { get; set; }
        public string DateCreated { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public uint Quantity { get; set; }
        public decimal Price { get; set; }
        public string ImgName { get; set; }
        public bool IsAvailable { get; set; }
        public PropertyStringValueDto Manufacturer { get; set; }
        public PropertyStringValueDto ClassOfBike { get; set; }
        public PropertyIntValueDto CommonDate { get; set; }
        public float Weight { get; set; }
        public PropertyStringValueDto FrameMaterial { get; set; }
        public PropertyStringValueDto FrameSize { get; set; }
        public PropertyStringValueDto ForkMaterial { get; set; }
        public PropertyFloatValueDto WheelDiameter { get; set; }
        public PropertyStringValueDto FrameColor { get; set; }
        public PropertyStringValueDto ForkModel { get; set; }
        public PropertyStringValueDto ForkType { get; set; }
        public PropertyIntValueDto AbsorberStroke { get; set; }
        public PropertyIntValueDto SpeedNumber { get; set; }
        public PropertyStringValueDto SystemModel { get; set; }
        public PropertyIntValueDto FrontStar { get; set; }
        public PropertyIntValueDto RearStar { get; set; }
        public PropertyStringValueDto Cassette { get; set; }
        public bool HardTail { get; set; }
        public bool Female { get; set; }
        public bool KidTeen { get; set; }
        public bool DoubleRim { get; set; }
        public bool TiresTubeless { get; set; }
        public bool ForkLock { get; set; }
        public PropertyStringValueDto FrontSwitch { get; set; }
        public PropertyStringValueDto RearSwitch { get; set; }
        public PropertyStringValueDto Chain { get; set; }
        public PropertyStringValueDto ShifterModel { get; set; }
        public PropertyStringValueDto ShifterType { get; set; }
        public PropertyStringValueDto FrontBrakeModel { get; set; }
        public PropertyStringValueDto FrontBrakeType { get; set; }
        public PropertyStringValueDto RearBrakeModel { get; set; }
        public PropertyStringValueDto RearBrakeType { get; set; }
        public PropertyStringValueDto RimMaterial { get; set; }
        public PropertyStringValueDto TiresModel { get; set; }
        public PropertyStringValueDto HandlebarModel { get; set; }
        public PropertyStringValueDto HandlebarType { get; set; }
        public PropertyStringValueDto GripsModel { get; set; }
        public PropertyStringValueDto SaddleType { get; set; }
        public PropertyStringValueDto SaddleModel { get; set; }
        public PropertyStringValueDto PedalsType { get; set; }
        public PropertyStringValueDto PedalsModel { get; set; }
        public PropertyStringValueDto PedalsMaterial { get; set; }
    }
}
