﻿namespace ShopOfBikes.BLL.DTO.Property
{
    public class PropertyStringValueForCreationUpdateDto
    {
        public string Value { get; set; }
    }
}
