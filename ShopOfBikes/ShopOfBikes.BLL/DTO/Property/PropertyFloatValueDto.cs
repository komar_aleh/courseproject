﻿namespace ShopOfBikes.BLL.DTO.Property
{
    public class PropertyFloatValueDto
    {
        public int Id { get; set; }
        public float Value { get; set; }
    }
}
