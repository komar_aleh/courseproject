﻿namespace ShopOfBikes.BLL.DTO.Property
{
    public class PropertyIntValueDto
    {
        public int Id { get; set; }
        public int Value { get; set; }
    }
}
