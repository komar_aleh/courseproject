﻿namespace ShopOfBikes.BLL.DTO.Property
{
    public class PropertyFloatValueForCreationUpdateDto
    {
        public float Value { get; set; }
    }
}
