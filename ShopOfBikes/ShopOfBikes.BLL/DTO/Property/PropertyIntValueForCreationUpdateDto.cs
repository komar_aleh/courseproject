﻿namespace ShopOfBikes.BLL.DTO.Property
{
    public class PropertyIntValueForCreationUpdateDto
    {
        public int Value { get; set; }
    }
}
