﻿using ShopOfBikes.BLL.DTO.Product;

namespace ShopOfBikes.BLL.DTO.OrderItem
{
    public class OrderItemDto
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int OrderId { get; set; }
        public uint Quantity { get; set; }
        public ProductDetailsDto Product { get; set; }
    }
}
