﻿namespace ShopOfBikes.BLL.DTO.OrderItem
{
    public class OrderItemForUpdateDto
    {
        public int ProductId { get; set; }
        public uint Quantity { get; set; }
    }
}
