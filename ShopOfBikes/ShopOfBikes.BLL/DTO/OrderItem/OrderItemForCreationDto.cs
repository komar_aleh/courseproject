﻿namespace ShopOfBikes.BLL.DTO.OrderItem
{
    public class OrderItemForCreationDto
    {
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public uint Quantity { get; set; }
    }
}
