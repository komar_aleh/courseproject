﻿using CodeCarvings.Piczard;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using ShopOfBikes.BLL.Exceptions;
using ShopOfBikes.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using static System.IO.Path;

namespace ShopOfBikes.BLL.Services
{
    public class FileService : IFileService
    {
        private readonly int _sizeS;
        private readonly int _sizeM;
        private readonly int _sizeL;
        private readonly int _sizeLimit;
        private readonly string _folderLargeName;
        private readonly string _folderMediumName;
        private readonly string _folderSmallName;

        private readonly string _imagePath;

        private const string Ext = ".jpg";

        private static readonly Dictionary<string, List<byte[]>> FileSignature = new()
        {
            { ".gif", new List<byte[]> { new byte[] { 0x47, 0x49, 0x46, 0x38 } } },
            { ".png", new List<byte[]> { new byte[] { 0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A } } },
            {
                ".jpeg",
                new List<byte[]>
                {
                    new byte[] { 0xFF, 0xD8, 0xFF, 0xE0 },
                    new byte[] { 0xFF, 0xD8, 0xFF, 0xE2 },
                    new byte[] { 0xFF, 0xD8, 0xFF, 0xE3 },
                }
            },
            {
                ".jpg",
                new List<byte[]>
                {
                    new byte[] { 0xFF, 0xD8, 0xFF, 0xE0 },
                    new byte[] { 0xFF, 0xD8, 0xFF, 0xE1 },
                    new byte[] { 0xFF, 0xD8, 0xFF, 0xE8 },
                }
            }
        };
        public FileService(IConfiguration configuration)
        {
            _imagePath = Combine(
                configuration.GetValue<string>("ImageFolder1"),
                configuration.GetValue<string>("ImageFolder2"),
                configuration.GetValue<string>("ImageFolder3"));
            _sizeS = configuration.GetValue<int>("ImageSizes:Small");
            _sizeM = configuration.GetValue<int>("ImageSizes:Medium");
            _sizeL = configuration.GetValue<int>("ImageSizes:Large");
            _sizeLimit = configuration.GetValue<int>("SizeLimit");
            _folderLargeName = Combine(_imagePath, "LargeImages");
            _folderMediumName = Combine(_imagePath, "MediumImages");
            _folderSmallName = Combine(_imagePath, "SmallImages");
        }

        private static bool IsValidSignature(IFormFile file)
        {
            using var sourceImage = ImageArchiver.LoadImage(file.OpenReadStream());
            if (sourceImage.Size.Height > 5000 | sourceImage.Size.Width > 5000)
                throw new RequestException($"Invalid image size {{Width={sourceImage.Size.Width}, Height={sourceImage.Size.Height}}}. The maximum image size is: {{Width=5000, Height=5000}}.");

            var ext = GetExtension(file.FileName).ToLowerInvariant();

            using var reader = new BinaryReader(file.OpenReadStream());
            var signatures = FileSignature[ext];
            var headerBytes = reader.ReadBytes(signatures.Max(m => m.Length));

            return signatures.Any(signature =>
                headerBytes.Take(signature.Length).SequenceEqual(signature));
        }

        private static void SaveAndResize(IFormFile file, Stream stream, int size)
        {
            using var sourceImage = ImageArchiver.LoadImage(file.OpenReadStream());
            var filter = new FixedResizeConstraint(size, size)
            {
                CanvasColor = BackgroundColor.GetStatic(System.Drawing.Color.White),
            };
            filter.SaveProcessedImageToStream(sourceImage, stream, new JpegFormatEncoderParams(100));
        }

        public string UploadImage(IFormFile file)
        {
            if (file == null || !IsValidSignature(file))
                throw new RequestException("Attempt to send empty form or not picture! Select Image!");

            if (file.Length > _sizeLimit)
                throw new RequestException($"File size must be less than {_sizeLimit / 1048576:N1} MB");

            var dateBegin = new DateTime(2001, 1, 1);
            var currentDate = DateTime.UtcNow;
            var elapsedTicks = currentDate.Ticks - dateBegin.Ticks;

            var fileName = elapsedTicks + Ext;

            var fullLargePath = Combine(Directory.GetCurrentDirectory(), _folderLargeName, fileName);
            var fullMediumPath = Combine(Directory.GetCurrentDirectory(), _folderMediumName, fileName);
            var fullSmallPath = Combine(Directory.GetCurrentDirectory(), _folderSmallName, fileName);

            using (var stream = new FileStream(fullLargePath, FileMode.Create))
            {
                SaveAndResize(file, stream, _sizeL);
            }
            using (var stream = new FileStream(fullMediumPath, FileMode.Create))
            {
                SaveAndResize(file, stream, _sizeM);
            }
            using (var stream = new FileStream(fullSmallPath, FileMode.Create))
            {
                SaveAndResize(file, stream, _sizeS);
            }

            return fileName;
        }
    }
}
