﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ShopOfBikes.BLL.DTO.Cart;
using ShopOfBikes.BLL.Exceptions;
using ShopOfBikes.BLL.Interfaces;
using ShopOfBikes.DAL.Entities;
using ShopOfBikes.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopOfBikes.BLL.Services
{
    public class CartService : ICartService
    {
        private readonly ICartRepositoryAsync _repository;
        private readonly IProductService _productService;
        private readonly IMapper _mapper;

        public CartService(ICartRepositoryAsync repository, IProductService productService, IMapper mapper, IUriService uriService)
        {
            _repository = repository;
            _productService = productService;
            _mapper = mapper;
        }

        public async Task<List<CartDto>> GetAll(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new AuthorizationException();
            }

            var cartItems = await _repository.GetAllWithProductByUserAsync(userId);
            var result = _mapper.Map<List<CartDto>>(cartItems);

            return result;
        }

        public async Task<List<CartDto>> GetAllWithoutUser(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new AuthorizationException();
            }
            var cartItems = await _repository.FindByCondition(x => x.UserId.Equals(userId)).ToListAsync();
            var result = _mapper.Map<List<CartDto>>(cartItems);

            return result;
        }

        public async Task<CartDto> GetById(int id)
        {
            var item = await _repository.GetByIdWithProductAsync(id);
            if (item == null)
                throw new NotFoundException($"Объект с id={id} отсутствует в базе данных.");

            var result = _mapper.Map<CartDto>(item);
            return result;
        }

        public async Task<CartDto> Add(CartForCreationDto item, string userId)
        {
            if (item == null)
                throw new NullArgumentException("Отсутствует значение");

            var product = await _productService.GetById(item.ProductId);
            if (item.Quantity > product.Quantity)
            {
                throw new RequestException("Невозможно купить такое количество велосипедов, попробуйте указать меньше!");
            }

            var itemMapped = _mapper.Map<Cart>(item);
            itemMapped.TotalPrice = item.Quantity * product.Price;
            itemMapped.UserId = userId;

            var itemAdded = await _repository.CreateAsync(itemMapped);
            var newItem = await _repository.GetByIdWithProductAsync(itemAdded.Id);

            var itemCreated = _mapper.Map<CartDto>(newItem);

            return itemCreated;
        }

        public async Task Delete(int id)
        {
            var item = await _repository.FindByIdAsync(id);
            if (item == null)
                throw new NotFoundException($"Объект с id={id} отсутствует в базе данных.");

            await _repository.DeleteAsync(item);
        }

        public async Task DeleteAll(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new AuthorizationException();
            }

            var cartItems = _repository.FindByCondition(x => x.UserId.Equals(userId));
            var carts = cartItems.ToArray();
            foreach (var cart in carts)
            {
                await _repository.DeleteAsync(cart);
            }
        }

        public async Task<CartDto> Update(int id, CartForUpdateDto item)
        {
            if (item == null)
                throw new NullArgumentException("Отсутствует значение");

            var element = await _repository.GetByIdWithProductAsync(id);
            if (element == null)
                throw new NotFoundException($"Элемент корзины с id={id} не найден.");

            var product = await _productService.GetById(element.ProductId);
            if (item.Quantity > product.Quantity)
            {
                throw new RequestException("Невозможно купить такое количество велосипедов, попробуйте указать меньше!");
            }

            _mapper.Map(item, element);
            element.TotalPrice = product.Price * item.Quantity;

            var newItem = await _repository.UpdateAsync(element);

            var updatedItem = _mapper.Map<CartDto>(newItem);

            return updatedItem;
        }
    }
}
