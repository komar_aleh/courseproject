﻿using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;
using ShopOfBikes.BLL.Configuration;
using ShopOfBikes.BLL.DTO.Email;
using ShopOfBikes.BLL.Interfaces;
using System.Text;
using System.Threading.Tasks;

namespace ShopOfBikes.BLL.Services
{
    public class EmailService : IEmailService
    {
        private readonly EmailConfiguration _emailConfiguration;

        public EmailService(IOptions<EmailConfiguration> options)
        {
            _emailConfiguration = options.Value;
        }
        public async Task SendEmailAsync(EmailModel model)
        {
            using var client = new SmtpClient();
            await client.ConnectAsync(_emailConfiguration.Host, _emailConfiguration.Port, _emailConfiguration.UseSSL);
            if (_emailConfiguration.RequiredAuthentication)
            {
                if (client.AuthenticationMechanisms.Contains("NTLM"))
                {
                    var auth = new SaslMechanismNtlm(_emailConfiguration.UserName, _emailConfiguration.Password);
                    await client.AuthenticateAsync(auth);
                }
                else
                {
                    await client.AuthenticateAsync(_emailConfiguration.UserName, _emailConfiguration.Password);
                }
            }

            var message = new MimeMessage
            {
                Subject = model.Subject,
                Body = new BodyBuilder
                {
                    HtmlBody = model.Body
                }
                    .ToMessageBody()
            };
            message.From.Add(new MailboxAddress(Encoding.UTF8, _emailConfiguration.SenderName, _emailConfiguration.SenderEmail));
            message.To.Add(new MailboxAddress(Encoding.UTF8, string.Empty, model.To));

            await client.SendAsync(message);
            await client.DisconnectAsync(true);
        }
    }
}
