﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using ShopOfBikes.BLL.Configuration;
using ShopOfBikes.BLL.DTO.Email;
using ShopOfBikes.BLL.DTO.Parameters;
using ShopOfBikes.BLL.DTO.User;
using ShopOfBikes.BLL.Exceptions;
using ShopOfBikes.BLL.Helpers;
using ShopOfBikes.BLL.Interfaces;
using ShopOfBikes.BLL.Wrappers;
using ShopOfBikes.DAL.Entities;
using ShopOfBikes.DAL.Shared;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ShopOfBikes.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IEmailService _emailService;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;
        private readonly IUriService _uriService;

        public UserService(
            UserManager<User> userManager,
            RoleManager<IdentityRole> roleManager,
            IEmailService emailService,
            IConfiguration configuration,
            IMapper mapper,
            IUriService uriService
            )
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _emailService = emailService;
            _configuration = configuration;
            _mapper = mapper;
            _uriService = uriService;
        }

        public async Task<UserDto> Register(UserRegisterDto model)
        {
            if (model == null)
                throw new RequestException("Данные пользователя отсутствуют");

            var userExists = await _userManager.FindByNameAsync(model.UserName);
            if (userExists != null)
                throw new RequestException("Пользователь с таким никнеймом уже существует!");

            var userEmailExists = await _userManager.FindByEmailAsync(model.Email);
            if (userEmailExists != null)
                throw new RequestException("Пользователь с таким емейлом уже существует!");

            var user = new User
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                UserName = model.UserName
            };

            var result = await _userManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
            {
                throw new RequestException("Создание пользователя завершилось ошибкой. Попробуйте еще раз.");
            }

            if (await _roleManager.RoleExistsAsync(UserRoles.User))
                await _userManager.AddToRoleAsync(user, UserRoles.User);

            await SendEmailConfirmation(user);

            var createdUser = _mapper.Map<UserDto>(user);
            return (createdUser);
        }

        public async Task<UserDto> RegisterAdmin(UserRegisterDto model)
        {
            if (model == null)
                throw new RequestException("Данные пользователя отсутствуют");

            var userExists = await _userManager.FindByNameAsync(model.UserName);
            if (userExists != null)
                throw new RequestException("Пользователь с таким никнеймом уже существует!");

            var userEmailExists = await _userManager.FindByEmailAsync(model.Email);
            if (userEmailExists != null)
                throw new RequestException("Пользователь с таким емейлом уже существует!");

            var user = new User
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                UserName = model.UserName,
                EmailConfirmed = true
            };

            var result = await _userManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
            {
                throw new RequestException("Создание пользователя завершилось ошибкой. Попробуйте еще раз.");
            }

            if (await _roleManager.RoleExistsAsync(UserRoles.Admin))
                await _userManager.AddToRoleAsync(user, UserRoles.Admin);

            var emailModel = new EmailModel
            {
                To = user.Email,
                Subject = "Администрирование интернет-магазина ShopOfBikes.com",
                Body = "Здравствуйте. Вы назначены новым администратором на сайте интернет-магазина ShopOfBikes.com" +
                       $"Ваш логин для входа: {user.UserName}" +
                       $"Ваш пароль: {model.Password}" +
                       "Настоятельно рекомендуем изменить пароль после первого входа в аккаунт!"
            };

            await _emailService.SendEmailAsync(emailModel);

            var createdUser = _mapper.Map<UserDto>(user);
            return (createdUser);
        }

        public async Task<JwtSecurityToken> Login(UserForLoginDto model)
        {
            if (model == null)
                throw new RequestException("Данные пользователя отсутствуют");

            var user = await _userManager.FindByNameAsync(model.UserName);

            if (user == null || !await _userManager.CheckPasswordAsync(user, model.Password))
                throw new AuthorizationException("Ошибка входа. Возможно вы ввели неверный никнейм или пароль");

            if (!await _userManager.IsEmailConfirmedAsync(user))
            {
                throw new AuthorizationException("Ваш аккаунт не активирован.");
            }
            var userRoles = await _userManager.GetRolesAsync(user);
            var expiresDays = DateTime.Now.AddDays(Convert.ToInt32(_configuration["JWT:ExpireDays"]));
            var key = Encoding.UTF8.GetBytes(_configuration["JWT:SecretKey"]);
            var signingKey = new SymmetricSecurityKey(key);
            var credentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);

            var authorizationClaims = new List<Claim>
            {
                new(MyClaims.UserName, user.UserName),
                new(MyClaims.Email, user.Email),
                new(MyClaims.UserId, user.Id),
            };
            authorizationClaims.AddRange(userRoles.Select(userRole => new Claim(ClaimTypes.Role, userRole)));
            authorizationClaims.AddRange(userRoles.Select(userRole => new Claim(MyClaims.Role, userRole)));

            var token = new JwtSecurityToken(
                issuer: _configuration["JWT:ServerUrl"],
                audience: _configuration["JWT:ServerUrl"],
                expires: expiresDays,
                claims: authorizationClaims,
                signingCredentials: credentials);
            return token;
        }

        public async Task<UserDto> GetById(string id)
        {
            var item = await _userManager.Users
                .AsNoTracking()
                .AsSingleQuery()
                .Where(u => u.Id.Equals(id))
                .Include(u => u.Orders)
                .ThenInclude(u => u.OrderItems)
                .ThenInclude(u => u.Product)
                .FirstOrDefaultAsync();

            if (item == null)
                throw new NotFoundException("Пользователь не найден в базе данных.");

            var roles = await _userManager.GetRolesAsync(item);
            var role = roles[0];

            var result = _mapper.Map<UserDto>(item);
            result.Role = role;
            return result;
        }

        public async Task<UserDto> Update(string id, UserForUpdateDto user)
        {
            if (user == null)
                throw new RequestException("Данные пользователя отсутствуют");

            var userEntity = await _userManager.FindByIdAsync(id);
            if (userEntity == null)
                throw new NotFoundException("Пользователь не найден в базе данных.");

            _mapper.Map(user, userEntity);

            var result = await _userManager.UpdateAsync(userEntity);
            if (!result.Succeeded)
                throw new RequestException("Обновление данных пользователя завершилось ошибкой. Попробуйте еще раз.");

            var updatedUser = _mapper.Map<UserDto>(userEntity);

            return updatedUser;
        }

        public async Task Delete(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
                throw new NotFoundException($"Пользователь не найден в базе данных.");

            await _userManager.DeleteAsync(user);
        }

        public async Task<UserDto> ConfirmEmail(string userId, string token)
        {
            if (string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(token))
                throw new RequestException("Отсутствуют данные");

            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
                throw new NotFoundException("Пользователь не найден в базе данных.");

            if (await _userManager.IsEmailConfirmedAsync(user))
                throw new RequestException("Пользователь уже активирован!");

            var decodedToken = WebEncoders.Base64UrlDecode(token);
            var normalToken = Encoding.UTF8.GetString(decodedToken);

            var result = await _userManager.ConfirmEmailAsync(user, normalToken);

            if (!result.Succeeded)
                throw new RequestException("Ошибка подтверждения пароля. Попробуйте еще раз.");

            var emailModel = new EmailModel
            {
                To = user.Email,
                Subject = "Аккаунт активирован",
                Body = "Спасибо. Ваш аккаунт активирован."
            };

            await _emailService.SendEmailAsync(emailModel);

            var userCormirmed = _mapper.Map<UserDto>(user);

            return userCormirmed;
        }

        public async Task<UserDto> ForgotPassword(SendEmailModel model)
        {
            if (model == null)
                throw new RequestException("Email пустой. Введите email!");

            var user = await _userManager.FindByEmailAsync(model.Email);

            if (user == null)
                throw new RequestException("Невозможно отправить ссылку для смены пароля.");

            var token = await _userManager.GeneratePasswordResetTokenAsync(user);
            var encodedToken = Encoding.UTF8.GetBytes(token);
            var normalToken = WebEncoders.Base64UrlEncode(encodedToken);

            var clientUrl = _configuration.GetValue<string>("ClientUrl");
            var link = $"{clientUrl}/reset-password?userId={user.Id}&token={normalToken}";

            var emailModel = new EmailModel
            {
                To = model.Email,
                Subject = "Сброс пароля пользователя на сайте ShopOfBikes.by",
                Body = $"Вы или кто-то другой запросил сброс пароля пользователя с email: {user.Email} для на сайте ShopOfBikes.by" +
                       "<br/><br/>" +
                       "Если это не вы, то просто не отвечайте на это письмо, иначе для сброса пароля пройдите по ссылке ниже:" +
                       "<br/><br/>" +
                       $"<a href=\"{link}\" target='_blank'>Сбросить пароль</a>"
            };

            await _emailService.SendEmailAsync(emailModel);

            var userForgotted = _mapper.Map<UserDto>(user);

            return userForgotted;
        }

        public async Task<UserDto> Activate(SendEmailModel model)
        {
            if (model == null)
                throw new RequestException("Email is null");

            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
                throw new RequestException("Невозможно отправить ссылку активации. Проверьте корректность ввода email.");

            var isEmailConfirmed = await _userManager.IsEmailConfirmedAsync(user);
            if (isEmailConfirmed)
                throw new RequestException("Невозможно отправить ссылку активации. Проверьте корректность ввода email.");

            await SendEmailConfirmation(user);

            var userActivated = _mapper.Map<UserDto>(user);

            return userActivated;
        }

        public async Task<UserDto> ResetPassword(ResetPasswordModel model)
        {
            if (model == null)
                throw new RequestException("Email не введен");

            var user = await _userManager.FindByIdAsync(model.UserId);

            if (user == null)
                throw new NotFoundException("Пользователь не найден в базе данных.");

            var decodedToken = WebEncoders.Base64UrlDecode(model.Token);
            var normalToken = Encoding.UTF8.GetString(decodedToken);

            var result = await _userManager.ResetPasswordAsync(user, normalToken, model.Password);

            if (!result.Succeeded)
                throw new RequestException("Ошибка сброса пароля.");

            var userResetted = _mapper.Map<UserDto>(user);
            return userResetted;
        }

        public async Task<PagedResponse<List<UserDto>>> GetAllPagedResponseAsync(UsersParameters requestParameters, PagingParameters pagingParameters, string route)
        {
            var name = requestParameters.UserName;
            var pageNumber = pagingParameters.PageNumber;
            var pageSize = pagingParameters.PageSize;
            var orderBy = requestParameters.OrderBy;
            var userRole = requestParameters.Role;

            var result = _userManager.Users;
            var recordsTotal = result.Count();
            result = SearchByValue(result, name);

            if (!string.IsNullOrWhiteSpace(orderBy))
                result = result.OrderBy(orderBy);

            var resultData = await result.ToListAsync();

            var users = _mapper.Map<List<UserDto>>(resultData);

            for (var i = resultData.Count - 1; i >= 0; i--)
            {
                var item = resultData[i];
                var roles = await _userManager.GetRolesAsync(item);
                var role = roles[0];
                users[i].Role = role;
            }

            if (!string.IsNullOrWhiteSpace(userRole))
                users = users.Where(x => x.Role.ToLower().Contains(userRole.Trim().ToLower())).ToList();

            var recordsFiltered = users.Count;
            var recordsCount = new RecordsCount
            {
                RecordsFiltered = recordsFiltered,
                RecordsTotal = recordsTotal
            };

            users = users.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

            var pagedResponse = PaginationHelper.CreatePagedResponse(users, pagingParameters, _uriService, route, recordsCount);

            return pagedResponse;
        }

        private static IQueryable<User> SearchByValue(IQueryable<User> items, string value)
        {
            if (!items.Any() || string.IsNullOrEmpty(value))
                return items;

            items = items.Where(o => o.UserName.ToLower().Contains(value.Trim().ToLower()));
            return items;
        }

        private async Task SendEmailConfirmation(User user)
        {
            var token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            var encodedToken = Encoding.UTF8.GetBytes(token);
            var normalToken = WebEncoders.Base64UrlEncode(encodedToken);

            var clientUrl = _configuration.GetValue<string>("ClientUrl");
            var link = $"{clientUrl}/confirm?userId={user.Id}&token={normalToken}";

            var emailModel = new EmailModel
            {
                To = user.Email,
                Subject = "Активация аккаунта на сайте ShopOfBikes.by",
                Body = "Подтвердите регистрацию на сайте ShopOfBikes.by" +
                       "<br/><br/>" +
                       "Для активации вашего аккаунта, пожалуйста кликните по ссылке ниже: " +
                       "<br/><br/>" +
                       $"<a href=\"{link}\" target='_blank'>Активировать аккаунт</a>"
            };

            await _emailService.SendEmailAsync(emailModel);
        }

        public async Task<UserDto> ChangePassword(string userId, ChangePasswordModel model)
        {
            if (model == null)
                throw new RequestException("Email не введен");

            var user = await _userManager.FindByIdAsync(userId);

            if (user == null)
                throw new NotFoundException("Пользователь не найден в базе данных.");
            var result =
                await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
            if (!result.Succeeded)
                throw new RequestException("Ошибка изменения пароля.");

            var userChanged = _mapper.Map<UserDto>(user);
            return userChanged;
        }
    }
}
