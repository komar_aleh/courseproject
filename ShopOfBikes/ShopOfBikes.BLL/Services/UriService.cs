﻿using Microsoft.AspNetCore.WebUtilities;
using ShopOfBikes.BLL.DTO.Parameters;
using ShopOfBikes.BLL.Interfaces;
using System;

namespace ShopOfBikes.BLL.Services
{
    public class UriService : IUriService
    {
        private readonly string _baseUri;
        public UriService(string baseUri)
        {
            _baseUri = baseUri;
        }
        public Uri GetPageUri(PagingParameters queryParameters, string route)
        {
            var endpointUri = new Uri(string.Concat(_baseUri, route));
            var modifiedUri = QueryHelpers.AddQueryString(endpointUri.ToString(), "pageNumber", queryParameters.PageNumber.ToString());
            modifiedUri = QueryHelpers.AddQueryString(modifiedUri, "pageSize", queryParameters.PageSize.ToString());
            return new Uri(modifiedUri);
        }
    }
}
