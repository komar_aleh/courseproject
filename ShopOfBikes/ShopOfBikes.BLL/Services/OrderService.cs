﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ShopOfBikes.BLL.DTO.Order;
using ShopOfBikes.BLL.DTO.OrderItem;
using ShopOfBikes.BLL.DTO.Parameters;
using ShopOfBikes.BLL.DTO.Product;
using ShopOfBikes.BLL.Exceptions;
using ShopOfBikes.BLL.Helpers;
using ShopOfBikes.BLL.Interfaces;
using ShopOfBikes.BLL.Wrappers;
using ShopOfBikes.DAL.Entities;
using ShopOfBikes.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace ShopOfBikes.BLL.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepositoryAsync _repository;
        private readonly ICartService _cartService;
        private readonly IOrderItemService _orderItemService;
        private readonly IProductService _productService;
        private readonly IMapper _mapper;
        private readonly IUriService _uriService;

        public OrderService(IOrderRepositoryAsync repository, ICartService cartService,
            IOrderItemService orderItemService, IProductService productService, IMapper mapper, IUriService uriService)
        {
            _repository = repository;
            _cartService = cartService;
            _productService = productService;
            _orderItemService = orderItemService;
            _mapper = mapper;
            _uriService = uriService;
        }

        public async Task<OrderDto> GetById(int id)
        {
            var item = await _repository.GetByIdWithOrderItemsAndProductAsync(id);
            if (item == null)
                throw new NotFoundException($"Объект с id={id} отсутствует в базе данных.");

            var result = _mapper.Map<OrderDto>(item);
            return result;
        }

        public async Task<OrderAfterCreationDto> Add(OrderForCreationDto item, string userId)
        {
            if (item == null)
                throw new NullArgumentException("Отсутствует значение");

            var cartItems = await _cartService.GetAllWithoutUser(userId);

            var order = _mapper.Map<Order>(item);
            order.UserId = userId;

            await _repository.CreateAsync(order);

            decimal totalAmount = 0;
            foreach (var cart in cartItems)
            {
                totalAmount += cart.TotalPrice;
                var orderItemEntity = _mapper.Map<OrderItemForCreationDto>(cart);
                orderItemEntity.OrderId = order.Id;

                await _orderItemService.Add(orderItemEntity);
                await _cartService.Delete(cart.Id);

                await _productService.DecreaseQuantity(orderItemEntity.ProductId, orderItemEntity.Quantity);
            }

            var createdOrder = await _repository.FindByIdAsync(order.Id);
            createdOrder.TotalAmount = totalAmount;

            await _repository.UpdateAsync(createdOrder);
            var itemCreated = _mapper.Map<OrderAfterCreationDto>(createdOrder);

            return itemCreated;
        }

        public async Task Delete(int id)
        {
            var item = await _repository.GetByIdWithOrderItemsAndProductAsync(id);

            if (item == null)
                throw new NotFoundException($"Заказ с номером {id} отсутствует.");

            switch (item.StatusId)
            {
                case 1:
                    foreach (var orderItem in item.OrderItems)
                    {
                        var orderItemDto = await _orderItemService.GetById(orderItem.Id);
                        await _productService.IncreaseQuantity(orderItemDto.ProductId, orderItemDto.Quantity);
                    }
                    await _repository.DeleteAsync(item);
                    break;
                case 5:
                    await _repository.DeleteAsync(item);
                    break;
                default:
                    throw new RequestException("Сначала нужно завершить либо отменить заказ.");
            }
        }

        public async Task<OrderDto> Update(int id, OrderForUpdateDto item)
        {
            if (item == null)
                throw new NullArgumentException("Отсутствует значение");

            var order = await _repository.GetByIdWithOrderItemsAndProductAsync(id);
            if (order == null)
                throw new NotFoundException($"Заказ с номером {id} отсутствует в базе данных.");

            _mapper.Map(item, order);

            await _repository.UpdateAsync(order);

            if (item.StatusId == 2)
            {
                foreach (var orderItem in order.OrderItems)
                {
                    var oi = await _orderItemService.GetById(orderItem.Id);

                    var product = await _productService.GetById(oi.ProductId);
                    product.Quantity += oi.Quantity;
                    if (product.IsAvailable == false)
                        product.IsAvailable = true;
                    var productMapped = _mapper.Map<ProductForUpdateDto>(product);
                    await _productService.UpdateAfterCancellation(product.Id, productMapped);
                }
            }
            var orderUpdated = await _repository.GetByIdWithOrderItemsAndProductAsync(id);
            var updatedItem = _mapper.Map<OrderDto>(orderUpdated);

            return updatedItem;
        }

        public async Task<PagedResponse<List<OrderDto>>> GetAllPagedResponse(OrdersParameters requestParameters, PagingParameters pagingParameters, string route)
        {
            var status = requestParameters.StatusId;
            var pageNumber = pagingParameters.PageNumber;
            var pageSize = pagingParameters.PageSize;
            var orderBy = requestParameters.OrderBy;
            var userId = requestParameters.UserId;

            var result = _repository.GetAllWithStatus();
            var recordsTotal = result.Count();

            result = SearchByValue(result, status, userId);
            var recordsFiltered = result.Count();

            var recordsCount = new RecordsCount
            {
                RecordsFiltered = recordsFiltered,
                RecordsTotal = recordsTotal
            };

            if (!string.IsNullOrWhiteSpace(orderBy))
                result = result.OrderBy(orderBy);

            result = result
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize);

            var resultData = await result.ToListAsync();
            var resultNew = _mapper.Map<List<OrderDto>>(resultData);

            var pagedResponse = PaginationHelper.CreatePagedResponse(resultNew, pagingParameters, _uriService, route, recordsCount);

            return pagedResponse;
        }

        private static IQueryable<Order> SearchByValue(IQueryable<Order> items, int status, string userId)
        {
            if (!items.Any())
                return items;

            if (!string.IsNullOrWhiteSpace(userId))
            {
                items = items.Where(x => x.UserId == userId);
            }

            if ( status != 0)
                items = items.Where(o => o.StatusId.Equals(status));

            return items;
        }
    }
}
