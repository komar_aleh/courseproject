﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ShopOfBikes.BLL.DTO.Parameters;
using ShopOfBikes.BLL.DTO.Product;
using ShopOfBikes.BLL.Exceptions;
using ShopOfBikes.BLL.Helpers;
using ShopOfBikes.BLL.Interfaces;
using ShopOfBikes.BLL.Wrappers;
using ShopOfBikes.DAL.Entities;
using ShopOfBikes.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace ShopOfBikes.BLL.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductRepositoryAsync _repository;
        private readonly IMapper _mapper;
        private readonly IUriService _uriService;

        public ProductService(IProductRepositoryAsync repository, IMapper mapper, IUriService uriService)
        {
            _repository = repository;
            _mapper = mapper;
            _uriService = uriService;
        }

        public async Task<ProductDetailsDto> GetById(int id)
        {
            var item = await _repository.GetByIdWithProperties(id);
            if (item == null)
                throw new NotFoundException($"Объект с id={id} отсутствует в базе данных.");

            var result = _mapper.Map<ProductDetailsDto>(item);

            return result;
        }

        public async Task<ProductDetailsDto> Add(ProductForCreationDto item)
        {
            if (item == null)
                throw new NullArgumentException("Отсутствует значение");

            var itemMapped = _mapper.Map<Product>(item);
            await _repository.CreateAsync(itemMapped);

            var newProduct = await _repository.GetByIdWithProperties(itemMapped.Id);
            string classOfBike = null;
            if (!string.IsNullOrEmpty(newProduct.ClassOfBike?.Value))
                classOfBike = $"{newProduct.ClassOfBike?.Value}";
            string frameMaterial = null;
            if (!string.IsNullOrEmpty(newProduct.FrameMaterial?.Value))
                frameMaterial = $", материал рамы: {newProduct.FrameMaterial?.Value}";
            string wheelDiameter = null;
            if (newProduct.WheelDiameter?.Value != null)
                wheelDiameter = $", колеса {newProduct.WheelDiameter?.Value} дюймов";
            string forkType = null;
            if (!string.IsNullOrEmpty(newProduct.ForkType?.Value))
                forkType = $", вилка {newProduct.ForkType?.Value} ";
            string absorberStroke = null;
            if (newProduct.AbsorberStroke?.Value != null)
                absorberStroke = $"с ходом {newProduct.AbsorberStroke?.Value} мм";
            string speedNumber = null;
            if (newProduct.SpeedNumber?.Value != null)
                speedNumber = $", трансмиссия {newProduct.SpeedNumber?.Value} скор.";
            string weight = null;
            if (newProduct.Weight != 0)
                weight = $", {newProduct.Weight:вес: 0.##} кг";
            string frontBreakType = null;
            if (!string.IsNullOrEmpty(newProduct.FrontBrakeType?.Value))
                frontBreakType = $", тормоз передний: {newProduct.FrontBrakeType?.Value}";
            string rearBreakType = null;
            if (!string.IsNullOrEmpty(newProduct.RearBrakeType?.Value))
                rearBreakType = $", тормоз передний: {newProduct.FrontBrakeType?.Value}";
            string frameColor = null;
            if (!string.IsNullOrEmpty(newProduct.FrameColor?.Value))
                frameColor = $"({newProduct.FrameColor?.Value})";

            newProduct.Description = classOfBike + frameMaterial + wheelDiameter + forkType + absorberStroke +
                                     speedNumber + frontBreakType + rearBreakType + weight;
            newProduct.Name = $"Велосипед {newProduct.Manufacturer?.Value} {item.Name} {newProduct.WheelDiameter?.Value} {newProduct.FrameSize?.Value} {newProduct.CommonDate?.Value} {frameColor}";
            var productCreated = await _repository.UpdateAsync(newProduct);
            var itemCreated = _mapper.Map<ProductDetailsDto>(productCreated);
            return itemCreated;
        }

        public async Task Delete(int id)
        {
            var item = await _repository.FindByIdAsync(id);
            if (item == null)
                throw new NotFoundException($"Велосипед с id={id} отсутствует в базе данных.");

            await _repository.DeleteAsync(item);
        }

        public async Task<ProductDetailsDto> Update(int id, ProductForUpdateDto item)
        {
            if (item == null)
                throw new NullArgumentException("Отсутствует значение");

            var element = await _repository.FindByIdAsync(id);
            if (element == null)
                throw new NotFoundException($"Велосипед с id={id} отсутствует в базе данных.");

            _mapper.Map(item, element);

            await _repository.UpdateAsync(element);

            var newProduct = await _repository.GetByIdWithProperties(element.Id);

            string classOfBike = null;
            if (!string.IsNullOrEmpty(newProduct.ClassOfBike?.Value))
                classOfBike = $"{newProduct.ClassOfBike?.Value}";
            string frameMaterial = null;
            if (!string.IsNullOrEmpty(newProduct.FrameMaterial?.Value))
                frameMaterial = $", материал рамы: {newProduct.FrameMaterial?.Value}";
            string wheelDiameter = null;
            if (newProduct.WheelDiameter?.Value != null)
                wheelDiameter = $", колеса {newProduct.WheelDiameter?.Value} дюймов";
            string forkType = null;
            if (!string.IsNullOrEmpty(newProduct.ForkType?.Value))
                forkType = $", вилка {newProduct.ForkType?.Value} ";
            string absorberStroke = null;
            if (newProduct.AbsorberStroke?.Value != 0)
                absorberStroke = $"с ходом {newProduct.AbsorberStroke?.Value} мм";
            string speedNumber = null;
            if (newProduct.SpeedNumber?.Value != 0)
                speedNumber = $", трансмиссия {newProduct.SpeedNumber?.Value} скор.";
            string frontBreakType = null;
            if (!string.IsNullOrEmpty(newProduct.FrontBrakeType?.Value))
                frontBreakType = $", тормоз передний: {newProduct.FrontBrakeType?.Value}";
            string rearBreakType = null;
            if (!string.IsNullOrEmpty(newProduct.RearBrakeType?.Value))
                rearBreakType = $", тормоз передний: {newProduct.FrontBrakeType?.Value}";
            string weight = null;
            if (newProduct.Weight != 0)
                weight = $", {newProduct.Weight:вес: 0.##} кг";
            string frameColor = null;
            if (!string.IsNullOrEmpty(newProduct.FrameColor?.Value))
                frameColor = $"({newProduct.FrameColor?.Value})";

            newProduct.Description = classOfBike + frameMaterial + wheelDiameter + forkType + absorberStroke +
                                     speedNumber + frontBreakType + rearBreakType + weight;
            newProduct.Name = $"Велосипед {newProduct.Manufacturer?.Value} {item.Name} {newProduct.WheelDiameter?.Value} {newProduct.FrameSize?.Value} {frameColor}";

            var productCreated = await _repository.UpdateAsync(newProduct);

            var updatedItem = _mapper.Map<ProductDetailsDto>(productCreated);
            return updatedItem;
        }

        public async Task UpdateAfterCancellation(int id, ProductForUpdateDto item)
        {
            if (item == null)
                throw new NullArgumentException("Отсутствует значение");

            var element = await _repository.FindByIdAsync(id);
            if (element == null)
                throw new NotFoundException($"Велосипед с id={id} отсутствует в базе данных.");

            _mapper.Map(item, element);

            await _repository.UpdateAsync(element);
        }

        public async Task<PagedResponse<List<ProductDetailsDto>>> GetAllPagedResponse(ProductsParameters requestParameters, PagingParameters pagingParameters, string route)
        {
            var pageNumber = pagingParameters.PageNumber;
            var pageSize = pagingParameters.PageSize;
            var orderBy = requestParameters.OrderBy;
            var orderByThen = requestParameters.OrderByThen;
            var minPrice = requestParameters.MinPrice;
            var maxPrice = requestParameters.MaxPrice;
            var minCommonDate = requestParameters.MinCommonDate;
            var maxCommonDate = requestParameters.MaxCommonDate;
            var minAbsorberStroke = requestParameters.MinAbsorberStroke;
            var maxAbsorberStroke = requestParameters.MaxAbsorberStroke;
            var minSpeedNumber = requestParameters.MinSpeedNumber;
            var maxSpeedNumber = requestParameters.MaxSpeedNumber;
            var minFrontStar = requestParameters.MinFrontStar;
            var maxFrontStar = requestParameters.MaxFrontStar;
            var minRearStar = requestParameters.MinRearStar;
            var maxRearStar = requestParameters.MaxRearStar;
            var minWeight = requestParameters.MinWeight;
            var maxWeight = requestParameters.MaxWeight;
            var minWheelDiameter = requestParameters.MinWheelDiameter;
            var maxWheelDiameter = requestParameters.MaxWheelDiameter;
            var cassette = requestParameters.CassetteId;
            var chain = requestParameters.ChainId;
            var classOfBike = requestParameters.ClassOfBikeId;
            var forkMaterial = requestParameters.ForkMaterialId;
            var forkModel = requestParameters.ForkModelId;
            var forkType = requestParameters.ForkTypeId;
            var frameColor = requestParameters.FrameColorId;
            var frameMaterial = requestParameters.FrameMaterialId;
            var frameSize = requestParameters.FrameSizeId;
            var frontBrakeModel = requestParameters.FrontBrakeModelId;
            var frontBrakeType = requestParameters.FrontBrakeTypeId;
            var frontSwitch = requestParameters.FrontSwitchId;
            var gripsModel = requestParameters.GripsModelId;
            var handlebarModel = requestParameters.HandlebarModelId;
            var handlebarType = requestParameters.HandlebarTypeId;
            var manufacturer = requestParameters.ManufacturerId;
            var pedalsMaterial = requestParameters.PedalsMaterialId;
            var pedalsModel = requestParameters.PedalsModelId;
            var pedalsType = requestParameters.PedalsTypeId;
            var rearBrakeModel = requestParameters.RearBrakeModelId;
            var rearBrakeType = requestParameters.RearBrakeTypeId;
            var rearSwitch = requestParameters.RearSwitchId;
            var rimMaterial = requestParameters.RimMaterialId;
            var saddleModel = requestParameters.SaddleModelId;
            var saddleType = requestParameters.SaddleTypeId;
            var shifterModel = requestParameters.ShifterModelId;
            var shifterType = requestParameters.ShifterTypeId;
            var systemModel = requestParameters.SystemModelId;
            var tiresModel = requestParameters.TiresModelId;
            var hardTail = requestParameters.HardTail;
            var female = requestParameters.Female;
            var kidTeen = requestParameters.KidTeen;
            var doubleRim = requestParameters.DoubleRim;
            var tiresTubeless = requestParameters.TiresTubeless;
            var forkLock = requestParameters.ForkLock;
            var searchName = requestParameters.SearchName;
            var isAvailable = requestParameters.IsAvailable;

            if (minPrice != null && maxPrice != null && !(maxPrice >= minPrice))
                throw new RequestException("Неверно указаны максимальная цена и минимальная");

            if (minCommonDate != null && maxCommonDate != null && !(maxCommonDate >= minCommonDate))
                throw new RequestException("Неверно указаны максимальный год выхода на рынок и минимальный");

            if (minAbsorberStroke != null && maxAbsorberStroke != null && !(maxAbsorberStroke >= minAbsorberStroke))
                throw new RequestException("Неверно указаны максимальный и минимальный ход амортизатора");

            if (minFrontStar != null && maxFrontStar != null && !(maxFrontStar >= minFrontStar))
                throw new RequestException("Неверно указаны максимальное и минимальное количество звезд в системе");

            if (minRearStar != null && maxRearStar != null && !(maxRearStar >= minRearStar))
                throw new RequestException("Неверно указаны максимальное и минимальное количество звезд в кассете");

            if (minSpeedNumber != null && maxSpeedNumber != null && !(maxSpeedNumber >= minSpeedNumber))
                throw new RequestException("Неверно указаны максимальное и минимальное количество скоростей");

            if (minWeight != null && maxWeight != null && !(maxWeight >= minWeight))
                throw new RequestException("Неверно указаны максимальный и минимальный вес велосипеда");

            if (minWheelDiameter != null && maxWheelDiameter != null && !(maxWheelDiameter >= minWheelDiameter))
                throw new RequestException("Неверно указаны максимальный и минимальный диаметр колес");


            var result = _repository.FindAll();

            if (isAvailable == true)
                result = result
                    .Where(p => p.IsAvailable == true);

            var recordsTotal = result.Count();

            result = Filter(result, minPrice, maxPrice, minCommonDate, maxCommonDate, minAbsorberStroke, maxAbsorberStroke,
                minSpeedNumber, maxSpeedNumber, minFrontStar, maxFrontStar, minRearStar, maxRearStar, minWeight, maxWeight,
                minWheelDiameter, maxWheelDiameter, cassette, chain, classOfBike, forkMaterial, forkModel, forkType,
                frameColor, frameMaterial, frameSize, frontBrakeModel, frontBrakeType, frontSwitch, gripsModel, handlebarModel,
                handlebarType, manufacturer, pedalsMaterial, pedalsModel, pedalsType, rearBrakeModel, rearBrakeType,
                rearSwitch, rimMaterial, saddleModel, saddleType, shifterModel, shifterType, systemModel, tiresModel,
                hardTail, female, kidTeen, doubleRim,
                tiresTubeless, forkLock, searchName);

            var recordsFiltered = result.Count();
            var recordsCount = new RecordsCount
            {
                RecordsFiltered = recordsFiltered,
                RecordsTotal = recordsTotal
            };

            if (!string.IsNullOrWhiteSpace(orderBy) || !string.IsNullOrWhiteSpace(orderByThen))
            {
                result = result.OrderBy(orderBy).ThenBy(orderByThen);
            }

            result = result
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize);

            result = result
                .Include(x => x.Manufacturer);

            var resultData = await result.ToListAsync();
            var resultNew = _mapper.Map<List<ProductDetailsDto>>(resultData);

            var pagedResponse = PaginationHelper.CreatePagedResponse(resultNew, pagingParameters, _uriService, route, recordsCount);

            return pagedResponse;
        }

        private static IQueryable<Product> Filter(IQueryable<Product> items, decimal? minPrice, decimal? maxPrice, int? minCommonDate,
            int? maxCommonDate, int? minAbsorberStroke, int? maxAbsorberStroke, int? minSpeedNumber, int? maxSpeedNumber,
            int? minFrontStar, int? maxFrontStar, int? minRearStar, int? maxRearStar, float? minWeight, float? maxWeight,
            float? minWheelDiameter, float? maxWheelDiameter, int? cassette, int? chain, int? classOfBike,
            int? forkMaterial, int? forkModel, int? forkType, int? frameColor,
            int? frameMaterial, int? frameSize, int? frontBrakeModel, int? frontBrakeType, int? frontSwitch,
            int? gripsModel, int? handlebarModel, int? handlebarType, int? manufacturer,
            int? pedalsMaterial, int? pedalsModel, int? pedalsType, int? rearBrakeModel, int? rearBrakeType,
            int? rearSwitch, int? rimMaterial, int? saddleModel, int? saddleType,
            int? shifterModel, int? shifterType, int? systemModel, int? tiresModel,
            bool? hardTail, bool? female, bool? kidTeen, bool? doubleRim, bool? tiresTubeless,
            bool? forkLock, string searchName)
        {
            if (!items.Any())
                return items;

            if (minPrice != null && maxPrice == null)
                items = items
                    .Where(p => p.Price >= minPrice);
            if (maxPrice != null && minPrice == null)
                items = items
                    .Where(p => p.Price <= maxPrice);
            if (minPrice != null && maxPrice != null)
                items = items
                    .Where(p =>
                        p.Price >= minPrice && p.Price <= maxPrice);

            if (minCommonDate != null && maxCommonDate == null)
                items = items
                    .Where(p => p.CommonDate.Value >= minCommonDate);
            if (maxCommonDate != null && minCommonDate == null)
                items = items
                    .Where(p => p.CommonDate.Value <= maxCommonDate);
            if (minCommonDate != null && maxCommonDate != null)
                items = items
                    .Where(p =>
                        p.CommonDate.Value >= minCommonDate && p.CommonDate.Value <= maxCommonDate);

            if (minAbsorberStroke != null && maxAbsorberStroke == null)
                items = items
                    .Where(p => p.AbsorberStroke.Value >= minAbsorberStroke);
            if (maxAbsorberStroke != null && minAbsorberStroke == null)
                items = items
                    .Where(p => p.AbsorberStroke.Value <= maxAbsorberStroke);
            if (minAbsorberStroke != null && maxAbsorberStroke != null)
                items = items
                    .Where(p => p.AbsorberStroke.Value >= minAbsorberStroke && p.AbsorberStroke.Value <= maxAbsorberStroke);

            if (minFrontStar != null && maxFrontStar == null)
                items = items
                    .Where(p => p.FrontStar.Value >= minFrontStar);
            if (maxFrontStar != null && minFrontStar == null)
                items = items
                    .Where(p => p.FrontStar.Value <= maxFrontStar);
            if (minFrontStar != null && maxFrontStar != null)
                items = items
                    .Where(p =>
                        p.FrontStar.Value >= minFrontStar && p.FrontStar.Value <= maxFrontStar);

            if (minRearStar != null && maxRearStar == null)
                items = items
                    .Where(p => p.RearStar.Value >= minRearStar);
            if (maxRearStar != null && minRearStar == null)
                items = items
                    .Where(p => p.RearStar.Value <= maxRearStar);
            if (minRearStar != null && maxRearStar != null)
                items = items
                    .Where(p =>
                        p.RearStar.Value >= minRearStar && p.RearStar.Value <= maxRearStar);

            if (minSpeedNumber != null && maxSpeedNumber == null)
                items = items
                    .Where(p => p.SpeedNumber.Value >= minSpeedNumber);
            if (maxSpeedNumber != null && minSpeedNumber == null)
                items = items
                    .Where(p => p.SpeedNumber.Value <= maxSpeedNumber);
            if (minSpeedNumber != null && maxSpeedNumber != null)
                items = items
                    .Where(p =>
                        p.SpeedNumber.Value >= minSpeedNumber && p.SpeedNumber.Value <= maxSpeedNumber);
            if (minWheelDiameter != null && maxWheelDiameter == null)
                items = items
                    .Where(p => p.WheelDiameter.Value >= minWheelDiameter);
            if (maxWheelDiameter != null && minWheelDiameter == null)
                items = items
                    .Where(p => p.WheelDiameter.Value <= maxWheelDiameter);
            if (minWheelDiameter != null && maxWheelDiameter != null)
                items = items
                    .Where(p =>
                        p.WheelDiameter.Value >= minWheelDiameter && p.WheelDiameter.Value <= maxWheelDiameter);

            if (minWeight != null && maxWeight == null)
                items = items
                    .Where(p => p.Weight >= minWeight);
            if (maxWeight != null && minWeight == null)
                items = items
                    .Where(p => p.Weight <= maxWeight);
            if (minWeight != null && maxWeight != null)
                items = items
                    .Where(p =>
                        p.Weight >= minWeight && p.Weight <= maxWeight);


            if (hardTail != null)
                items = items
                    .Where(p => p.HardTail == hardTail);

            if (female != null)
                items = items
                    .Where(p => p.Female == female);

            if (kidTeen != null)
                items = items
                    .Where(p => p.KidTeen == kidTeen);

            if (doubleRim != null)
                items = items
                    .Where(p => p.DoubleRim == doubleRim);

            if (tiresTubeless != null)
                items = items
                    .Where(p => p.TiresTubeless == tiresTubeless);

            if (forkLock != null)
                items = items
                    .Where(p => p.ForkLock == forkLock);

            if (!string.IsNullOrEmpty(searchName))
                items = items
                    .Where(o => o.Name.ToLower().Contains(searchName.Trim().ToLower()));

            if (cassette != null)
                items = items
                    .Where(o => o.Cassette.Id == cassette);

            if (chain != null)
                items = items
                    .Where(o => o.Chain.Id == chain);

            if (classOfBike != null)
                items = items
                    .Where(o => o.ClassOfBike.Id == classOfBike);

            if (forkMaterial != null)
                items = items
                    .Where(o => o.ForkMaterial.Id == forkMaterial);

            if (forkModel != null)
                items = items
                    .Where(o => o.ForkModel.Id == forkModel);

            if (forkType != null)
                items = items
                    .Where(o => o.ForkType.Id == forkType);

            if (frameColor != null)
                items = items
                    .Where(o => o.FrameColor.Id == frameColor);

            if (frameMaterial != null)
                items = items
                    .Where(o => o.FrameMaterial.Id == frameMaterial);

            if (frameSize != null)
                items = items
                    .Where(o => o.FrameSize.Id == frameSize);

            if (frontBrakeModel != null)
                items = items
                    .Where(o => o.FrontBrakeModel.Id == frontBrakeModel);

            if (frontBrakeType != null)
                items = items
                    .Where(o => o.FrontBrakeType.Id == frontBrakeType);

            if (frontSwitch != null)
                items = items
                    .Where(o => o.FrontSwitch.Id == frontSwitch);

            if (gripsModel != null)
                items = items
                    .Where(o => o.GripsModel.Id == gripsModel);

            if (handlebarModel != null)
                items = items
                    .Where(o => o.HandlebarModel.Id == handlebarModel);

            if (handlebarType != null)
                items = items
                    .Where(o => o.HandlebarType.Id == handlebarType);

            if (manufacturer != null)
                items = items
                    .Where(o => o.Manufacturer.Id == manufacturer);

            if (pedalsMaterial != null)
                items = items
                    .Where(o => o.PedalsMaterial.Id == pedalsMaterial);

            if (pedalsModel != null)
                items = items
                    .Where(o => o.PedalsModel.Id == pedalsModel);

            if (pedalsType != null)
                items = items
                    .Where(o => o.PedalsType.Id == pedalsType);

            if (rearBrakeModel != null)
                items = items
                    .Where(o => o.RearBrakeModel.Id == rearBrakeModel);

            if (rearBrakeType != null)
                items = items
                    .Where(o => o.RearBrakeType.Id == rearBrakeType);

            if (rearSwitch != null)
                items = items
                    .Where(o => o.RearSwitch.Id == rearSwitch);

            if (rimMaterial != null)
                items = items
                    .Where(o => o.RimMaterial.Id == rimMaterial);

            if (saddleModel != null)
                items = items
                    .Where(o => o.SaddleModel.Id == saddleModel);

            if (saddleType != null)
                items = items
                    .Where(o => o.SaddleType.Id == saddleType);

            if (shifterModel != null)
                items = items
                    .Where(o => o.ShifterModel.Id == shifterModel);

            if (shifterType != null)
                items = items
                    .Where(o => o.ShifterType.Id == shifterType);

            if (systemModel != null)
                items = items
                    .Where(o => o.SystemModel.Id == systemModel);

            if (tiresModel != null)
                items = items
                    .Where(o => o.TiresModel.Id == tiresModel);

            return items;
        }

        public async Task DecreaseQuantity(int id, uint quantity)
        {
            var item = await _repository.FindByIdAsync(id);
            item.Quantity -= quantity;

            if (item.Quantity == 0 && item.IsAvailable)
                item.IsAvailable = false;

            await _repository.UpdateAsync(item);
        }

        public async Task IncreaseQuantity(int id, uint quantity)
        {
            var item = await _repository.FindByIdAsync(id);
            item.Quantity += quantity;

            if (item.Quantity == 0 && item.IsAvailable == false)
                item.IsAvailable = true;

            await _repository.UpdateAsync(item);
        }

        public (decimal min, decimal max) GetMinMaxPrice()
        {
            var collection = _repository.FindAll().AsNoTracking();
            var q = from d in collection
                    group d by true into g
                    select new
                    {
                        Min = g.Min(gi => gi.Price),
                        Max = g.Max(gi => gi.Price)
                    };

            var result = q.FirstOrDefault();
            var min = result.Min;
            var max = result.Max;

            return (min, max);
        }

        public (float?, float?) GetMinMaxWeight()
        {
            var collection = _repository.FindAll().AsNoTracking();

            var q = from d in collection
                    group d by true into g
                    select new
                    {
                        Min = g.Min(gi => gi.Weight),
                        Max = g.Max(gi => gi.Weight)
                    };
            var result = q.FirstOrDefault();
            var min = result.Min;
            var max = result.Max;
            return (min, max);
        }

        public async Task<decimal> GetMinPrice()
        {
            var item = await _repository.FindAll().OrderBy("Price").ToListAsync();
            var min = item[0].Price;
            return min;
        }

        public async Task<decimal> GetMaxPrice()
        {
            var item = await _repository.FindAll().OrderBy("Price desc").ToListAsync();
            var max = item[0].Price;
            return max;
        }

        public async Task<float?> GetMinWeight()
        {
            var item = await _repository.FindAll().OrderBy("Weight").ToListAsync();
            var min = item[0].Weight;
            return min;
        }

        public async Task<float?> GetMaxWeight()
        {
            var item = await _repository.FindAll().OrderBy("Weight desc").ToListAsync();
            var max = item[0].Weight;
            return max;
        }
    }
}
