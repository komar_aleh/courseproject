﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ShopOfBikes.BLL.DTO.OrderItem;
using ShopOfBikes.BLL.DTO.Parameters;
using ShopOfBikes.BLL.Exceptions;
using ShopOfBikes.BLL.Helpers;
using ShopOfBikes.BLL.Interfaces;
using ShopOfBikes.BLL.Wrappers;
using ShopOfBikes.DAL.Entities;
using ShopOfBikes.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace ShopOfBikes.BLL.Services
{
    public class OrderItemService : IOrderItemService
    {
        private readonly IOrderItemRepositoryAsync _repository;
        private readonly IMapper _mapper;
        private readonly IUriService _uriService;

        public OrderItemService(IOrderItemRepositoryAsync repository, IMapper mapper, IUriService uriService)
        {
            _repository = repository;
            _mapper = mapper;
            _uriService = uriService;
        }

        public async Task<OrderItemDto> GetById(int id)
        {
            var item = await _repository.GetByIdWithProductAsync(id);
            if (item == null)
                throw new NotFoundException($"Объект с id={id} отсутствует в базе данных.");
            var result = _mapper.Map<OrderItemDto>(item);
            return result;
        }

        public async Task<OrderItemDto> Add(OrderItemForCreationDto item)
        {
            if (item == null)
                throw new NullArgumentException("Отсутствует значение");

            var itemMapped = _mapper.Map<OrderItem>(item);
            await _repository.CreateAsync(itemMapped);
            var itemCreated = _mapper.Map<OrderItemDto>(itemMapped);

            return itemCreated;
        }

        public async Task Delete(int id)
        {
            var item = await _repository.FindByIdAsync(id);
            if (item == null)
                throw new NotFoundException($"Объект с id={id} отсутствует в базе данных.");

            await _repository.DeleteAsync(item);
        }

        public async Task<OrderItemDto> Update(int id, OrderItemForUpdateDto item)
        {
            if (item == null)
                throw new NullArgumentException("Отсутствует значение");

            var element = await _repository.GetByIdWithProductAsync(id);
            if (element == null)
                throw new NotFoundException($"Объект с id={id} отсутствует в базе данных.");

            _mapper.Map(item, element);

            await _repository.UpdateAsync(element);

            var updatedItem = _mapper.Map<OrderItemDto>(element);

            return updatedItem;
        }

        public async Task<PagedResponse<List<OrderItemDto>>> GetAllPagedResponse(OrderItemsParameters requestParameters, PagingParameters pagingParameters, string route)
        {
            var pageNumber = pagingParameters.PageNumber;
            var pageSize = pagingParameters.PageSize;
            var orderBy = requestParameters.OrderBy;

            var result = _repository.FindAll();
            var recordsTotal = result.Count();

            var recordsCount = new RecordsCount
            {
                RecordsFiltered = recordsTotal,
                RecordsTotal = recordsTotal
            };

            if (!string.IsNullOrWhiteSpace(orderBy))
            {
                result = result.OrderBy(orderBy);
            }

            result = result
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize);

            var resultData = await result.ToListAsync();
            var resultNew = _mapper.Map<List<OrderItemDto>>(resultData);

            var pagedResponse = PaginationHelper.CreatePagedResponse(resultNew, pagingParameters, _uriService, route, recordsCount);

            return pagedResponse;
        }
    }
}
