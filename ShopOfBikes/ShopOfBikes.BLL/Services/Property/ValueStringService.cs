﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ShopOfBikes.BLL.DTO.Parameters;
using ShopOfBikes.BLL.DTO.Property;
using ShopOfBikes.BLL.Exceptions;
using ShopOfBikes.BLL.Helpers;
using ShopOfBikes.BLL.Interfaces;
using ShopOfBikes.BLL.Interfaces.Property;
using ShopOfBikes.BLL.Wrappers;
using ShopOfBikes.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace ShopOfBikes.BLL.Services.Property
{
    public class ValueStringService<T> : IValueStringService<T> where T : class, ITypedEntity<string>
    {
        private readonly IRepositoryBase<T> _repository;
        private readonly IMapper _mapper;
        private readonly IUriService _uriService;

        public ValueStringService(IRepositoryBase<T> repository, IMapper mapper, IUriService uriService)
        {
            _repository = repository;
            _mapper = mapper;
            _uriService = uriService;
        }

        public async Task<PropertyStringValueDto> GetById(int id)
        {
            var item = await _repository.FindByIdAsync(id);
            if (item == null)
                throw new NotFoundException($"Объект с id={id} отсутствует в базе данных.");
            var result = _mapper.Map<PropertyStringValueDto>(item);
            return result;
        }

        public async Task<PropertyStringValueDto> Add(PropertyStringValueForCreationUpdateDto item)
        {
            if (item == null)
                throw new NullArgumentException("Отсутствует значение");

            var itemMapped = _mapper.Map<T>(item);
            await _repository.CreateAsync(itemMapped);
            var itemCreated = _mapper.Map<PropertyStringValueDto>(itemMapped);

            return itemCreated;
        }

        public async Task Delete(int id)
        {
            var item = await _repository.FindByIdAsync(id);
            if (item == null)
                throw new NotFoundException($"Объект с id={id} отсутствует в базе данных.");

            await _repository.DeleteAsync(item);
        }

        public async Task<PropertyStringValueDto> Update(int id, PropertyStringValueForCreationUpdateDto item)
        {
            if (item == null)
                throw new NullArgumentException("Отсутствует значение");

            var element = await _repository.FindByIdAsync(id);
            if (element == null)
                throw new NotFoundException($"Объект с id={id} отсутствует в базе данных.");

            _mapper.Map(item, element);

            await _repository.UpdateAsync(element);

            var updatedItem = _mapper.Map<PropertyStringValueDto>(element);

            return updatedItem;
        }

        public async Task<PagedResponse<List<PropertyStringValueDto>>> GetAllPagedResponse(StringPropertyParameters requestParameters, PagingParameters pagingParameters, string route)
        {
            var value = requestParameters.Value;
            var pageNumber = pagingParameters.PageNumber;
            var pageSize = pagingParameters.PageSize;
            var orderBy = requestParameters.OrderBy;


            var result = _repository.FindAll();
            var recordsTotal = result.Count();

            int recordsFiltered;
            if (string.IsNullOrEmpty(value))
            {
                recordsFiltered = recordsTotal;
            }
            else
            {
                result = SearchByValue(result, value);
                recordsFiltered = result.Count();
            }

            var recordsCount = new RecordsCount
            {
                RecordsFiltered = recordsFiltered,
                RecordsTotal = recordsTotal
            };

            if (!string.IsNullOrWhiteSpace(orderBy))
            {
                result = result.OrderBy(orderBy);
            }

            result = result
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize);

            var resultData = await result.ToListAsync();
            var resultNew = _mapper.Map<List<PropertyStringValueDto>>(resultData);

            var pagedResponse = PaginationHelper.CreatePagedResponse(resultNew, pagingParameters, _uriService, route, recordsCount);

            return pagedResponse;
        }

        public async Task<List<PropertyStringValueDto>> GetAllItems()
        {
            var data = _repository.FindAll();
            var resultData = await data.ToListAsync();
            var result = _mapper.Map<List<PropertyStringValueDto>>(resultData);
            var propertyList = new List<PropertyStringValueDto>(result);
            return propertyList;
        }

        private static IQueryable<T> SearchByValue(IQueryable<T> items, string value)
        {
            if (!items.Any())
                return items;

            items = items.Where(o => o.Value.ToLower().Contains(value.Trim().ToLower()));
            return items;
        }
    }
}
