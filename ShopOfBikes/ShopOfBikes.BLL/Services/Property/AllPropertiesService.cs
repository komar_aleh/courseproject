﻿using AutoMapper;
using ShopOfBikes.BLL.Interfaces;
using ShopOfBikes.BLL.Interfaces.Property;
using ShopOfBikes.BLL.Wrappers;
using ShopOfBikes.DAL.EF;
using ShopOfBikes.DAL.Entities.Property;
using ShopOfBikes.DAL.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShopOfBikes.BLL.Services.Property
{
    public class AllPropertiesService : IAllPropertiesService
    {
        private readonly IValueIntService<AbsorberStroke> _absorberStrokeService;
        private readonly IValueIntService<CommonDate> _commonDateService;
        private readonly IValueIntService<FrontStar> _frontStarService;
        private readonly IValueIntService<RearStar> _rearStarService;
        private readonly IValueIntService<SpeedNumber> _speedNumberService;
        private readonly IValueStringService<Cassette> _cassetteService;
        private readonly IValueStringService<Chain> _chainService;
        private readonly IValueStringService<ClassOfBike> _classOfBikeService;
        private readonly IValueStringService<ForkMaterial> _forkMaterialService;
        private readonly IValueStringService<ForkModel> _forkModelService;
        private readonly IValueStringService<ForkType> _forkTypeService;
        private readonly IValueStringService<FrameColor> _frameColorService;
        private readonly IValueStringService<FrameMaterial> _frameMaterialService;
        private readonly IValueStringService<FrameSize> _frameSizeService;
        private readonly IValueStringService<FrontBrakeModel> _frontBrakeModelService;
        private readonly IValueStringService<FrontBrakeType> _frontBrakeTypeService;
        private readonly IValueStringService<FrontSwitch> _frontSwitchService;
        private readonly IValueStringService<GripsModel> _gripsModelService;
        private readonly IValueStringService<HandlebarModel> _handleBarModelService;
        private readonly IValueStringService<HandlebarType> _handleBarTypeService;
        private readonly IValueStringService<Manufacturer> _manufacturerService;
        private readonly IValueStringService<PedalsMaterial> _pedalsMaterialService;
        private readonly IValueStringService<PedalsModel> _pedalsModelService;
        private readonly IValueStringService<PedalsType> _pedalsTypeService;
        private readonly IValueStringService<RearBrakeModel> _rearBrakeModelService;
        private readonly IValueStringService<RearBrakeType> _rearBrakeTypeService;
        private readonly IValueStringService<RearSwitch> _rearSwitchService;
        private readonly IValueStringService<RimMaterial> _rimMaterialService;
        private readonly IValueStringService<SaddleModel> _saddleModelService;
        private readonly IValueStringService<SaddleType> _saddleTypeService;
        private readonly IValueStringService<ShifterModel> _shifterModelService;
        private readonly IValueStringService<ShifterType> _shifterTypeService;
        private readonly IValueStringService<SystemModel> _systemModelService;
        private readonly IValueStringService<TiresModel> _tiresModelService;
        private readonly IValueFloatService<WheelDiameter> _wheelDiameterService;
        private readonly IProductService _productService;
        private readonly IProductService _productService2;
        private readonly IProductService _productService3;
        private readonly IProductService _productService4;

        public AllPropertiesService(AppDbContext dbContext, IMapper mapper, IUriService uriService, IProductService productService)
        {
            _absorberStrokeService = new ValueIntService<AbsorberStroke>(new RepositoryBase<AbsorberStroke>(dbContext), mapper, uriService);
            _commonDateService = new ValueIntService<CommonDate>(new RepositoryBase<CommonDate>(dbContext), mapper, uriService);
            _frontStarService = new ValueIntService<FrontStar>(new RepositoryBase<FrontStar>(dbContext), mapper, uriService);
            _rearStarService = new ValueIntService<RearStar>(new RepositoryBase<RearStar>(dbContext), mapper, uriService);
            _speedNumberService = new ValueIntService<SpeedNumber>(new RepositoryBase<SpeedNumber>(dbContext), mapper, uriService);
            _cassetteService = new ValueStringService<Cassette>(new RepositoryBase<Cassette>(dbContext), mapper, uriService);
            _chainService = new ValueStringService<Chain>(new RepositoryBase<Chain>(dbContext), mapper, uriService);
            _classOfBikeService = new ValueStringService<ClassOfBike>(new RepositoryBase<ClassOfBike>(dbContext), mapper, uriService);
            _forkMaterialService = new ValueStringService<ForkMaterial>(new RepositoryBase<ForkMaterial>(dbContext), mapper, uriService);
            _forkModelService = new ValueStringService<ForkModel>(new RepositoryBase<ForkModel>(dbContext), mapper, uriService);
            _forkTypeService = new ValueStringService<ForkType>(new RepositoryBase<ForkType>(dbContext), mapper, uriService);
            _frameColorService = new ValueStringService<FrameColor>(new RepositoryBase<FrameColor>(dbContext), mapper, uriService);
            _frameMaterialService = new ValueStringService<FrameMaterial>(new RepositoryBase<FrameMaterial>(dbContext), mapper, uriService);
            _frameSizeService = new ValueStringService<FrameSize>(new RepositoryBase<FrameSize>(dbContext), mapper, uriService);
            _frontBrakeModelService = new ValueStringService<FrontBrakeModel>(new RepositoryBase<FrontBrakeModel>(dbContext), mapper, uriService);
            _frontBrakeTypeService = new ValueStringService<FrontBrakeType>(new RepositoryBase<FrontBrakeType>(dbContext), mapper, uriService);
            _frontSwitchService = new ValueStringService<FrontSwitch>(new RepositoryBase<FrontSwitch>(dbContext), mapper, uriService);
            _gripsModelService = new ValueStringService<GripsModel>(new RepositoryBase<GripsModel>(dbContext), mapper, uriService);
            _handleBarModelService = new ValueStringService<HandlebarModel>(new RepositoryBase<HandlebarModel>(dbContext), mapper, uriService);
            _handleBarTypeService = new ValueStringService<HandlebarType>(new RepositoryBase<HandlebarType>(dbContext), mapper, uriService);
            _manufacturerService = new ValueStringService<Manufacturer>(new RepositoryBase<Manufacturer>(dbContext), mapper, uriService);
            _pedalsMaterialService = new ValueStringService<PedalsMaterial>(new RepositoryBase<PedalsMaterial>(dbContext), mapper, uriService);
            _pedalsModelService = new ValueStringService<PedalsModel>(new RepositoryBase<PedalsModel>(dbContext), mapper, uriService);
            _pedalsTypeService = new ValueStringService<PedalsType>(new RepositoryBase<PedalsType>(dbContext), mapper, uriService);
            _rearBrakeModelService = new ValueStringService<RearBrakeModel>(new RepositoryBase<RearBrakeModel>(dbContext), mapper, uriService);
            _rearBrakeTypeService = new ValueStringService<RearBrakeType>(new RepositoryBase<RearBrakeType>(dbContext), mapper, uriService);
            _rearSwitchService = new ValueStringService<RearSwitch>(new RepositoryBase<RearSwitch>(dbContext), mapper, uriService);
            _rimMaterialService = new ValueStringService<RimMaterial>(new RepositoryBase<RimMaterial>(dbContext), mapper, uriService);
            _saddleModelService = new ValueStringService<SaddleModel>(new RepositoryBase<SaddleModel>(dbContext), mapper, uriService);
            _saddleTypeService = new ValueStringService<SaddleType>(new RepositoryBase<SaddleType>(dbContext), mapper, uriService);
            _shifterModelService = new ValueStringService<ShifterModel>(new RepositoryBase<ShifterModel>(dbContext), mapper, uriService);
            _shifterTypeService = new ValueStringService<ShifterType>(new RepositoryBase<ShifterType>(dbContext), mapper, uriService);
            _systemModelService = new ValueStringService<SystemModel>(new RepositoryBase<SystemModel>(dbContext), mapper, uriService);
            _tiresModelService = new ValueStringService<TiresModel>(new RepositoryBase<TiresModel>(dbContext), mapper, uriService);
            _wheelDiameterService = new ValueFloatService<WheelDiameter>(new RepositoryBase<WheelDiameter>(dbContext), mapper, uriService);
            _productService = new ProductService(new ProductRepositoryAsync(dbContext), mapper, uriService);
            _productService2 = new ProductService(new ProductRepositoryAsync(dbContext), mapper, uriService);
            _productService3 = new ProductService(new ProductRepositoryAsync(dbContext), mapper, uriService);
            _productService4 = new ProductService(new ProductRepositoryAsync(dbContext), mapper, uriService);
        }

        public async Task<ResponseListProperties> GetAllProperties()
        {
            var absorberStrokes = await _absorberStrokeService.GetAllItems();
            var commonDates = await _commonDateService.GetAllItems();
            var frontStars = await _frontStarService.GetAllItems();
            var rearStars = await _rearStarService.GetAllItems();
            var speedNumbers = await _speedNumberService.GetAllItems();
            var cassettes = await _cassetteService.GetAllItems();
            var chains = await _chainService.GetAllItems();
            var classOfBikes = await _classOfBikeService.GetAllItems();
            var forkMaterials = await _forkMaterialService.GetAllItems();
            var forkModels = await _forkModelService.GetAllItems();
            var forkTypes = await _forkTypeService.GetAllItems();
            var frameColors = await _frameColorService.GetAllItems();
            var frameMaterials = await _frameMaterialService.GetAllItems();
            var frameSizes = await _frameSizeService.GetAllItems();
            var frontBrakeModels = await _frontBrakeModelService.GetAllItems();
            var frontBrakeTypes = await _frontBrakeTypeService.GetAllItems();
            var frontSwitches = await _frontSwitchService.GetAllItems();
            var gripsModels = await _gripsModelService.GetAllItems();
            var handleBarModels = await _handleBarModelService.GetAllItems();
            var handleBarTypes = await _handleBarTypeService.GetAllItems();
            var manufacturers = await _manufacturerService.GetAllItems();
            var pedalsMaterials = await _pedalsMaterialService.GetAllItems();
            var pedalsModels = await _pedalsModelService.GetAllItems();
            var pedalsTypes = await _pedalsTypeService.GetAllItems();
            var rearBrakeModels = await _rearBrakeModelService.GetAllItems();
            var rearBrakeTypes = await _rearBrakeTypeService.GetAllItems();
            var rearSwitches = await _rearSwitchService.GetAllItems();
            var rimMaterials = await _rimMaterialService.GetAllItems();
            var saddleModels = await _saddleModelService.GetAllItems();
            var saddleTypes = await _saddleTypeService.GetAllItems();
            var shifterModels = await _shifterModelService.GetAllItems();
            var shifterTypes = await _shifterTypeService.GetAllItems();
            var systemModels = await _systemModelService.GetAllItems();
            var tiresModels = await _tiresModelService.GetAllItems();
            var wheelDiameters = await _wheelDiameterService.GetAllItems();
            var minPrice = await _productService.GetMinPrice();
            var maxPrice = await _productService2.GetMaxPrice();
            var minWeight = await _productService3.GetMinWeight();
            var maxWeight = await _productService4.GetMaxWeight();


            var result = new ResponseListProperties(
                absorberStrokes,
                cassettes,
                chains,
                classOfBikes,
                commonDates,
                forkMaterials,
                forkModels,
                forkTypes,
                frameColors,
                frameMaterials,
                frameSizes,
                frontBrakeModels,
                frontBrakeTypes,
                frontStars,
                frontSwitches,
                gripsModels,
                handleBarModels,
                handleBarTypes,
                manufacturers,
                pedalsMaterials,
                pedalsModels,
                pedalsTypes,
                rearBrakeModels,
                rearBrakeTypes,
                rearStars,
                rearSwitches,
                rimMaterials,
                saddleModels,
                saddleTypes,
                shifterModels,
                shifterTypes,
                speedNumbers,
                systemModels,
                tiresModels,
                wheelDiameters,
                minPrice,
                maxPrice,
                minWeight,
                maxWeight
            );
            return result;
        }

        public async Task<ResponseListProperties> GetAllProperties2()
        {
            var absorberStrokes = _absorberStrokeService.GetAllItems();
            var commonDates = _commonDateService.GetAllItems();
            var frontStars = _frontStarService.GetAllItems();
            var rearStars = _rearStarService.GetAllItems();
            var speedNumbers = _speedNumberService.GetAllItems();
            var cassettes = _cassetteService.GetAllItems();
            var chains = _chainService.GetAllItems();
            var classOfBikes = _classOfBikeService.GetAllItems();
            var forkMaterials = _forkMaterialService.GetAllItems();
            var forkModels = _forkModelService.GetAllItems();
            var forkTypes = _forkTypeService.GetAllItems();
            var frameColors = _frameColorService.GetAllItems();
            var frameMaterials = _frameMaterialService.GetAllItems();
            var frameSizes = _frameSizeService.GetAllItems();
            var frontBrakeModels = _frontBrakeModelService.GetAllItems();
            var frontBrakeTypes = _frontBrakeTypeService.GetAllItems();
            var frontSwitches = _frontSwitchService.GetAllItems();
            var gripsModels = _gripsModelService.GetAllItems();
            var handleBarModels = _handleBarModelService.GetAllItems();
            var handleBarTypes = _handleBarTypeService.GetAllItems();
            var manufacturers = _manufacturerService.GetAllItems();
            var pedalsMaterials = _pedalsMaterialService.GetAllItems();
            var pedalsModels = _pedalsModelService.GetAllItems();
            var pedalsTypes = _pedalsTypeService.GetAllItems();
            var rearBrakeModels = _rearBrakeModelService.GetAllItems();
            var rearBrakeTypes = _rearBrakeTypeService.GetAllItems();
            var rearSwitches = _rearSwitchService.GetAllItems();
            var rimMaterials = _rimMaterialService.GetAllItems();
            var saddleModels = _saddleModelService.GetAllItems();
            var saddleTypes = _saddleTypeService.GetAllItems();
            var shifterModels = _shifterModelService.GetAllItems();
            var shifterTypes = _shifterTypeService.GetAllItems();
            var systemModels = _systemModelService.GetAllItems();
            var tiresModels = _tiresModelService.GetAllItems();
            var wheelDiameters = _wheelDiameterService.GetAllItems();
            var (minPrice, maxPrice) = _productService.GetMinMaxPrice();
            var (minWeight, maxWeight) = _productService.GetMinMaxWeight();

            var list = new List<Task>()
            {
                absorberStrokes, commonDates, frontStars, rearStars, speedNumbers, cassettes, chains, classOfBikes,
                forkMaterials, forkModels, forkTypes, frameColors, frameMaterials, frameSizes, frontBrakeModels,
                frontBrakeTypes, frontSwitches, gripsModels, handleBarModels, handleBarTypes, manufacturers, pedalsMaterials,
                pedalsModels, pedalsTypes, rearBrakeModels, rearBrakeTypes, rearSwitches, rimMaterials, saddleModels,
                saddleTypes, shifterModels, shifterTypes, systemModels, tiresModels, wheelDiameters
            };

            await Task.WhenAll(list);
            var result = new ResponseListProperties(
                absorberStrokes.Result,
                cassettes.Result,
                chains.Result,
                classOfBikes.Result,
                commonDates.Result,
                forkMaterials.Result,
                forkModels.Result,
                forkTypes.Result,
                frameColors.Result,
                frameMaterials.Result,
                frameSizes.Result,
                frontBrakeModels.Result,
                frontBrakeTypes.Result,
                frontStars.Result,
                frontSwitches.Result,
                gripsModels.Result,
                handleBarModels.Result,
                handleBarTypes.Result,
                manufacturers.Result,
                pedalsMaterials.Result,
                pedalsModels.Result,
                pedalsTypes.Result,
                rearBrakeModels.Result,
                rearBrakeTypes.Result,
                rearStars.Result,
                rearSwitches.Result,
                rimMaterials.Result,
                saddleModels.Result,
                saddleTypes.Result,
                shifterModels.Result,
                shifterTypes.Result,
                speedNumbers.Result,
                systemModels.Result,
                tiresModels.Result,
                wheelDiameters.Result,
                minPrice,
                maxPrice,
                minWeight,
                maxWeight
            );
            return result;
        }
    }
}

