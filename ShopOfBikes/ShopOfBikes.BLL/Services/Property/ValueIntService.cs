﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ShopOfBikes.BLL.DTO.Parameters;
using ShopOfBikes.BLL.DTO.Property;
using ShopOfBikes.BLL.Exceptions;
using ShopOfBikes.BLL.Helpers;
using ShopOfBikes.BLL.Interfaces;
using ShopOfBikes.BLL.Interfaces.Property;
using ShopOfBikes.BLL.Wrappers;
using ShopOfBikes.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace ShopOfBikes.BLL.Services.Property
{
    public class ValueIntService<T> : IValueIntService<T> where T : class, ITypedEntity<int>
    {
        private readonly IRepositoryBase<T> _repository;
        private readonly IMapper _mapper;
        private readonly IUriService _uriService;

        public ValueIntService(IRepositoryBase<T> repository, IMapper mapper, IUriService uriService)
        {
            _repository = repository;
            _mapper = mapper;
            _uriService = uriService;
        }

        public async Task<PropertyIntValueDto> GetById(int id)
        {
            var item = await _repository.FindByIdAsync(id);
            if (item == null)
                throw new NotFoundException($"Объект с id={id} отсутствует в базе данных.");
            var result = _mapper.Map<PropertyIntValueDto>(item);
            return result;
        }

        public async Task<PropertyIntValueDto> Add(PropertyIntValueForCreationUpdateDto item)
        {
            if (item == null)
                throw new NullArgumentException("Отсутствует значение");

            var itemMapped = _mapper.Map<T>(item);
            await _repository.CreateAsync(itemMapped);
            var itemCreated = _mapper.Map<PropertyIntValueDto>(itemMapped);

            return itemCreated;
        }

        public async Task Delete(int id)
        {
            var item = await _repository.FindByIdAsync(id);
            if (item == null)
                throw new NotFoundException($"Объект с id={id} отсутствует в базе данных.");

            await _repository.DeleteAsync(item);
        }

        public async Task<PropertyIntValueDto> Update(int id, PropertyIntValueForCreationUpdateDto item)
        {
            if (item == null)
                throw new NullArgumentException("Отсутствует значение");

            var element = await _repository.FindByIdAsync(id);
            if (element == null)
                throw new NotFoundException($"Объект с id={id} отсутствует в базе данных.");

            _mapper.Map(item, element);

            await _repository.UpdateAsync(element);

            var updatedItem = _mapper.Map<PropertyIntValueDto>(element);

            return updatedItem;
        }

        public async Task<List<PropertyIntValueDto>> GetAllItems()
        {
            var data = _repository.FindAll();
            var resultData = await data.ToListAsync();
            var result = _mapper.Map<List<PropertyIntValueDto>>(resultData);
            return result;
        }

        public async Task<PagedResponse<List<PropertyIntValueDto>>> GetAllPagedResponse(IntPropertyParameters requestParameters, PagingParameters pagingParameters, string route)
        {
            var value = requestParameters.Value;
            var pageNumber = pagingParameters.PageNumber;
            var pageSize = pagingParameters.PageSize;
            var orderBy = requestParameters.OrderBy;


            var result = _repository.FindAll();
            var recordsTotal = result.Count();

            int recordsFiltered;
            if (value == null)
            {
                recordsFiltered = recordsTotal;
            }
            else
            {
                result = SearchByValue(result, value);
                recordsFiltered = result.Count();
            }

            var recordsCount = new RecordsCount
            {
                RecordsFiltered = recordsFiltered,
                RecordsTotal = recordsTotal
            };

            if (!string.IsNullOrWhiteSpace(orderBy))
            {
                result = result.OrderBy(orderBy);
            }

            result = result
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize);

            var resultData = await result.ToListAsync();
            var resultNew = _mapper.Map<List<PropertyIntValueDto>>(resultData);

            var pagedResponse = PaginationHelper.CreatePagedResponse(resultNew, pagingParameters, _uriService, route, recordsCount);

            return pagedResponse;
        }


        private static IQueryable<T> SearchByValue(IQueryable<T> items, int? value)
        {
            if (!items.Any())
                return items;

            items = items.Where(o => o.Value.Equals(value));
            return items;
        }
    }
}
