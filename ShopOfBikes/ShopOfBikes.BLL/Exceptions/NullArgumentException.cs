﻿using System;

namespace ShopOfBikes.BLL.Exceptions
{
    public class NullArgumentException : Exception
    {
        public NullArgumentException(string message) : base(message)
        {
        }
    }
}
