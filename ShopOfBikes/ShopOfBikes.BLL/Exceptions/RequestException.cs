﻿using System;

namespace ShopOfBikes.BLL.Exceptions
{
    public class RequestException : Exception
    {
        public RequestException(string message) : base(message)
        {
        }
    }
}