﻿using System;

namespace ShopOfBikes.BLL.Exceptions
{
    public class AuthorizationException : Exception
    {
        public AuthorizationException() : base("Вы не авторизованы. Залогиньтесь!")
        {
        }

        public AuthorizationException(string message) : base(message)
        {
        }
    }
}
