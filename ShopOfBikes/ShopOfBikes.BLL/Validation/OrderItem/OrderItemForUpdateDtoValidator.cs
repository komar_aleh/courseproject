﻿using FluentValidation;
using ShopOfBikes.BLL.DTO.OrderItem;

namespace ShopOfBikes.BLL.Validation.OrderItem
{
    public class OrderItemForUpdateDtoValidator : AbstractValidator<OrderItemForUpdateDto>
    {
        public OrderItemForUpdateDtoValidator()
        {
            RuleFor(x => x.ProductId)
                .NotEmpty().WithMessage("ID велосипеда не может быть пустым");
            RuleFor(x => x.Quantity)
                .NotEmpty().WithMessage("Количество не может быть пустым")
                .GreaterThanOrEqualTo(0u).WithMessage("Количество должно быть больше  либо равно 0");
        }
    }
}
