﻿using FluentValidation;
using ShopOfBikes.BLL.DTO.Order;

namespace ShopOfBikes.BLL.Validation.Order
{
    public class OrderForUpdateDtoValidator : AbstractValidator<OrderForUpdateDto>
    {
        public OrderForUpdateDtoValidator()
        {
            RuleFor(x => x.StatusId)
                .NotEmpty().WithMessage("Статус не может быть пустым");
        }
    }
}
