﻿using FluentValidation;
using ShopOfBikes.BLL.DTO.Order;
using System.Linq;

namespace ShopOfBikes.BLL.Validation.Order
{
    public class OrderForCreationDtoValidator : AbstractValidator<OrderForCreationDto>
    {
        public OrderForCreationDtoValidator()
        {
            RuleFor(x => x.ShippingFirstName)
                .NotEmpty().WithMessage("Имя получателя не может быть пустым")
                .Must(c => c.All(char.IsLetter)).WithMessage("Имя получателя должно содержать только буквы.");

            RuleFor(x => x.ShippingLastName)
                .NotEmpty().WithMessage("Фамилия получателя не может быть пустым")
                .Must(c => c.All(char.IsLetter)).WithMessage("Фамилия получателя должно содержать только буквы.");

            RuleFor(x => x.ShippingZip)
                .NotEmpty().WithMessage("Индекс не может быть пустым")
                .Matches("^[0-9]{6}$")
                .WithMessage("Индекс имеет неверный формат");

            RuleFor(x => x.ShippingCity)
                .NotEmpty().WithMessage("Город не может быть пустым");

            RuleFor(x => x.ShippingAddressLine)
                .NotEmpty().WithMessage("Адрес не может быть пустым");

            RuleFor(x => x.ShippingPhoneNumber)
                .NotEmpty().WithMessage("Телефон не может быть пустым")
                .Matches(@"^(\+375(25|29|33|44)[0-9]{7})|(\+375(\s+)?(25|29|33|44)(\s+)?[0-9]{7})|(\+375-?(25|29|33|44)-?[0-9]{7})$")
                .WithMessage("Телефон должен иметь формат: +375xxxxxxxxx / +375 xx xxxxxxx / +375-xx-xxxxxxx");
        }
    }
}
