﻿using FluentValidation;
using ShopOfBikes.BLL.DTO.Property;

namespace ShopOfBikes.BLL.Validation.Property
{
    public class PropertyFloatValueForCreationUpdateDtoValidator : AbstractValidator<PropertyFloatValueForCreationUpdateDto>
    {
        public PropertyFloatValueForCreationUpdateDtoValidator()
        {
            RuleFor(x => x.Value)
                .NotEmpty().WithMessage("Значение не может быть пустым");
        }
    }
}
