﻿using FluentValidation;
using ShopOfBikes.BLL.DTO.Property;

namespace ShopOfBikes.BLL.Validation.Property
{
    public class PropertyStringValueForCreationUpdateDtoValidator : AbstractValidator<PropertyStringValueForCreationUpdateDto>
    {
        public PropertyStringValueForCreationUpdateDtoValidator()
        {
            RuleFor(x => x.Value)
                .NotEmpty().WithMessage("Значение не может быть пустым")
                .MinimumLength(1).WithMessage("Минимальная длина значения не может быть меньше одного знака");
        }
    }
}
