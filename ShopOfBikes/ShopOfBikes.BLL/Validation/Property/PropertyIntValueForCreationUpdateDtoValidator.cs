﻿using FluentValidation;
using ShopOfBikes.BLL.DTO.Property;

namespace ShopOfBikes.BLL.Validation.Property
{
    public class PropertyIntValueForCreationUpdateDtoValidator : AbstractValidator<PropertyIntValueForCreationUpdateDto>
    {
        public PropertyIntValueForCreationUpdateDtoValidator()
        {
            RuleFor(x => x.Value)
                .NotEmpty().WithMessage("Значение не может быть пустым")
                .InclusiveBetween(1, 9999).WithMessage("Превышено количество символов");
        }
    }
}
