﻿using FluentValidation;
using ShopOfBikes.BLL.DTO.Cart;

namespace ShopOfBikes.BLL.Validation.Cart
{
    public class CartForCreationDtoValidator : AbstractValidator<CartForCreationDto>
    {
        public CartForCreationDtoValidator()
        {

            RuleFor(x => x.ProductId)
                .NotEmpty().WithMessage("Ид велосипеда не может быть пустым");

            RuleFor(x => x.Quantity)
                .NotEmpty().WithMessage("Количество не может быть пустым")
                .GreaterThanOrEqualTo(0u).WithMessage("Количество должно быть больше  либо равно 0");
        }
    }
}
