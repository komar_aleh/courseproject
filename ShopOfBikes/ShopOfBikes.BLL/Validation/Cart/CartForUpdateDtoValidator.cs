﻿using FluentValidation;
using ShopOfBikes.BLL.DTO.Cart;

namespace ShopOfBikes.BLL.Validation.Cart
{
    public class CartForUpdateDtoValidator : AbstractValidator<CartForUpdateDto>
    {
        public CartForUpdateDtoValidator()
        {
            RuleFor(x => x.Quantity)
                .NotEmpty().WithMessage("Количество не может быть пустым")
                .GreaterThanOrEqualTo(0u).WithMessage("Количество должно быть больше  либо равно 0");
        }
    }
}
