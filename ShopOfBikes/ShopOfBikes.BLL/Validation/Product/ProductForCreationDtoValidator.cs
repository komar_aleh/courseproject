﻿using FluentValidation;
using ShopOfBikes.BLL.DTO.Product;

namespace ShopOfBikes.BLL.Validation.Product
{
    public class ProductForCreationDtoValidator : AbstractValidator<ProductForCreationDto>
    {
        public ProductForCreationDtoValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty().WithMessage("Название не может быть пустым")
                .Length(2, 100).WithMessage("Название должно быть длинною от 2 до 100 символов");

            RuleFor(x => x.Price)
                .NotEmpty().WithMessage("Цена не может быть пустой");

            RuleFor(x => x.IsAvailable)
                .NotNull().WithMessage("Доступность не может быть пустой");

            RuleFor(x => x.Quantity)
                .NotEmpty().WithMessage("Количество не может быть пустым")
                .GreaterThanOrEqualTo(0u).WithMessage("Количество должно быть больше либо равно 0");
        }
    }
}
