﻿using FluentValidation;
using ShopOfBikes.BLL.DTO.Parameters;

namespace ShopOfBikes.BLL.Validation.Parameters
{
    public class IntPropertyParametersValidator : AbstractValidator<IntPropertyParameters>
    {
        public IntPropertyParametersValidator()
        {
            RuleFor(x => x.Value)
                .InclusiveBetween(0, 9999).WithMessage("Превышено количество символов");
        }
    }
}
