﻿using FluentValidation;
using ShopOfBikes.BLL.DTO.Parameters;

namespace ShopOfBikes.BLL.Validation.Parameters
{
    public class StringPropertyParametersValidator : AbstractValidator<StringPropertyParameters>
    {
        public StringPropertyParametersValidator()
        {
            RuleFor(x => x.Value)
                .Length(1, 100).WithMessage("Значение должно быть длинною от 2 до 50 символов");
        }
    }
}
