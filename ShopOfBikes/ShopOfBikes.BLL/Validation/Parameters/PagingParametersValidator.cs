﻿using FluentValidation;
using ShopOfBikes.BLL.DTO.Parameters;

namespace ShopOfBikes.BLL.Validation.Parameters
{
    public class PagingParametersValidator : AbstractValidator<PagingParameters>
    {
        public PagingParametersValidator()
        {
            RuleFor(x => x.PageNumber)
                .InclusiveBetween(0, 9999).WithMessage("Превышено количество символов");
            RuleFor(x => x.PageSize)
                .InclusiveBetween(0, 200).WithMessage("Размер страницы не может быть больше 200 элементов");
        }
    }
}
