﻿using FluentValidation;
using ShopOfBikes.BLL.DTO.User;
using System.Linq;

namespace ShopOfBikes.BLL.Validation.User
{
    public class UserForUpdateDtoValidator : AbstractValidator<UserForUpdateDto>
    {
        public UserForUpdateDtoValidator()
        {
            RuleFor(x => x.FirstName)
                .NotEmpty().WithMessage("Имя не может быть пустым")
                .Length(2, 20).WithMessage("Имя должно быть длинною от 2 до 20 символов")
                .Must(c => c.All(char.IsLetter)).WithMessage("Имя должно содержать только буквы.");

            RuleFor(x => x.LastName)
                .NotEmpty().WithMessage("Фамилия не может быть пустым")
                .Length(2, 30).WithMessage("Фамилия быть длинною от 2 до 30 символов")
                .Must(c => c.All(char.IsLetter)).WithMessage("Фамилия должна содержать только буквы.");
        }
    }
}
