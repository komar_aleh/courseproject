﻿using FluentValidation;
using ShopOfBikes.BLL.DTO.User;

namespace ShopOfBikes.BLL.Validation.User
{
    public class ResetPasswordModelValidator : AbstractValidator<ResetPasswordModel>
    {
        public ResetPasswordModelValidator()
        {
            RuleFor(x => x.UserId)
                .NotEmpty().WithMessage("{PropertyName} не может быть пустым");

            RuleFor(x => x.Password)
                .NotEmpty().WithMessage("Пароль не может быть пустым")
                .MinimumLength(6).WithMessage("Пароль должен быть длинною не менее 6 символов")
                .MaximumLength(100).WithMessage("Пароль должен быть длинною не более 100 символов")
                /*.Matches("[A-Z]").WithMessage("Password must contain 1 uppercase letter")
                .Matches("[a-z]").WithMessage("Password must contain 1 lowercase letter")
                .Matches("[0-9]").WithMessage("Password must contain a number")
                .Matches("[^a-zA-Z0-9]").WithMessage("Password must contain non alphanumeric")
                .Matches("^(?=\\S+$).{8,}$").WithMessage("Password may not contain whitespaces!")*/;

            RuleFor(x => x.ConfirmPassword)
                .NotEmpty().WithMessage("Поле подтверждения пароля не может быть пустым")
                .Equal(x => x.Password).WithMessage("Вы ввели два разных пароля. Попробуйте еще раз.");
        }
    }
}
