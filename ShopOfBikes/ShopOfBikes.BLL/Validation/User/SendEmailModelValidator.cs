﻿using FluentValidation;
using ShopOfBikes.BLL.DTO.User;

namespace ShopOfBikes.BLL.Validation.User
{
    public class SendEmailModelValidator : AbstractValidator<SendEmailModel>
    {
        public SendEmailModelValidator()
        {
            RuleFor(x => x.Email)
                .NotEmpty().WithMessage("Email адрес не может быть пустым")
                .EmailAddress().WithMessage("Поле Email имеет неверный формат.");
        }
    }
}
