﻿using FluentValidation;
using ShopOfBikes.BLL.DTO.User;
using System.Linq;

namespace ShopOfBikes.BLL.Validation.User
{
    public class UserRegisterDtoValidator : AbstractValidator<UserRegisterDto>
    {
        public UserRegisterDtoValidator()
        {
            RuleFor(x => x.UserName)
                .NotEmpty().WithMessage("Имя пользователя не может быть пустым")
                .Length(2, 20).WithMessage("Имя пользователя должно быть длинною от 2 до 20 символов");

            RuleFor(x => x.FirstName)
                .NotEmpty().WithMessage("Имя не может быть пустым")
                .Length(2, 20).WithMessage("Имя должно быть длинною от 2 до 20 символов")
                .Must(c => c.All(char.IsLetter)).WithMessage("Имя должно содержать только буквы.");

            RuleFor(x => x.LastName)
                .NotEmpty().WithMessage("Фамилия не может быть пустым")
                .Length(2, 30).WithMessage("Фамилия быть длинною от 2 до 30 символов")
                .Must(c => c.All(char.IsLetter)).WithMessage("Фамилия должна содержать только буквы.");

            RuleFor(x => x.Password)
                .NotEmpty().WithMessage("Пароль не может быть пустым")
                .MinimumLength(6).WithMessage("Пароль должен быть длинною не менее 6 символов")
                .MaximumLength(100).WithMessage("Пароль должен быть длинною не более 100 символов")
                /*.Matches("[A-Z]").WithMessage("Password must contain 1 uppercase letter")
                .Matches("[a-z]").WithMessage("Password must contain 1 lowercase letter")
                .Matches("[0-9]").WithMessage("Password must contain a number")
                .Matches("[^a-zA-Z0-9]").WithMessage("Password must contain non alphanumeric")
                .Matches("^(?=\\S+$).{8,}$").WithMessage("Password may not contain whitespaces!")*/;

            RuleFor(x => x.ConfirmPassword)
                .NotEmpty().WithMessage("Поле подтверждения пароля не может быть пустым")
                .Equal(x => x.Password).WithMessage("Вы ввели два разных пароля. Попробуйте еще раз.");

            RuleFor(x => x.Email)
                .NotEmpty().WithMessage("Email не может быть пустым")
                .EmailAddress().WithMessage("Email имеет неверный формат.");
        }
    }
}
