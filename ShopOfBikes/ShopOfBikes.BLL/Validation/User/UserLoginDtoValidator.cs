﻿using FluentValidation;
using ShopOfBikes.BLL.DTO.User;

namespace ShopOfBikes.BLL.Validation.User
{
    public class UserLoginDtoValidator : AbstractValidator<UserForLoginDto>
    {
        public UserLoginDtoValidator()
        {
            RuleFor(x => x.UserName)
                .NotEmpty().WithMessage("Имя не может быть пустым")
                .Length(2, 20).WithMessage("Имя должно быть длинною от 2 до 20 символов");

            RuleFor(x => x.Password)
                .NotEmpty().WithMessage("{PropertyName} не может быть пустым")
                .MinimumLength(6).WithMessage("Пароль должен быть длинною не менее 6 символов")
                .MaximumLength(100).WithMessage("Пароль должен быть длинною не более 100 символов")
                /*.Matches("[A-Z]").WithMessage("Password must contain 1 uppercase letter")
                .Matches("[a-z]").WithMessage("Password must contain 1 lowercase letter")
                .Matches("[0-9]").WithMessage("Password must contain a number")
                .Matches("[^a-zA-Z0-9]").WithMessage("Password must contain non alphanumeric")
                .Matches("^(?=\\S+$).{8,}$").WithMessage("Password may not contain whitespaces!")*/;
        }
    }
}
