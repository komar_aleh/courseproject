﻿using ShopOfBikes.BLL.DTO.Email;
using System.Threading.Tasks;

namespace ShopOfBikes.BLL.Interfaces
{
    public interface IEmailService
    {
        Task SendEmailAsync(EmailModel model);
    }
}
