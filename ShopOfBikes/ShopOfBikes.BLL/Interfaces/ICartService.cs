﻿using ShopOfBikes.BLL.DTO.Cart;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShopOfBikes.BLL.Interfaces
{
    public interface ICartService
    {
        Task<List<CartDto>> GetAll(string userId);
        Task<List<CartDto>> GetAllWithoutUser(string userId);

        Task<CartDto> GetById(int id);

        Task<CartDto> Add(CartForCreationDto item, string userId);

        Task Delete(int id);

        Task DeleteAll(string userId);

        Task<CartDto> Update(int id, CartForUpdateDto item);
    }
}
