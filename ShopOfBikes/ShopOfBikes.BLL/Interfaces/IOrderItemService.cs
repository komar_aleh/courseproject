﻿using ShopOfBikes.BLL.DTO.OrderItem;
using ShopOfBikes.BLL.DTO.Parameters;
using ShopOfBikes.BLL.Wrappers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShopOfBikes.BLL.Interfaces
{
    public interface IOrderItemService
    {
        Task<OrderItemDto> GetById(int id);

        Task<OrderItemDto> Add(OrderItemForCreationDto item);

        Task Delete(int id);

        Task<OrderItemDto> Update(int id, OrderItemForUpdateDto item);

        public Task<PagedResponse<List<OrderItemDto>>> GetAllPagedResponse(
            OrderItemsParameters requestParameters, PagingParameters pagingParameters, string route);
    }
}
