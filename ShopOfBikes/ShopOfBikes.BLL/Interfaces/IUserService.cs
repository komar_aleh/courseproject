﻿using ShopOfBikes.BLL.DTO.Parameters;
using ShopOfBikes.BLL.DTO.User;
using ShopOfBikes.BLL.Wrappers;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;

namespace ShopOfBikes.BLL.Interfaces
{
    public interface IUserService
    {
        Task<UserDto> Register(UserRegisterDto model);
        Task<UserDto> RegisterAdmin(UserRegisterDto model);
        Task<JwtSecurityToken> Login(UserForLoginDto model);
        Task<UserDto> GetById(string id);
        Task<UserDto> Update(string id, UserForUpdateDto user);
        Task Delete(string id);
        Task<UserDto> ConfirmEmail(string userId, string token);
        Task<UserDto> ForgotPassword(SendEmailModel model);
        Task<UserDto> Activate(SendEmailModel model);
        Task<UserDto> ResetPassword(ResetPasswordModel model);
        Task<PagedResponse<List<UserDto>>> GetAllPagedResponseAsync(UsersParameters requestParameters, PagingParameters pagingParameters, string route);
        Task<UserDto> ChangePassword(string userId, ChangePasswordModel model);
    }
}
