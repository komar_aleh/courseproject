﻿using Microsoft.AspNetCore.Http;

namespace ShopOfBikes.BLL.Interfaces
{
    public interface IFileService
    {
        string UploadImage(IFormFile file);
    }
}
