﻿using ShopOfBikes.BLL.DTO.Order;
using ShopOfBikes.BLL.DTO.Parameters;
using ShopOfBikes.BLL.Wrappers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShopOfBikes.BLL.Interfaces
{
    public interface IOrderService
    {
        Task<OrderDto> GetById(int id);

        Task<OrderAfterCreationDto> Add(OrderForCreationDto item, string userId);

        Task Delete(int id);

        Task<OrderDto> Update(int id, OrderForUpdateDto item);

        Task<PagedResponse<List<OrderDto>>> GetAllPagedResponse(
            OrdersParameters requestParameters, PagingParameters pagingParameters, string route);
    }
}
