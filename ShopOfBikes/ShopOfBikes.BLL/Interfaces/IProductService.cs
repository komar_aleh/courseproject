﻿using ShopOfBikes.BLL.DTO.Parameters;
using ShopOfBikes.BLL.DTO.Product;
using ShopOfBikes.BLL.Wrappers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShopOfBikes.BLL.Interfaces
{
    public interface IProductService
    {
        Task<ProductDetailsDto> GetById(int id);

        Task<ProductDetailsDto> Add(ProductForCreationDto item);

        Task Delete(int id);

        Task<ProductDetailsDto> Update(int id, ProductForUpdateDto item);

        public Task<PagedResponse<List<ProductDetailsDto>>> GetAllPagedResponse(
            ProductsParameters requestParameters, PagingParameters pagingParameters, string route);

        Task DecreaseQuantity(int id, uint quantity);

        Task IncreaseQuantity(int id, uint quantity);

        Task UpdateAfterCancellation(int id, ProductForUpdateDto item);

        (decimal min, decimal max) GetMinMaxPrice();

        (float?, float?) GetMinMaxWeight();

        Task<decimal> GetMinPrice();

        Task<decimal> GetMaxPrice();

        Task<float?> GetMinWeight();
        Task<float?> GetMaxWeight();
    }
}
