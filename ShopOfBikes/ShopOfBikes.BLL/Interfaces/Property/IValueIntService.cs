﻿using ShopOfBikes.BLL.DTO.Parameters;
using ShopOfBikes.BLL.DTO.Property;
using ShopOfBikes.BLL.Wrappers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShopOfBikes.BLL.Interfaces.Property
{
    public interface IValueIntService<T> where T : class
    {
        Task<PropertyIntValueDto> GetById(int id);

        Task<PropertyIntValueDto> Add(PropertyIntValueForCreationUpdateDto item);

        Task Delete(int id);

        Task<PropertyIntValueDto> Update(int id, PropertyIntValueForCreationUpdateDto item);

        Task<List<PropertyIntValueDto>> GetAllItems();

        Task<PagedResponse<List<PropertyIntValueDto>>> GetAllPagedResponse(
            IntPropertyParameters requestParameters, PagingParameters pagingParameters, string route);
    }
}
