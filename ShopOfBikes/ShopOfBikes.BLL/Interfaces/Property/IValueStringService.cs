﻿using ShopOfBikes.BLL.DTO.Parameters;
using ShopOfBikes.BLL.DTO.Property;
using ShopOfBikes.BLL.Wrappers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShopOfBikes.BLL.Interfaces.Property
{
    public interface IValueStringService<T> where T : class
    {
        Task<PropertyStringValueDto> GetById(int id);

        Task<PropertyStringValueDto> Add(PropertyStringValueForCreationUpdateDto item);

        Task Delete(int id);

        Task<PropertyStringValueDto> Update(int id, PropertyStringValueForCreationUpdateDto item);

        Task<List<PropertyStringValueDto>> GetAllItems();

        Task<PagedResponse<List<PropertyStringValueDto>>> GetAllPagedResponse(
            StringPropertyParameters requestParameters, PagingParameters pagingParameters, string route);
    }
}
