﻿using ShopOfBikes.BLL.Wrappers;
using System.Threading.Tasks;

namespace ShopOfBikes.BLL.Interfaces.Property
{
    public interface IAllPropertiesService
    {
        Task<ResponseListProperties> GetAllProperties();
    }
}
