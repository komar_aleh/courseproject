﻿using ShopOfBikes.BLL.DTO.Parameters;
using ShopOfBikes.BLL.DTO.Property;
using ShopOfBikes.BLL.Wrappers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShopOfBikes.BLL.Interfaces.Property
{
    public interface IValueFloatService<T> where T : class
    {
        Task<PropertyFloatValueDto> GetById(int id);

        Task<PropertyFloatValueDto> Add(PropertyFloatValueForCreationUpdateDto item);

        Task Delete(int id);

        Task<PropertyFloatValueDto> Update(int id, PropertyFloatValueForCreationUpdateDto item);

        Task<List<PropertyFloatValueDto>> GetAllItems();

        Task<PagedResponse<List<PropertyFloatValueDto>>> GetAllPagedResponse(
            FloatPropertyParameters requestParameters, PagingParameters pagingParameters, string route);
    }
}
