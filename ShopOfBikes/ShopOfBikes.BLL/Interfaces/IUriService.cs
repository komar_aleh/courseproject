﻿using ShopOfBikes.BLL.DTO.Parameters;
using System;

namespace ShopOfBikes.BLL.Interfaces
{
    public interface IUriService
    {
        public Uri GetPageUri(PagingParameters queryParameters, string route);
    }
}
