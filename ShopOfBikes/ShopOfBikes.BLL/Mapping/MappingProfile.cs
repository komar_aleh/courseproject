﻿using AutoMapper;
using ShopOfBikes.BLL.DTO.Cart;
using ShopOfBikes.BLL.DTO.Order;
using ShopOfBikes.BLL.DTO.OrderItem;
using ShopOfBikes.BLL.DTO.Product;
using ShopOfBikes.BLL.DTO.Property;
using ShopOfBikes.BLL.DTO.User;
using ShopOfBikes.DAL.Entities;
using ShopOfBikes.DAL.Entities.Property;
using System;

namespace ShopOfBikes.BLL.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Product, ProductDetailsDto>().ForMember(x => x.DateCreated,
                opt => opt.MapFrom(src => ((DateTime)src.DateCreated).ToString("O")));
            CreateMap<ProductForCreationDto, Product>();
            CreateMap<ProductForUpdateDto, Product>();
            CreateMap<ProductDetailsDto, ProductForUpdateDto>();

            CreateMap<Order, OrderDto>().ForMember(x => x.OrderDate,
                opt => opt.MapFrom(src => ((DateTime)src.OrderDate).ToString("O")));
            CreateMap<Order, OrderAfterCreationDto>();
            CreateMap<OrderForCreationDto, Order>();
            CreateMap<OrderForUpdateDto, Order>();

            CreateMap<Status, PropertyStringValueDto>().ReverseMap();

            CreateMap<OrderItem, OrderItemDto>();
            CreateMap<OrderItemForCreationDto, OrderItem>();
            CreateMap<OrderItemForUpdateDto, OrderItem>();

            CreateMap<User, UserDto>();
            CreateMap<UserForUpdateDto, User>();
            CreateMap<UserRegisterDto, UserDto>();

            CreateMap<Cart, CartDto>();
            CreateMap<CartForCreationDto, Cart>();
            CreateMap<CartForUpdateDto, Cart>();
            CreateMap<Cart, CartDto>();
            CreateMap<CartDto, OrderItemForCreationDto>();

            CreateMap<AbsorberStroke, PropertyIntValueDto>();
            CreateMap<PropertyIntValueForCreationUpdateDto, AbsorberStroke>();

            CreateMap<Cassette, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, Cassette>();

            CreateMap<Chain, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, Chain>();

            CreateMap<ClassOfBike, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, ClassOfBike>();

            CreateMap<CommonDate, PropertyIntValueDto>();
            CreateMap<PropertyIntValueForCreationUpdateDto, CommonDate>();

            CreateMap<ForkMaterial, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, ForkMaterial>();

            CreateMap<ForkModel, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, ForkModel>();

            CreateMap<ForkType, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, ForkType>();

            CreateMap<FrameColor, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, FrameColor>();

            CreateMap<FrameMaterial, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, FrameMaterial>();

            CreateMap<FrameSize, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, FrameSize>();

            CreateMap<FrontBrakeModel, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, FrontBrakeModel>();

            CreateMap<FrontBrakeType, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, FrontBrakeType>();

            CreateMap<FrontStar, PropertyIntValueDto>();
            CreateMap<PropertyIntValueForCreationUpdateDto, FrontStar>();

            CreateMap<FrontSwitch, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, FrontSwitch>();

            CreateMap<GripsModel, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, GripsModel>();

            CreateMap<HandlebarModel, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, HandlebarModel>();

            CreateMap<HandlebarType, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, HandlebarType>();

            CreateMap<Manufacturer, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, Manufacturer>();

            CreateMap<PedalsMaterial, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, PedalsMaterial>();

            CreateMap<PedalsModel, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, PedalsModel>();

            CreateMap<PedalsType, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, PedalsType>();

            CreateMap<RearBrakeModel, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, RearBrakeModel>();

            CreateMap<RearBrakeType, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, RearBrakeType>();

            CreateMap<RearStar, PropertyIntValueDto>();
            CreateMap<PropertyIntValueForCreationUpdateDto, RearStar>();

            CreateMap<RearSwitch, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, RearSwitch>();

            CreateMap<RimMaterial, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, RimMaterial>();

            CreateMap<SaddleModel, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, SaddleModel>();

            CreateMap<SaddleType, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, SaddleType>();

            CreateMap<ShifterModel, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, ShifterModel>();

            CreateMap<ShifterType, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, ShifterType>();

            CreateMap<SpeedNumber, PropertyIntValueDto>();
            CreateMap<PropertyIntValueForCreationUpdateDto, SpeedNumber>();

            CreateMap<SystemModel, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, SystemModel>();

            CreateMap<TiresModel, PropertyStringValueDto>();
            CreateMap<PropertyStringValueForCreationUpdateDto, TiresModel>();

            CreateMap<WheelDiameter, PropertyFloatValueDto>();
            CreateMap<PropertyFloatValueForCreationUpdateDto, WheelDiameter>();
        }
    }
}
