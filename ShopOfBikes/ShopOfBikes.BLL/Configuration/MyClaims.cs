﻿namespace ShopOfBikes.BLL.Configuration
{
    public class MyClaims
    {
        public const string UserName = "UserName";
        public const string Email = "Email";
        public const string UserId = "UserId";
        public const string ExpireDate = "ExpireDate";
        public const string Role = "Role";
    }
}