﻿using FluentValidation.AspNetCore;
using Microsoft.Extensions.DependencyInjection;
using ShopOfBikes.BLL.Validation.Cart;
using System.Reflection;

namespace ShopOfBikes.BLL.Extensions
{
    public static class ServiceExtensions
    {
        public static void AddAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(Assembly.GetExecutingAssembly());
        }

        public static void AddFluentValidation(this IServiceCollection services)
        {
            services.AddFluentValidation(x =>
                x.RegisterValidatorsFromAssemblyContaining<CartForCreationDtoValidator>());
        }
    }
}