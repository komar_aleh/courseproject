﻿using ShopOfBikes.BLL.DTO.Parameters;
using ShopOfBikes.BLL.Interfaces;
using ShopOfBikes.BLL.Wrappers;
using System.Collections.Generic;

namespace ShopOfBikes.BLL.Helpers
{
    public class PaginationHelper
    {
        public static PagedResponse<List<T>> CreatePagedResponse<T>(List<T> pagedData, PagingParameters queryParameters, IUriService uriService, string route, RecordsCount recordsCount)
        {
            var response = new PagedResponse<List<T>>(pagedData, queryParameters.PageNumber, queryParameters.PageSize, recordsCount);
            response.NextPage =
                queryParameters.PageNumber >= 1 && queryParameters.PageNumber < response.TotalPages
                    ? uriService.GetPageUri(new PagingParameters { PageNumber = queryParameters.PageNumber + 1, PageSize = queryParameters.PageSize }, route)
                    : null;
            response.PreviousPage =
                queryParameters.PageNumber - 1 >= 1 && queryParameters.PageNumber <= response.TotalPages
                    ? uriService.GetPageUri(new PagingParameters { PageNumber = queryParameters.PageNumber - 1, PageSize = queryParameters.PageSize }, route)
                    : null;
            response.FirstPage = uriService.GetPageUri(new PagingParameters { PageNumber = 1, PageSize = queryParameters.PageSize }, route);
            response.LastPage = uriService.GetPageUri(new PagingParameters { PageNumber = response.TotalPages, PageSize = queryParameters.PageSize }, route);
            response.Message = $"Выбрано { recordsCount.RecordsFiltered } элементов";
            return response;
        }
    }
}