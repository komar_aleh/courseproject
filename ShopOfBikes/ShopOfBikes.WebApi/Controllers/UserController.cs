﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ShopOfBikes.BLL.DTO.Parameters;
using ShopOfBikes.BLL.DTO.User;
using ShopOfBikes.BLL.Exceptions;
using ShopOfBikes.BLL.Interfaces;
using ShopOfBikes.BLL.Wrappers;
using ShopOfBikes.DAL.Shared;
using ShopOfBikes.WebApi.Extensions;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;

namespace ShopOfBikes.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _service;
        private readonly ILogger<UserController> _logger;
        private readonly IMemoryCache _memoryCache;
        private readonly IConfiguration _configuration;

        public UserController(
            IUserService service,
            IMemoryCache memoryCache,
            ILogger<UserController> logger,
            IConfiguration configuration
            )
        {
            _service = service;
            _logger = logger;
            _memoryCache = memoryCache;
            _configuration = configuration;
        }

        /// <summary>
        /// POST api/controller/register
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] UserRegisterDto model)
        {
            var baseUrl = Request.Path.Value?.ToLower().Remove(9);

            var user = await _service.Register(model);

            if (_memoryCache is MemoryCache memoryCache)
                memoryCache.Compact(1.0);

            _logger.LogInformation("New user added to the database");

            return Created(string.Concat(baseUrl, "/", user.Id), new Response<UserDto>(user)
            {
                Succeeded = true,
                Message = "Пользователь создан успешно! Подтвердите свою учётную запись перейдя по ссылке, отправленной вам на электронную почту."
            });
        }

        /// <summary>
        /// POST api/controller/reg-admin
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = UserRoles.Admin)]
        [Route("reg-admin")]
        public async Task<IActionResult> RegisterAdmin([FromBody] UserRegisterDto model)
        {
            var baseUrl = Request.Path.Value?.ToLower().Remove(9);

            var user = await _service.RegisterAdmin(model);

            _logger.LogInformation("New user with role 'Admin' added to the database");

            return Created(string.Concat(baseUrl, "/", user.Id), new Response<UserDto>(user)
            {
                Succeeded = true,
                Message = "Пользователь с ролью администратора создан успешно!"
            });
        }

        /// <summary>
        /// POST api/controller/login
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] UserForLoginDto model)
        {
            var item = await _service.Login(model);
            var token = new JwtSecurityTokenHandler().WriteToken(item);

            return Ok(new Response<string>(token, "Авторизация выполнена успешно."));
        }

        /// <summary>
        /// GET: api/controller
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="pagingParameters"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = UserRoles.Admin)]
        [Produces("application/json")]
        public async Task<IActionResult> GetAll([FromQuery] UsersParameters parameters, [FromQuery] PagingParameters pagingParameters)
        {
            var key = Request.Method + ":" + Request.Path.Value + Request.QueryString;
            var cacheTime = _configuration.GetValue<int>("CacheTimeInSecond");
            var route = Request.Path.Value;

            if (_memoryCache.TryGetValue(key, out PagedResponse<List<UserDto>> pagedResponse))
                return Ok(pagedResponse);

            pagedResponse = await _service.GetAllPagedResponseAsync(parameters, pagingParameters, route);

            _memoryCache.Set(key, pagedResponse,
                new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromSeconds(cacheTime)));

            return Ok(pagedResponse);
        }

        /// <summary>
        /// GET api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [Authorize(Roles = UserRoles.AdminOrUser)]
        [Produces("application/json")]
        public async Task<IActionResult> GetById(string id)
        {
            var cacheTime = _configuration.GetValue<int>("CacheTimeInSecond");
            var key = Request.Method + ":" + Request.Path.Value;

            if (_memoryCache.TryGetValue(key, out UserDto item))
                return Ok(new Response<UserDto>(item));

            item = await _service.GetById(id);

            _memoryCache.Set(key, item,
                new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromSeconds(cacheTime)));

            return Ok(new Response<UserDto>(item)
            {
                Message = $"Выбран пользователь с никнеймом: {item.UserName}"
            });
        }

        /// <summary>
        /// PUT api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Authorize(Roles = UserRoles.AdminOrUser)]
        [Produces("application/json")]
        public async Task<IActionResult> Update(string id, [FromBody] UserForUpdateDto user)
        {
            var updatedUser = await _service.Update(id, user);

            if (_memoryCache is MemoryCache memoryCache)
                memoryCache.Compact(1.0);

            _logger.LogInformation($"User with id={id} updated in DataBase");
            return Ok(new Response<UserDto>(updatedUser, "Пользователь обновлен."));
        }

        /// <summary>
        /// DELETE api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Authorize(Roles = UserRoles.Admin)]
        public async Task<IActionResult> Delete(string id)
        {
            var userId = Request.GetUserId();
            if (userId == id)
                throw new RequestException("Администратор сам себя удалить не может!");

            await _service.Delete(id);

            if (_memoryCache is MemoryCache memoryCache)
                memoryCache.Compact(1.0);

            _logger.LogInformation($"User with id={id} deleted from DataBase");

            return Ok(new Response<string>
            {
                Succeeded = true,
                Message = $"Пользователь с id={id} удален из базы данных."
            });
        }

        /// <summary>
        /// GET: api/controller/confirm-email
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("confirm-email")]
        public async Task<IActionResult> ConfirmEmail([FromQuery] string userId, [FromQuery] string token)
        {
            var user = await _service.ConfirmEmail(userId, token);

            return Ok(new Response<string>()
            {
                Succeeded = true,
                Message = $"Email подтвержден. Пользователь с никнеймом: {user.UserName} активирован"
            });
        }

        /// <summary>
        /// POST: api/controller/forgot-password
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("forgot-password")]
        public async Task<IActionResult> ForgotPassword([FromBody] SendEmailModel model)
        {
            var user = await _service.ForgotPassword(model);

            return Ok(new Response<string>
            {
                Succeeded = true,
                Message = $"Ссылка для сброса пароля отправлена на электронную почту - {user.Email}"
            });
        }

        /// <summary>
        /// POST: api/controller/activate
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("activate")]
        public async Task<IActionResult> Activate([FromBody] SendEmailModel model)
        {
            var user = await _service.Activate(model);

            return Ok(new Response<string>
            {
                Succeeded = true,
                Message = $"Ссылка для активации аккаунта отправлена вам на электронную почту - {user.Email}."
            });
        }


        /// <summary>
        /// POST: api/controller/reset-password
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("reset-password")]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordModel model)
        {
            var user = await _service.ResetPassword(model);

            return Ok(new Response<string>
            {
                Succeeded = true,
                Message = "Пароль изменен."
            });
        }

        /// <summary>
        /// POST: api/controller/reset-password
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = UserRoles.AdminOrUser)]
        [Route("change-password")]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordModel model)
        {
            var userId = Request.GetUserId();
            await _service.ChangePassword(userId, model);

            return Ok(new Response<string>
            {
                Succeeded = true,
                Message = "Пароль изменен."
            });
        }
    }
}
