﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ShopOfBikes.BLL.DTO.OrderItem;
using ShopOfBikes.BLL.DTO.Parameters;
using ShopOfBikes.BLL.Interfaces;
using ShopOfBikes.BLL.Wrappers;
using ShopOfBikes.DAL.Shared;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShopOfBikes.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = UserRoles.AdminOrUser)]
    public class OrderItemController : ControllerBase
    {
        private readonly IOrderItemService _service;
        private readonly ILogger<OrderItemController> _logger;
        private readonly IMemoryCache _memoryCache;
        private readonly IConfiguration _configuration;

        public OrderItemController(
            IOrderItemService service,
            ILogger<OrderItemController> logger,
            IMemoryCache memoryCache,
            IConfiguration configuration)
        {
            _service = service;
            _logger = logger;
            _memoryCache = memoryCache;
            _configuration = configuration;
        }

        /// <summary>
        /// GET: api/controller
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="pagingParameters"></param>
        /// <returns></returns>
        [HttpGet]
        [Produces("application/json")]
        public async Task<IActionResult> GetAll([FromQuery] OrderItemsParameters parameters, [FromQuery] PagingParameters pagingParameters)
        {
            var key = Request.Method + ":" + Request.Path.Value + Request.QueryString;
            var cacheTime = _configuration.GetValue<int>("CacheTimeInSecond");
            var route = Request.Path.Value;

            if (_memoryCache.TryGetValue(key, out PagedResponse<List<OrderItemDto>> pagedResponse))
                return Ok(pagedResponse);

            pagedResponse = await _service.GetAllPagedResponse(parameters, pagingParameters, route);

            _memoryCache.Set(key, pagedResponse,
                new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromSeconds(cacheTime)));

            return Ok(pagedResponse);
        }

        /// <summary>
        /// GET api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}", Name = "OrderItemById")]
        [Produces("application/json")]
        public async Task<IActionResult> GetById(int id)
        {
            var cacheTime = _configuration.GetValue<int>("CacheTimeInSecond");
            var key = Request.Method + ":" + Request.Path.Value;

            if (_memoryCache.TryGetValue(key, out OrderItemDto item))
                return Ok(new Response<OrderItemDto>(item));

            item = await _service.GetById(id);

            _memoryCache.Set(key, item,
                new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromSeconds(cacheTime)));

            return Ok(new Response<OrderItemDto>(item));
        }

        /// <summary>
        /// POST api/controller
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<IActionResult> Create([FromBody] OrderItemForCreationDto item)
        {
            var createdItem = await _service.Add(item);

            if (_memoryCache is MemoryCache memoryCache)
                memoryCache.Compact(1.0);

            _logger.LogInformation($"OrderItem with Id = '{createdItem.Id}' added to the database");
            return Created(string.Concat(Request.Path.Value?.ToLower(), "/", createdItem.Id.ToString()),
                new Response<OrderItemDto>(createdItem, "Товар добавлен в заказ"));
        }

        /// <summary>
        /// DELETE api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _service.Delete(id);

            if (_memoryCache is MemoryCache memoryCache)
                memoryCache.Compact(1.0);

            _logger.LogInformation($"OrderItem with id={id} deleted from DataBase");

            return Ok(new Response<string>
            {
                Succeeded = true,
                Message = $"Элемент с id={id} удален из базы данных"
            });
        }

        /// <summary>
        /// PUT api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPut("{id:int}")]
        [Produces("application/json")]
        public async Task<IActionResult> Update(int id, [FromBody] OrderItemForUpdateDto item)
        {
            var updatedItem = await _service.Update(id, item);

            if (_memoryCache is MemoryCache memoryCache)
                memoryCache.Compact(1.0);

            _logger.LogInformation($"OrderItem with id={id} updated in DataBase.");
            return Ok(new Response<OrderItemDto>(updatedItem, "Элемент корзины обновлен."));
        }
    }
}
