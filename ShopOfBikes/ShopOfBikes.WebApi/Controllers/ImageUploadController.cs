﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ShopOfBikes.BLL.Interfaces;
using ShopOfBikes.DAL.Shared;

namespace ShopOfBikes.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImageUploadController : ControllerBase
    {
        private readonly IFileService _fileService;
        private readonly ILogger<ImageUploadController> _logger;

        public ImageUploadController(IFileService fileService, ILogger<ImageUploadController> logger, IConfiguration configuration)
        {
            _fileService = fileService;
            _logger = logger;
        }

        /// <summary>
        /// POST api/controller
        /// </summary>
        /// <returns></returns>
        [HttpPost, DisableRequestSizeLimit]
        [Authorize(Roles = UserRoles.Admin)]
        public IActionResult ImageUpload([FromForm] IFormFile file)
        {
            var fileName = _fileService.UploadImage(file);

            _logger.LogInformation($"Image {file.FileName} was uploaded. New imageName: {fileName}");
            return StatusCode(201, new
            {
                ImgName = fileName,
                Succeeded = true,
                Message = "Изображение было успешно загружено на сервер"
            });
        }

    }
}