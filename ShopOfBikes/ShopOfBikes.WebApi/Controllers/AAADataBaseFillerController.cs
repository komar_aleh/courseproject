﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ShopOfBikes.DAL.Entities;
using ShopOfBikes.DAL.Interfaces;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ShopOfBikes.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AAADataBaseFillerController : ControllerBase
    {
        private readonly IProductRepositoryAsync _productRepository;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ILogger<AAADataBaseFillerController> _logger;
        private readonly Random _rng;
        private readonly IConfiguration _configuration;


        public AAADataBaseFillerController(ILogger<AAADataBaseFillerController> logger,
            IProductRepositoryAsync productRepository,
            UserManager<User> userManager, RoleManager<IdentityRole> roleManager, IConfiguration configuration)
        {
            _productRepository = productRepository;
            _logger = logger;
            _userManager = userManager;
            _roleManager = roleManager;
            _rng = new Random();
            _configuration = configuration;
        }

        /// <summary>
        ///     Fill data in DataBase
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> FillData()
        {
            var pathToFolder = Path.Combine("Resources", "Images", "Products", "LargeImages");
            bool[] boolean = { true, false };
            float[] weights = { 14.44f, 12.9f, 10.36f, 15.5f, 13.2f, 16.6f, 12.28f, 12.5f, 15.3f, 9.99f, 13.9f };
            string[] names = { "Navigator", "Apache", "Jazz", "Escape 2 City", "Talon 3", "Avalanche Comp", "Aggressor", "Marlin", "Procaliber", "Reacto Team-E", "Rocco", "Rebel", "Scultura", "Flash" };
            var allFiles = Directory.EnumerateFiles(pathToFolder);

            var images = allFiles.Select(filename => filename.Remove(0, 38)).ToList();

            for (var i = 0; i < 100; i++)
            {
                var product = new Product
                {
                    Name = "",
                    AbsorberStrokeId = _rng.Next(1, 13),
                    CassetteId = _rng.Next(1, 7),
                    ChainId = _rng.Next(1, 7),
                    ClassOfBikeId = _rng.Next(1, 6),
                    CommonDateId = _rng.Next(1, 7),
                    DoubleRim = boolean[_rng.Next(0, boolean.Length)],
                    Female = boolean[_rng.Next(0, boolean.Length)],
                    ForkLock = boolean[_rng.Next(0, boolean.Length)],
                    ForkMaterialId = _rng.Next(1, 5),
                    ForkModelId = _rng.Next(1, 70),
                    ForkTypeId = _rng.Next(1, 3),
                    FrameColorId = _rng.Next(1, 31),
                    FrameMaterialId = _rng.Next(1, 5),
                    FrameSizeId = _rng.Next(1, 78),
                    FrontBrakeModelId = _rng.Next(1, 130),
                    FrontBrakeTypeId = _rng.Next(1, 5),
                    FrontStarId = _rng.Next(1, 4),
                    FrontSwitchId = _rng.Next(1, 7),
                    GripsModelId = _rng.Next(1, 5),
                    HandlebarModelId = _rng.Next(1, 4),
                    HandlebarTypeId = _rng.Next(1, 4),
                    HardTail = boolean[_rng.Next(0, boolean.Length)],
                    ImgName = images[_rng.Next(0, images.Count)],
                    IsAvailable = boolean[_rng.Next(0, boolean.Length)],
                    KidTeen = boolean[_rng.Next(0, boolean.Length)],
                    ManufacturerId = _rng.Next(1, 46),
                    PedalsMaterialId = _rng.Next(1, 3),
                    PedalsModelId = _rng.Next(1, 5),
                    PedalsTypeId = _rng.Next(1, 3),
                    Price = _rng.Next(80000, 200000) * 0.01m,
                    Quantity = (uint)_rng.Next(1, 50),
                    RearBrakeModelId = _rng.Next(1, 4),
                    RearBrakeTypeId = _rng.Next(1, 5),
                    RearStarId = _rng.Next(1, 4),
                    RearSwitchId = _rng.Next(1, 7),
                    RimMaterialId = _rng.Next(1, 4),
                    SaddleModelId = _rng.Next(1, 12),
                    SaddleTypeId = _rng.Next(1, 4),
                    ShifterModelId = _rng.Next(1, 96),
                    ShifterTypeId = _rng.Next(1, 6),
                    SystemModelId = _rng.Next(1, 185),
                    TiresModelId = _rng.Next(1, 108),
                    TiresTubeless = boolean[_rng.Next(0, boolean.Length)],
                    Weight = weights[_rng.Next(0, weights.Length)],
                    WheelDiameterId = _rng.Next(1, 7)
                };
                product.SpeedNumberId = product.FrontStarId * product.RearStarId;
                await _productRepository.CreateAsync(product);
                var newProduct = await _productRepository.GetByIdWithProperties(product.Id);

                newProduct.Name = $"Велосипед {newProduct.Manufacturer.Value} {names[_rng.Next(0, names.Length)]} {newProduct.WheelDiameter.Value} {newProduct.FrameSize.Value} {newProduct.CommonDate.Value} ({newProduct.FrameColor.Value})";

                string classOfBike = null;
                string frameMaterial = null;
                string wheelDiameter = null;
                string forkType = null;
                string absorberStroke = null;
                string speedNumber = null;
                string weight = null;
                if (!string.IsNullOrEmpty(newProduct.ClassOfBike?.Value))
                    classOfBike = $"{newProduct.ClassOfBike?.Value}";
                if (!string.IsNullOrEmpty(newProduct.FrameMaterial?.Value))
                    frameMaterial = $", материал рамы: {newProduct.FrameMaterial?.Value}";
                if (newProduct.WheelDiameter?.Value != 0)
                    wheelDiameter = $", колеса {newProduct.WheelDiameter?.Value} дюймов";
                if (!string.IsNullOrEmpty(newProduct.ForkType?.Value))
                    forkType = $", вилка {newProduct.ForkType?.Value} ";
                if (newProduct.AbsorberStroke?.Value != 0)
                    absorberStroke = $"с ходом {newProduct.AbsorberStroke?.Value} мм";
                if (newProduct.SpeedNumber?.Value != 0)
                    speedNumber = $", трансмиссия {newProduct.SpeedNumber?.Value} скор.";
                var breakType = $", тормоза: {newProduct.FrontBrakeType.Value} + {newProduct.RearBrakeType.Value}";

                if (newProduct.Weight != 0)
                    weight = $", {newProduct.Weight:вес: 0.##} кг";

                newProduct.Description = classOfBike + frameMaterial + wheelDiameter + forkType + absorberStroke +
                                         speedNumber + breakType + weight;

                await _productRepository.UpdateAsync(newProduct);
            }

            _logger.LogInformation("100 records uploaded to DataBase.");

            return Ok();
        }
    }
}
