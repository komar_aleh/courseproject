﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ShopOfBikes.BLL.DTO.Parameters;
using ShopOfBikes.BLL.DTO.Product;
using ShopOfBikes.BLL.Interfaces;
using ShopOfBikes.BLL.Wrappers;
using ShopOfBikes.DAL.Shared;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShopOfBikes.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _service;
        private readonly ILogger<ProductController> _logger;
        private readonly IMemoryCache _memoryCache;
        private readonly IConfiguration _configuration;

        public ProductController(
            ILogger<ProductController> logger,
            IProductService service,
            IMemoryCache memoryCache,
            IConfiguration configuration
            )
        {
            _logger = logger;
            _service = service;
            _memoryCache = memoryCache;
            _configuration = configuration;
        }

        /// <summary>
        /// GET: api/controller
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="pagingParameters"></param>
        /// <returns></returns>
        [HttpGet]
        [Produces("application/json")]
        public async Task<IActionResult> GetAll([FromQuery] ProductsParameters parameters, [FromQuery] PagingParameters pagingParameters)
        {
            var key = Request.Method + ":" + Request.Path.Value + Request.QueryString;
            var cacheTime = _configuration.GetValue<int>("CacheTimeInSecond");
            var route = Request.Path.Value;

            if (_memoryCache.TryGetValue(key, out PagedResponse<List<ProductDetailsDto>> pagedResponse))
                return Ok(pagedResponse);

            pagedResponse = await _service.GetAllPagedResponse(parameters, pagingParameters, route);

            _memoryCache.Set(key, pagedResponse,
                new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromSeconds(cacheTime)));

            return Ok(pagedResponse);
        }

        /// <summary>
        /// GET api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        [Produces("application/json")]
        public async Task<IActionResult> GetById(int id)
        {
            var cacheTime = _configuration.GetValue<int>("CacheTimeInSecond");
            var key = Request.Method + ":" + Request.Path.Value;

            if (_memoryCache.TryGetValue(key, out ProductDetailsDto item))
                return Ok(new Response<ProductDetailsDto>(item));

            item = await _service.GetById(id);

            _memoryCache.Set(key, item,
                new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromSeconds(cacheTime)));

            return Ok(new Response<ProductDetailsDto>(item));
        }

        /// <summary>
        /// POST api/controller
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = UserRoles.Admin)]
        [Produces("application/json")]
        public async Task<IActionResult> Create([FromBody] ProductForCreationDto item)
        {
            var createdItem = await _service.Add(item);

            if (_memoryCache is MemoryCache memoryCache)
                memoryCache.Compact(1.0);

            _logger.LogInformation($"Item with Id = '{createdItem.Id}' added to the database");
            return Created(string.Concat(Request.Path.Value?.ToLower(), "/", createdItem.Id.ToString()), new Response<ProductDetailsDto>(createdItem, "Новый велосипед добавлен в базу данных."));
        }

        /// <summary>
        /// DELETE api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        [Authorize(Roles = UserRoles.Admin)]
        public async Task<IActionResult> Delete(int id)
        {
            await _service.Delete(id);

            if (_memoryCache is MemoryCache memoryCache)
                memoryCache.Compact(1.0);

            _logger.LogInformation($"Product with id={id} deleted from DataBase");

            return Ok(new Response<string>
            {
                Succeeded = true,
                Message = $"Велосипед с id={id} удален из базы данных"
            });
        }

        /// <summary>
        /// PUT api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPut("{id:int}")]
        [Authorize(Roles = UserRoles.Admin)]
        [Produces("application/json")]
        public async Task<IActionResult> Update(int id, [FromBody] ProductForUpdateDto item)
        {
            var updatedItem = await _service.Update(id, item);

            if (_memoryCache is MemoryCache memoryCache)
                memoryCache.Compact(1.0);

            _logger.LogInformation($"Product with id={id} updated in DataBase.");


            return Ok(new Response<ProductDetailsDto>(updatedItem, $"Велосипед с id={id} обновлен в базе данных."));
        }
    }
}
