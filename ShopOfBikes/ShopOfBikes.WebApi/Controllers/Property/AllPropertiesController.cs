﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using ShopOfBikes.BLL.Interfaces.Property;
using ShopOfBikes.BLL.Wrappers;
using System;
using System.Threading.Tasks;

namespace ShopOfBikes.WebApi.Controllers.Property
{
    [AllowAnonymous]
    [Route("api/[controller]")]
    [ApiController]
    public class AllPropertiesController : ControllerBase
    {
        private readonly IAllPropertiesService _service;
        private readonly IMemoryCache _memoryCache;
        private readonly IConfiguration _configuration;

        public AllPropertiesController(IAllPropertiesService service, IMemoryCache memoryCache, IConfiguration configuration)
        {
            _service = service;
            _memoryCache = memoryCache;
            _configuration = configuration;
        }

        /// <summary>
        /// GET: api/controller
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Produces("application/json")]
        public async Task<IActionResult> GetAll()
        {
            var key = Request.Method + ":" + Request.Path.Value + Request.QueryString;
            var cacheTime = _configuration.GetValue<int>("CacheTimeInSecond");

            if (_memoryCache.TryGetValue(key, out ResponseListProperties properties))
                return Ok(properties);

            properties = await _service.GetAllProperties();

            _memoryCache.Set(key, properties,
                new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromSeconds(cacheTime)));

            return Ok(properties);
        }

    }
}
