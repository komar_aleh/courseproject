﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ShopOfBikes.BLL.DTO.Parameters;
using ShopOfBikes.BLL.DTO.Property;
using ShopOfBikes.BLL.Interfaces.Property;
using ShopOfBikes.BLL.Wrappers;
using ShopOfBikes.DAL.Entities.Property;
using ShopOfBikes.DAL.Shared;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace ShopOfBikes.WebApi.Controllers.Property
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = UserRoles.Admin)]
    public class SaddleTypeController : ControllerBase
    {
        private readonly IValueStringService<SaddleType> _service;
        private readonly ILogger<SaddleTypeController> _logger;
        private readonly IMemoryCache _memoryCache;
        private readonly IConfiguration _configuration;

        public SaddleTypeController(
            IValueStringService<SaddleType> service,
            ILogger<SaddleTypeController> logger,
            IMemoryCache memoryCache,
            IConfiguration configuration
            )
        {
            _service = service;
            _logger = logger;
            _memoryCache = memoryCache;
            _configuration = configuration;
        }


        /// <summary>
        /// GET: api/controller
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="pagingParameters"></param>
        /// <returns></returns>
        [HttpGet]
        [Produces("application/json")]
        public async Task<IActionResult> GetAll([FromQuery] StringPropertyParameters parameters, [FromQuery] PagingParameters pagingParameters)
        {
            var key = Request.Method + ":" + Request.Path.Value + Request.QueryString;
            var cacheTime = _configuration.GetValue<int>("CacheTimeInSecond");
            var route = Request.Path.Value;

            if (_memoryCache.TryGetValue(key, out PagedResponse<List<PropertyStringValueDto>> pagedResponse))
                return Ok(pagedResponse);

            pagedResponse = await _service.GetAllPagedResponse(parameters, pagingParameters, route);

            _memoryCache.Set(key, pagedResponse,
                new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromSeconds(cacheTime)));

            return Ok(pagedResponse);
        }

        /// <summary>
        /// GET api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        [Produces("application/json")]
        public async Task<IActionResult> GetById(int id)
        {
            var cacheTime = _configuration.GetValue<int>("CacheTimeInSecond");
            var key = Request.Method + ":" + Request.Path.Value;

            if (_memoryCache.TryGetValue(key, out PropertyStringValueDto item))
                return Ok(new Response<PropertyStringValueDto>(item));

            item = await _service.GetById(id);

            _memoryCache.Set(key, item,
                new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromSeconds(cacheTime)));

            return Ok(new Response<PropertyStringValueDto>(item));
        }

        /// <summary>
        /// POST api/controller
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        public async Task<IActionResult> Create([FromBody] PropertyStringValueForCreationUpdateDto item)
        {
            var createdItem = await _service.Add(item);

            if (_memoryCache is MemoryCache memoryCache)
                memoryCache.Compact(1.0);

            _logger.LogInformation($"Item with Id = '{createdItem.Id}' added to the database");
            return Created(string.Concat(Request.Path.Value?.ToLower(), "/", createdItem.Id.ToString()),
                new Response<PropertyStringValueDto>(createdItem, $"элемент со значением '{createdItem.Value}' добавлен в базу данных"));
        }

        /// <summary>
        /// DELETE api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _service.Delete(id);

            if (_memoryCache is MemoryCache memoryCache)
                memoryCache.Compact(1.0);

            _logger.LogInformation($"SaddleType with id={id} deleted from DataBase");

            return Ok(new Response<string>
            {
                Succeeded = true,
                Message = $"Элемент с id={id} удален из базы данных"
            });
        }

        /// <summary>
        /// PUT api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPut("{id:int}")]
        [Produces("application/json")]
        public async Task<IActionResult> Update(int id, [FromBody] PropertyStringValueForCreationUpdateDto item)
        {
            var updatedItem = await _service.Update(id, item);

            if (_memoryCache is MemoryCache memoryCache)
                memoryCache.Compact(1.0);

            _logger.LogInformation($"SaddleType with id={id} updated in DataBase. Updated value - {item.Value}");
            return Ok(new Response<PropertyStringValueDto>(updatedItem, $"Элемент с id={id} обновлен в базе данных. Новое значение - {item.Value}"));
        }

        /// <summary>
        /// GET: api/controller
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getAll")]
        [AllowAnonymous]
        [Produces("application/json")]
        public async Task<IActionResult> GetAllItems()
        {
            var key = Request.Method + ":" + Request.Path.Value + Request.QueryString;
            var cacheTime = _configuration.GetValue<int>("CacheTimeInSecond");

            if (_memoryCache.TryGetValue(key, out List<PropertyStringValueDto> response))
                return Ok(response);

            response = await _service.GetAllItems();

            _memoryCache.Set(key, response,
                new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromSeconds(cacheTime)));

            return Ok(response);
        }
    }
}
