﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ShopOfBikes.BLL.DTO.Property;
using ShopOfBikes.BLL.Interfaces.Property;
using ShopOfBikes.DAL.Entities.Property;
using ShopOfBikes.DAL.Shared;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace ShopOfBikes.WebApi.Controllers.Property
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = UserRoles.Admin)]
    public class StatusController : ControllerBase
    {
        private readonly IValueStringService<Status> _service;
        private readonly ILogger<StatusController> _logger;
        private readonly IMemoryCache _memoryCache;
        private readonly IConfiguration _configuration;

        public StatusController(
            IValueStringService<Status> service,
            ILogger<StatusController> logger,
            IMemoryCache memoryCache,
            IConfiguration configuration
            )
        {
            _service = service;
            _logger = logger;
            _memoryCache = memoryCache;
            _configuration = configuration;
        }

        /// <summary>
        /// GET: api/controller
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = UserRoles.Admin)]
        [Produces("application/json")]
        public async Task<IActionResult> GetAllItems()
        {
            var key = Request.Method + ":" + Request.Path.Value + Request.QueryString;
            var cacheTime = _configuration.GetValue<int>("CacheTimeInSecond");

            if (_memoryCache.TryGetValue(key, out List<PropertyStringValueDto> response))
                return Ok(response);

            response = await _service.GetAllItems();

            _memoryCache.Set(key, response,
                new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromSeconds(cacheTime)));

            return Ok(response);
        }
    }
}
