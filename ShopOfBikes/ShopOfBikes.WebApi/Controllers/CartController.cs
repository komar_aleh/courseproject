﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ShopOfBikes.BLL.DTO.Cart;
using ShopOfBikes.BLL.Interfaces;
using ShopOfBikes.BLL.Wrappers;
using ShopOfBikes.DAL.Shared;
using ShopOfBikes.WebApi.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShopOfBikes.WebApi.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class CartController : ControllerBase
    {
        private readonly ILogger<CartController> _logger;
        private readonly ICartService _service;

        public CartController(ILogger<CartController> logger, ICartService service)
        {
            _logger = logger;
            _service = service;
        }

        /// <summary>
        /// GET: api/controller
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = UserRoles.AdminOrUser)]
        [Produces("application/json")]
        public async Task<IActionResult> GetAll()
        {
            var userId = Request.GetUserId();
            var result = await _service.GetAll(userId);

            return Ok(new Response<List<CartDto>>(result));
        }

        /// <summary>
        /// GET api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        [Authorize(Roles = UserRoles.User)]
        [Produces("application/json")]
        public async Task<IActionResult> GetById(int id)
        {
            var item = await _service.GetById(id);

            return Ok(new Response<CartDto>(item));
        }

        /// <summary>
        /// POST api/controller
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = UserRoles.User)]
        [Produces("application/json")]
        public async Task<IActionResult> Create([FromBody] CartForCreationDto item)
        {
            var userId = Request.GetUserId();
            var createdItem = await _service.Add(item, userId);

            _logger.LogInformation($"New CartItem added to the database. Id = {createdItem.Id}");
            return Created(string.Concat(Request.Path.Value?.ToLower(), "/", createdItem.Id.ToString()),
                new Response<CartDto>(createdItem) { Message = "Товар добавлен в корзину" });
        }

        /// <summary>
        /// DELETE api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        [Authorize(Roles = UserRoles.User)]
        public async Task<IActionResult> Delete(int id)
        {
            await _service.Delete(id);

            _logger.LogInformation($"CartItem with id={id} deleted from DataBase");

            return Ok(new Response<string>
            {
                Succeeded = true,
                Message = "Товар удален из корзины"
            });
        }

        /// <summary>
        /// DELETE api/controller
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        [Authorize(Roles = UserRoles.User)]
        public async Task<IActionResult> DeleteAll()
        {
            var userId = Request.GetUserId();

            await _service.DeleteAll(userId);

            return Ok(new Response<string>
            {
                Succeeded = true,
                Message = "Все товары удалены из корзины"
            });
        }

        /// <summary>
        /// PUT api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPut("{id:int}")]
        [Authorize(Roles = UserRoles.User)]
        [Produces("application/json")]
        public async Task<IActionResult> Update(int id, [FromBody] CartForUpdateDto item)
        {
            var updatedItem = await _service.Update(id, item);

            _logger.LogInformation($"OrderItem with id={id} updated in DataBase.");
            return Ok(new Response<CartDto>(updatedItem, "Товар в корзине обновлен."));
        }
    }
}
