﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ShopOfBikes.BLL.DTO.Order;
using ShopOfBikes.BLL.DTO.Parameters;
using ShopOfBikes.BLL.Interfaces;
using ShopOfBikes.BLL.Wrappers;
using ShopOfBikes.DAL.Shared;
using ShopOfBikes.WebApi.Extensions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShopOfBikes.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _service;
        private readonly ILogger<OrderController> _logger;
        private readonly IMemoryCache _memoryCache;
        private readonly IConfiguration _configuration;


        public OrderController(
            IOrderService service,
            ILogger<OrderController> logger,
            IMemoryCache memoryCache,
            IConfiguration configuration
        )
        {
            _service = service;
            _logger = logger;
            _memoryCache = memoryCache;
            _configuration = configuration;
        }

        /// <summary>
        /// GET: api/controller
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="pagingParameters"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = UserRoles.AdminOrUser)]
        [Produces("application/json")]
        public async Task<IActionResult> GetAll([FromQuery] OrdersParameters parameters, [FromQuery] PagingParameters pagingParameters)
        {
            var key = Request.Method + ":" + Request.Path.Value + Request.QueryString;
            var cacheTime = _configuration.GetValue<int>("CacheTimeInSecond");
            var route = Request.Path.Value;

            if (_memoryCache.TryGetValue(key, out PagedResponse<List<OrderDto>> pagedResponse))
                return Ok(pagedResponse);

            pagedResponse = await _service.GetAllPagedResponse(parameters, pagingParameters, route);

            _memoryCache.Set(key, pagedResponse,
                new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromSeconds(cacheTime)));

            return Ok(pagedResponse);
        }

        /// <summary>
        /// GET api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        [Authorize(Roles = UserRoles.AdminOrUser)]
        [Produces("application/json")]
        public async Task<IActionResult> GetById(int id)
        {
            var cacheTime = _configuration.GetValue<int>("CacheTimeInSecond");
            var key = Request.Method + ":" + Request.Path.Value;

            if (_memoryCache.TryGetValue(key, out OrderDto item))
                return Ok(new Response<OrderDto>(item));

            item = await _service.GetById(id);

            _memoryCache.Set(key, item,
                new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromSeconds(cacheTime)));

            return Ok(new Response<OrderDto>(item));
        }


        /// <summary>
        /// POST api/controller
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = UserRoles.User)]
        [Produces("application/json")]
        public async Task<IActionResult> Create([FromBody] OrderForCreationDto item)
        {
            var userId = Request.GetUserId();
            var createdItem = await _service.Add(item, userId);

            if (_memoryCache is MemoryCache memoryCache)
                memoryCache.Compact(1.0);

            _logger.LogInformation($"New order added to the database. Id = {createdItem.Id}");
            return Created(string.Concat(Request.Path.Value?.ToLower(), "/", createdItem.Id.ToString()), new Response<OrderAfterCreationDto>(createdItem)
            { Message = $"Ваш заказ создан. Номер заказа: {createdItem.Id}" });
        }

        /// <summary>
        /// DELETE api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        [Authorize(Roles = UserRoles.Admin)]
        public async Task<IActionResult> Delete(int id)
        {
            await _service.Delete(id);

            if (_memoryCache is MemoryCache memoryCache)
                memoryCache.Compact(1.0);

            _logger.LogInformation($"Order with id={id} deleted from DataBase");

            return Ok(new Response<string>
            {
                Succeeded = true,
                Message = $"Заказ с номером {id} удален."
            });
        }

        /// <summary>
        /// PUT api/controller/5
        /// </summary>
        /// <param name="id"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPut("{id:int}")]
        [Authorize(Roles = UserRoles.AdminOrUser)]
        [Produces("application/json")]
        public async Task<IActionResult> Update(int id, [FromBody] OrderForUpdateDto item)
        {
            var updatedItem = await _service.Update(id, item);

            if (_memoryCache is MemoryCache memoryCache)
                memoryCache.Compact(1.0);

            _logger.LogInformation($"Order with id={id} updated in DataBase.");
            return Ok(new Response<OrderDto>(updatedItem));
        }
    }
}
