using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ShopOfBikes.BLL.Configuration;
using ShopOfBikes.BLL.Extensions;
using ShopOfBikes.DAL.EF;
using ShopOfBikes.DAL.Entities;
using ShopOfBikes.DAL.Extensions;
using ShopOfBikes.DAL.Shared;
using ShopOfBikes.WebApi.Extensions;

namespace ShopOfBikes.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersExtension();
            services.AddSingleton(Configuration);
            services.AddEntityDbContext(Configuration);

            services.AddRepositories();
            services.AddServices();

            services.AddFluentValidation();
            services.AddAutoMapper();
            services.AddSwaggerExtension();
            services.AddMemoryCache();
            services.AddMiniProfiler(o => o.RouteBasePath = "/profiler").AddEntityFramework();

            services.AddCorsExtension();
            services.AddJwtAuthentication(Configuration);

            services.Configure<EmailConfiguration>(Configuration.GetSection("EmailConfiguration"));

            services.Configure<FormOptions>(o =>
            {
                o.ValueLengthLimit = int.MaxValue;
                o.MultipartBodyLengthLimit = int.MaxValue;
                o.MemoryBufferThreshold = int.MaxValue;
            });

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, AppDbContext dbContext, UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
        {

            dbContext.Database.Migrate();

            SeedData(userManager, roleManager);

            app.UseUploadFiles();

            app.UseRouting();
            app.UseCorsExtension();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseExceptionHandler("/error");
            app.UseResponseStatusCodesHandlingMiddleware();
            app.UseErrorHandlingMiddleware();

            app.UseMiniProfiler();

            app.UseSwaggerExtension();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private void SeedData(UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
        {
            var email = Configuration["AdminData:Email"];
            var password = Configuration["AdminData:Password"];
            var user = new User
            {
                FirstName = "Administrator",
                LastName = "Admin",
                UserName = "admin",
                Email = email,
                EmailConfirmed = true
            };
            var userGuest = new User
            {
                FirstName = "user",
                LastName = "User",
                UserName = "user",
                Email = "string@string.ru",
                EmailConfirmed = true
            };

            if (!roleManager.RoleExistsAsync(UserRoles.User).GetAwaiter().GetResult())
            {
                roleManager.CreateAsync(new IdentityRole(UserRoles.User)).GetAwaiter().GetResult();
            }
            if (!roleManager.RoleExistsAsync(UserRoles.Admin).GetAwaiter().GetResult())
            {
                roleManager.CreateAsync(new IdentityRole(UserRoles.Admin)).GetAwaiter().GetResult();
            }
            var userExistsResult = userManager.FindByEmailAsync(email).GetAwaiter().GetResult();
            if (userExistsResult == null)
            {
                var result = userManager.CreateAsync(user, password).GetAwaiter().GetResult();
                if (result.Succeeded)
                    userManager.AddToRoleAsync(user, UserRoles.Admin).GetAwaiter().GetResult();
            }

            var userExists2Result = userManager.FindByEmailAsync(userGuest.Email).GetAwaiter().GetResult();
            if (userExists2Result != null) return;
            {
                var result = userManager.CreateAsync(userGuest, password).GetAwaiter().GetResult();
                if (result.Succeeded)
                    userManager.AddToRoleAsync(userGuest, UserRoles.User).GetAwaiter().GetResult();
            }
        }
    }
}
