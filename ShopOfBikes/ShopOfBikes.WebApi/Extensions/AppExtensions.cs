﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.FileProviders;
using Newtonsoft.Json;
using ShopOfBikes.BLL.Wrappers;
using ShopOfBikes.WebApi.Middlewares;
using System.IO;
using System.Net;
using static System.IO.Path;

namespace ShopOfBikes.WebApi.Extensions
{
    public static class AppExtensions
    {
        private const string ImgPath = "Resources";
        public static void UseSwaggerExtension(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "ShopOfBikes.WebApi");
            });
        }

        public static void UseErrorHandlingMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<ErrorHandlerMiddleware>();
        }


        public static void UseResponseStatusCodesHandlingMiddleware(this IApplicationBuilder app)
        {
            app.Use(async (context, next) =>
            {
                await next();

                switch (context.Response.StatusCode)
                {
                    case (int)HttpStatusCode.Unauthorized:
                        {
                            var response = context.Response;
                            response.ContentType = "application/json";
                            var responseModel = new Response<string>
                            { Succeeded = false, Message = "Вы не авторизованы. Залогиньтесь!" };
                            var result = JsonConvert.SerializeObject(responseModel,
                                new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });

                            response.StatusCode = (int)HttpStatusCode.Unauthorized;
                            await response.WriteAsync(result);
                            break;
                        }
                    case (int)HttpStatusCode.Forbidden:
                        {
                            var response = context.Response;
                            response.ContentType = "application/json";
                            response.StatusCode = (int)HttpStatusCode.Forbidden;
                            var responseModel = new Response<string>
                            { Succeeded = false, Message = "Вам не разрешено данное действие" };
                            var result = JsonConvert.SerializeObject(responseModel,
                                new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                            await response.WriteAsync(result);
                            break;
                        }
                }
            });
        }

        public static void UseCorsExtension(this IApplicationBuilder app)
        {
            app.UseCors();
        }

        public static void UseUploadFiles(this IApplicationBuilder app)
        {
            var folderLargeName = Combine(ImgPath, "Images", "Products", "LargeImages");
            var folderMediumName = Combine(ImgPath, "Images", "Products", "MediumImages");
            var folderSmallName = Combine(ImgPath, "Images", "Products", "SmallImages");

            var folderLargeExists = Directory.Exists(folderLargeName);
            if (!folderLargeExists)
                Directory.CreateDirectory(folderLargeName);

            var folderMediumExists = Directory.Exists(folderMediumName);
            if (!folderMediumExists)
                Directory.CreateDirectory(folderMediumName);

            var folderSmallExists = Directory.Exists(folderSmallName);
            if (!folderSmallExists)
                Directory.CreateDirectory(folderSmallName);

            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), @"Resources")),
                RequestPath = new PathString("/Resources"),
                OnPrepareResponse = c =>
                {
                    c.Context.Response.Headers.Add("Cache-Control", "public,max-age=86400");
                }
            });
        }
    }
}