﻿using Microsoft.AspNetCore.Http;
using ShopOfBikes.BLL.Configuration;
using System.Linq;

namespace ShopOfBikes.WebApi.Extensions
{
    public static class RequestExtension
    {
        public static string GetUserId(this HttpRequest request)
        {
            if (request == null || string.IsNullOrEmpty(request.Headers["Authorization"]))
                return string.Empty;

            return request
                .HttpContext
                .User
                .Claims
                .SingleOrDefault(x => x.Type == MyClaims.UserId)?
                .Value;
        }
    }
}
