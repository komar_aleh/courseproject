﻿using Microsoft.AspNetCore.Mvc.Filters;
using ShopOfBikes.BLL.Exceptions;
using System.Linq;

namespace ShopOfBikes.WebApi.Filters
{
    public class ModelStateFilter : IActionFilter
    {
        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.ModelState.IsValid) return;
            var failures = context.ModelState.Values.SelectMany(v => v.Errors).Select(x => x.ErrorMessage).ToList();
            throw new ValidationException(failures);
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
        }
    }
}
