﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ShopOfBikes.BLL.Exceptions;
using ShopOfBikes.BLL.Wrappers;
using System;
using System.Net;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ShopOfBikes.WebApi.Middlewares
{
    public class ErrorHandlerMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ErrorHandlerMiddleware> _logger;

        public ErrorHandlerMiddleware(RequestDelegate next, ILogger<ErrorHandlerMiddleware> logger)
        {
            _logger = logger;
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception error)
            {
                var response = context.Response;
                response.ContentType = "application/json";
                var responseModel = new Response<string> { Succeeded = false, Message = error.Message };

                switch (error)
                {
                    case RequestException e:
                        response.StatusCode = (int)HttpStatusCode.BadRequest;
                        _logger.LogError(error, $"{error.Message} {e.InnerException?.Message}");
                        break;

                    case ValidationException e:
                        response.StatusCode = (int)HttpStatusCode.BadRequest;
                        responseModel.Errors = e.Errors;
                        _logger.LogError(error, $"{error.Message}");
                        break;

                    case NotFoundException:
                        response.StatusCode = (int)HttpStatusCode.NotFound;
                        _logger.LogError(error, $"{error.Message}");
                        break;

                    case AuthorizationException:
                        response.StatusCode = (int)HttpStatusCode.Unauthorized;
                        _logger.LogError(error, responseModel.Message);
                        break;

                    case DbUpdateException e:
                        response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        responseModel.Message = "При обновлении базы данных произошла ошибка. " +
                                                "Для более подробной информации изучите лог-файл";
                        _logger.LogError(error, $"{e.InnerException?.Message}. {error.Message}");
                        break;

                    default:
                        response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        _logger.LogError(error, $"Something went wrong. {error.Message}");
                        break;
                }
                var result = JsonSerializer.Serialize(responseModel, new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase, DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull });

                await response.WriteAsync(result);
            }
        }
    }
}