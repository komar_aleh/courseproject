﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ShopOfBikes.DAL.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AbsorberStrokes",
                columns: table => new
                {
                    AbsorberStrokeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<int>(type: "int", maxLength: 3, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AbsorberStrokes", x => x.AbsorberStrokeId);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    UserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    Email = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    PasswordHash = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SecurityStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "bit", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "bit", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Cassettes",
                columns: table => new
                {
                    CassetteId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cassettes", x => x.CassetteId);
                });

            migrationBuilder.CreateTable(
                name: "Chains",
                columns: table => new
                {
                    ChainId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Chains", x => x.ChainId);
                });

            migrationBuilder.CreateTable(
                name: "ClassOfBikes",
                columns: table => new
                {
                    ClassOfBikeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassOfBikes", x => x.ClassOfBikeId);
                });

            migrationBuilder.CreateTable(
                name: "CommonDates",
                columns: table => new
                {
                    CommonDateId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<int>(type: "int", maxLength: 4, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommonDates", x => x.CommonDateId);
                });

            migrationBuilder.CreateTable(
                name: "ForkMaterials",
                columns: table => new
                {
                    ForkMaterialId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ForkMaterials", x => x.ForkMaterialId);
                });

            migrationBuilder.CreateTable(
                name: "ForkModels",
                columns: table => new
                {
                    ForkModelId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ForkModels", x => x.ForkModelId);
                });

            migrationBuilder.CreateTable(
                name: "ForkTypes",
                columns: table => new
                {
                    ForkTypeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ForkTypes", x => x.ForkTypeId);
                });

            migrationBuilder.CreateTable(
                name: "FrameColors",
                columns: table => new
                {
                    FrameColorId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FrameColors", x => x.FrameColorId);
                });

            migrationBuilder.CreateTable(
                name: "FrameMaterials",
                columns: table => new
                {
                    FrameMaterialId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FrameMaterials", x => x.FrameMaterialId);
                });

            migrationBuilder.CreateTable(
                name: "FrameSizes",
                columns: table => new
                {
                    FrameSizeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FrameSizes", x => x.FrameSizeId);
                });

            migrationBuilder.CreateTable(
                name: "FrontBrakeModels",
                columns: table => new
                {
                    FrontBrakeModelId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FrontBrakeModels", x => x.FrontBrakeModelId);
                });

            migrationBuilder.CreateTable(
                name: "FrontBrakeTypes",
                columns: table => new
                {
                    FrontBrakeTypeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FrontBrakeTypes", x => x.FrontBrakeTypeId);
                });

            migrationBuilder.CreateTable(
                name: "FrontStars",
                columns: table => new
                {
                    FrontStarId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<int>(type: "int", maxLength: 1, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FrontStars", x => x.FrontStarId);
                });

            migrationBuilder.CreateTable(
                name: "FrontSwitches",
                columns: table => new
                {
                    FrontSwitchId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FrontSwitches", x => x.FrontSwitchId);
                });

            migrationBuilder.CreateTable(
                name: "GripsModels",
                columns: table => new
                {
                    GripsModelId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GripsModels", x => x.GripsModelId);
                });

            migrationBuilder.CreateTable(
                name: "HandlebarModels",
                columns: table => new
                {
                    HandlebarModelId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HandlebarModels", x => x.HandlebarModelId);
                });

            migrationBuilder.CreateTable(
                name: "HandlebarTypes",
                columns: table => new
                {
                    HandlebarTypeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HandlebarTypes", x => x.HandlebarTypeId);
                });

            migrationBuilder.CreateTable(
                name: "Manufacturers",
                columns: table => new
                {
                    ManufacturerId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Manufacturers", x => x.ManufacturerId);
                });

            migrationBuilder.CreateTable(
                name: "PedalsMaterials",
                columns: table => new
                {
                    PedalsMaterialId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PedalsMaterials", x => x.PedalsMaterialId);
                });

            migrationBuilder.CreateTable(
                name: "PedalsModels",
                columns: table => new
                {
                    PedalsModelId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PedalsModels", x => x.PedalsModelId);
                });

            migrationBuilder.CreateTable(
                name: "PedalsTypes",
                columns: table => new
                {
                    PedalsTypeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PedalsTypes", x => x.PedalsTypeId);
                });

            migrationBuilder.CreateTable(
                name: "RearBrakeModels",
                columns: table => new
                {
                    RearBrakeModelId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RearBrakeModels", x => x.RearBrakeModelId);
                });

            migrationBuilder.CreateTable(
                name: "RearBrakeTypes",
                columns: table => new
                {
                    RearBrakeTypeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RearBrakeTypes", x => x.RearBrakeTypeId);
                });

            migrationBuilder.CreateTable(
                name: "RearStars",
                columns: table => new
                {
                    RearStarId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<int>(type: "int", maxLength: 1, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RearStars", x => x.RearStarId);
                });

            migrationBuilder.CreateTable(
                name: "RearSwitches",
                columns: table => new
                {
                    RearSwitchId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RearSwitches", x => x.RearSwitchId);
                });

            migrationBuilder.CreateTable(
                name: "RimMaterials",
                columns: table => new
                {
                    RimMaterialId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RimMaterials", x => x.RimMaterialId);
                });

            migrationBuilder.CreateTable(
                name: "SaddleModels",
                columns: table => new
                {
                    SaddleModelId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SaddleModels", x => x.SaddleModelId);
                });

            migrationBuilder.CreateTable(
                name: "SaddleTypes",
                columns: table => new
                {
                    SaddleTypeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SaddleTypes", x => x.SaddleTypeId);
                });

            migrationBuilder.CreateTable(
                name: "ShifterModels",
                columns: table => new
                {
                    ShifterModelId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShifterModels", x => x.ShifterModelId);
                });

            migrationBuilder.CreateTable(
                name: "ShifterTypes",
                columns: table => new
                {
                    ShifterTypeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShifterTypes", x => x.ShifterTypeId);
                });

            migrationBuilder.CreateTable(
                name: "SpeedNumbers",
                columns: table => new
                {
                    SpeedNumberId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<int>(type: "int", maxLength: 2, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SpeedNumbers", x => x.SpeedNumberId);
                });

            migrationBuilder.CreateTable(
                name: "Status",
                columns: table => new
                {
                    StatusId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Status", x => x.StatusId);
                });

            migrationBuilder.CreateTable(
                name: "SystemModels",
                columns: table => new
                {
                    SystemModelId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SystemModels", x => x.SystemModelId);
                });

            migrationBuilder.CreateTable(
                name: "TiresModels",
                columns: table => new
                {
                    TiresModelId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TiresModels", x => x.TiresModelId);
                });

            migrationBuilder.CreateTable(
                name: "WheelDiameters",
                columns: table => new
                {
                    WheelDiameterId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<float>(type: "real", maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelDiameters", x => x.WheelDiameterId);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderKey = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderDisplayName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    RoleId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    OrderId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "123, 1"),
                    OrderDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    StatusId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    TotalAmount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    ShippingFirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ShippingLastName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ShippingZip = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ShippingCity = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ShippingAddressLine = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ShippingPhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.OrderId);
                    table.ForeignKey(
                        name: "FK_Orders_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders_Status_StatusId",
                        column: x => x.StatusId,
                        principalTable: "Status",
                        principalColumn: "StatusId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    ProductId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Price = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Quantity = table.Column<long>(type: "bigint", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ImgName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsAvailable = table.Column<bool>(type: "bit", nullable: false),
                    ClassOfBikeId = table.Column<int>(type: "int", nullable: true),
                    CommonDateId = table.Column<int>(type: "int", nullable: true),
                    Weight = table.Column<float>(type: "real", nullable: true),
                    HardTail = table.Column<bool>(type: "bit", nullable: true),
                    Female = table.Column<bool>(type: "bit", nullable: true),
                    KidTeen = table.Column<bool>(type: "bit", nullable: true),
                    DoubleRim = table.Column<bool>(type: "bit", nullable: true),
                    TiresTubeless = table.Column<bool>(type: "bit", nullable: true),
                    ForkLock = table.Column<bool>(type: "bit", nullable: true),
                    ManufacturerId = table.Column<int>(type: "int", nullable: true),
                    FrameMaterialId = table.Column<int>(type: "int", nullable: true),
                    FrameSizeId = table.Column<int>(type: "int", nullable: true),
                    ForkMaterialId = table.Column<int>(type: "int", nullable: true),
                    WheelDiameterId = table.Column<int>(type: "int", nullable: true),
                    FrameColorId = table.Column<int>(type: "int", nullable: true),
                    ForkModelId = table.Column<int>(type: "int", nullable: true),
                    AbsorberStrokeId = table.Column<int>(type: "int", nullable: true),
                    SpeedNumberId = table.Column<int>(type: "int", nullable: true),
                    SystemModelId = table.Column<int>(type: "int", nullable: true),
                    FrontStarId = table.Column<int>(type: "int", nullable: true),
                    RearStarId = table.Column<int>(type: "int", nullable: true),
                    CassetteId = table.Column<int>(type: "int", nullable: true),
                    ForkTypeId = table.Column<int>(type: "int", nullable: true),
                    FrontSwitchId = table.Column<int>(type: "int", nullable: true),
                    RearSwitchId = table.Column<int>(type: "int", nullable: true),
                    ChainId = table.Column<int>(type: "int", nullable: true),
                    ShifterModelId = table.Column<int>(type: "int", nullable: true),
                    ShifterTypeId = table.Column<int>(type: "int", nullable: true),
                    FrontBrakeModelId = table.Column<int>(type: "int", nullable: true),
                    FrontBrakeTypeId = table.Column<int>(type: "int", nullable: true),
                    RearBrakeModelId = table.Column<int>(type: "int", nullable: true),
                    RearBrakeTypeId = table.Column<int>(type: "int", nullable: true),
                    RimMaterialId = table.Column<int>(type: "int", nullable: true),
                    TiresModelId = table.Column<int>(type: "int", nullable: true),
                    HandlebarModelId = table.Column<int>(type: "int", nullable: true),
                    HandlebarTypeId = table.Column<int>(type: "int", nullable: true),
                    GripsModelId = table.Column<int>(type: "int", nullable: true),
                    SaddleTypeId = table.Column<int>(type: "int", nullable: true),
                    SaddleModelId = table.Column<int>(type: "int", nullable: true),
                    PedalsTypeId = table.Column<int>(type: "int", nullable: true),
                    PedalsModelId = table.Column<int>(type: "int", nullable: true),
                    PedalsMaterialId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.ProductId);
                    table.ForeignKey(
                        name: "FK_Products_AbsorberStrokes_AbsorberStrokeId",
                        column: x => x.AbsorberStrokeId,
                        principalTable: "AbsorberStrokes",
                        principalColumn: "AbsorberStrokeId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_Cassettes_CassetteId",
                        column: x => x.CassetteId,
                        principalTable: "Cassettes",
                        principalColumn: "CassetteId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_Chains_ChainId",
                        column: x => x.ChainId,
                        principalTable: "Chains",
                        principalColumn: "ChainId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_ClassOfBikes_ClassOfBikeId",
                        column: x => x.ClassOfBikeId,
                        principalTable: "ClassOfBikes",
                        principalColumn: "ClassOfBikeId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_CommonDates_CommonDateId",
                        column: x => x.CommonDateId,
                        principalTable: "CommonDates",
                        principalColumn: "CommonDateId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_ForkMaterials_ForkMaterialId",
                        column: x => x.ForkMaterialId,
                        principalTable: "ForkMaterials",
                        principalColumn: "ForkMaterialId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_ForkModels_ForkModelId",
                        column: x => x.ForkModelId,
                        principalTable: "ForkModels",
                        principalColumn: "ForkModelId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_ForkTypes_ForkTypeId",
                        column: x => x.ForkTypeId,
                        principalTable: "ForkTypes",
                        principalColumn: "ForkTypeId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_FrameColors_FrameColorId",
                        column: x => x.FrameColorId,
                        principalTable: "FrameColors",
                        principalColumn: "FrameColorId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_FrameMaterials_FrameMaterialId",
                        column: x => x.FrameMaterialId,
                        principalTable: "FrameMaterials",
                        principalColumn: "FrameMaterialId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_FrameSizes_FrameSizeId",
                        column: x => x.FrameSizeId,
                        principalTable: "FrameSizes",
                        principalColumn: "FrameSizeId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_FrontBrakeModels_FrontBrakeModelId",
                        column: x => x.FrontBrakeModelId,
                        principalTable: "FrontBrakeModels",
                        principalColumn: "FrontBrakeModelId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_FrontBrakeTypes_FrontBrakeTypeId",
                        column: x => x.FrontBrakeTypeId,
                        principalTable: "FrontBrakeTypes",
                        principalColumn: "FrontBrakeTypeId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_FrontStars_FrontStarId",
                        column: x => x.FrontStarId,
                        principalTable: "FrontStars",
                        principalColumn: "FrontStarId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_FrontSwitches_FrontSwitchId",
                        column: x => x.FrontSwitchId,
                        principalTable: "FrontSwitches",
                        principalColumn: "FrontSwitchId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_GripsModels_GripsModelId",
                        column: x => x.GripsModelId,
                        principalTable: "GripsModels",
                        principalColumn: "GripsModelId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_HandlebarModels_HandlebarModelId",
                        column: x => x.HandlebarModelId,
                        principalTable: "HandlebarModels",
                        principalColumn: "HandlebarModelId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_HandlebarTypes_HandlebarTypeId",
                        column: x => x.HandlebarTypeId,
                        principalTable: "HandlebarTypes",
                        principalColumn: "HandlebarTypeId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_Manufacturers_ManufacturerId",
                        column: x => x.ManufacturerId,
                        principalTable: "Manufacturers",
                        principalColumn: "ManufacturerId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_PedalsMaterials_PedalsMaterialId",
                        column: x => x.PedalsMaterialId,
                        principalTable: "PedalsMaterials",
                        principalColumn: "PedalsMaterialId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_PedalsModels_PedalsModelId",
                        column: x => x.PedalsModelId,
                        principalTable: "PedalsModels",
                        principalColumn: "PedalsModelId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_PedalsTypes_PedalsTypeId",
                        column: x => x.PedalsTypeId,
                        principalTable: "PedalsTypes",
                        principalColumn: "PedalsTypeId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_RearBrakeModels_RearBrakeModelId",
                        column: x => x.RearBrakeModelId,
                        principalTable: "RearBrakeModels",
                        principalColumn: "RearBrakeModelId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_RearBrakeTypes_RearBrakeTypeId",
                        column: x => x.RearBrakeTypeId,
                        principalTable: "RearBrakeTypes",
                        principalColumn: "RearBrakeTypeId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_RearStars_RearStarId",
                        column: x => x.RearStarId,
                        principalTable: "RearStars",
                        principalColumn: "RearStarId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_RearSwitches_RearSwitchId",
                        column: x => x.RearSwitchId,
                        principalTable: "RearSwitches",
                        principalColumn: "RearSwitchId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_RimMaterials_RimMaterialId",
                        column: x => x.RimMaterialId,
                        principalTable: "RimMaterials",
                        principalColumn: "RimMaterialId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_SaddleModels_SaddleModelId",
                        column: x => x.SaddleModelId,
                        principalTable: "SaddleModels",
                        principalColumn: "SaddleModelId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_SaddleTypes_SaddleTypeId",
                        column: x => x.SaddleTypeId,
                        principalTable: "SaddleTypes",
                        principalColumn: "SaddleTypeId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_ShifterModels_ShifterModelId",
                        column: x => x.ShifterModelId,
                        principalTable: "ShifterModels",
                        principalColumn: "ShifterModelId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_ShifterTypes_ShifterTypeId",
                        column: x => x.ShifterTypeId,
                        principalTable: "ShifterTypes",
                        principalColumn: "ShifterTypeId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_SpeedNumbers_SpeedNumberId",
                        column: x => x.SpeedNumberId,
                        principalTable: "SpeedNumbers",
                        principalColumn: "SpeedNumberId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_SystemModels_SystemModelId",
                        column: x => x.SystemModelId,
                        principalTable: "SystemModels",
                        principalColumn: "SystemModelId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_TiresModels_TiresModelId",
                        column: x => x.TiresModelId,
                        principalTable: "TiresModels",
                        principalColumn: "TiresModelId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_WheelDiameters_WheelDiameterId",
                        column: x => x.WheelDiameterId,
                        principalTable: "WheelDiameters",
                        principalColumn: "WheelDiameterId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CartItems",
                columns: table => new
                {
                    CartItemId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductId = table.Column<int>(type: "int", nullable: false),
                    Quantity = table.Column<long>(type: "bigint", nullable: false),
                    UserId = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TotalPrice = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CartItems", x => x.CartItemId);
                    table.ForeignKey(
                        name: "FK_CartItems_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OrderItems",
                columns: table => new
                {
                    OrderItemId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Quantity = table.Column<long>(type: "bigint", nullable: false),
                    ProductId = table.Column<int>(type: "int", nullable: false),
                    OrderId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderItems", x => x.OrderItemId);
                    table.ForeignKey(
                        name: "FK_OrderItems_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "OrderId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderItems_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AbsorberStrokes_Value",
                table: "AbsorberStrokes",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_CartItems_ProductId",
                table: "CartItems",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_Cassettes_Value",
                table: "Cassettes",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Chains_Value",
                table: "Chains",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ClassOfBikes_Value",
                table: "ClassOfBikes",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CommonDates_Value",
                table: "CommonDates",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ForkMaterials_Value",
                table: "ForkMaterials",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ForkModels_Value",
                table: "ForkModels",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ForkTypes_Value",
                table: "ForkTypes",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FrameColors_Value",
                table: "FrameColors",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FrameMaterials_Value",
                table: "FrameMaterials",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FrameSizes_Value",
                table: "FrameSizes",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FrontBrakeModels_Value",
                table: "FrontBrakeModels",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FrontBrakeTypes_Value",
                table: "FrontBrakeTypes",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FrontStars_Value",
                table: "FrontStars",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FrontSwitches_Value",
                table: "FrontSwitches",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_GripsModels_Value",
                table: "GripsModels",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_HandlebarModels_Value",
                table: "HandlebarModels",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_HandlebarTypes_Value",
                table: "HandlebarTypes",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Manufacturers_Value",
                table: "Manufacturers",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_OrderItems_OrderId",
                table: "OrderItems",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderItems_ProductId",
                table: "OrderItems",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_StatusId",
                table: "Orders",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_UserId",
                table: "Orders",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_PedalsMaterials_Value",
                table: "PedalsMaterials",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PedalsModels_Value",
                table: "PedalsModels",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PedalsTypes_Value",
                table: "PedalsTypes",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Products_AbsorberStrokeId",
                table: "Products",
                column: "AbsorberStrokeId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_CassetteId",
                table: "Products",
                column: "CassetteId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_ChainId",
                table: "Products",
                column: "ChainId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_ClassOfBikeId",
                table: "Products",
                column: "ClassOfBikeId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_CommonDateId",
                table: "Products",
                column: "CommonDateId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_ForkMaterialId",
                table: "Products",
                column: "ForkMaterialId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_ForkModelId",
                table: "Products",
                column: "ForkModelId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_ForkTypeId",
                table: "Products",
                column: "ForkTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_FrameColorId",
                table: "Products",
                column: "FrameColorId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_FrameMaterialId",
                table: "Products",
                column: "FrameMaterialId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_FrameSizeId",
                table: "Products",
                column: "FrameSizeId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_FrontBrakeModelId",
                table: "Products",
                column: "FrontBrakeModelId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_FrontBrakeTypeId",
                table: "Products",
                column: "FrontBrakeTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_FrontStarId",
                table: "Products",
                column: "FrontStarId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_FrontSwitchId",
                table: "Products",
                column: "FrontSwitchId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_GripsModelId",
                table: "Products",
                column: "GripsModelId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_HandlebarModelId",
                table: "Products",
                column: "HandlebarModelId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_HandlebarTypeId",
                table: "Products",
                column: "HandlebarTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_ManufacturerId",
                table: "Products",
                column: "ManufacturerId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_PedalsMaterialId",
                table: "Products",
                column: "PedalsMaterialId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_PedalsModelId",
                table: "Products",
                column: "PedalsModelId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_PedalsTypeId",
                table: "Products",
                column: "PedalsTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_RearBrakeModelId",
                table: "Products",
                column: "RearBrakeModelId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_RearBrakeTypeId",
                table: "Products",
                column: "RearBrakeTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_RearStarId",
                table: "Products",
                column: "RearStarId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_RearSwitchId",
                table: "Products",
                column: "RearSwitchId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_RimMaterialId",
                table: "Products",
                column: "RimMaterialId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_SaddleModelId",
                table: "Products",
                column: "SaddleModelId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_SaddleTypeId",
                table: "Products",
                column: "SaddleTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_ShifterModelId",
                table: "Products",
                column: "ShifterModelId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_ShifterTypeId",
                table: "Products",
                column: "ShifterTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_SpeedNumberId",
                table: "Products",
                column: "SpeedNumberId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_SystemModelId",
                table: "Products",
                column: "SystemModelId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_TiresModelId",
                table: "Products",
                column: "TiresModelId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_WheelDiameterId",
                table: "Products",
                column: "WheelDiameterId");

            migrationBuilder.CreateIndex(
                name: "IX_RearBrakeModels_Value",
                table: "RearBrakeModels",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_RearBrakeTypes_Value",
                table: "RearBrakeTypes",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_RearStars_Value",
                table: "RearStars",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_RearSwitches_Value",
                table: "RearSwitches",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_RimMaterials_Value",
                table: "RimMaterials",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SaddleModels_Value",
                table: "SaddleModels",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SaddleTypes_Value",
                table: "SaddleTypes",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ShifterModels_Value",
                table: "ShifterModels",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ShifterTypes_Value",
                table: "ShifterTypes",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SpeedNumbers_Value",
                table: "SpeedNumbers",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Status_Value",
                table: "Status",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SystemModels_Value",
                table: "SystemModels",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TiresModels_Value",
                table: "TiresModels",
                column: "Value",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WheelDiameters_Value",
                table: "WheelDiameters",
                column: "Value",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "CartItems");

            migrationBuilder.DropTable(
                name: "OrderItems");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Status");

            migrationBuilder.DropTable(
                name: "AbsorberStrokes");

            migrationBuilder.DropTable(
                name: "Cassettes");

            migrationBuilder.DropTable(
                name: "Chains");

            migrationBuilder.DropTable(
                name: "ClassOfBikes");

            migrationBuilder.DropTable(
                name: "CommonDates");

            migrationBuilder.DropTable(
                name: "ForkMaterials");

            migrationBuilder.DropTable(
                name: "ForkModels");

            migrationBuilder.DropTable(
                name: "ForkTypes");

            migrationBuilder.DropTable(
                name: "FrameColors");

            migrationBuilder.DropTable(
                name: "FrameMaterials");

            migrationBuilder.DropTable(
                name: "FrameSizes");

            migrationBuilder.DropTable(
                name: "FrontBrakeModels");

            migrationBuilder.DropTable(
                name: "FrontBrakeTypes");

            migrationBuilder.DropTable(
                name: "FrontStars");

            migrationBuilder.DropTable(
                name: "FrontSwitches");

            migrationBuilder.DropTable(
                name: "GripsModels");

            migrationBuilder.DropTable(
                name: "HandlebarModels");

            migrationBuilder.DropTable(
                name: "HandlebarTypes");

            migrationBuilder.DropTable(
                name: "Manufacturers");

            migrationBuilder.DropTable(
                name: "PedalsMaterials");

            migrationBuilder.DropTable(
                name: "PedalsModels");

            migrationBuilder.DropTable(
                name: "PedalsTypes");

            migrationBuilder.DropTable(
                name: "RearBrakeModels");

            migrationBuilder.DropTable(
                name: "RearBrakeTypes");

            migrationBuilder.DropTable(
                name: "RearStars");

            migrationBuilder.DropTable(
                name: "RearSwitches");

            migrationBuilder.DropTable(
                name: "RimMaterials");

            migrationBuilder.DropTable(
                name: "SaddleModels");

            migrationBuilder.DropTable(
                name: "SaddleTypes");

            migrationBuilder.DropTable(
                name: "ShifterModels");

            migrationBuilder.DropTable(
                name: "ShifterTypes");

            migrationBuilder.DropTable(
                name: "SpeedNumbers");

            migrationBuilder.DropTable(
                name: "SystemModels");

            migrationBuilder.DropTable(
                name: "TiresModels");

            migrationBuilder.DropTable(
                name: "WheelDiameters");
        }
    }
}
