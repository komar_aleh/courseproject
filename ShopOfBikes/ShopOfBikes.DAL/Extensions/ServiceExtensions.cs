﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ShopOfBikes.DAL.EF;
using ShopOfBikes.DAL.Entities;
using ShopOfBikes.DAL.Interfaces;
using ShopOfBikes.DAL.Repositories;

namespace ShopOfBikes.DAL.Extensions
{
    public static class ServiceExtensions
    {
        public static void AddEntityDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<AppDbContext>(options =>
            {
                options.UseSqlServer(
                    configuration.GetConnectionString("DefaultConnection"),
                    x =>
                    {
                        x.MigrationsAssembly(typeof(AppDbContext).Assembly.FullName);
                        x.UseQuerySplittingBehavior(QuerySplittingBehavior.SplitQuery);
                    });
            });

            services.AddIdentity<User, IdentityRole>()
                .AddEntityFrameworkStores<AppDbContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(o =>
            {
                o.Password.RequireDigit = false;
                o.Password.RequireLowercase = false;
                o.Password.RequireUppercase = false;
                o.Password.RequireNonAlphanumeric = false;
                o.Password.RequiredLength = 6;
                o.User.RequireUniqueEmail = true;
                o.User.AllowedUserNameCharacters =
                    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
                o.SignIn.RequireConfirmedEmail = true;
            });
        }

        public static void AddRepositories(this IServiceCollection services)
        {
            services.AddScoped(typeof(IRepositoryBase<>), typeof(RepositoryBase<>));
            services.AddScoped<IProductRepositoryAsync, ProductRepositoryAsync>();
            services.AddScoped<IOrderRepositoryAsync, OrderRepositoryAsync>();
            services.AddScoped<IOrderItemRepositoryAsync, OrderItemRepositoryAsync>();
            services.AddScoped<ICartRepositoryAsync, CartRepositoryAsync>();
        }
    }
}