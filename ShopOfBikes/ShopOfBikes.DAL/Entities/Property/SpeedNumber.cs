﻿using Microsoft.EntityFrameworkCore;
using ShopOfBikes.DAL.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShopOfBikes.DAL.Entities.Property
{
    [Index(nameof(Value), IsUnique = true)]
    public class SpeedNumber : ITypedEntity<int> //Количество скоростей
    {
        [Column("SpeedNumberId")]
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(2)]
        public int Value { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
