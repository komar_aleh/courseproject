﻿using Microsoft.EntityFrameworkCore;
using ShopOfBikes.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShopOfBikes.DAL.Entities.Property
{
    [Index(nameof(Value), IsUnique = true)]
    public class Status : ITypedEntity<string>
    {
        [Column("StatusId")]
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(20)]
        public string Value { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }
}
