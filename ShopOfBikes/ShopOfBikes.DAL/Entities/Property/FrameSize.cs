﻿using Microsoft.EntityFrameworkCore;
using ShopOfBikes.DAL.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShopOfBikes.DAL.Entities.Property
{
    [Index(nameof(Value), IsUnique = true)]
    public class FrameSize : ITypedEntity<string> //Размер рамы
    {
        [Column("FrameSizeId")]
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(10)]
        public string Value { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
