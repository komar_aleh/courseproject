﻿using Microsoft.EntityFrameworkCore;
using ShopOfBikes.DAL.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShopOfBikes.DAL.Entities.Property
{
    [Index(nameof(Value), IsUnique = true)]
    public class CommonDate : ITypedEntity<int>  //Год выхода на рынок
    {
        [Column("CommonDateId")]
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(4)]
        public int Value { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
