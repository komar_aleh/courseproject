﻿using Microsoft.EntityFrameworkCore;
using ShopOfBikes.DAL.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShopOfBikes.DAL.Entities.Property
{
    [Index(nameof(Value), IsUnique = true)]
    public class FrontStar : ITypedEntity<int> //Количество звезд в системе
    {
        [Column("FrontStarId")]
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(1)]
        public int Value { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
