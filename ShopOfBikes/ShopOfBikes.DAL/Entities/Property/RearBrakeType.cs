﻿using Microsoft.EntityFrameworkCore;
using ShopOfBikes.DAL.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShopOfBikes.DAL.Entities.Property
{
    [Index(nameof(Value), IsUnique = true)]
    public class RearBrakeType : ITypedEntity<string>  //Тип заднего тормоза
    {
        [Column("RearBrakeTypeId")]
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(30)]
        public string Value { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
