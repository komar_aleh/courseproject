﻿using ShopOfBikes.DAL.Entities.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShopOfBikes.DAL.Entities
{
    public class Order
    {
        [Required]
        [Column("OrderId")]
        [Key]
        public int Id { get; set; }

        public DateTime OrderDate { get; set; }

        [Required]
        [ForeignKey(nameof(Status))]
        public int StatusId { get; set; }
        public virtual Status Status { get; set; }

        [Required]
        [ForeignKey(nameof(User))]
        public string UserId { get; set; }
        public virtual User User { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal TotalAmount { get; set; }

        #region Shipping

        public string ShippingFirstName { get; set; }
        public string ShippingLastName { get; set; }
        public string ShippingZip { get; set; }
        public string ShippingCity { get; set; }
        public string ShippingAddressLine { get; set; }
        public string ShippingPhoneNumber { get; set; }

        #endregion

        public virtual ICollection<OrderItem> OrderItems { get; set; } = new List<OrderItem>();

        public Order()
        {
            OrderDate = DateTime.UtcNow;
            StatusId = 1;
        }
    }
}
