﻿using ShopOfBikes.DAL.Entities.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShopOfBikes.DAL.Entities
{
    public class Product
    {

        [Required]
        [Column("ProductId")]
        [Key]
        public int Id { get; set; }
        //Дата создания товара
        [Required]
        public DateTime DateCreated { get; set; }
        //Цена
        [Required]
        [Column(TypeName = "decimal(18,2)")]
        public decimal Price { get; set; }
        //Название
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        //Количество
        [Required]
        public uint Quantity { get; set; }
        public string Description { get; set; }
        public string ImgName { get; set; }
        [Required]
        public bool IsAvailable { get; set; }


        #region AdditionalProperties
        //Класс велосипеда
        [ForeignKey(nameof(ClassOfBike))]
        public int? ClassOfBikeId { get; set; }
        public virtual ClassOfBike ClassOfBike { get; set; }
        //Дата выхода на рынок
        [ForeignKey(nameof(CommonDate))]
        public int? CommonDateId { get; set; }
        public virtual CommonDate CommonDate { get; set; }
        //Вес
        public float? Weight { get; set; }
        //Задний амортизатор
        public bool? HardTail { get; set; }
        //Женский
        public bool? Female { get; set; }
        //Подростковый
        public bool? KidTeen { get; set; }
        //Двойные ободья
        public bool? DoubleRim { get; set; }
        //Бескамерные покрышки
        public bool? TiresTubeless { get; set; }
        //Блокировка вилки
        public bool? ForkLock { get; set; }
        //Производитель
        [ForeignKey(nameof(Manufacturer))]
        public int? ManufacturerId { get; set; }
        public virtual Manufacturer Manufacturer { get; set; }
        //Материал рамы
        [ForeignKey(nameof(FrameMaterial))]
        public int? FrameMaterialId { get; set; }
        public virtual FrameMaterial FrameMaterial { get; set; }
        //Размер рамы
        [ForeignKey(nameof(FrameSize))]
        public int? FrameSizeId { get; set; }
        public virtual FrameSize FrameSize { get; set; }
        //Материал вилки
        [ForeignKey(nameof(ForkMaterial))]
        public int? ForkMaterialId { get; set; }
        public virtual ForkMaterial ForkMaterial { get; set; }
        //Диаметр колес
        [ForeignKey(nameof(WheelDiameter))]
        public int? WheelDiameterId { get; set; }
        public virtual WheelDiameter WheelDiameter { get; set; }
        //Цвет рамы
        [ForeignKey(nameof(FrameColor))]
        public int? FrameColorId { get; set; }
        public virtual FrameColor FrameColor { get; set; }
        //Производитель вилки
        [ForeignKey(nameof(ForkModel))]
        public int? ForkModelId { get; set; }
        public virtual ForkModel ForkModel { get; set; }
        //Ход амортизатора
        [ForeignKey(nameof(AbsorberStroke))]
        public int? AbsorberStrokeId { get; set; }
        public virtual AbsorberStroke AbsorberStroke { get; set; }
        //Количество скоростей
        [ForeignKey(nameof(SpeedNumber))]
        public int? SpeedNumberId { get; set; }
        public virtual SpeedNumber SpeedNumber { get; set; }
        //Система шатунов
        [ForeignKey(nameof(SystemModel))]
        public int? SystemModelId { get; set; }
        public virtual SystemModel SystemModel { get; set; }
        //Количество звезд в системе
        [ForeignKey(nameof(FrontStar))]
        public int? FrontStarId { get; set; }
        public virtual FrontStar FrontStar { get; set; }
        //Количество звезд в кассете
        [ForeignKey(nameof(RearStar))]
        public int? RearStarId { get; set; }
        public virtual RearStar RearStar { get; set; }
        //Кассета
        [ForeignKey(nameof(Cassette))]
        public int? CassetteId { get; set; }
        public virtual Cassette Cassette { get; set; }
        //Тип вилки
        [ForeignKey(nameof(ForkType))]
        public int? ForkTypeId { get; set; }
        public virtual ForkType ForkType { get; set; }
        //Передний переключатель
        [ForeignKey(nameof(FrontSwitch))]
        public int? FrontSwitchId { get; set; }
        public virtual FrontSwitch FrontSwitch { get; set; }
        //Задний переключатель
        [ForeignKey(nameof(RearSwitch))]
        public int? RearSwitchId { get; set; }
        public virtual RearSwitch RearSwitch { get; set; }
        //Цепь
        [ForeignKey(nameof(Chain))]
        public int? ChainId { get; set; }
        public virtual Chain Chain { get; set; }
        //Модель манеток
        [ForeignKey(nameof(ShifterModel))]
        public int? ShifterModelId { get; set; }
        public virtual ShifterModel ShifterModel { get; set; }
        //Тип манеток
        [ForeignKey(nameof(ShifterType))]
        public int? ShifterTypeId { get; set; }
        public virtual ShifterType ShifterType { get; set; }
        //Модель переднего тормоза
        [ForeignKey(nameof(FrontBrakeModel))]
        public int? FrontBrakeModelId { get; set; }
        public virtual FrontBrakeModel FrontBrakeModel { get; set; }
        //Тип переднего тормоза
        [ForeignKey(nameof(FrontBrakeType))]
        public int? FrontBrakeTypeId { get; set; }
        public virtual FrontBrakeType FrontBrakeType { get; set; }
        //Модель заднего тормоза
        [ForeignKey(nameof(RearBrakeModel))]
        public int? RearBrakeModelId { get; set; }
        public virtual RearBrakeModel RearBrakeModel { get; set; }
        //Тип заднего тормоза
        [ForeignKey(nameof(RearBrakeType))]
        public int? RearBrakeTypeId { get; set; }
        public virtual RearBrakeType RearBrakeType { get; set; }
        //Материал ободьев
        [ForeignKey(nameof(RimMaterial))]
        public int? RimMaterialId { get; set; }
        public virtual RimMaterial RimMaterial { get; set; }
        //Модель покрышек
        [ForeignKey(nameof(TiresModel))]
        public int? TiresModelId { get; set; }
        public virtual TiresModel TiresModel { get; set; }
        //Модель руля
        [ForeignKey(nameof(HandlebarModel))]
        public int? HandlebarModelId { get; set; }
        public virtual HandlebarModel HandlebarModel { get; set; }
        //Тип руля
        [ForeignKey(nameof(HandlebarType))]
        public int? HandlebarTypeId { get; set; }
        public virtual HandlebarType HandlebarType { get; set; }
        //Грипсы
        [ForeignKey(nameof(GripsModel))]
        public int? GripsModelId { get; set; }
        public virtual GripsModel GripsModel { get; set; }
        //Тип седла
        [ForeignKey(nameof(SaddleType))]
        public int? SaddleTypeId { get; set; }
        public virtual SaddleType SaddleType { get; set; }
        //Модель седла
        [ForeignKey(nameof(SaddleModel))]
        public int? SaddleModelId { get; set; }
        public virtual SaddleModel SaddleModel { get; set; }
        //Тип педалей
        [ForeignKey(nameof(PedalsType))]
        public int? PedalsTypeId { get; set; }
        public virtual PedalsType PedalsType { get; set; }
        //Модель педалей
        [ForeignKey(nameof(PedalsModel))]
        public int? PedalsModelId { get; set; }
        public virtual PedalsModel PedalsModel { get; set; }
        //Материал педалей
        [ForeignKey(nameof(PedalsMaterial))]
        public int? PedalsMaterialId { get; set; }
        public virtual PedalsMaterial PedalsMaterial { get; set; }
        #endregion AdditionalProperties

        public virtual ICollection<OrderItem> OrderItems { get; set; }

        public Product()
        {
            DateCreated = DateTime.UtcNow;
        }
    }
}
