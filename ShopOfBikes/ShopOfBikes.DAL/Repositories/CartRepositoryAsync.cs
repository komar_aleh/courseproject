﻿using Microsoft.EntityFrameworkCore;
using ShopOfBikes.DAL.EF;
using ShopOfBikes.DAL.Entities;
using ShopOfBikes.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopOfBikes.DAL.Repositories
{
    public class CartRepositoryAsync : RepositoryBase<Cart>, ICartRepositoryAsync
    {
        private readonly DbSet<Cart> _database;

        public CartRepositoryAsync(AppDbContext dbContext) : base(dbContext)
        {
            _database = dbContext.Set<Cart>();
        }

        public async Task<Cart> GetByIdWithProductAsync(int id)
        {
            return await _database.AsNoTracking()
                .AsSingleQuery()
                .Include(x => x.Product)
                .SingleOrDefaultAsync(u => u.Id.Equals(id));
        }

        public async Task<IEnumerable<Cart>> GetAllWithProductByUserAsync(string id)
        {
            return await _database.AsNoTracking()
                .Where(u => u.UserId.Equals(id))
                .Include(p => p.Product)
                .ThenInclude(p => p.Manufacturer)
                .OrderBy(p => p.Product.Name)
                .ToListAsync();
        }
    }
}