﻿using Microsoft.EntityFrameworkCore;
using ShopOfBikes.DAL.EF;
using ShopOfBikes.DAL.Entities;
using ShopOfBikes.DAL.Interfaces;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace ShopOfBikes.DAL.Repositories
{
    public class ProductRepositoryAsync : RepositoryBase<Product>, IProductRepositoryAsync
    {
        private readonly DbSet<Product> _database;
        public ProductRepositoryAsync(AppDbContext dbContext)
            : base(dbContext)
        {
            _database = dbContext.Set<Product>();
        }

        public async Task<Product> GetByIdWithProperties(int id)
        {
            return await _database
                .AsNoTracking()
                .AsSingleQuery()
                .Where(b => b.Id.Equals(id))
                .Include(b => b.Manufacturer)
                .Include(b => b.ClassOfBike)
                .Include(b => b.CommonDate)
                .Include(b => b.FrameMaterial)
                .Include(b => b.FrameSize)
                .Include(b => b.FrameColor)
                .Include(b => b.ForkMaterial)
                .Include(b => b.ForkModel)
                .Include(b => b.ForkType)
                .Include(b => b.WheelDiameter)
                .Include(b => b.AbsorberStroke)
                .Include(b => b.SpeedNumber)
                .Include(b => b.SystemModel)
                .Include(b => b.FrontStar)
                .Include(b => b.RearStar)
                .Include(b => b.Cassette)
                .Include(b => b.FrontSwitch)
                .Include(b => b.RearSwitch)
                .Include(b => b.Chain)
                .Include(b => b.ShifterModel)
                .Include(b => b.ShifterType)
                .Include(b => b.FrontBrakeModel)
                .Include(b => b.FrontBrakeType)
                .Include(b => b.RearBrakeModel)
                .Include(b => b.RearBrakeType)
                .Include(b => b.RimMaterial)
                .Include(b => b.TiresModel)
                .Include(b => b.HandlebarModel)
                .Include(b => b.HandlebarType)
                .Include(b => b.GripsModel)
                .Include(b => b.SaddleModel)
                .Include(b => b.SaddleType)
                .Include(b => b.PedalsModel)
                .Include(b => b.PedalsMaterial)
                .Include(b => b.PedalsType)
                .SingleOrDefaultAsync();
        }

    }
}
