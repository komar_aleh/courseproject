﻿using Microsoft.EntityFrameworkCore;
using ShopOfBikes.DAL.EF;
using ShopOfBikes.DAL.Entities;
using ShopOfBikes.DAL.Interfaces;
using System.Threading.Tasks;

namespace ShopOfBikes.DAL.Repositories
{
    public class OrderItemRepositoryAsync : RepositoryBase<OrderItem>, IOrderItemRepositoryAsync
    {
        private readonly DbSet<OrderItem> _database;
        public OrderItemRepositoryAsync(AppDbContext dbContext)
            : base(dbContext)
        {
            _database = dbContext.Set<OrderItem>();
        }

        public async Task<OrderItem> GetByIdWithProductAsync(int id)
        {
            return await _database.AsNoTracking()
                .AsSingleQuery()
                .Include(x => x.Product)
                .SingleOrDefaultAsync(u => u.Id.Equals(id));
        }
    }
}
