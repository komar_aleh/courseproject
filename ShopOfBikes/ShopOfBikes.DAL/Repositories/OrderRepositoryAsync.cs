﻿using Microsoft.EntityFrameworkCore;
using ShopOfBikes.DAL.EF;
using ShopOfBikes.DAL.Entities;
using ShopOfBikes.DAL.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace ShopOfBikes.DAL.Repositories
{
    public class OrderRepositoryAsync : RepositoryBase<Order>, IOrderRepositoryAsync
    {
        private readonly DbSet<Order> _database;
        public OrderRepositoryAsync(AppDbContext dbContext)
            : base(dbContext)
        {
            _database = dbContext.Set<Order>();
        }

        public async Task<Order> GetByIdWithOrderItemsAndProductAsync(int id)
        {
            return await _database.AsNoTracking()
                .AsSingleQuery()
                .Include(x => x.OrderItems)
                .ThenInclude(x => x.Product)
                .Include(x => x.Status)
                .SingleOrDefaultAsync(u => u.Id.Equals(id));
        }

        public IQueryable<Order> GetAllWithStatus()
        {
            return _database
                .AsNoTracking()
                .AsQueryable()
                .Include(x => x.Status)
                .Include(x => x.OrderItems)
                .ThenInclude(x => x.Product);
        }
    }
}
