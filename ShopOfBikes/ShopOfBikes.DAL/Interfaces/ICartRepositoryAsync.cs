﻿using ShopOfBikes.DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShopOfBikes.DAL.Interfaces
{
    public interface ICartRepositoryAsync : IRepositoryBase<Cart>
    {
        Task<Cart> GetByIdWithProductAsync(int id);
        Task<IEnumerable<Cart>> GetAllWithProductByUserAsync(string id);
    }
}