﻿using ShopOfBikes.DAL.Entities;
using System.Threading.Tasks;

namespace ShopOfBikes.DAL.Interfaces
{
    public interface IOrderItemRepositoryAsync : IRepositoryBase<OrderItem>
    {
        Task<OrderItem> GetByIdWithProductAsync(int id);
    }
}
