﻿namespace ShopOfBikes.DAL.Interfaces
{
    public interface ITypedEntity<T>
    {
        public T Value { get; set; }
    }
}
