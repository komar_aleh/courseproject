﻿using ShopOfBikes.DAL.Entities;
using System.Threading.Tasks;

namespace ShopOfBikes.DAL.Interfaces
{
    public interface IProductRepositoryAsync : IRepositoryBase<Product>
    {
        Task<Product> GetByIdWithProperties(int id);
    }
}
