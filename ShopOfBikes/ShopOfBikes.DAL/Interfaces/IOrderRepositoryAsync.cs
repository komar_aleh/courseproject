﻿using ShopOfBikes.DAL.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace ShopOfBikes.DAL.Interfaces
{
    public interface IOrderRepositoryAsync : IRepositoryBase<Order>
    {
        Task<Order> GetByIdWithOrderItemsAndProductAsync(int id);
        IQueryable<Order> GetAllWithStatus();
    }
}
