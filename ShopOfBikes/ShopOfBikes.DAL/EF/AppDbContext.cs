﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ShopOfBikes.DAL.Entities;
using ShopOfBikes.DAL.Entities.Property;

namespace ShopOfBikes.DAL.EF
{
    public class AppDbContext : IdentityDbContext<User>
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Cart> CartItems { get; set; }
        public DbSet<AbsorberStroke> AbsorberStrokes { get; set; }
        public DbSet<Manufacturer> Manufacturers { get; set; }
        public DbSet<ClassOfBike> ClassOfBikes { get; set; }
        public DbSet<CommonDate> CommonDates { get; set; }
        public DbSet<FrameMaterial> FrameMaterials { get; set; }
        public DbSet<FrameSize> FrameSizes { get; set; }
        public DbSet<FrameColor> FrameColors { get; set; }
        public DbSet<ForkMaterial> ForkMaterials { get; set; }
        public DbSet<ForkModel> ForkModels { get; set; }
        public DbSet<WheelDiameter> WheelDiameters { get; set; }
        public DbSet<SystemModel> SystemModels { get; set; }
        public DbSet<SpeedNumber> SpeedNumbers { get; set; }
        public DbSet<FrontStar> FrontStars { get; set; }
        public DbSet<RearStar> RearStars { get; set; }
        public DbSet<Cassette> Cassettes { get; set; }
        public DbSet<ForkType> ForkTypes { get; set; }
        public DbSet<FrontSwitch> FrontSwitches { get; set; }
        public DbSet<RearSwitch> RearSwitches { get; set; }
        public DbSet<Chain> Chains { get; set; }
        public DbSet<ShifterModel> ShifterModels { get; set; }
        public DbSet<ShifterType> ShifterTypes { get; set; }
        public DbSet<FrontBrakeModel> FrontBrakeModels { get; set; }
        public DbSet<FrontBrakeType> FrontBrakeTypes { get; set; }
        public DbSet<RearBrakeModel> RearBrakeModels { get; set; }
        public DbSet<RearBrakeType> RearBrakeTypes { get; set; }
        public DbSet<RimMaterial> RimMaterials { get; set; }
        public DbSet<TiresModel> TiresModels { get; set; }
        public DbSet<HandlebarModel> HandlebarModels { get; set; }
        public DbSet<HandlebarType> HandlebarTypes { get; set; }
        public DbSet<GripsModel> GripsModels { get; set; }
        public DbSet<SaddleModel> SaddleModels { get; set; }
        public DbSet<SaddleType> SaddleTypes { get; set; }
        public DbSet<PedalsModel> PedalsModels { get; set; }
        public DbSet<PedalsType> PedalsTypes { get; set; }
        public DbSet<PedalsMaterial> PedalsMaterials { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }
    }
}